//
//  main.m
//  _BlocksWorld
//
//  Created by eviom on 06.01.13.
//  Copyright (c) 2013 eviom. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
