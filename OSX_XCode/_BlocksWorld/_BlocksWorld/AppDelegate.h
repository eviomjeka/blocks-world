//
//  AppDelegate.h
//  _BlocksWorld
//
//  Created by eviom on 06.01.13.
//  Copyright (c) 2013 eviom. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "../../../Src/Computer/OSX/CCocoaWindow.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet CCocoaWindow* CocoaWindow;
//@property (assign) IBOutlet NSWindow *window;

@end
