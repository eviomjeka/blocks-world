

uniform	float ColorExp;

varying	vec3 ColorYxy;

vec3 ConvertColor()
{
	vec3 ClrYxy = ColorYxy;

	ClrYxy.x = 1.0 - exp (-ClrYxy.x / ColorExp);

	float Ratio = ClrYxy.x / ClrYxy.z;
	
	vec3 XYZ;
	XYZ.x = ClrYxy.y * Ratio;
	XYZ.y = ClrYxy.x;
	XYZ.z = Ratio - XYZ.x - XYZ.y;

	const vec3 RCoeffs = vec3 (3.240479, -1.53715, -0.49853);
	const vec3 GCoeffs = vec3 (-0.969256, 1.875991, 0.041556);
	const vec3 BCoeffs = vec3 (0.055684, -0.204043, 1.057311);

	vec3 Color = vec3 (dot (RCoeffs, XYZ), dot (GCoeffs, XYZ), dot (BCoeffs, XYZ));
	Color.z = clamp (Color.z, 0.05, 1.0);
	return Color;
}

void main()
{
	gl_FragColor = vec4	(clamp (ConvertColor(), 0.0, 1.0), 1.0);
}