


varying vec3 Color;
varying vec3 Normal;

void main()
{
	vec3 l = normalize (vec3 (0.0, 0.2, -1.0));
	vec3 v = normalize (vec3 (-1.0, 0.0, 0.0));
	vec3 h = normalize (l + v);
	
	vec4 FinalColor = vec4 (Color, 1.0);
	
	vec4 Diff = FinalColor * max (1.5 * dot (Normal, l), 0.4);
	vec4 Spec = clamp ((FinalColor + vec4 (0.2,0.2,0.2,0.0)) * 2.0, 0.0, 1.0) * pow (max (dot (Normal, h), 0.0), 10.0);
	
	gl_FragColor = Diff + Spec;
}