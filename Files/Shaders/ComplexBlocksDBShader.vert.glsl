

attribute vec3 Vertex_Position;

uniform mat4 Matrix;

void main()
{
	gl_Position = Matrix * vec4 (Vertex_Position / (2.0*256.0), 1.0);
}