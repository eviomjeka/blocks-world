

attribute vec3 Vertex_Position;

uniform mat4 Matrix;

centroid varying vec4 FragPosition;

void main()
{
	FragPosition = Matrix * vec4 (Vertex_Position, 1.0);
	vec4 a = FragPosition; // TODO: fix
	a.x = a.x / (abs (a.x) + 1.0);
	a.y = a.y / (abs (a.y) + 1.0);

	gl_Position = a;
}