

uniform sampler2D Texture;

centroid varying vec2 TextureCoord;
varying vec4 Color;

void main()
{
	vec4 TextureColor = texture2D (Texture, TextureCoord);
	gl_FragColor = vec4 (TextureColor.x * Color.x / 255.0, TextureColor.y * Color.y / 255.0, 
						 TextureColor.z * Color.z / 255.0, TextureColor.w * Color.w / 255.0);
}