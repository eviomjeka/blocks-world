

uniform vec3 FogColor;
uniform float FogDistance;
uniform vec3 SMColor;
uniform sampler2D Texture;
uniform sampler2D ShadowMap;

varying vec3 Position;
varying vec2 LightVector;
centroid varying vec4 LightDistance;
centroid varying vec2 TextureCoord;

float ShadowCoef (vec2 TextureCoord)
{
	return float (2.0 * (LightDistance.z - 0.5) - 0.0001 <= texture2D (ShadowMap, TextureCoord.xy).x);
	//clamp ((texture2D (ShadowMap, TextureCoord).x - 2.0 * (LightDistance.z - 0.5) + 0.01) * 100.0, 0.0, 1.0);
}

void main()
{
	float FogCoord = length (Position);
	float FogFactor = (FogCoord - FogDistance * 0.6) / (FogDistance * 0.7);
	FogFactor = pow (clamp (FogFactor, 0.0, 1.0), 0.5);

	vec3 SM = SMColor * LightVector.y;
	vec3 Lighting = vec3 (max (LightVector.x, SM.x), 
						  max (LightVector.x, SM.y), 
						  max (LightVector.x, SM.z));
	Lighting /= 31.0;
	Lighting *= Lighting;

	vec4 a = LightDistance;
	a.x = 2.0 * (a.x - 0.5); // TODO: fix
	a.y = 2.0 * (a.y - 0.5);
	a.x = a.x / (abs (a.x) + 1.0);
	a.y = a.y / (abs (a.y) + 1.0);
	a.x = 0.5 * a.x + 0.5;
	a.y = 0.5 * a.y + 0.5;

	float Shadow = ShadowCoef (a.xy);

	Lighting *= (Shadow * 0.6 + 0.4);

	vec4 TextureColor = texture2D (Texture, TextureCoord);
	if (TextureColor.w == 0.0)
		discard;
	vec4 Color = vec4 (TextureColor.x * Lighting.x, 
					   TextureColor.y * Lighting.y, 
					   TextureColor.z * Lighting.z, TextureColor.w);
	gl_FragColor = vec4 (mix (Color.xyz, FogColor, FogFactor), Color.w);
}