


attribute vec3  Vertex_Position;
attribute vec3  Vertex_Normal;
attribute float JointWeight0;

uniform mat4 Matrix;
uniform mat4 JointMatrix0;
uniform vec3 Mesh_Color;

varying vec3 Color;
varying vec3 Normal;

void main()
{
	gl_Position = Matrix * (JointWeight0 * (JointMatrix0 * vec4(Vertex_Position, 1.0)));
	Normal = vec3(Matrix * (JointWeight0 * (JointMatrix0 * vec4(Vertex_Normal,   0.0))));
	
	Color = Mesh_Color;
}
