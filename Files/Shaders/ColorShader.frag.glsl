

varying vec4 FragColor;

void main()
{
	gl_FragColor = vec4 (FragColor.r / 255.0, FragColor.g / 255.0, FragColor.b / 255.0, FragColor.w / 255.0);
}