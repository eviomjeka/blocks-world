

uniform vec3 FogColor;
uniform float FogDistance;
uniform vec3 SMColor;
uniform sampler2D Texture;

varying	vec3 ColorYxy;
varying vec3 Position;
varying vec2 LightVector;
centroid varying vec2 TextureCoord;

void main()
{
	float FogCoord = length (Position);
	float FogFactor = (FogCoord - FogDistance * 0.6) / (FogDistance * 0.7);
	FogFactor = pow (clamp (FogFactor, 0.0, 1.0), 0.5);

	vec3 SM = SMColor * LightVector.y;
	vec3 Lighting = vec3 (max (LightVector.x, SM.x), 
						  max (LightVector.x, SM.y), 
						  max (LightVector.x, SM.z));
	Lighting /= 31.0;
	Lighting *= Lighting;

	vec4 TextureColor = texture2D (Texture, TextureCoord);
	if (TextureColor.w == 0.0)
		discard;
	vec4 Color = vec4 (TextureColor.x * Lighting.x, 
					   TextureColor.y * Lighting.y, 
					   TextureColor.z * Lighting.z, TextureColor.w);
	gl_FragColor = vec4 (mix (Color.xyz, FogColor, FogFactor), Color.w);
}