

uniform sampler2D Texture;

centroid varying vec2 TextureCoord;
varying vec4 Color;

void main()
{
	vec4 TextureColor = texture2D (Texture, TextureCoord);
	if (TextureColor.x == 0.0)
		discard;

	gl_FragColor = vec4 (Color.x / 255.0, Color.y / 255.0, Color.z / 255.0, TextureColor.x * Color.w / 255.0);
}