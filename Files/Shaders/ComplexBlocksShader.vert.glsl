

attribute vec3 Vertex_Position;
attribute vec2 Vertex_TextureCoord;
attribute vec2 Vertex_LightVector;

uniform mat4 Matrix;

varying vec3 Position;
varying vec2 LightVector;
centroid varying vec2 TextureCoord;

void main()
{
	gl_Position = Matrix * vec4 (Vertex_Position / (2.0*256.0), 1.0);
	
	Position = vec3 (gl_Position);
	LightVector = Vertex_LightVector;
	TextureCoord = vec2 ((Vertex_TextureCoord.x + 128.0) / (2.0*16.0*256.0), (Vertex_TextureCoord.y + 128.0) / (2.0*16.0*256.0));
}