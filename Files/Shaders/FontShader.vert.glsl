

attribute vec3 Vertex_Position;
attribute vec2 Vertex_TextureCoord;
attribute vec4 Vertex_Color;

uniform mat4 Matrix;

centroid varying vec2 TextureCoord;
varying vec4 Color;

void main()
{
	gl_Position = Matrix * vec4 (Vertex_Position, 1.0);

	TextureCoord = Vertex_TextureCoord;
	Color = Vertex_Color;
}