

attribute vec3 Vertex_Position;
attribute vec2 Vertex_TextureCoord;
attribute vec2 Vertex_LightVector;

uniform mat4 Matrix;
uniform mat4 Matrix_ModelView;
uniform mat4 LightMatrix;

varying vec3 Position;
varying vec2 LightVector;
centroid varying vec4 LightDistance;
centroid varying vec2 TextureCoord;

void main()
{
	LightDistance = LightMatrix * (Matrix_ModelView * vec4 (Vertex_Position, 1.0));
	gl_Position = Matrix * vec4 (Vertex_Position, 1.0);

	Position = vec3 (gl_Position);
	LightVector = Vertex_LightVector;
	TextureCoord = vec2 ((Vertex_TextureCoord.x + 0.5) / (2.0*16.0), (Vertex_TextureCoord.y + 0.5) / (2.0*16.0));
}