

attribute vec3 Vertex_Position;
attribute vec4 Vertex_Color;

uniform mat4 Matrix;

varying vec4 FragColor;

void main()
{
	gl_Position = Matrix * vec4 (Vertex_Position, 1.0);
	FragColor = Vertex_Color;
}