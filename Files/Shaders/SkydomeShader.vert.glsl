

attribute vec3 Vertex_Position;

uniform mat4 Matrix;
uniform	vec3 SunPosition;
uniform	float Turbidity;

varying	vec3 ColorYxy;

vec3 PerezZenith (float t, float ThetaSun)
{
	const float	PI = 3.1415926;
	const vec4	cx1 = vec4 (0.0,      0.00209, -0.00375,  0.00165);
	const vec4	cx2 = vec4 (0.00394, -0.03202,  0.06377, -0.02903);
	const vec4	cx3 = vec4 (0.25886,  0.06052, -0.21196,  0.11693);
	const vec4	cy1 = vec4 (0.0,      0.00317, -0.00610,  0.00275);
	const vec4	cy2 = vec4 (0.00516, -0.04153,  0.08970, -0.04214);
	const vec4	cy3 = vec4 (0.26688,  0.06670, -0.26756,  0.15346);

	float	t2    = t*t;
	float	Chi   = (4.0 / 9.0 - t / 120.0) * (PI - 2.0 * ThetaSun);
	vec4	Theta = vec4 (1, ThetaSun, ThetaSun*ThetaSun, ThetaSun*ThetaSun*ThetaSun);

	float	Y = (4.0453 * t - 4.9710) * tan (Chi) - 0.2155 * t + 2.4192;
	float	x = t2 * dot (cx1, Theta) + t * dot (cx2, Theta) + dot (cx3, Theta);
	float	y = t2 * dot (cy1, Theta) + t * dot (cy2, Theta) + dot (cy3, Theta);

	return vec3 (Y, x, y);
}

vec3 PerezFunc (float t, float CosTheta, float CosGamma)
{
	float	Gamma      = acos (CosGamma);
	float	CosGammaSq = CosGamma * CosGamma;
	float	aY =  0.17872 * t - 1.46303;
	float	bY = -0.35540 * t + 0.42749;
	float	cY = -0.02266 * t + 5.32505;
	float	dY =  0.12064 * t - 2.57705;
	float	eY = -0.06696 * t + 0.37027;
	float	ax = -0.01925 * t - 0.25922;
	float	bx = -0.06651 * t + 0.00081;
	float	cx = -0.00041 * t + 0.21247;
	float	dx = -0.06409 * t - 0.89887;
	float	ex = -0.00325 * t + 0.04517;
	float	ay = -0.01669 * t - 0.26078;
	float	by = -0.09495 * t + 0.00921;
	float	cy = -0.00792 * t + 0.21023;
	float	dy = -0.04405 * t - 1.65369;
	float	ey = -0.01092 * t + 0.05291;

	return vec3 ((1.0 + aY * exp(bY/CosTheta)) * (1.0 + cY * exp(dY * Gamma) + eY*CosGammaSq),
	             (1.0 + ax * exp(bx/CosTheta)) * (1.0 + cx * exp(dx * Gamma) + ex*CosGammaSq),
				 (1.0 + ay * exp(by/CosTheta)) * (1.0 + cy * exp(dy * Gamma) + ey*CosGammaSq));
}

vec3 PerezSky (float t, float CosTheta, float CosGamma, float CosThetaSun)
{
	const float	PI = 3.1415926;
	
	CosTheta = clamp (CosTheta, 0.0, PI / 2.0);
	float	ThetaSun = acos        (CosThetaSun);
	vec3	Zenith   = PerezZenith (t, ThetaSun);
	vec3	ClrYxy   = Zenith * PerezFunc (t, CosTheta, CosGamma) / PerezFunc (t, 1.0, CosThetaSun);
	ClrYxy.x *= smoothstep (0.0, 0.1, CosThetaSun);
	
	return ClrYxy;
}

void main()
{
	vec3 tmp = normalize (Vertex_Position);
	vec3 v   = vec3 (tmp.x, tmp.z, tmp.y);
	vec3 l   = normalize (SunPosition);
	float lv = dot (l, v);
	ColorYxy = PerezSky (Turbidity, v.z, lv, l.z);

	gl_Position = Matrix * vec4 (Vertex_Position, 1.0);
}