﻿#include "CBlockSelection.h"
#include "CApplication.h"

void CBlockSelection::Init()
{
	SelectionMeshConstructor_.Init (&CShaders::ColorShader, 6 * 4 * 6 * MAX_BLOCK_BOUNDING_BOXES);
	BlockSelected_ = false;
}

void CBlockSelection::Destroy()
{
	if (Selection_)
		Selection_.Destroy();
}

void CBlockSelection::InteractWithBlock()
{
	if (SelectionPosition_.y >= 0 && SelectionPosition_.y <= SUBWORLD_SIZE_Y && 
		BlockSelected_ && INVENTORY.GetCurrentItem().ID() != 0)
	{
		SInteractWithBlockInfo InteractWithBlockInfo = {};
		InteractWithBlockInfo.BlockPosition		= BlockPosition_;
		InteractWithBlockInfo.SelectionPosition	= SelectionPosition_;
		InteractWithBlockInfo.Block				= CLIENT.MapLoader().GetBlock (BlockPosition_);
		InteractWithBlockInfo.Face				= PreviewFace_;
		InteractWithBlockInfo.Angle				= PLAYERCONTROLLER.Player_->Orientation_;
		InteractWithBlockInfo.Item				= INVENTORY.GetCurrentItem();

		PLAYERCONTROLLER.Game_->InteractWithBlock (InteractWithBlockInfo);
		RefreshFocusedBlock();
	}
}

void CBlockSelection::RemoveBlock()
{
	if (BlockSelected_ && CLIENT.MapLoader().GetBlock (BlockPosition_).ID())
	{
		PLAYERCONTROLLER.Game_->RemoveBlock(BlockPosition_);
		RefreshFocusedBlock();
	}
}

void CBlockSelection::Render()
{
	if (!BlockSelected_)
		return;

	UseShaderProgram (ColorShader);

	glDisable (GL_CULL_FACE);
	glEnable (GL_POLYGON_OFFSET_FILL);
	glPolygonOffset (-2.0f, -4.0f);

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.Translate (SFVector (BlockPosition_));
	MATRIXENGINE.Apply (&CShaders::ColorShader);

	Selection_.Render();
		
	MATRIXENGINE.PopModelViewMatrix();

	glDisable (GL_POLYGON_OFFSET_FILL);
	glEnable (GL_CULL_FACE);
}

void CBlockSelection::RefreshFocusedBlock()
{
	CPlayer* Player = PLAYERCONTROLLER.Player_;

	float Alpha = Player->Orientation_.y * PI / 180.0f;
	float Beta  = Player->Orientation_.x * PI / 180.0f;
	SFVector Ray (sin (Alpha) * cos (Beta), -sin (Beta), -cos (Alpha) * cos (Beta));
	SFVector FCameraPosition = Player->Position_ + SFVector (0.0f, PLAYER_EYES_HEIGHT, 0.0f);
	SIVector CameraPosition = FloorVector (FCameraPosition);
	
	SIVector CurrentPosition ((SIVector(1) - Ray.Sign()) / 2);
	SIVector BlockPosition = CameraPosition;
	SFVector RayStart = Player->Position_ + SFVector (0, PLAYER_EYES_HEIGHT, 0) - SFVector(CameraPosition);

	BlockSelected_ = false;

	CBlockData Block = (BlockPosition.y < 0 || BlockPosition.y > SUBWORLD_SIZE_Y) ? CBlockData() : 
						CLIENT.MapLoader().GetBlock (BlockPosition);
	if (Block.ID() && BlockSelection_ (BlockPosition, Block, Player->Position_, Ray))
	{
		SelectionPosition_.y = -1; // so we can only remove block, not interact
		return;
	}

	for (;;)
	{
		SIVector Delta = NextFace_ (RayStart, CurrentPosition, Ray);
		CurrentPosition += Delta;
		if (CurrentPosition.SqLen() > PLAYER_INTERACTION_RANGE * PLAYER_INTERACTION_RANGE)
			break;

		BlockPosition += Delta;

		CBlockData Block = (BlockPosition.y < 0 || BlockPosition.y > SUBWORLD_SIZE_Y) ? CBlockData() : 
							CLIENT.MapLoader().GetBlock (BlockPosition);
		if (Block.ID() && BlockSelection_ (BlockPosition, Block, Player->Position_, Ray))
			return;
	}
	
	INVENTORY.SetSelectedItem (ItemData (Void));
}

bool CBlockSelection::BlockSelection_ (SIVector BlockPosition, CBlockData Block, 
									   SFVector PlayerPosition, SFVector Ray)
{
	CArray<SBlockAABB> AABBList (MAX_BLOCK_BOUNDING_BOXES);
	BlocksList (Block.ID())->GetAABBList (Block.Attributes(), BlockPosition, AABBList);
			
	bool BlockSelected = false;
	SFVector RayStart = PlayerPosition + SFVector (0, PLAYER_EYES_HEIGHT, 0) - (SFVector) BlockPosition;
	BlockRotate_ = Block.Rotate();
	
	byte PreviewFace = 0;
	float MinSqDistance = (PLAYER_INTERACTION_RANGE + 10.0f) * (PLAYER_INTERACTION_RANGE + 10.0f);

	for (int i = 0; i < AABBList.Size(); i++)
	{
		if (AABBList[i].Type == BLOCK_AABB_PHYSICS)
			continue;
		assert (AABBList[i].Type == BLOCK_AABB_SELECTION_AND_PHYSICS || 
				AABBList[i].Type == BLOCK_AABB_SELECTION);
		RotateBlockAABB (AABBList[i], BlockRotate_);

		float SqDistance = 0.0f;
		byte Face = 0;
		if (CollisionAABBWithLine_ (AABBList[i], RayStart, Ray, SqDistance, Face))
		{
			BlockSelected = true;
			
			if (SqDistance < MinSqDistance)
			{
				MinSqDistance = SqDistance;
				
				SIVector Vector = FaceToVector (SIVector (0), Face);
				Vector = RotateVector (Vector, Block.Rotate());
				PreviewFace = VectorToFace (Vector);
			}
		}
	}
			
	if (BlockSelected)
	{
		BlockPosition_ = BlockPosition;
		BlockSelected_ = true;
		SelectionPosition_ = FaceToVector (BlockPosition, InvertFace (PreviewFace));
		PreviewFace_ = PreviewFace;

		GenerateBoundingBoxMesh_();
		INVENTORY.SetSelectedItem (CItemData (Block.ID(), Block.Attributes()));

		return true;
	}

	return false;
}

bool CBlockSelection::CollisionAABBWithLine_ (SBlockAABB BoundingBox, SFVector Position, 
													 SFVector Direction, float& Distance, byte& Face)
{
	Face = LXFACE_MASK; // to prevent crashing

	float Enter = 0.0f;
	float Exit = PLAYER_INTERACTION_RANGE + 2.0f;

	float Enter2 = 0.0f;
	if (!CollisionAABBWithLine1D_ (BoundingBox.Start.x, BoundingBox.End.x, Position.x, Direction.x, Enter2, Exit))
		return false;
	
	if (Enter != Enter2)
		Face = LXFACE_MASK;
	Enter = Enter2;
	if (!CollisionAABBWithLine1D_ (BoundingBox.Start.y, BoundingBox.End.y, Position.y, Direction.y, Enter2, Exit))
		return false;
	
	if (Enter != Enter2)
		Face = LYFACE_MASK;
	Enter = Enter2;
	if (!CollisionAABBWithLine1D_ (BoundingBox.Start.z, BoundingBox.End.z, Position.z, Direction.z, Enter2, Exit))
		return false;
	if (Enter != Enter2)
		Face = LZFACE_MASK;
	
	if (Face == LXFACE_MASK && Direction.x > 0.0f)
		Face = HXFACE_MASK;
	if (Face == LYFACE_MASK && Direction.y > 0.0f)
		Face = HYFACE_MASK;
	if (Face == LZFACE_MASK && Direction.z > 0.0f)
		Face = HZFACE_MASK;

	SFVector Vector = Direction * Enter;
	Distance = Vector.SqLen();

	return true;
}

bool CBlockSelection::CollisionAABBWithLine1D_ (float Start, float End, float Position, 
												float Direction, float& Enter, float& Exit)
{
	if (fabs (Direction) < 0.0001f)
		return (Position >= Start && Position <= End);

	float OneDevideByDir = 1.0f / Direction;
	float T0 = (Start - Position) * OneDevideByDir;
	float T1 = (End - Position) * OneDevideByDir;

	if (T0 > T1)
	{
		float Temp = T1;
		T1 = T0;
		T0 = Temp;
	}

	if (T0 > Exit || T1 < Enter)
		return false;

	if (T0 > Enter)
		Enter = T0;
	if (T1 < Exit)
		Exit = T1;

	return true;
}

SIVector CBlockSelection::NextFace_ (SFVector RayStart, SIVector CurrentPosition, SFVector Ray)
{
#define Cond(a, b) (fabs ((CurrentPosition.a + Sign(Ray.a) - RayStart.a) * Ray.b) <	\
					fabs ((CurrentPosition.b + Sign(Ray.b) - RayStart.b) * Ray.a))

	if (Cond (x, z))
	{
		if (Cond (x, y))
			return SIVector(Sign(Ray.x), 0, 0);
		else	
			return SIVector(0, Sign(Ray.y), 0);
	}
	else
	{
		if (Cond (z, y))
			return SIVector(0, 0, Sign(Ray.z));
		else		
			return SIVector(0, Sign(Ray.y), 0);
	}

#undef Cond
}

void CBlockSelection::GenerateBoundingBoxMesh_()
{
	CBlockData Block = CLIENT.MapLoader().GetBlock (BlockPosition_);
	CArray<SBlockAABB> BoundingBoxes (MAX_BLOCK_BOUNDING_BOXES);
	BlocksList (Block.ID())->GetAABBList (Block.Attributes(), BlockPosition_, BoundingBoxes);

	byte Rotate = Block.Rotate();
	
	SelectionMeshConstructor_.Begin (&Selection_);
	SelectionMeshConstructor_.SetColor (SColor (0, 0, 0, 255));

	for (int i = 0; i < BoundingBoxes.Size(); i++)
	{
		if (BoundingBoxes[i].Type == BLOCK_AABB_PHYSICS)
			continue;
		assert (BoundingBoxes[i].Type == BLOCK_AABB_SELECTION_AND_PHYSICS || 
				BoundingBoxes[i].Type == BLOCK_AABB_SELECTION);

		for (int j = 0; j < 6; j++)
		{
			byte Face = IndexToFace ((byte) j);
			SIVector v[4] = {};
			GetFace (Face, v);

			for (int k = 0; k < 4; k++)
			{
				SFVector va = SFVector ((float) v[(k + 0) % 4].x, (float) v[(k + 0) % 4].y, (float) v[(k + 0) % 4].z);
				SFVector vb = SFVector ((float) v[(k + 1) % 4].x, (float) v[(k + 1) % 4].y, (float) v[(k + 1) % 4].z);
				va = BoundingBoxes[i].Start + va.PerComponentProduct (BoundingBoxes[i].End - BoundingBoxes[i].Start);
				vb = BoundingBoxes[i].Start + vb.PerComponentProduct (BoundingBoxes[i].End - BoundingBoxes[i].Start);
				SFVector Vertex[4];
				SFVector LineWidth;

				switch (Face)
				{
					case LXFACE_MASK: case HXFACE_MASK:
						LineWidth (0.01f, 0.0f, 0.0f);
					break;

					case LYFACE_MASK: case HYFACE_MASK:
						LineWidth (0.0f, 0.01f, 0.0f);
					break;
					
					case LZFACE_MASK: case HZFACE_MASK:
						LineWidth (0.0f, 0.0f, 0.01f);
					break;
				}
				Vertex[0] = va - LineWidth / 2.0f;
				Vertex[1] = vb - LineWidth / 2.0f;
				Vertex[2] = vb + LineWidth / 2.0f;
				Vertex[3] = va + LineWidth / 2.0f;

				for (int l = 0; l < 4; l++)
					Vertex[l] = RotateVector (Vertex[l] - SFVector (0.5f), Rotate) + SFVector (0.5f);
				SelectionMeshConstructor_.AddQuad (Vertex[0], Vertex[1], Vertex[2], Vertex[3]);
			}
		}
	}

	SelectionMeshConstructor_.End (&Selection_);
}
