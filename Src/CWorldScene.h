﻿#ifndef CWORLDSCENE
#define CWORLDSCENE

#include <Engine.h>
#include "World/CSubWorld.h"
#include "World/CWorld.h"
#include "CMeshGenerator.h"

enum EMeshType
{
	MESHTYPE_BLOCKS, 
	MESHTYPE_COMPLEX_BLOCKS, 
	MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS, 
	MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB
};

enum ERenderType
{
	RENDERTYPE_NOT_SHADOWED_SCENE, 
	RENDERTYPE_TO_SHADOW_MAP, 
	RENDERTYPE_SHADOWED_SCENE
};

struct SFBO
{
	unsigned FBO;
	unsigned ColorBuffer;
	unsigned DepthBuffer;
};

class CWorldScene
{
public:

	void Init(SIVector2D WorldSize);
	void Destroy();
	void Render();

	void GenerateMeshes (CClientSubWorlds3x3& Neighbours);
	void UnloadMeshes (SIVector2D Position);
	void ClearQueues();

private:

	struct tSectorMeshes
	{
		bool		Loaded;
		SIVector	Position;
		CMesh 		BlocksMesh;
		CMesh 		ComplexBlocksMesh;
		CMesh		TranslucentComplexBlocksMesh;
	};

	void RenderScene_ (int MeshType, int RenderType);
	void SendMeshes_();

	void RenderToShadowMap_ (int MeshType);
	void RenderShadowedScene_ (int MeshType);
	void RenderNotShadowedScene_ (int MeshType);

	CClippingFrustum	ClippingFrustum_;
	CMeshGenerator		MeshGenerator_;

	SFBO FBO_;
	int ShadowTextureSize_;
	CMatrix<4,4> LightMatrix_;
	CArray<TAttribute> BlocksShadowAttributes_;

	CArray<TAttribute> BlocksAttributes_;
	int BlocksElementSize_;

	CArray<TAttribute> ComplexBlocksAttributes_;
	CArray<TAttribute> ComplexBlocksPositionAttributes_;
	int ComplexBlocksElementSize_;

	CCircularArray3D<tSectorMeshes> Meshes_;
	SSubWorldMeshQueues MeshQueues_;

};

#endif
