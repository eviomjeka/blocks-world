﻿#ifndef CLIGHTINGENGINE
#define CLIGHTINGENGINE

#include <Engine.h>
#include "World/CClientSubWorld.h"

#define MAX_LIGHT_INTENSITY 31

struct SChangedBlock
{
	CBlockData		PrevBlock;
	SVector<byte>	Position;
};

// LIGHT_ENGINE_NEED_LIGHT_SOURCES remark: Subworld blocks in square 3x3 - 4 half subworld = 7
#define LIGHT_ENGINE_NEED_LIGHT_SOURCES	(SUBWORLD_SIZE_XZ*SUBWORLD_SIZE_Y*SUBWORLD_SIZE_XZ * 7)
#define LIGHT_SOURCE_POOL_SIZE			(1 << 15)
#define NUM_LIGHT_SOURCE_POOLS			((LIGHT_ENGINE_NEED_LIGHT_SOURCES / LIGHT_SOURCE_POOL_SIZE) + (MAX_LIGHT_INTENSITY - 1))

static_assert (LIGHT_ENGINE_NEED_LIGHT_SOURCES % NUM_LIGHT_SOURCE_POOLS == 0, 
			   "LIGHT_ENGINE_NEED_LIGHT_SOURCES must be divisible by NUM_LIGHT_SOURCE_POOLS");

struct SLightSourcePool
{
	CSArray<SVector<byte>, LIGHT_SOURCE_POOL_SIZE>	LightSources;
	int												NumLightSoruces;
	SLightSourcePool*								Next;
};

// TODO: bool SunMoon template function parameter?
// TODO: SectorY -> i
class CLightingEngine // TODO: use primary block and light set/get in engine (include AddLightSources_)
{
public:

	void Init();

	void PrepareSubWorldLighting		(CClientSubWorld* SubWorld);
	void CalculateSubWorldLighting		(CClientSubWorlds3x3& SubWorlds, int NeighboursMask);
	void RecalculateSubWorldLighting	(CClientSubWorlds3x3& SubWorlds, CArray<SChangedBlock> ChangedBlocks);
	// Do not try to understand RecalculateSubWorldLighting function, even I don't understand how it works.
	// TODO: Add RecalculateSubWorldLighting, after that remove DumpCheck_

private:

	void DumpCheck_					(bool CheckEmpty = false);
	void AddLightSource_			(SVector<byte> Position, byte Intensity);
	SLightSourcePool* RemovePool_	(SLightSourcePool* Pool);
	void RemoveLightIntensityPool_	(byte Intensity);
	byte GetLight_					(SIVector Position, CClientSubWorld* SubWorld, bool SunMoon);
	void SetLight_					(SIVector Position, byte Light, CClientSubWorld* SubWorld, bool SunMoon);

	void AddSunMoonLightSources_	(CClientSubWorld* SubWorld);
	void AddUsualLightSources_		(CClientSubWorld* SubWorld);
	void CaclulateLightSources_		(CClientSubWorld* SubWorld, bool SunMoon);
	void CalculateLightSourceStep_	(CClientSubWorld* SubWorld, bool SunMoon, 
									 SVector<byte> Position, byte Intensity);
	
	void AddLightSources_			(CClientSubWorlds3x3& SubWorlds, int NeighboursMask, bool SunMoon);

	void AddLightSourcesAndClear_	(CClientSubWorlds3x3& SubWorlds, CArray<SChangedBlock> ChangedBlocks, bool SunMoon);
	
	void CaclulateLightSources_		(CClientSubWorlds3x3& SubWorlds, int NeighboursMask, bool SunMoon);
	void CalculateLightSourceStep_	(CClientSubWorlds3x3& SubWorlds, bool SunMoon, 
									 SVector<byte> Position, byte Intensity, int NeighboursMask);

	CSArray<SLightSourcePool, NUM_LIGHT_SOURCE_POOLS>	Pools_;
	SLightSourcePool*									EmptyPoolsStart_;
	SLightSourcePool*									EmptyPoolsEnd_;
	CSArray<SLightSourcePool*, MAX_LIGHT_INTENSITY - 1>	LightIntensityPoolsStart_;
	CSArray<SLightSourcePool*, MAX_LIGHT_INTENSITY - 1>	LightIntensityPoolsEnd_;

};

#endif