﻿#include "CPlayerController.h"
#include "CApplication.h"

CPlayerController::CPlayerController() :
Initialized_(false)
{
}

void CPlayerController::Init(CGame* Game)
{
	Player_ = NULL;

	Game_ = Game;
	CommandsManager_.Init();
	for (int i = 0; i < NUM_MOVE_DIRECTIONS; i++)
		MoveDirection_[i] = false;
	
	CTextureAtlas::GetTextureAtlas ("World.png", WorldTexture_, SIVector2D (16, 16));

	INVENTORY.Init();
	INVENTORY.New();

	BlockSelection_.Init();
	CColorMeshConstructor GL;
	GL.Init (&CShaders::ColorShader, 3 * 10);
	GL.Begin (&PointMesh_);
	GL.SetColor (SColor(255, 255, 255, 255));
	GL.AddCircle (SFVector (0.0f, 0.0f, 0.0f), 0.2f, 10);
	GL.End (&PointMesh_);

	Sky_.Init();
	WorldScene_.Init(SIVector2D((SETTINGS.SightRange + 2) * 2 + 1)); //TODO: get sight range properly!!!!!!!!!!!!!!!!

	PlayerModel_.Init("LegoMan.bwm");
	WalkingSound_.Init("Walking");

	Initialized_ = true;
}

void CPlayerController::Destroy()
{
	PlayerModel_.Destroy();
	PointMesh_.Destroy();
	WalkingSound_.Destroy();

	WorldTexture_.Destroy();
	BlockSelection_.Destroy();
	Sky_.Destroy();
	WorldScene_.Destroy();
}

void CPlayerController::MoveDirection (int Direction, bool Move)
{
	float Speed = Player_->FlyMode_ ? PLAYER_FLY_SPEED : PLAYER_SPEED;

	if (MoveDirection_[Direction] == Move)
		return;

	if (!Move)
		Speed = 0.0f;

	MoveDirection_[Direction] = Move;
	switch (Direction)
	{
		case MOVE_DIRECTION_LOW_X:  Player_->Velocity_.x = -Speed;					break;
		case MOVE_DIRECTION_HIGH_X: Player_->Velocity_.x = Speed;					break;
		case MOVE_DIRECTION_LOW_Z:  Player_->Velocity_.z = -Speed;					break;
		case MOVE_DIRECTION_HIGH_Z: Player_->Velocity_.z = Speed;					break;
		case MOVE_DIRECTION_LOW_Y:  if (FlyMode()) Player_->Velocity_.y = -Speed;	break;
		case MOVE_DIRECTION_HIGH_Y: if (FlyMode()) Player_->Velocity_.y = Speed;	break;
		default: error ("Unknown move direction");									break;
	};
}

void CPlayerController::InteractWithBlock()
{
	BlockSelection_.InteractWithBlock();
	Controlled()->Interacted_ = true;
}

void CPlayerController::RemoveBlock()
{
	BlockSelection_.RemoveBlock();
	Controlled()->Removed_ = true;
}

void CPlayerController::Stop()
{
	for (int i = 0; i < NUM_MOVE_DIRECTIONS; i++)
		MoveDirection_[i] = false;

	Player_->Velocity_ = SFVector (0.0f, Player_->FlyMode_ ? 0.0f : Player_->Velocity_.y, 0.0f);
}

void CPlayerController::Rotate (SFVector2D Delta)
{
	Player_->Orientation_ += Delta;
	if (Player_->Orientation_.x >  90.0f) Player_->Orientation_.x =  90.0f;
	if (Player_->Orientation_.x < -90.0f) Player_->Orientation_.x = -90.0f;

	if (Player_->Orientation_.y <= 0.0f || Player_->Orientation_.y >= 360.0f)
		Player_->Orientation_.y = Player_->Orientation_.y - floor (Player_->Orientation_.y / 360.0f) * 360.0f;
} 

void CPlayerController::Jump()
{
	Player_->Jump();
}

void CPlayerController::SetPlayerModelViewMatrix (bool TranslatePosition)
{
	MATRIXENGINE.ResetModelViewMatrix();

#if 1 // TODO: remove
	if (SETTINGS.ThirdPerson)
	{
		MATRIXENGINE.Translate(0, -2, -5);
		MATRIXENGINE.RotateX(Player_->Orientation_.x);
		MATRIXENGINE.RotateY(Player_->Orientation_.y);

		if (TranslatePosition)
		{
			MATRIXENGINE.Translate (-Player_->Position_);
		}
	}
	else
	{
		MATRIXENGINE.RotateX(Player_->Orientation_.x);
		MATRIXENGINE.RotateY(Player_->Orientation_.y);

		if (TranslatePosition)
		{
			MATRIXENGINE.Translate (-Player_->Position_.x,
				-(Player_->Position_.y + PLAYER_EYES_HEIGHT), -Player_->Position_.z);
		}
	}
#else
	CMatrix<4, 4> Matrix = Controlled()->ModelContext_.Joints[7];
	MATRIXENGINE.ModelViewMatrix_ = Matrix * MATRIXENGINE.ModelViewMatrix_;
	MATRIXENGINE.RotateX(90);
	MATRIXENGINE.RotateY(90);
	MATRIXENGINE.RotateY(Player_->Orientation_.y);
	MATRIXENGINE.Translate (-Player_->Position_.x,
		-(Player_->Position_.y/* + PLAYER_EYES_HEIGHT*/), -Player_->Position_.z);
#endif

}

void CPlayerController::RenderWaterBlueRect_()
{
	/*SIVector position = ToIntVector (Controlled.Position_);
	position.y = (int) (Controlled.Position_.y + PLAYER_EYES_HEIGHT);
	CBlockData block0 = CLIENTWORLD.GetBlock (position);
	position.y++;
	CBlockData blockY = CLIENTWORLD.GetBlock (position);

	int PlayerY = (int) abs (Controlled.Position_.y + PLAYER_EYES_HEIGHT - 
		((int) PLAYER_EYES_HEIGHT - (int) Controlled.Position_.y));
	bool InWater = (block0.ID() == BLOCK_WATER && 
		(PlayerY <= 0.8f || PlayerY >= 1.0f)) || 
		(block0.ID() == BLOCK_WATER && blockY.ID() == BLOCK_WATER);
	if (!InWater)
		return;

	CMesh Mesh;
	CColorMeshConstructor GL;
	GL.Init(&APPMODULE.ColorShaderProgram, 6);
	GL.Begin(&Mesh);
	GL.SetColor(SColor (0, 120, 120, 180));
	GL.AddRect (SFVector (0, 0, -1.0f), 
		SFVector (APPLICATION->GetFullSizeX(), APPLICATION->GetGuiSizeY(), -1.0f));
	GL.End (&Mesh);
	
	APPMODULE.ColorShaderProgram.UseProgram (true);
	GUIRENDERER.GetMatrixEngine()->ResetModelViewMatrix();
	GUIRENDERER.GetMatrixEngine()->Apply (&APPMODULE.ColorShaderProgram);
	Mesh.Render();
	APPMODULE.ColorShaderProgram.UseProgram (false);*/
}

void CPlayerController::RenderPoint_()
{
	if (!INVENTORY.IsOpened() && !APPLICATION->IsMenu())
	{
		glEnable (GL_BLEND);
		glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
		UseShaderProgram (ColorShader);
		CMatrixEngine* ME = GUIRENDERER.GetMatrixEngine();
		ME->PushModelViewMatrix();
		ME->Translate (APPLICATION->GetGuiShift() + CApplication::GUI_SIZE_X / 2.0f, 
					   CApplication::GUI_SIZE_Y / 2.0f, 0.0f);
		GUIRENDERER.GetMatrixEngine()->Apply (&CShaders::ColorShader);

		PointMesh_.Render();
		
		ME->PopModelViewMatrix();
		glDisable (GL_BLEND);
	}
}

void CPlayerController::RenderPlayers_(int Time)
{
	for (int i = 0; i < CLIENTWORLD.Players_.Size(); i++)
		if (SETTINGS.ThirdPerson || Player_ != &CLIENTWORLD.Players_[i])
			PlayerModel_.Draw(CLIENTWORLD.Players_[i], Time);
}

void CPlayerController::Proc (int Time, bool Render)
{
	if (!Render)
	{
		//TODO: if (Connected to server)
			CLIENTWORLD.Proc (Time);
		return;
	}

	CLIENTWORLD.Proc (Time);

	SetPlayerModelViewMatrix();
	COpenAL::MoveListener(Controlled()->Position_, Controlled()->Velocity_);
	Controlled()->ItemData_ = INVENTORY.GetCurrentItem();

	for (int i = 0; i < CLIENTWORLD.Players_.Size(); i++)
	{
		CPlayer& Player = CLIENTWORLD.Players_[i];

		if (!Animations_.count(Player.ID_))
		{
			PLAYERCONTROLLER.GetModel().InitContext(Animations_[Player.ID_].ModelContext_);
			Animations_[Player.ID_].PlayerSound_.Init();
		}

		if (Player.Interacted_ || Player.Removed_)
			PlayerModel_.PlayAnimation(Animations_[Player.ID_].ModelContext_, 1);

		if (Player.BlockUnder_ && Player.Velocity_.SqLen() > 0.0f)
		{
			PlayerModel_.PlayAnimation(Animations_[Player.ID_].ModelContext_, 0);
			Animations_[Player.ID_].PlayerSound_.PlayQueue(WalkingSound_);
		}
		else
		{
			PlayerModel_.StopAnimation(Animations_[Player.ID_].ModelContext_, 0);
			Animations_[Player.ID_].PlayerSound_.Stop();
		}

		Animations_[Player.ID_].PlayerSound_.Move(Player.Position_, Player.Velocity_);
	}

	for (map<int, SPlayerAnimation>::iterator i = Animations_.begin(); i != Animations_.end(); i++)
	{
		bool NotRemoved = false;

		for (int j = 0; j < CLIENTWORLD.Players_.Size(); j++)
			if (i->first == CLIENTWORLD.Players_[j].ID_)
				NotRemoved = true;

		if (!NotRemoved)
			Animations_.erase(i);

	}

	BlockSelection_.Render();
	Sky_.Render (CLIENTWORLD.Time());
	RenderPlayers_(Time);

	WorldScene_.Render();
	BlockSelection_.RefreshFocusedBlock();
	RenderWaterBlueRect_();
	RenderPoint_();

	Controlled()->Interacted_	= false;
	Controlled()->Removed_		= false;
	Controlled()->ItemData_		= INVENTORY.GetCurrentItem();
}

CTexture& CPlayerController::WorldTexture()
{
	return WorldTexture_;
}

CWorldScene* CPlayerController::WorldScene()
{
	return &WorldScene_;
}

CPlayer* CPlayerController::Controlled()
{
	return Player_;
}

bool CPlayerController::FlyMode()
{
	return Player_->FlyMode_;
}

void CPlayerController::SetPosition(SFVector Position)
{
	Player_->Position_ = Position;
}

SFVector CPlayerController::Position()
{
	return Player_->Position_;
}

void CPlayerController::SetControlled (int Controlled)
{
	Player_ = &CLIENTWORLD.Players_[Controlled];
}

int CPlayerController::ToGameTime (int Hours, int Minutes)
{
	float Time = DAY_DURATION * (Hours*60.0f+Minutes - 12.0f*60.0f) / (60.0f*24.0f);
	if (Time < 0) Time += DAY_DURATION;
	return (int) Time;
}

SIVector2D CPlayerController::ToNormalTime (int Time)
{
	int Time2 = (Time * (60*24) / DAY_DURATION + (60*24) / 2) % (60*24);
	SIVector2D TimeHM;
	TimeHM.x = Div (Time2, 60);
	TimeHM.y = Mod (Time2, 60);

	return TimeHM;
}

CPlayer* CPlayerController::FindPlayer (int ID)
{
	CArray<CPlayer>& Players = CLIENTWORLD.Players_;
	for (int i = 0; i < Players.Size(); i++)
	{
		if (Players[i].ID_ == ID)
			return &Players[i];
	}
	return NULL;
}

void CPlayerController::SendChatMessage (CString& Message)
{
	CommandsManager_.SendMessage (Message);
}

void CPlayerController::AddMessageToChat (CString& Message, SColor Color)
{
	CHATWINDOW.AddString (Message, Color);
}

//==============================================================================

const SColor CPlayerController::INFO_MESSAGE_COLOR = SColor (255, 128, 64);

void CPlayerController::ChatMessage (int PlayerID, CString& Message)
{
	CString String;
	if (PlayerID != -1)
	{
		String.Print ("%d:\t%s", PlayerID + 1, Message.Data());
		SColor Color (255, 255, 255);
		AddMessageToChat (String, Color);
	}
	else
	{
		AddMessageToChat (Message, INFO_MESSAGE_COLOR);
	}
}

void CPlayerController::SetTime (int PlayerID, int Time)
{
	CLIENTWORLD.SetTime (Time);

	if (PlayerID != -1)
	{
		CString Message;
		SIVector2D TimeHM = ToNormalTime (Time);
		Message.Print ("Player #%d set time %d:%02d", PlayerID + 1, TimeHM.x, TimeHM.y);
		AddMessageToChat (Message);
	}
}

void CPlayerController::FlyPlayer (int PlayerID, bool FlyMode)
{
	CPlayer* Player = FindPlayer (PlayerID);
	assert (Player);
	if (Game_->PlayerID() == PlayerID)
		Stop();
	Player->SetFlyMode (FlyMode);

	CString Message;
	if (FlyMode)
		Message.Print ("Player #%d now can fly", PlayerID + 1);
	else
		Message.Print ("Player #%d now can't fly", PlayerID + 1);
	AddMessageToChat (Message);
}

CPlayerModel& CPlayerController::GetModel()
{
	return PlayerModel_;
}
