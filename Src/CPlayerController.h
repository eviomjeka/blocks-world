﻿#ifndef CPLAYERCONTROLLER
#define CPLAYERCONTROLLER

#include <Engine.h>
#include "Network/CPlayer.h"
#include "World/CWorld.h"
#include "Network/Packets.h"
#include "CSky.h"
#include "CWorldScene.h"
#include "CBlockSelection.h"
#include "CCommandsManager.h"
#include "CGame.h"
#include "World/CPlayerModel.h"

using std::map;

class CClient;

class CPlayerController
{
private:
	struct SPlayerAnimation
	{
		SModelContext	ModelContext_;
		CSoundSource	PlayerSound_;
	};

	friend class CBlockSelection;
	friend class CPlayerModel;
	bool						Initialized_;
	CWorldScene					WorldScene_;
	CMesh						PointMesh_;
	CSky						Sky_;
	CTexture					WorldTexture_;
	bool						MoveDirection_[NUM_MOVE_DIRECTIONS];
	CBlockSelection				BlockSelection_;
	map<int, SPlayerAnimation>	Animations_;

	CGame*						Game_;
	CPlayer*					Player_;
	CCommandsManager			CommandsManager_;
	
	CPlayerModel				PlayerModel_;
	CMSound						WalkingSound_;

	void RenderWaterBlueRect_();
	void RenderPoint_();
	void RenderPlayers_(int Time);

public:
	
	static const SColor INFO_MESSAGE_COLOR;

	CPlayerController();
	void Init(CGame* Game);
	void Destroy();

	void InteractWithBlock();
	void RemoveBlock();
	void MoveDirection(int Direction, bool Move);
	void Jump();
	void Rotate(SFVector2D Delta);
	void SetPlayerModelViewMatrix (bool TranslatePosition = true);
	void Proc(int Time, bool Render);
	void Stop();
	
	CTexture& WorldTexture();
	CWorldScene* WorldScene();
	SFVector Position();
	CPlayer* Controlled();
	bool FlyMode();
	void SetPosition(SFVector Position);
	void SetControlled (int Controlled);
	int ToGameTime (int Hours, int Minutes);
	SIVector2D ToNormalTime (int Time);
	CPlayer* FindPlayer (int ID);
	void SendChatMessage (CString& Message);
	void AddMessageToChat (CString& Message, SColor Color = INFO_MESSAGE_COLOR);
	
	void ChatMessage (int PlayerID, CString& Message);
	void SetTime (int PlayerID, int Time);
	void FlyPlayer (int PlayerID, bool FlyMode);
	CPlayerModel& GetModel();

};

#endif
