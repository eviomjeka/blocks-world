﻿#include "CMeshGenerator.h"
#include "World/CClientSubWorld.h"
#include "CApplication.h"

void CMeshGenerator::Init()
{
	UpdateSmoothLighting();

	Rotate_ = false;
	SubWorld_ = NULL;
	CurrentComplexBlocksMQ_ = NULL;
	CurrentSector_ = 0;
	BlockLightCalculated_ = false;
}

void CMeshGenerator::UpdateSmoothLighting()
{
	SmoothLighting_ = SETTINGS.SmoothLighting;
}

CBlockData CMeshGenerator::GetBlock (SIVector Position)
{
	Position.y += CurrentSector_ * SECTOR_SIZE;

	if (Position.y < 0 || Position.y > SUBWORLD_SIZE_Y)
		return CBlockData();

	SIVector2D SubWorldPosition	= DivXZ2D (Position, SUBWORLD_SIZE_XZ);
	SIVector   BlockPosition	= ModXZ (Position, SUBWORLD_SIZE_XZ);
	
	return SubWorlds_ (SubWorldPosition)->GetBlock (BlockPosition);
}

CBlockData CMeshGenerator::GetBlock_ (SIVector Position)
{
	if (Position.y < 0 || Position.y > SUBWORLD_SIZE_Y)
		return CBlockData();

	SIVector2D SubWorldPosition	= DivXZ2D (Position, SUBWORLD_SIZE_XZ) - SubWorld_->Position();
	SIVector   BlockPosition	= ModXZ (Position, SUBWORLD_SIZE_XZ);
	
	return SubWorlds_ (SubWorldPosition)->GetBlock (BlockPosition);
}

SVector2D<byte> CMeshGenerator::GetLight_ (SIVector Position)
{
	if (Position.y < 0 || Position.y > SUBWORLD_SIZE_Y)
		return SVector2D<byte> (0, MAX_LIGHT_INTENSITY);

	SIVector2D SubWorldPosition	= DivXZ2D (Position, SUBWORLD_SIZE_XZ) - SubWorld_->Position();
	SIVector   BlockPosition	= ModXZ (Position, SUBWORLD_SIZE_XZ);

	return SubWorlds_ (SubWorldPosition)->GetLight (BlockPosition);
}

void CMeshGenerator::GenerateMeshes (SSubWorldMeshQueues& MeshQueues, int Sector, CClientSubWorlds3x3& SubWorlds)
{
	SubWorlds_ = SubWorlds;
	SubWorld_ = SubWorlds_ (0, 0);
	if (!SubWorld_->Blocks_[Sector].Blocks)
	{
		MeshQueues.BlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));
		MeshQueues.ComplexBlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));
		MeshQueues.TranslucentComplexBlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));
		MeshQueues.BlocksMQ.End();
		MeshQueues.ComplexBlocksMQ.End();
		MeshQueues.TranslucentComplexBlocksMQ.End();
		return;
	}
	assert (Sector >= 0 && Sector < SUBWORLD_SECTORS);
	CurrentSector_ = Sector;
	
	MeshQueues.BlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));
	MeshQueues.ComplexBlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));
	MeshQueues.TranslucentComplexBlocksMQ.Begin(SIVector (SubWorld_->Position().x, Sector, SubWorld_->Position().y));

	CBlocksSector* SectorData = SubWorld_->GetSector (Sector);
	for (int x = 0; x < SECTOR_SIZE; x++)
		for (int z = 0; z < SECTOR_SIZE; z++)
			for (int y = 0; y < SECTOR_SIZE; y++)
			{
				if (y + Sector * SECTOR_SIZE == SUBWORLD_SIZE_Y)
					continue;

				SIVector P0 (x, y, z);
				CBlockData Block0 = (*SectorData) (P0);
				if (Block0.Type() == BLOCKTYPE_VOID)
					continue;
				if (Block0.Type() != BLOCKTYPE_BLOCK)
				{
					BlockLightCalculated_ = false;
					BlockPosition_ = P0;

					RenderBlock_ (Block0, MeshQueues);
					continue;
				}

				if (x < SECTOR_SIZE - 1)
				{
					SIVector PX (x + 1, y, z);
					CBlockData BlockX = (*SectorData) (PX);

					AddFace_ (Block0, HXFACE_MASK, BlockX, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}
				if (x > 0)
				{
					SIVector PX (x - 1, y, z);
					CBlockData BlockX = (*SectorData) (PX);

					AddFace_ (Block0, LXFACE_MASK, BlockX, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}

				if (y < SECTOR_SIZE - 1)
				{
					SIVector PY (x, y + 1, z);
					CBlockData BlockY = (*SectorData) (PY);
					AddFace_ (Block0, HYFACE_MASK, BlockY, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}
				if (y > 0)
				{
					SIVector PY (x, y - 1, z);
					CBlockData BlockY = (*SectorData) (PY);
					AddFace_ (Block0, LYFACE_MASK, BlockY, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}

				if (z < SECTOR_SIZE - 1)
				{
					SIVector PZ (x, y, z + 1);
					CBlockData BlockZ = (*SectorData) (PZ);
					AddFace_ (Block0, HZFACE_MASK, BlockZ, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}
				if (z > 0)
				{
					SIVector PZ (x, y, z - 1);
					CBlockData BlockZ = (*SectorData) (PZ);
					AddFace_ (Block0, LZFACE_MASK, BlockZ, 
						P0 + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
				}
			}
	
	CBlocksSector* SectorDataLX = SubWorlds_ (-1, 0)->GetSector (Sector);
	CBlocksSector* SectorDataHX = SubWorlds_ (1,  0)->GetSector (Sector);
	CBlocksSector* SectorDataLZ = SubWorlds_ (0, -1)->GetSector (Sector);
	CBlocksSector* SectorDataHZ = SubWorlds_ (0,  1)->GetSector (Sector);
	for (int y = 0; y < SECTOR_SIZE; y++)
	{
		if (y + Sector * SECTOR_SIZE == SUBWORLD_SIZE_Y)
			continue;

		for (int xz = 0; xz < SUBWORLD_SIZE_XZ; xz++)
		{
			SIVector PLX (0, y, xz);
			SIVector PHX (SUBWORLD_SIZE_XZ - 1, y, xz);
			SIVector PLZ (xz, y, 0);
			SIVector PHZ (xz, y, SUBWORLD_SIZE_XZ - 1);

			CBlockData BlockALX = (*SectorData) (PLX);
			CBlockData BlockBLX = SectorDataLX ? (*SectorDataLX) (PHX) : CBlockData();
			if (BlockALX.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockALX, LXFACE_MASK, BlockBLX, 
					PLX + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
			
			CBlockData BlockAHX = (*SectorData) (PHX);
			CBlockData BlockBHX = SectorDataHX ? (*SectorDataHX) (PLX) : CBlockData();
			if (BlockAHX.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockAHX, HXFACE_MASK, BlockBHX, 
					PHX + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
			
			CBlockData BlockALZ = (*SectorData) (PLZ);
			CBlockData BlockBLZ = SectorDataLZ ? (*SectorDataLZ) (PHZ) : CBlockData();
			if (BlockALZ.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockALZ, LZFACE_MASK, BlockBLZ, 
					PLZ + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
			
			CBlockData BlockAHZ = (*SectorData) (PHZ);
			CBlockData BlockBHZ = SectorDataHZ ? (*SectorDataHZ) (PLZ) : CBlockData();
			if (BlockAHZ.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockAHZ, HZFACE_MASK, BlockBHZ, 
					PHZ + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
		}
	}
	
	CBlocksSector* SectorDataY = (Sector + 1 >= SUBWORLD_SECTORS) ? NULL : SubWorld_->GetSector (Sector + 1);
	for (int x = 0; x < SECTOR_SIZE; x++)
		for (int z = 0; z < SECTOR_SIZE; z++)
		{
			SIVector PLY (x, SECTOR_SIZE - 1, z);
			SIVector PHY (x, 0, z);

			CBlockData BlockALY = (*SectorData) (PLY);
			CBlockData BlockBLY = SectorDataY ? (*SectorDataY) (PHY) : CBlockData();
			if (BlockALY.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockALY, HYFACE_MASK, BlockBLY, 
					PLY + SIVector (0, Sector * SECTOR_SIZE, 0), Sector, MeshQueues);
			if (BlockBLY.Type() == BLOCKTYPE_BLOCK)
				AddFace_ (BlockBLY, LYFACE_MASK, BlockALY, 
					PHY + SIVector (0, (Sector + 1) * SECTOR_SIZE, 0), Sector, MeshQueues);
		}

	MeshQueues.BlocksMQ.End();
	MeshQueues.ComplexBlocksMQ.End();
	MeshQueues.TranslucentComplexBlocksMQ.End();
}

void CMeshGenerator::RenderBlock_ (CBlockData Block, SSubWorldMeshQueues& MeshQueues)
{
	if (Block.Type() == BLOCKTYPE_COMPLEX_BLOCK)
		CurrentComplexBlocksMQ_ = &MeshQueues.ComplexBlocksMQ;
	else if (Block.Type() == BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK)
		CurrentComplexBlocksMQ_ = &MeshQueues.TranslucentComplexBlocksMQ;

	BlockRotate_ = Block.Rotate();
	Rotate_ = (BlockRotate_ != (ROTATEX_NONE | ROTATEY_NONE | ROTATEZ_NONE));

	BlocksList (Block.ID())->Render (Block.Attributes(), this, BlockPosition_, this);
}

void CMeshGenerator::AddVertex (SFVector VertexPosition, SVector2D<byte> TextureShift, SVector2D<unsigned short> TextureCoord)
{
	SFVector Position = VertexPosition * (2.0f*256.0f);
	SVector<short> SPosition = SVector<short> ((short) ceil (Position.x), (short) ceil (Position.y), (short) ceil (Position.z));

	if (Rotate_)
	{
		SPosition = RotateVector (SPosition - BlockPosition_ * 2*256 - SIVector (256), BlockRotate_) + 
			BlockPosition_ * 2*256 + SIVector (256);
	}
	
	SVector2D<byte> LightVector;
	/*if (SmoothLighting_)
	{
		if (!BlockLightCalculated_)
		{
			SIVector AbsoluteBlockPosition = BlockPosition_ + SIVector (SubWorld_->Position().x * SUBWORLD_SIZE_XZ, 
				CurrentSector_ * SECTOR_SIZE, SubWorld_->Position().y * SUBWORLD_SIZE_XZ);
			SVector2D<byte> BlockLight[4] = {};

			GetFaceLightVector_ (BlockLight, AbsoluteBlockPosition - SIVector (0, 1, 0), HYFACE_MASK, 1.0f);
			BlockLight_[0][0][0][0] = BlockLight[0].x; BlockLight_[1][0][0][0] = BlockLight[0].y;
			BlockLight_[0][0][0][1] = BlockLight[1].x; BlockLight_[1][0][0][1] = BlockLight[1].y;
			BlockLight_[0][1][0][1] = BlockLight[2].x; BlockLight_[1][1][0][1] = BlockLight[2].y;
			BlockLight_[0][1][0][0] = BlockLight[3].x; BlockLight_[1][1][0][0] = BlockLight[3].y;
			GetFaceLightVector_ (BlockLight, AbsoluteBlockPosition + SIVector (0, 1, 0), LYFACE_MASK, 1.0f);
			BlockLight_[0][0][1][0] = BlockLight[0].x; BlockLight_[1][0][1][0] = BlockLight[0].y;
			BlockLight_[0][1][1][0] = BlockLight[1].x; BlockLight_[1][1][1][0] = BlockLight[1].y;
			BlockLight_[0][1][1][1] = BlockLight[2].x; BlockLight_[1][1][1][1] = BlockLight[2].y;
			BlockLight_[0][0][1][1] = BlockLight[3].x; BlockLight_[1][0][1][1] = BlockLight[3].y;
			BlockLightCalculated_ = true;
		}

		SFVector v = (SFVector) SPosition / 2.0f / 256.0f - BlockPosition_;
		if (v.x < 0) v.x = 0; if (v.y < 0) v.y = 0; if (v.z < 0) v.z = 0;
		if (v.x > 1) v.x = 1; if (v.y > 1) v.y = 1; if (v.z > 1) v.z = 1;
		LightVector (TrilinearInterpolationLight_ ((SFVector) v, BlockLight_[0]), 
					 TrilinearInterpolationLight_ ((SFVector) v, BlockLight_[1]));
	}
	else*/
	{
		SIVector2D SumLightVector;
		SIVector Position = BlockPosition_ + SIVector (SubWorld_->Position().x * SUBWORLD_SIZE_XZ, 
			CurrentSector_ * SECTOR_SIZE, SubWorld_->Position().y * SUBWORLD_SIZE_XZ);
		int Num = 0;
		for (int i = 0; i < NUM_FACES; i++)
		{
			byte Face = IndexToFace ((byte) i);
			
			SIVector NeighbourPosition = FaceToVector (Position, Face);
			if (GetBlock_ (NeighbourPosition).LightAttenuation (InvertFace (Face)) <= 1)
			{
				SumLightVector += GetLight_ (NeighbourPosition);
				Num++;
			}
		}
		if (Num != 0)
			LightVector = (SVector2D<byte>) (((SFVector2D) SumLightVector) / (float) Num);
		else
			LightVector = SVector2D<byte> (0, 0);
	}

	CurrentComplexBlocksMQ_->AddVertex (SPosition, 
		SVector2D<unsigned short> (TextureShift) * 2 * 256 + TextureCoord, LightVector);
}

byte CMeshGenerator::TrilinearInterpolationLight_ (SFVector v, byte f[2][2][2])
{
	float Value = 
		(float) f[0][0][0] * (1 - v.x) * (1 - v.y) * (1 - v.z) + 
		(float) f[0][0][1] * (1 - v.x) * (1 - v.y) * (v.z - 0) + 
		(float) f[0][1][0] * (1 - v.x) * (v.y - 0) * (1 - v.z) + 
		(float) f[0][1][1] * (1 - v.x) * (v.y - 0) * (v.z - 0) + 
		(float) f[1][0][0] * (v.x - 0) * (1 - v.y) * (1 - v.z) + 
		(float) f[1][0][1] * (v.x - 0) * (1 - v.y) * (v.z - 0) + 
		(float) f[1][1][0] * (v.x - 0) * (v.y - 0) * (1 - v.z) + 
		(float) f[1][1][1] * (v.x - 0) * (v.y - 0) * (v.z - 0);
	
	return (byte) Value;
}

void CMeshGenerator::AddFace_ (CBlockData Block, byte Face, CBlockData NBlock, 
							   SIVector Position, int Sector, SSubWorldMeshQueues& MeshQueues)
{
	if (NBlock.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
		return;
	
	CClientSubWorld* SubWorld = SubWorlds_ (0, 0);

	SIVector v[4] = {};
	SVector2D<byte> TextureCoord[4] = {};
	GetFace (Face, v, Position - SIVector (0, Sector * SECTOR_SIZE, 0));
	
	SVector2D<byte> LightVector[4];
	GetFaceLightVector_ (LightVector, Position + SIVector 
		(SubWorld->Position().x * SUBWORLD_SIZE_XZ, 0, SubWorld->Position().y * SUBWORLD_SIZE_XZ), Face, 0.93f);

	TextureCoord[0] = Block.BlockFaceTextureOffset (Face);
	__assert (TextureCoord[0].x < 16 && TextureCoord[0].y < 16);
	TextureCoord[0] *= 2;

	GetTextureCoordsForFace (TextureCoord, Face);

	const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
	for (int i = 0; i < 6; i++)
	{
		int VertexIndex = VertexIndexes[i];
		MeshQueues.BlocksMQ.AddVertex (v[VertexIndex], TextureCoord[VertexIndex], LightVector[VertexIndex]);
	}
}

void CMeshGenerator::GetFaceLightVector_ (SVector2D<byte> LightVector[4], SIVector Position, byte Face, float Coef)
{
	if (SmoothLighting_)
	{
		SIVector NeighbourPosition = FaceToVector (Position, Face);

		SIVector v[4] = {};
		GetFace (Face, v, Position);
		CBlockData Block = GetBlock_ (NeighbourPosition);
		SVector2D<byte> LBlock = GetLight_ (NeighbourPosition);

#define GET_BLOCK(xx,yy,zz) GetBlock_ (NeighbourPosition +			\
	SIVector ((xx == 0) ? 0 : ((v[i].x == Position.x) ? -1 : 1),	\
				(yy == 0) ? 0 : ((v[i].y == Position.y) ? -1 : 1),	\
				(zz == 0) ? 0 : ((v[i].z == Position.z) ? -1 : 1)))
#define GET_LIGHT(xx,yy,zz) GetLight_ (NeighbourPosition +			\
	SIVector ((xx == 0) ? 0 : ((v[i].x == Position.x) ? -1 : 1),	\
				(yy == 0) ? 0 : ((v[i].y == Position.y) ? -1 : 1),	\
				(zz == 0) ? 0 : ((v[i].z == Position.z) ? -1 : 1)))
#define TO_NEIGHBOUR_BLOCK(xx,yy,zz) (xx ? ((v[i].x == Position.x) ? LXFACE_MASK : HXFACE_MASK) : 	\
									 (yy ? ((v[i].y == Position.y) ? LYFACE_MASK : HYFACE_MASK) : 	\
									 (	   ((v[i].z == Position.z) ? LZFACE_MASK : HZFACE_MASK))))
#define TO_MAIN_BLOCK(xx,yy,zz)		 (xx ? ((v[i].x == Position.x) ? HXFACE_MASK : LXFACE_MASK) : 	\
									 (yy ? ((v[i].y == Position.y) ? HYFACE_MASK : LYFACE_MASK) : 	\
									 (	  ((v[i].z == Position.z) ? HZFACE_MASK : LZFACE_MASK))))

		for (int i = 0; i < 4; i++)
		{
			SFVector2D FaceLightVector = (SFVector2D) LBlock;

			int IsX = 0, IsY = 0, IsZ = 0, IsXY = 0, IsXZ = 0, IsYZ = 0;

			CBlockData BlockX  = GET_BLOCK (1, 0, 0);
			CBlockData BlockY  = GET_BLOCK (0, 1, 0);
			CBlockData BlockZ  = GET_BLOCK (0, 0, 1);
			CBlockData BlockXY = GET_BLOCK (1, 1, 0);
			CBlockData BlockXZ = GET_BLOCK (1, 0, 1);
			CBlockData BlockYZ = GET_BLOCK (0, 1, 1);

			SVector2D<byte> LBlockX  = GET_LIGHT (1, 0, 0);
			SVector2D<byte> LBlockY  = GET_LIGHT (0, 1, 0);
			SVector2D<byte> LBlockZ  = GET_LIGHT (0, 0, 1);
			SVector2D<byte> LBlockXY = GET_LIGHT (1, 1, 0);
			SVector2D<byte> LBlockXZ = GET_LIGHT (1, 0, 1);
			SVector2D<byte> LBlockYZ = GET_LIGHT (0, 1, 1);

			if (Block.LightAttenuation (TO_NEIGHBOUR_BLOCK (1, 0, 0)) <= 1 && BlockX.LightAttenuation (TO_MAIN_BLOCK (1, 0, 0)) <= 1 && 
				!(Face == LXFACE_MASK || Face == HXFACE_MASK))
			{
				IsX = 1;
				FaceLightVector += (SFVector2D) LBlockX;
			}
			if (Block.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 1, 0)) <= 1 && BlockY.LightAttenuation (TO_MAIN_BLOCK (0, 1, 0)) <= 1 && 
				!(Face == LYFACE_MASK || Face == HYFACE_MASK))
			{
				IsY = 1;
				FaceLightVector += (SFVector2D) LBlockY;
			}
			if (Block.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 0, 1)) <= 1 && BlockZ.LightAttenuation (TO_MAIN_BLOCK (0, 0, 1)) <= 1 && 
				!(Face == LZFACE_MASK || Face == HZFACE_MASK))
			{
				IsZ = 1;
				FaceLightVector += (SFVector2D) LBlockZ;
			}
		
			if ((Face == LZFACE_MASK || Face == HZFACE_MASK) && 
				((IsX && BlockX.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 1, 0)) <= 1 && BlockXY.LightAttenuation (TO_MAIN_BLOCK (0, 1, 0)) <= 1) || 
				 (IsY && BlockY.LightAttenuation (TO_NEIGHBOUR_BLOCK (1, 0, 0)) <= 1 && BlockXY.LightAttenuation (TO_MAIN_BLOCK (1, 0, 0)) <= 1)))
			{
				IsXY = 1;
				FaceLightVector += (SFVector2D) LBlockXY;
			}
			if ((Face == LYFACE_MASK || Face == HYFACE_MASK) && 
				((IsX && BlockX.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 0, 1)) <= 1 && BlockXZ.LightAttenuation (TO_MAIN_BLOCK (0, 0, 1)) <= 1) || 
				 (IsZ && BlockZ.LightAttenuation (TO_NEIGHBOUR_BLOCK (1, 0, 0)) <= 1 && BlockXZ.LightAttenuation (TO_MAIN_BLOCK (1, 0, 0)) <= 1)))
			{
				IsXZ = 1;
				FaceLightVector += (SFVector2D) LBlockXZ;
			}
			if ((Face == LXFACE_MASK || Face == HXFACE_MASK) && 
				((IsY && BlockY.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 0, 1)) <= 1 && BlockYZ.LightAttenuation (TO_MAIN_BLOCK (0, 0, 1)) <= 1) || 
				 (IsZ && BlockZ.LightAttenuation (TO_NEIGHBOUR_BLOCK (0, 1, 0)) <= 1 && BlockYZ.LightAttenuation (TO_MAIN_BLOCK (0, 1, 0)) <= 1)))
			{
				IsYZ = 1;
				FaceLightVector += (SFVector2D) LBlockYZ;
			}

			FaceLightVector /= 1.0f + (IsX + IsY + IsZ + IsXY + IsXZ + IsYZ) * Coef + 3 * (1.0f - Coef);

			byte FoggingLighting = 0;
			if (Face == LYFACE_MASK || Face == HYFACE_MASK)
				FoggingLighting = 0;
			else if (Face == LXFACE_MASK || Face == HXFACE_MASK)
				FoggingLighting = 4;
			else
				FoggingLighting = 6;

			if (FaceLightVector.x < FoggingLighting)
				FaceLightVector.x = 0;
			else
				FaceLightVector.x -= FoggingLighting;
			if (FaceLightVector.y < FoggingLighting)
				FaceLightVector.y = 0;
			else
				FaceLightVector.y -= FoggingLighting;

			LightVector[i] = SVector2D<byte> (FaceLightVector);
		}
#undef GET_BLOCK
#undef GET_LIGHT
#undef TO_NEIGHBOUR_BLOCK
#undef TO_MAIN_BLOCK
	}
	else
	{
		SIVector NeighbourPosition = FaceToVector (Position, Face);
	
		SVector2D<byte> FaceLightVector = GetLight_ (NeighbourPosition);
		__assert (FaceLightVector.x <= 31 && FaceLightVector.y <= 31);

		byte FoggingLighting = 0;
		if (Face == LYFACE_MASK || Face == HYFACE_MASK)
			FoggingLighting = 0;
		else if (Face == LXFACE_MASK || Face == HXFACE_MASK)
			FoggingLighting = 4;
		else
			FoggingLighting = 6;

		if (FaceLightVector.x < FoggingLighting)
			FaceLightVector.x = 0;
		else
			FaceLightVector.x -= FoggingLighting;
		if (FaceLightVector.y < FoggingLighting)
			FaceLightVector.y = 0;
		else
			FaceLightVector.y -= FoggingLighting;

		for (int i = 0; i < 4; i++)
			LightVector[i] = FaceLightVector;
	}
}