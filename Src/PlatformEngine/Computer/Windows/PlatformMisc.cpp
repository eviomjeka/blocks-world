﻿#include "PlatformMisc.h"
#include <Engine.h>

struct __SThreadInit
{
	void* Param;
	TThreadFunction* Func;
	volatile bool Ready;
};

DWORD WINAPI __ThreadFunction (LPVOID Param)
{
	__SThreadInit* PInitializer = (__SThreadInit*) Param;
	__SThreadInit Initializer = *PInitializer;
	PInitializer->Ready = true;

	Initializer.Func (Initializer.Param);

	return 0;
}

void LaunchThread (TThreadFunction ThreadFunction, void* Param)
{
	__SThreadInit Initializer = {};
	Initializer.Param = Param;
	Initializer.Func  = ThreadFunction;
	Initializer.Ready = false;

	CreateThread (0, 0, (LPTHREAD_START_ROUTINE) __ThreadFunction, &Initializer, 0, 0);

	while (!Initializer.Ready);
}

void PrintLog (const char* Data)
{
	printf ("%s", Data);
	OutputDebugString (Data);
}

void SetWarningColor()
{
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
}

void SetNormalColor()
{
	SetConsoleTextAttribute (GetStdHandle (STD_OUTPUT_HANDLE), 
		FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
}

int TimeMS()
{
	return timeGetTime();
}