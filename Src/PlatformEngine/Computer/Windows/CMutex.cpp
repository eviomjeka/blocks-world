﻿#include "CMutex.h"
#include <Engine.h>

CMutex::CMutex() :
Locked_(false)
{
	InitializeCriticalSection (&Mutex_);
}

CMutex::~CMutex()
{
	DeleteCriticalSection (&Mutex_);
}

void CMutex::Lock()
{
	EnterCriticalSection (&Mutex_);
	assert (!Locked_);
	Locked_ = true;
}

void CMutex::Unlock()
{
	assert (Locked_);
	Locked_ = false;
	LeaveCriticalSection (&Mutex_);
}

bool CMutex::TryLock()
{
	bool Locked = TryEnterCriticalSection (&Mutex_) != 0;
	if (Locked)
	{
		assert (!Locked_);
		Locked_ = true;
	}

	return Locked;
}