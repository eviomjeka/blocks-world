﻿#ifndef PLATFORMMISC
#define PLATFORMMISC

#include <PlatformInclude.h>
#include "../../PlatformBase.h"

#undef CreateWindow
#undef DeleteFile

void LaunchThread (TThreadFunction ThreadFunction, void* Param);

void PrintLog (const char* Data);
void SetWarningColor();
void SetNormalColor();

// void Sleep (int MSec); Function already exists
int TimeMS();

#define snprintf(a,b,...)	_snprintf_s (a, b, _TRUNCATE, __VA_ARGS__)
#define vsnprintf(a,b,c,d)	vsnprintf_s (a, b, _TRUNCATE, c, d)
//#define sscanf(a,b,...)		sscanf_s (a, b, __VA_ARGS__)
#define strncat(a,b,c)		strncat_s (a, c, b, _TRUNCATE)
#define strncpy(a,b,c)		strncpy_s (a, c, b, _TRUNCATE)

#define EADDRNOTAVAIL WSAEADDRNOTAVAIL
#define ETIMEDOUT WSAETIMEDOUT
#define ECONNRESET WSAECONNRESET
#define ECONNREFUSED WSAECONNREFUSED
#define EWOULDBLOCK WSAEWOULDBLOCK
#define SocketError() WSAGetLastError()
typedef SOCKET Socket;

#endif