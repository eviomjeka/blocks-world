﻿#ifndef CFILESYSTEM
#define CFILESYSTEM

#include <PlatformMisc.h>
#include "../../PlatformBase.h"
#include "../../../Engine/Containers/CArray.h"

class CFileSystem
{
public:

	static void FileList (CArray<CFileSystemElement>* FilesArray, const char* Path);
	static bool CreateDir (const char* Path);
	static bool DeleteDir (const char* Path);
	static bool DeleteFile (const char* Path);
	static char RenameDir (const char* Src, const char* Dest);
	static char* ToGameFolderPath (const char* Path, char* GamePath);

private:
	CFileSystem();
};

#endif