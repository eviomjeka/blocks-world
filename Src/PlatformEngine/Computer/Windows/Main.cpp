﻿#include "../../../CApplication.h"

#if RELEASE == FALSE

int main (int argc, char** argv)
{
	_CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_ALWAYS_DF);
	//_CrtSetBreakAlloc ();

	bool IsGame = (argc != 2 || strcmp (argv[1], "--server") != 0);
	CApplication Application;
	Application.Init (IsGame);
	Application.MainLoop();

	return 0;
}

#else

int CALLBACK WinMain (HINSTANCE, HINSTANCE, LPSTR, int)
{
	bool IsGame = (__argc != 2 || strcmp (__argv[1], "--server") != 0);
	CApplication Application;
	Application.Init (IsGame);
	Application.MainLoop();

	return 0;
}

#endif