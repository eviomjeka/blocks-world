﻿#include "CWindow.h"
#include <Engine.h>

CWindow* __CWindow;

CWindow::CWindow()
{}

CWindow::~CWindow()
{}

bool CWindow::CreateWindow (bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize)
{
	__CWindow = this;

	HideCursor_ = false;
	Focused_	= true;
	Fullscreen_	= Fullscreen;
	Destroy_	= false;

	MinWindowSize_ = MinWindowSize;
	PrevousCursorPosition_ = SIVector2D (-1, -1);

	WSADATA Data = {};
	assert (WSAStartup (MAKEWORD (2, 2), (WSADATA*) &Data) == 0);

	CString WindowClassName;
	WindowClassName.Print ("%s_Window::Class", APPLICATION_NAME);

	WNDCLASSEX WndClass = {};
	WndClass.cbSize			= sizeof (WNDCLASSEX);
	WndClass.style			= CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc	= WndProc_;
	WndClass.cbClsExtra		= 0;
	WndClass.cbWndExtra		= 0;
	WndClass.hInstance		= GetModuleHandle (NULL);
	WndClass.hIcon			= LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (IDI_ICON));
	WndClass.hCursor		= LoadCursor (NULL, IDC_ARROW);
	WndClass.hbrBackground	= (HBRUSH) GetStockObject (WHITE_BRUSH);
	WndClass.lpszMenuName	= NULL;
	WndClass.lpszClassName	= WindowClassName.Data();

	assert (RegisterClassEx (&WndClass) != NULL);
	
	PIXELFORMATDESCRIPTOR PixelFormatDescriptor = {};
	PixelFormatDescriptor.nSize      = sizeof (PIXELFORMATDESCRIPTOR);
	PixelFormatDescriptor.nVersion   = 1;
	PixelFormatDescriptor.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_GENERIC_ACCELERATED;
	PixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	PixelFormatDescriptor.cColorBits = 16;
	PixelFormatDescriptor.cDepthBits = 32;

	InitGLEW_ (WindowClassName, &PixelFormatDescriptor);

	WindowBorderSize_ = SIVector2D (2 * GetSystemMetrics (SM_CXFRAME), 
		GetSystemMetrics (SM_CYCAPTION) + 2 * GetSystemMetrics (SM_CYFRAME));
	
	int Style = 0;
	if (!Fullscreen_)
	{
		Style = WS_OVERLAPPEDWINDOW;
		WindowSize_		= WindowSize + WindowBorderSize_;
		WindowPosition_	= SIVector2D ((GetSystemMetrics (SM_CXSCREEN) - WindowSize.x) / 2,
									  (GetSystemMetrics (SM_CYSCREEN) - WindowSize.y) / 2);
	}
	else
	{
		Style = WS_POPUP;
		WindowSize_		= SIVector2D (GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN));
		WindowPosition_	= SIVector2D (0, 0);

		WindowSize					= WindowSize + WindowBorderSize_;
		NoFullscreenWindowSize_		= WindowSize;
		NoFullscreenWindowPosition_	= SIVector2D ((GetSystemMetrics (SM_CXSCREEN) - WindowSize.x) / 2,
												  (GetSystemMetrics (SM_CYSCREEN) - WindowSize.y) / 2);
		
		WindowSize		= WindowSize_;
	}

	Window_ = CreateWindowA (WindowClassName.Data(), APPLICATION_NAME, Style, WindowPosition_.x, WindowPosition_.y,
							 WindowSize.x, WindowSize.y, NULL, (HMENU) NULL, GetModuleHandle (NULL), NULL);
	assert (Window_ != NULL);
	
	DC_ = GetDC (Window_);
	assert (DC_);

	int Attributes[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB, 24,
		WGL_ALPHA_BITS_ARB, 8,
		WGL_DEPTH_BITS_ARB, 16,
		WGL_STENCIL_BITS_ARB, 0,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
		WGL_SAMPLES_ARB, 1 /* TODO: wtf? */, 
		0, 0 };
	
	int PixelFormat = 0;
	UINT NumFormats = 0;
	assert (wglChoosePixelFormatARB);
	if (!wglChoosePixelFormatARB (DC_, Attributes, NULL, 1, &PixelFormat, &NumFormats))
	{
		// no multisampling
		int Attributes2[] = {
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
			WGL_COLOR_BITS_ARB, 24,
			WGL_ALPHA_BITS_ARB, 8,
			WGL_DEPTH_BITS_ARB, 16,
			WGL_STENCIL_BITS_ARB, 0,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			0, 0 };

		assert (wglChoosePixelFormatARB (DC_, Attributes2, NULL, 1, &PixelFormat, &NumFormats));
		// ^ okay, without multisampling doesn't work too
		
		WARNING ("Multisampling doesn't supported");
	}
	assert (NumFormats >= 1);
	
	assert (SetPixelFormat (DC_, PixelFormat, &PixelFormatDescriptor));
	
	RC_ = wglCreateContext (DC_);
	assert (RC_);
	assert (wglMakeCurrent (DC_, RC_));

#if RELEASE == FALSE
	if (wglSwapIntervalEXT)
	{
		assert (wglSwapIntervalEXT (0));
	}
	else
		WARNING ("wglSwapIntervalEXT doesn't supported");
#endif
	
	ShowWindow (Window_, SW_SHOW);
	assert (SetFocus (Window_));

	Resize();

	return true;
}

void CWindow::InitGLEW_ (CString& WindowClassName, PIXELFORMATDESCRIPTOR* PixelFormatDescriptor)
{
	IgnoreResize_ = true;
	Window_ = CreateWindowA (WindowClassName.Data(), APPLICATION_NAME, WS_POPUP, 0, 0,
							 1, 1, NULL, (HMENU) NULL, GetModuleHandle (NULL), NULL);
	assert (Window_ != NULL);
	IgnoreResize_ = false;

	DC_ = GetDC (Window_);
	assert (DC_);

	int PixelFormat = ChoosePixelFormat (DC_, PixelFormatDescriptor);
	assert (PixelFormat);
	assert (SetPixelFormat (DC_, PixelFormat, PixelFormatDescriptor));

	RC_ = wglCreateContext (DC_);
	assert (RC_);
	assert (wglMakeCurrent (DC_, RC_));

	GLenum ErrorCode = glewInit();
	if (ErrorCode != GLEW_OK)
		error("%s", (const char*) glewGetErrorString (ErrorCode));
	assert (glewIsSupported ("GL_VERSION_2_0"));
	assert (glewIsSupported ("GL_ARB_vertex_buffer_object")); // TODO: remove? + add more checks
	glassert();

	assert (wglMakeCurrent (NULL, NULL));
	assert (wglDeleteContext (RC_));
	assert (ReleaseDC (Window_, DC_));

	assert (::DestroyWindow (Window_));
}

void CWindow::MainLoop()
{
	assert (__CWindow == this);

	MSG Message = {};
	int Time = timeGetTime();

	while (!Destroy_)
	{
		if (PeekMessage (&Message, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage (&Message);
			DispatchMessage  (&Message);
		}
		else
		{
			int NewTime = timeGetTime();
			Proc (NewTime - Time);
			SwapBuffers (DC_);
			Time = NewTime;
		}
	}
	
	Destroy();
	
	assert (wglMakeCurrent (NULL, NULL));
	assert (wglDeleteContext (RC_));
	assert (ReleaseDC (Window_, DC_));

	assert (::DestroyWindow (Window_));
}

//==============================================================================

SIVector2D CWindow::GetWindowSize()
{
	return WindowSize_;
}

void CWindow::SetCursorPosition (SIVector2D Position)
{
	if (!Focused_)
		return;

	if (Fullscreen_)
		SetCursorPos (Position.x, Position.y);
	else
		SetCursorPos (Position.x + WindowPosition_.x, Position.y + WindowPosition_.y);
}

void CWindow::ShowCursor (bool Show)
{
	if (Show && HideCursor_)
	{
		HideCursor_ = false;
		if (Focused_)
			assert (ClipCursor (NULL));
		while (::ShowCursor (true) < 0);
	}
	else if (!Show && !HideCursor_)
	{
		HideCursor_ = true;
		if (Focused_)
		{
			RECT Rect = { WindowPosition_.x, WindowPosition_.y, 
				WindowPosition_.x + WindowSize_.x, WindowPosition_.y + WindowSize_.y };
			assert (ClipCursor (&Rect));
		}
		while (::ShowCursor (false) >= 0);
	}
}

bool CWindow::IsFullScreen()
{
	return Fullscreen_;
}

void CWindow::FullscreenMode (bool Fullscreen)
{
	if (Fullscreen == Fullscreen_)
		return;

	Fullscreen_ = Fullscreen;

	if (!Fullscreen)
	{
		IgnoreResize_ = true;
		assert (SetWindowLong (Window_, GWL_STYLE, WS_OVERLAPPEDWINDOW));
		assert (SetWindowPos (Window_, HWND_TOP, 0, 0, 0, 0, 
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED));
		IgnoreResize_ = false;

		WindowSize_		= NoFullscreenWindowSize_;
		WindowPosition_	= NoFullscreenWindowPosition_;
		
		SIVector2D WindowSize = WindowSize_ + WindowBorderSize_;
		assert (SetWindowPos (Window_, HWND_TOP, WindowPosition_.x, 
				WindowPosition_.y, WindowSize.x, WindowSize.y, 0));
	}
	else
	{
		IgnoreResize_ = true;
		assert (SetWindowLong (Window_, GWL_STYLE, WS_POPUP));
		assert (SetWindowPos (Window_, HWND_TOP, 0, 0, 0, 0, 
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED));
		IgnoreResize_ = false;

		NoFullscreenWindowSize_		= WindowSize_;
		NoFullscreenWindowPosition_	= WindowPosition_;
		WindowSize_		= SIVector2D (GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN));
		WindowPosition_	= SIVector2D (0, 0);

		assert (SetWindowPos (Window_, HWND_TOP, WindowPosition_.x, 
				WindowPosition_.y, WindowSize_.x, WindowSize_.y, 0));
	}
	
	ShowWindow (Window_, SW_SHOW);
	Resize();

	PrevousCursorPosition_ = WindowSize_ / 2;
	SetCursorPosition (PrevousCursorPosition_);
	MouseMotion (PrevousCursorPosition_);

	if (HideCursor_)
	{
		RECT Rect = { WindowPosition_.x, WindowPosition_.y, 
			WindowPosition_.x + WindowSize_.x, WindowPosition_.y + WindowSize_.y };
		assert (ClipCursor (&Rect));
	}
}

void CWindow::DestroyWindow()
{
	Destroy_ = true;
}

void CWindow::SetRGBA()
{
	
}

void CWindow::SetMinimumColorSize (int, int, int, int)
{
	
}

void CWindow::SetDoubleBuffer()
{

}
void CWindow::SetMinimumDepthSize (int)
{

}

void CWindow::RequestMSAA (int)
{

}

//==============================================================================

LRESULT CALLBACK CWindow::WndProc_ (HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
	switch (Message)
	{
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* Info = (MINMAXINFO*) LParam;
			POINT Min = { __CWindow->MinWindowSize_.x + __CWindow->WindowBorderSize_.x, 
						  __CWindow->MinWindowSize_.y + __CWindow->WindowBorderSize_.y };
			POINT Max = { GetSystemMetrics (SM_CXSCREEN), GetSystemMetrics (SM_CYSCREEN) };
			Info->ptMinTrackSize = Min;
			Info->ptMaxTrackSize = Max;
		}
		return 0;

		case WM_SYSCOMMAND:
			switch (WParam)
			{
				case SC_SCREENSAVE:		
				case SC_MONITORPOWER:	
					return 0;
			}
		break;

		case WM_KEYDOWN:
			if ((LParam & 0x40000000) == 0)
				__CWindow->KeyDown (WParam);
		return 0;

		case WM_KEYUP:
			__CWindow->KeyUp (WParam);
		return 0;

		case WM_LBUTTONDOWN:
			__CWindow->LeftButtonDown();
		return 0;

		case WM_LBUTTONUP:
			__CWindow->LeftButtonUp();
		return 0;

		case WM_RBUTTONDOWN:
			__CWindow->RightButtonDown();
		return 0;

		case WM_RBUTTONUP:
			__CWindow->RightButtonUp();
		return 0;

		case WM_MOUSEWHEEL:
		{
			int Delta = GET_WHEEL_DELTA_WPARAM (WParam);
			__CWindow->MouseWhell (-Delta / WHEEL_DELTA);
		}
		return 0;

		case WM_MOVE:
			__CWindow->WindowPosition_.x = LOWORD (LParam);
			__CWindow->WindowPosition_.y = HIWORD (LParam);
		return 0;

		case WM_MOUSEMOVE:
			if (__CWindow->PrevousCursorPosition_.x != LOWORD (LParam) || 
				__CWindow->PrevousCursorPosition_.y != HIWORD (LParam))
			{
				__CWindow->PrevousCursorPosition_ = SIVector2D (LOWORD (LParam), HIWORD (LParam));
				__CWindow->MouseMotion (SIVector2D (LOWORD (LParam), HIWORD (LParam)));
			}
		return 0;

		case WM_SIZE:
			if ((LOWORD (LParam) != 0 && HIWORD (LParam) != 0) && !__CWindow->IgnoreResize_ && 
				(LOWORD (LParam) != __CWindow->WindowSize_.x || HIWORD (LParam) != __CWindow->WindowSize_.y))
			{
				if (__CWindow->HideCursor_)
				{
					RECT Rect = { __CWindow->WindowPosition_.x, __CWindow->WindowPosition_.y, 
								  __CWindow->WindowPosition_.x + __CWindow->WindowSize_.x, 
								  __CWindow->WindowPosition_.y + __CWindow->WindowSize_.y };
					assert (ClipCursor (&Rect));
				}

				__CWindow->WindowSize_.x = LOWORD (LParam);
				__CWindow->WindowSize_.y = HIWORD (LParam);
				__CWindow->Resize();
			}
		return 0;

 		case WM_CLOSE:
			__CWindow->Destroy_ = true;
		return 0;

		case WM_ACTIVATE:
			if (LOWORD (WParam) != WA_INACTIVE && HIWORD (WParam) == 0)
			{
				__CWindow->Focused_ = true;
				if (__CWindow->HideCursor_)
				{
					RECT Rect = { __CWindow->WindowPosition_.x, __CWindow->WindowPosition_.y, 
								  __CWindow->WindowPosition_.x + __CWindow->WindowSize_.x, 
								  __CWindow->WindowPosition_.y + __CWindow->WindowSize_.y };
					assert (ClipCursor (&Rect));
				}
			}
			else
			{
				__CWindow->Focused_ = false;
				if (__CWindow->HideCursor_)
				{
					assert (ClipCursor (NULL));
				}
				__CWindow->Unfocus();
			}
		return 0;

		case WM_DESTROY:
			PostQuitMessage (0);
		return 0;
	}

	return DefWindowProc (Window, Message, WParam, LParam);
}