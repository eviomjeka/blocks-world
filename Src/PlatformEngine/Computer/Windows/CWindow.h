﻿#ifndef CWINDOW
#define CWINDOW

#include <PlatformMisc.h>
#include "../../PlatformBase.h"
#include "../../../Engine/Utilities/SVector2D.h"
#include "../../../Engine/Containers/CArray.h"
#include <Keys.h>

class CWindow : public CWindowInterface
{
public:

	CWindow();
	virtual ~CWindow();

	bool CreateWindow (bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize);
	void MainLoop();

	SIVector2D GetWindowSize();
	void SetCursorPosition (SIVector2D Position);
	void ShowCursor (bool Show);
	bool IsFullScreen();
	void FullscreenMode (bool Fullscreen);
	void DestroyWindow();
	
	void SetRGBA();
	void SetMinimumColorSize (int R, int G, int B, int A);
	void SetDoubleBuffer();
	void SetMinimumDepthSize (int D);

	void RequestMSAA (int MinimumBuffers);

private:

	CArray<int>	RequiredAttributes_;
	CArray<int>	DesiredAttributes_;

	HWND  Window_;
	HDC	  DC_;
	HGLRC RC_;

	bool HideCursor_;
	bool Focused_;
	bool Fullscreen_;
	bool Destroy_;
	bool IgnoreResize_;

	SIVector2D PrevousCursorPosition_;
	SIVector2D WindowPosition_;
	SIVector2D WindowSize_;
	SIVector2D NoFullscreenWindowPosition_;
	SIVector2D NoFullscreenWindowSize_;
	SIVector2D MinWindowSize_;
	SIVector2D WindowBorderSize_;

	void InitGLEW_ (CString& WindowClassName, PIXELFORMATDESCRIPTOR* PixelFormatDescriptor);

	static LRESULT CALLBACK WndProc_ (HWND Window, UINT Message, WPARAM WParam, LPARAM LParam);

};

#endif
