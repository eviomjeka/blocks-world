﻿#include "CFileSystem.h"
#include <Engine.h>

void CFileSystem::FileList (CArray<CFileSystemElement>* FilesArray, const char* Path)
{	
	WIN32_FIND_DATA Data = {};
	char GamePath[MAX_PATH] = "";
	ToGameFolderPath (Path, GamePath);
	char FilesAddr[MAX_PATH] = "";
	snprintf (FilesAddr, MAX_PATH, "%s/*", GamePath);

	HANDLE Handle = FindFirstFile (FilesAddr, &Data);
	assert (Handle != INVALID_HANDLE_VALUE);
	
	for (;;)
	{
		if (strcmp (Data.cFileName, ".") != 0 && strcmp (Data.cFileName, "..") != 0)
		{
			CFileSystemElement FileSystemElement = {};
			FileSystemElement.Name = Data.cFileName;
			FileSystemElement.Directory = (Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
			
			FilesArray->Insert (FileSystemElement, true);
		}

		if (FindNextFileA (Handle, &Data) == 0)
		{
			int Error = GetLastError();
			if (Error == ERROR_NO_MORE_FILES)
				break;
			error ("Get file list error");
		}
	}
	
	FindClose (Handle);
}

bool CFileSystem::CreateDir (const char* Path)
{
	char GamePath[MAX_PATH] = "";
	if (!CreateDirectory (ToGameFolderPath (Path, GamePath), NULL))
	{
		if (GetLastError() == ERROR_ALREADY_EXISTS)
			return false;
		else
			error ("Create directory error");
	}

	return true;
}

bool CFileSystem::DeleteDir (const char* Path)
{
	char GamePath[MAX_PATH] = "";
	ToGameFolderPath (Path, GamePath);
	char FilesAddr[MAX_PATH] = "";
	snprintf (FilesAddr, MAX_PATH, "%s/*", GamePath);

	WIN32_FIND_DATAA Data = {};
	HANDLE Handle = FindFirstFile (FilesAddr, &Data);
	if (Handle == INVALID_HANDLE_VALUE)
	{
		int Error = GetLastError();
		if (Error == ERROR_PATH_NOT_FOUND)
			return false;
		else if (Error != ERROR_FILE_NOT_FOUND)
			error ("Delete directory error (handle == INVALID_HANDLE_VALUE)");
	}
	
	if (Handle != INVALID_HANDLE_VALUE)
	{
		for (;;)
		{
			if (strcmp (Data.cFileName, ".") != 0 && strcmp (Data.cFileName, "..") != 0)
			{
				char SubPath[MAX_PATH] = "";
				if ((Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
				{
					snprintf (SubPath, MAX_PATH, "%s/%s", Path, Data.cFileName);
					assert (DeleteDir (SubPath));
				}
				else
				{
					snprintf (SubPath, MAX_PATH, "%s/%s", GamePath, Data.cFileName);
					assert (DeleteFileA (SubPath));
				}
			}
			
			if (!FindNextFile (Handle, &Data))
			{
				int Error = GetLastError();
				if (Error == ERROR_NO_MORE_FILES)
					break;
				error ("Delete directory error (FindNextFileA (handle, &data) == false)");
			}
		}
	}
	FindClose (Handle);

	assert (RemoveDirectory (GamePath));

	return true;
}

bool CFileSystem::DeleteFile (const char* Path)
{
	char GamePath[MAX_PATH] = "";
	ToGameFolderPath (Path, GamePath);

	return DeleteFileA(GamePath) != 0;
}

char CFileSystem::RenameDir (const char* Src, const char* Dest)
{
	char GameSrcPath [MAX_PATH] = "";
	char GameDestPath[MAX_PATH] = "";
	if (!MoveFile (ToGameFolderPath (Src, GameSrcPath), ToGameFolderPath (Dest, GameDestPath)))
	{
		int Error = GetLastError();
		if (Error == ERROR_ALREADY_EXISTS)
			return FILESYSTEM_RENAMEDIR_EXISTS;
		else if (Error == ERROR_FILE_NOT_FOUND)
			return FILESYSTEM_RENAMEDIR_NOTFOUND;
		else
			error ("Rename directory error");
	}
	return FILESYSTEM_RENAMEDIR_SUCCESS;
}

char* CFileSystem::ToGameFolderPath (const char* Path, char* GamePath)
{
	snprintf (GamePath, MAX_PATH, "Files/%s", Path);
	return GamePath;
}
