﻿#include "CRWLock.h"
#include <Engine.h>

CRWLock::CRWLock()
{
	assert(!pthread_rwlock_init(&RWLock_, NULL));
}

CRWLock::~CRWLock()
{
	assert(!pthread_rwlock_destroy(&RWLock_));
}

void CRWLock::LockRead()
{
	assert(!pthread_rwlock_rdlock(&RWLock_));
}

void CRWLock::LockWrite()
{
	assert(!pthread_rwlock_wrlock(&RWLock_));
}

bool CRWLock::TryLockRead()
{
	int Error = pthread_rwlock_tryrdlock(&RWLock_);
	assert(!Error || Error == EBUSY);
	return !Error;
}


bool CRWLock::TryLockWrite()
{
	int Error = pthread_rwlock_trywrlock(&RWLock_);
	assert(!Error || Error == EBUSY);
	return !Error;
}

void CRWLock::Unlock()
{
	assert(!pthread_rwlock_unlock(&RWLock_));
}

