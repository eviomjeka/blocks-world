﻿#include "../../../CApplication.h"

int main (int argc, char** argv)
{

	bool IsGame = (argc < 2 || strcmp (argv[1], "--server") != 0);
	int Port = 0;
	if (!IsGame)
	{
		if (argc != 3 || sscanf (argv[2], "%d", &Port) != 1)
			Port = CApplicationModule::STANDART_PORT;
	}
	
	CApplication Application;
	Application.Init (IsGame, Port);
	Application.MainLoop();

	return 0;
}
