﻿#ifndef PLATFORMMISC
#define PLATFORMMISC

#include <PlatformInclude.h>
#include "../../PlatformBase.h"

#define MAX_PATH 260 //TODO: redefine later

#define static_assert(COND,MSG) typedef char static_assertion[(COND)?1:-1]

void LaunchThread (TThreadFunction ThreadFunction, void* Param);
void SetWarningColor();
void SetNormalColor();

void PrintLog (const char* Data);

void Sleep (int MSec);
int TimeMS();

#define closesocket close
#define SocketError() errno
typedef int Socket;

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#endif
