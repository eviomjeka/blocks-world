﻿#ifndef PROGRAMERROR
#define PROGRAMERROR

#include <PlatformMisc.h>
#include "../../PlatformBase.h"
#include "../../../Engine/Utilities/CLog.h"

#define PROGRAM_ERROR_STR_SIZE 4096

#if ASSERTS == ASSERTS_ALL || ASSERTS == ASSERTS_NORMAL

#define assert(OK)													\
{																	\
	if (!(OK))														\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "Assertation failed: " #OK "\n"						\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "errno: %d (%s)\n"										\
			 "====================================\n", 				\
			 __FILE__, __PRETTY_FUNCTION__, __LINE__,				\
			 errno, strerror (errno));								\
		SetNormalColor();											\
																	\
		/*asm("int $3");*/											\
		abort();													\
	}																\
}

#define glassert()													\
{																	\
	int OpenGLError = glGetError();									\
	if (OpenGLError)												\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "OpenGL Error: %d\n"									\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "====================================\n",				\
			 OpenGLError,  __FILE__,								\
			__PRETTY_FUNCTION__, __LINE__);							\
		SetNormalColor();											\
																	\
		/*asm("int $3");*/											\
		abort();													\
	}																\
}

#define alassert()													\
{																	\
	int OpenALError = alGetError();									\
	if (OpenALError != AL_NO_ERROR)									\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "OpenAL error: %d\n"									\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "Description:%s\n"										\
			 "====================================\n",				\
			 OpenALError,  __FILE__, __PRETTY_FUNCTION__,			\
			 __LINE__, alGetString (OpenALError));					\
		SetNormalColor();											\
																	\
		/*asm("int $3");*/											\
		abort();													\
	}																\
	int ALCError = alcGetError (COpenAL::Device());					\
	if (ALCError != ALC_NO_ERROR)									\
	{																\
		SetWarningColor();											\
		LOG ("Error!\n====================================\n"		\
			 "ALC error: %d\n"										\
			 "File: %s\n"											\
			 "Function: %s\n"										\
			 "Line: %d\n"											\
			 "Description:%s\n"										\
			 "====================================\n",				\
			 ALCError,  __FILE__, __PRETTY_FUNCTION__,				\
			 __LINE__, alcGetString (COpenAL::Device(), ALCError));	\
		SetNormalColor();											\
																	\
		/*asm("int $3");*/											\
		abort();													\
	}																\
}


#define error(Text, ...)											\
{																	\
	char __Str[PROGRAM_ERROR_STR_SIZE] = "";						\
	sprintf (__Str, Text, ##__VA_ARGS__);							\
																	\
	SetWarningColor();												\
	LOG ("Error!\n====================================\n"			\
		 "Error: %s\n"												\
		 "File: %s\n"												\
		 "Function: %s\n"											\
		 "Line: %d\n"												\
		 "errno: %d (%s)\n"											\
		 "====================================\n", 					\
		 __Str, __FILE__, __PRETTY_FUNCTION__, __LINE__, 			\
		 errno, strerror (errno));									\
	SetNormalColor();												\
																	\
	/*asm("int $3");*/												\
	abort();														\
}


#if ASSERTS == ASSERTS_ALL
#define __assert(OK) assert(OK)
#else
#define __assert(OK) (OK)
#endif

#else

#define assert(OK) (OK)
#define __assert(OK) (OK)
#define glassert()
#define alassert()
#define error(Text, ...)

#endif

#endif
