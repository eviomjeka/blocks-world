﻿#ifndef CCOND
#define CCOND

#include <PlatformMisc.h>
#include <CMutex.h>
#include "../../PlatformBase.h"

class CCond
{

private:
	CMutex			Mutex_;
	pthread_cond_t	Cond_;

public:
	CCond();
	~CCond();

	void Wait();
	void Broadcast();
};


#endif