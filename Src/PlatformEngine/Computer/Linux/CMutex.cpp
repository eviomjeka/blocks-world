﻿#include "CMutex.h"
#include <Engine.h>

CMutex::CMutex()
{
	pthread_mutex_init(&Mutex_, NULL);
	//pthread_mutex_t Initializer = PTHREAD_MUTEX_INITIALIZER; /*any other way? xD*/
	//Mutex_ = Initializer;
}

void CMutex::Lock()
{
 	assert(!pthread_mutex_lock(&Mutex_));
}

void CMutex::Unlock()
{
 	assert(!pthread_mutex_unlock(&Mutex_));
}

bool CMutex::TryLock()
{
 	int ret = pthread_mutex_trylock(&Mutex_);
	if (!ret) return true;
	assert(errno == EBUSY);
	return false;
}

CMutex::~CMutex()
{
 	assert(!pthread_mutex_destroy(&Mutex_));
}
