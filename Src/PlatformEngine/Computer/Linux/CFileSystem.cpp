﻿#include "CFileSystem.h"
#include <Engine.h>

char CFileSystem::GameFolder_[MAX_PATH] = {};

void CFileSystem::FileList(CArray<CFileSystemElement>* List, const char* Path)
{
	char GamePath[MAX_PATH] = "";
	dirent* Entry = NULL;
	DIR* Directory = opendir(ToGameFolderPath (Path, GamePath));

	assert(Directory);

	while ((Entry = readdir(Directory)))
	{
		if (strcmp(Entry->d_name, ".") && strcmp(Entry->d_name, ".."))
		{
			CFileSystemElement FileSystemElement = {};
			FileSystemElement.Name = Entry->d_name;
			FileSystemElement.Directory = Entry->d_type == DT_DIR;
			List->Insert(FileSystemElement, true);
		}
	}

	closedir(Directory);
}

bool CFileSystem::CreateDir(const char* Path)
{
	struct stat	Stat; // Type name conflicts with function

	char GamePath[MAX_PATH] = "";
	ToGameFolderPath (Path, GamePath);
	if (stat(GamePath, &Stat))
	{
		assert(!mkdir(GamePath, 0777));
		return true;
	}
	else
	{
		assert(S_ISDIR(Stat.st_mode));
		return false;
	}
}

bool CFileSystem::DeleteDir(const char* Path)
{
	dirent* Entry = NULL;
	char EntryPath[MAX_PATH] = "";
	char GamePath[MAX_PATH] = "";
	ToGameFolderPath (Path, GamePath);
	DIR* Directory = opendir(GamePath);

	if (!Directory)
	{
		assert(errno == ENOENT);
		return false;
	}

	while ((Entry = readdir(Directory)))
	{
		if (strcmp(Entry->d_name, ".") && strcmp(Entry->d_name, ".."))
		{
			snprintf(EntryPath, (size_t) MAX_PATH, "%s/%s", Path, Entry->d_name);

			if (Entry->d_type == DT_DIR)
				assert(DeleteDir(EntryPath))
			else
				assert(DeleteFile(EntryPath))

		}
	}

	closedir(Directory);
	assert(!remove(GamePath));

	return true;
}

bool CFileSystem::DeleteFile(const char* Path)
{
	char CorrectPath[MAX_PATH] = "";
	ToGameFolderPath(Path, CorrectPath);

	return remove(CorrectPath) == 0;
}

char CFileSystem::RenameDir(const char* Src, const char* Dest)
{
	char GameSrcPath[MAX_PATH] = "";
	char GameDestPath[MAX_PATH] = "";

	if (rename(ToGameFolderPath (Src, GameSrcPath), ToGameFolderPath (Dest, GameDestPath)))
	{
		if (errno == ENOENT)
			return FILESYSTEM_RENAMEDIR_NOTFOUND;
		else if (errno == EEXIST)
			return FILESYSTEM_RENAMEDIR_EXISTS;
		else
			error("Not same device or other");
	}

	return FILESYSTEM_RENAMEDIR_SUCCESS;
}

char* CFileSystem::ToGameFolderPath (const char* Path, char* GamePath)
{
	if (!*GameFolder_)
	{
		const char* HomeDirectory = getenv("HOME");
		assert(HomeDirectory);
		snprintf(GameFolder_, (size_t) MAX_PATH, "%s/BlocksWorld/Files", HomeDirectory);
		LOG("BlocksWorld files path: %s", GameFolder_);
	}

	snprintf(GamePath, (size_t) MAX_PATH, "%s/%s", GameFolder_, Path);
	return GamePath;
}
