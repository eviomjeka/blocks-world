﻿#ifndef CRWLOCK
#define CRWLOCK

#include <PlatformMisc.h>
#include "../../PlatformBase.h"

class CRWLock
{

private:
	pthread_rwlock_t RWLock_;

public:
	CRWLock();
	~CRWLock();

	void LockRead();
	void LockWrite();
	bool TryLockRead();
	bool TryLockWrite();
	void Unlock();
};


#endif