﻿#include "CCond.h"
#include <Engine.h>

CCond::CCond()
{
	assert(!pthread_cond_init(&Cond_, NULL));
}

CCond::~CCond()
{
	assert(!pthread_cond_destroy(&Cond_));
}

void CCond::Wait()
{
	Mutex_.Lock();
	assert(!pthread_cond_wait(&Cond_, &Mutex_.Mutex_));
	Mutex_.Unlock();
}

void CCond::Broadcast()
{
	assert(!pthread_cond_broadcast(&Cond_));
}

