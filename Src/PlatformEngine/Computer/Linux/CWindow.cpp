﻿#include "CWindow.h"
#include <Engine.h>

CWindow* __CWindow;

int CWindow::KeyTable_[256] = {};

CWindow::CWindow() :
	Display_(NULL),
	Screen_(0),
	Window_(0),
	Context_(0),
	EmptyCursor_(0),
	DeleteWindowAtom_(0),
	Position_(-1, -1),
	Size_(-1, -1),
	Fullscreen_(false),
	Closed_(true),
	HideCursor_(false),
	RequiredAttributes_(256),
	DesiredAttributes_(256)
{}

CWindow::~CWindow()
{}

bool CWindow::CreateWindow(bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize)
{
	Fullscreen_	= Fullscreen;
	Closed_		= false;
	HideCursor_	= false;

	Size_		= WindowSize;
	Position_	= SIVector2D (0, 0);

	Display_ = XOpenDisplay(NULL);
	assert(Display_);

	InitKeyTable_();

	assert(glXQueryExtension (Display_, NULL, NULL));
	Screen_ = DefaultScreen(Display_);

	GLXFBConfig FBConfig;
	bool Desired = GetFBConfig_(FBConfig);

	XVisualInfo* Visual = glXGetVisualFromFBConfig(Display_, FBConfig);
	assert (Visual);


	XSetWindowAttributes WinAttr;
	WinAttr.colormap 			= XCreateColormap (Display_,
									   			   RootWindow (Display_, Visual->screen),
									   			   Visual->visual, AllocNone);
	WinAttr.border_pixel		= 0;
	WinAttr.background_pixel	= 0;
	WinAttr.event_mask 			= KeyPressMask | KeyReleaseMask | ExposureMask | FocusChangeMask |
		StructureNotifyMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask;

	Window_ = XCreateWindow (Display_, RootWindow (Display_, Visual->screen),
							 Position_.x, Position_.y, Size_.x, Size_.y,
							 0, Visual->depth, InputOutput, Visual->visual,
							 CWBorderPixel | CWColormap | CWEventMask, &WinAttr);

	assert(Window_);

	XStoreName(Display_, Window_, APPLICATION_NAME);

	Context_ = glXCreateNewContext(Display_, FBConfig, GLX_RGBA_TYPE, None, True);
	assert (Context_);
	glXMakeCurrent (Display_, Window_, Context_);

	EmptyCursor_ = GetEmptyCursor_();
	DeleteWindowAtom_ = XInternAtom(Display_, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(Display_, Window_, &DeleteWindowAtom_, 1);

	XMapWindow (Display_, Window_);
	FullscreenMode(Fullscreen);

	GLenum ErrorCode = glewInit();
	if (ErrorCode != GLEW_OK)
		error((const char*) glewGetErrorString(ErrorCode));

	assert (glewIsSupported ("GL_VERSION_2_0"));
	glassert();
	SetIcon();

	return Desired;
}

void CWindow::MainLoop()
{
	timeval OldTime;
	gettimeofday(&OldTime, NULL);
	Resize();
	
	glXMakeCurrent (Display_, Window_, Context_);

	while (!Closed_)
	{
		timeval NewTime;
		gettimeofday(&NewTime, NULL);
		int DeltaTime = ((NewTime.tv_sec  - OldTime.tv_sec) * 1000 +
						 (NewTime.tv_usec - OldTime.tv_usec) / 1000);

		Proc(DeltaTime);
		OldTime = NewTime;
		//XFlush(Display_);
		glXSwapBuffers (Display_, Window_);
		HandleEvents_();
	}
}

//==============================================================================

SIVector2D CWindow::GetWindowSize()
{
	return Size_;
}

void CWindow::SetCursorPosition(SIVector2D Position)
{
	XWarpPointer(Display_, Window_, Window_, Position_.x, Position_.y,
				 Size_.x, Size_.y, Position.x, Position.y);
}

void CWindow::ShowCursor(bool Show)
{
	if (Show && HideCursor_)
	{
		HideCursor_ = false;
		XUndefineCursor(Display_, Window_);
	}
	else if (!Show && !HideCursor_)
	{
		HideCursor_ = true;
		XDefineCursor (Display_, Window_, EmptyCursor_);
	}
}

bool CWindow::IsFullScreen()
{
	return Fullscreen_;
}

void CWindow::FullscreenMode(bool Fullscreen)
{	
	if (Fullscreen == Fullscreen_)
		return;
	
	Fullscreen_ = Fullscreen;

	XWindowAttributes WindowAttributes;
	XGetWindowAttributes(Display_, Window_, &WindowAttributes);

	XEvent Event = {};
	Event.xclient.type = ClientMessage;
	Event.xclient.message_type = XInternAtom(Display_,
			"_NET_WM_STATE", false);
	Event.xclient.display = Display_;
	Event.xclient.window = Window_;
	Event.xclient.format = 32;
	Event.xclient.data.l[0] = 2;
	Event.xclient.data.l[1] = XInternAtom(Display_,
			"_NET_WM_STATE_FULLSCREEN", false );

	XSendEvent(Display_, WindowAttributes.root,
		false, SubstructureNotifyMask | SubstructureRedirectMask, &Event);
}

void CWindow::DestroyWindow()
{
	XEvent CloseEvent;
	memset(&CloseEvent, 0, sizeof CloseEvent);

	CloseEvent.xclient.type			= ClientMessage;
	CloseEvent.xclient.send_event	= True;
	//CloseEvent.xclient.display		= Display_;
	CloseEvent.xclient.window		= Window_;
	CloseEvent.xclient.message_type	= XInternAtom(Display_, "WM_PROTOCOLS", true);;
	CloseEvent.xclient.format		= 32;
	CloseEvent.xclient.data.l[0]	= DeleteWindowAtom_;
	//CloseEvent.xclient.data.l[1] = CurrentTime;

	XSendEvent(Display_, Window_, False, NoEventMask, &CloseEvent);
}

//==============================================================================

Cursor CWindow::GetEmptyCursor_()
{
	char EmptyCursorBits[32] = "";
	XColor DontCare;
	Cursor EmptyCursor;
	Pixmap EmptyCursorPixmap;

	memset(EmptyCursorBits, 0, sizeof EmptyCursorBits);
	memset(&DontCare, 0, sizeof DontCare);
	EmptyCursorPixmap = XCreateBitmapFromData(Display_,
		RootWindow(Display_, Screen_), EmptyCursorBits, 16, 16 );

	EmptyCursor = XCreatePixmapCursor(Display_, EmptyCursorPixmap,
			EmptyCursorPixmap,  &DontCare, &DontCare, 0, 0);
	XFreePixmap(Display_, EmptyCursorPixmap );

	return EmptyCursor;
}

void CWindow::Destroy_()
{
	Destroy();
	glXMakeCurrent(Display_, None, None);
	glXDestroyContext(Display_, Context_);
	XDestroyWindow(Display_, Window_);
	XCloseDisplay(Display_);
	Closed_ = true;
}

void CWindow::HandleEvents_()
{
	XEvent Event;
	while (!Closed_ && XPending(Display_))
	{
		XNextEvent(Display_, &Event);

		switch (Event.type)
		{
			case ConfigureNotify:
				Size_ = SIVector2D(Event.xconfigure.width, Event.xconfigure.height);
				Resize();
			break;

			case ButtonPress:
				switch (Event.xbutton.button)
				{
					case Button1:
						LeftButtonDown();
					break;

					case Button3:
						RightButtonDown();
					break;

					default:
					break;
				}
			break;

			case ButtonRelease:
				switch (Event.xbutton.button)
				{
					case Button1:
						LeftButtonUp();
					break;

					case Button3:
						RightButtonUp();
					break;

					case Button4:
						MouseWhell(-1);
					break;

					case Button5:
						MouseWhell(1);
					break;

					default:
					break;
				}
			break;

			case KeyPress:
			{
				//wchar_t Str[1];
				//Status ReturnStatus;
				int StrLength = 0/*XwcLookupString
				(input_context_, &(xevent.xkey), Str, 10, NULL, &ReturnStatus)*/;
				if (!StrLength)
				{
					int KeyCode = Event.xkey.keycode;
					assert(KeyCode >= 0 && KeyCode < 256);
					KeyDown(KeyTable_[KeyCode]);
				}
				else
				{
					//assert(StrLength == 1);
					//Character(*Str);
				}
			}
			break;

			case KeyRelease:
			{
				int KeyCode = Event.xkey.keycode;
				assert(KeyCode >= 0 && KeyCode < 256);
				KeyUp(KeyTable_[KeyCode]);
			}
			break;

			case MotionNotify:
				 MouseMotion (SIVector2D(Event.xmotion.x, Event.xmotion.y));
			break;

			case FocusIn:
				break;

			case FocusOut:
				Unfocus();
				break;

			case ClientMessage:
				if ((Atom) Event.xclient.data.l[0] == DeleteWindowAtom_)
					Destroy_();
			break;
		}
	}
}

void CWindow::InitKeyTable_()
{
#define DECL_KEY(APPNAME, XNAME) (int) APPNAME
	int KeySyms[] =
		{
			#include <Keys.h>
		};
#undef DECL_KEY

	for (int i = 0; i < sizeof KeySyms / sizeof(int); i++)
	{
		int KeyCode = XKeysymToKeycode(Display_, KeySyms[i]);

		if (KeyCode)
			KeyTable_[KeyCode] = KeySyms[i];
	}
}

void CWindow::SetIcon(const char* PngIcon)
{
	//32bit an 64bit magic detected!!!
	byte* _Data = NULL;
	SIVector2D IconSize = CPngImage::Instance.LoadFromFile(PngIcon, NULL, &_Data);
	unsigned long* Data = new unsigned long[IconSize.x * IconSize.y + 2];
	Data[0] = IconSize.x;
	Data[1] = IconSize.y;


	//shit just got serious
	for (int x = 0; x < IconSize.x; x++)
		for (int y = 0; y < IconSize.y; y++)
		{
			byte* Pixel = (byte*) &Data[(IconSize.y - y - 1) * IconSize.x + x + 2];

			int i = y * IconSize.x + x;
			Pixel[0] = _Data[i * 4 + 2];
			Pixel[1] = _Data[i * 4 + 1];
			Pixel[2] = _Data[i * 4 + 0];
			Pixel[3] = _Data[i * 4 + 3];
		}

    Atom _NET_WM_ICON	= XInternAtom(Display_, "_NET_WM_ICON", False);
    Atom CARDINAL 		= XInternAtom(Display_, "CARDINAL", False);

    XChangeProperty(Display_, Window_, _NET_WM_ICON, CARDINAL, 32, PropModeReplace, (byte*) Data, IconSize.x * IconSize.y + 2);

    CPngImage::Instance.DeleteData();
    delete[] Data;
}

bool CWindow::GetFBConfig_(GLXFBConfig& FBConfig)
{
	RequiredAttributes_.Insert(None, true);
	DesiredAttributes_.Append(RequiredAttributes_, true);

	int NFBConfigs = 0;
	GLXFBConfig* FBConfigs = glXChooseFBConfig(Display_, Screen_, DesiredAttributes_.Data(), &NFBConfigs);
	bool Desired = (bool) FBConfigs;

	if (!FBConfigs)
	{
		WARNING ("unable to get requested OpenGL context. Trying simpler one.");
		WARNING ("it seems your machine does not support MSAA or whatever. Check your settings.");
		FBConfigs = glXChooseFBConfig(Display_, Screen_, RequiredAttributes_.Data(), &NFBConfigs);
	}

	assert(FBConfigs);
	FBConfig = FBConfigs[0];

	XFree(FBConfigs);

	return Desired;
}

void CWindow::SetRGBA()
{
	RequiredAttributes_.Insert(GLX_RENDER_TYPE, true);
	RequiredAttributes_.Insert(GL_TRUE,         true);
}

void CWindow::SetMinimumColorSize(int R, int G, int B, int A)
{
	RequiredAttributes_.Insert(GLX_RED_SIZE,   true);
	RequiredAttributes_.Insert(R, true);
	RequiredAttributes_.Insert(GLX_GREEN_SIZE, true);
	RequiredAttributes_.Insert(G, true);
	RequiredAttributes_.Insert(GLX_BLUE_SIZE,  true);
	RequiredAttributes_.Insert(B, true);
	RequiredAttributes_.Insert(GLX_ALPHA_SIZE, true);
	RequiredAttributes_.Insert(A, true);
}

void CWindow::SetDoubleBuffer()
{
	RequiredAttributes_.Insert(GLX_DOUBLEBUFFER, true);
	RequiredAttributes_.Insert(GL_TRUE,          true);
}

void CWindow::SetMinimumDepthSize(int D)
{
	RequiredAttributes_.Insert(GLX_DEPTH_SIZE, true);
	RequiredAttributes_.Insert(D,              true);
}

void CWindow::RequestMSAA(int MinimumBuffers)
{
	DesiredAttributes_.Insert(GLX_SAMPLE_BUFFERS, true);
	DesiredAttributes_.Insert(GL_TRUE,            true);
	DesiredAttributes_.Insert(GLX_SAMPLES,        true);
	DesiredAttributes_.Insert(MinimumBuffers,     true);
}
