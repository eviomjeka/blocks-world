﻿#ifndef CWINDOW
#define CWINDOW

#include <PlatformMisc.h>
#include "../../PlatformBase.h"
#include "../../../Engine/Utilities/SVector2D.h"
#include "../../../Engine/Containers/CArray.h"
#include "../../../Engine/Graphics/CPngImage.h"


#define DECL_KEY(APPNAME, XNAME) APPNAME = XNAME
enum Keys
{
	#include <Keys.h>
};
#undef DECL_KEY


class CWindow :
	public CWindowInterface
{
public:
	CWindow();
	virtual ~CWindow();

	bool CreateWindow (bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize);
	void MainLoop();

	SIVector2D GetWindowSize();
	void SetCursorPosition (SIVector2D Position);
	void ShowCursor (bool Show);
	bool IsFullScreen();
	void FullscreenMode (bool Fullscreen);
	void DestroyWindow();
	void SetIcon(const char* PngIcon = "Icon.png");

	void SetRGBA();
	void SetMinimumColorSize(int R, int G, int B, int A);
	void SetDoubleBuffer();
	void SetMinimumDepthSize(int D);

	void RequestMSAA(int MinimumBuffers);

private:
	CArray<int>	RequiredAttributes_;
	CArray<int>	DesiredAttributes_;

	Display*	Display_;
	unsigned	Screen_;
	Window	  	Window_;
	GLXContext	Context_;
	Cursor		EmptyCursor_;
	Atom		DeleteWindowAtom_;

	SIVector2D Position_;
	SIVector2D Size_;
	bool Fullscreen_;
	bool Closed_;
	bool HideCursor_;
	
	static int KeyTable_[256];

	Cursor GetEmptyCursor_();
	void Destroy_();
	void HandleEvents_();
	void InitKeyTable_();
	bool GetFBConfig_(GLXFBConfig& FBConfig);


};

#endif
