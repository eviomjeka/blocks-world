﻿#ifndef KEYS
#define KEYS

#include <PlatformInclude.h>
#include "../../PlatformBase.h"

#define ANSI(__letter) KEY_##__letter = kVK_ANSI_##__letter

enum Keys
{
	KEY_UP				= kVK_UpArrow,
	KEY_DOWN			= kVK_DownArrow,
	KEY_LEFT			= kVK_LeftArrow,
	KEY_RIGHT			= kVK_RightArrow,
	KEY_ENTER			= kVK_Return,
	KEY_BACKSPACE		= 0xbad0, //TODO: !!!
	KEY_SPACE			= kVK_Space,
	KET_CONTROL			= kVK_Control,
	KEY_SHIFT			= kVK_Shift,
	KEY_ALT				= kVK_Option,
	KEY_CAPS_LOOK		= kVK_CapsLock,
	KEY_TAB				= kVK_Tab,
	KEY_ESCAPE			= kVK_Escape,
	KEY_HOME			= kVK_Home,
	KEY_END				= kVK_End,
	KEY_DELETE			= kVK_Delete,
	KEY_PAGE_UP			= kVK_PageUp,
	KEY_PAGE_DOWN		= kVK_PageDown,
	KEY_MINUS			= kVK_ANSI_Minus,
	KEY_PLUS			= kVK_ANSI_Equal,
	KEY_LEFT_BRACKET	= kVK_ANSI_LeftBracket,
	KEY_RIGHT_BRACKET	= kVK_ANSI_RightBracket,
	KEY_COLON			= 0xbad1, // TODO: !!!
	KEY_QUOTE			= kVK_ANSI_Quote,
	KEY_BACKSLASH		= kVK_ANSI_Backslash,
	KEY_PERIOD			= kVK_ANSI_Period,
	KEY_COMMA			= kVK_ANSI_Comma,
	KEY_SLASH			= kVK_ANSI_Slash,
	KEY_NUMADD			= kVK_ANSI_KeypadPlus,
	KEY_NUMSUBTRACT		= kVK_ANSI_KeypadMinus,
	KEY_NUMMULTIPLY		= kVK_ANSI_KeypadMultiply,
	KEY_NUMDIVIDE		= kVK_ANSI_KeypadDivide,
	KEY_NUMDECIMAL		= kVK_ANSI_KeypadDecimal,
	KEY_INSERT			= 0xbad2, // TODO: !!!
	KEY_PAUSE_BREAK		= 0xbad3, // TODO: !!!
	KEY_PRINTSCREEN		= 0xbad4, // TODO: !!!
	KEY_TILDE			= kVK_ANSI_Grave,

	KEY_F1 = kVK_F1, KEY_F2  = kVK_F2,  KEY_F3  = kVK_F3,  KEY_F4  = kVK_F4,
	KEY_F5 = kVK_F5, KEY_F6  = kVK_F6,  KEY_F7  = kVK_F7,  KEY_F8  = kVK_F8, 
	KEY_F9 = kVK_F9, KEY_F10 = kVK_F10, KEY_F11 = kVK_F11, KEY_F12 = kVK_F12,

	KEY_NUM0 = kVK_ANSI_Keypad0,  KEY_NUM1, KEY_NUM2, KEY_NUM3,
	KEY_NUM4, KEY_NUM5, KEY_NUM6, KEY_NUM7, KEY_NUM8, KEY_NUM9,

	ANSI(A), ANSI(B), ANSI(C), ANSI(D), ANSI(E), ANSI(F),
	ANSI(G), ANSI(H), ANSI(I), ANSI(J), ANSI(K), ANSI(L),
	ANSI(M), ANSI(N), ANSI(O), ANSI(P), ANSI(Q), ANSI(R),
	ANSI(S), ANSI(T), ANSI(U), ANSI(V), ANSI(W), ANSI(X), 
	ANSI(Y), ANSI(Z),

	ANSI(0), ANSI(1), ANSI(2), ANSI(3), ANSI(4),
	ANSI(5), ANSI(6), ANSI(7), ANSI(8), ANSI(9)
};

#endif