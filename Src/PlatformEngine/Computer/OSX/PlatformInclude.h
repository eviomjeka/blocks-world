﻿#ifndef PLATFORMINCLUDE
#define PLATFORMINCLUDE

#include <pthread.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>

#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <cstdarg>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include <zlib.h>
#include <png.h>

#endif
