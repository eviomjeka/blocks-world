﻿#include "CWindow.h"
#include <Engine.h>

CWindow::CWindow()
{}

CWindow::~CWindow()
{}

void CWindow::CreateWindow (bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize)
{
	Fullscreen_	= Fullscreen;
	Closed_		= false;
	HideCursor_	= false;

	Size_		= WindowSize;
	Position_	= SIVector2D (0, 0);

	GLenum ErrorCode = glewInit();
	if (ErrorCode != GLEW_OK)
		error((const char*) glewGetErrorString(ErrorCode));
	assert (glewIsSupported ("GL_VERSION_2_0"));
	assert (glewIsSupported ("GL_ARB_vertex_buffer_object")); // TODO: remove? + add more checks
	glassert();
}

void CWindow::MainLoop()
{
}

//==============================================================================

SIVector2D CWindow::GetWindowSize()
{
}

void CWindow::SetCursorPosition (SIVector2D Position)
{
}

void CWindow::ShowMouseCursor (bool Show)
{
	if (Show && HideCursor_)
	{
		HideCursor_ = false;
	}
	else if (!Show && !HideCursor_)
	{
		HideCursor_ = true;
	}
}

bool CWindow::IsFullScreen()
{
}

void CWindow::FullscreenMode (bool Fullscreen)
{
}

void CWindow::DestroyWindow()
{
}

OSStatus CWindow::_KeyboardEventHandler_(EventHandlerCallRef NextHandler, EventRef Event, void*)
{
}

OSErr CWindow::_QuitEventHandler_(const AppleEvent* appleEvt, AppleEvent *reply, SInt32 refcon)
{
}