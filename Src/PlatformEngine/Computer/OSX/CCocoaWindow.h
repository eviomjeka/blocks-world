﻿#import "../../../CApplication.h"
#import <Cocoa/Cocoa.h>

@interface CCocoaWindow : NSOpenGLView<NSWindowDelegate>
{
	CApplication Application;
}

@end
