﻿//#import "../CApplication.h"
#import "CCocoaWindow.h"

//#define Application (*APPLICATION)


@implementation CCocoaWindow

//static NSTimer *timer = nil;

- (void)prepareOpenGL
{
	Application.Init(true);
	
	// this sets swap interval for double buffering
	GLint swapInt = 1;
	[[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	
	// this enables alpha in the frame buffer (commented now)
	//  GLint opaque = 0;
	//  [[self openGLContext] setValues:&opaque forParameter:NSOpenGLCPSurfaceOpacity];
}

- (void)drawRect:(NSRect) DirtyRect
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	
	Application.Proc(10); //Todo: Pass time
	
	[[self openGLContext] flushBuffer];
	
	
	//glassert();
	
	[self setNeedsDisplay:YES];
}

- (void)reshape
{
	float Width  = [self frame].size.width;
	float Height = [self frame].size.height;
	
	Application.Resize(SIVector2D((int) Width, (int) Height));
}

- (BOOL) acceptsFirstResponder
{
	return YES;
}

- (void) keyDown:(NSEvent*) Event
{
	if ([Event isARepeat])
		return;
	
   // NSString *str = [theEvent charactersIgnoringModifiers];
	//unichar c = [str characterAtIndex:0];
	
	Application.KeyDown([Event keyCode]);
}

- (void) keyUp:(NSEvent*) Event
{
	Application.KeyUp([Event keyCode]);
}

- (void) windowDidResignMain:(NSNotification*) Notification
{
	//[timer invalidate];
	
	Application.Unfocus();
	[self setNeedsDisplay:YES]; //?
}

- (void) windowDidBecomeMain:(NSNotification*) Notification
{
	//Application.Focus();
	
	[self setNeedsDisplay:YES]; //?
	
	/*
	timer = [NSTimer timerWithTimeInterval:FRAME_INTERVAL
					 target:self
					 selector:@selector(timerEvent:)
					 userInfo:nil
					 repeats:YES];
	
	[[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
	*/
}

/*
- (void)timerEvent:(NSTimer*) Time
{
	Window_->Update(10);
	[self setNeedsDisplay:YES];
}
*/
 
@end
