﻿#ifndef CWINDOW
#define CWINDOW

#include <PlatformMisc.h>
#include "../../PlatformBase.h"
#include "../../../Engine/Utilities/SVector2D.h"
#include "../../../Engine/Containers/CArray.h"

class CWindow : public CWindowInterface
{
public:

	CWindow();
	virtual ~CWindow();

	void CreateWindow (bool Fullscreen, SIVector2D WindowSize, SIVector2D MinWindowSize);
	void MainLoop();

	SIVector2D GetWindowSize();
	void SetCursorPosition (SIVector2D Position);
	void ShowCursor (bool Show);
	bool IsFullScreen();
	void FullscreenMode (bool Fullscreen);
	void DestroyWindow();

private:

	SIVector2D Position_;
	SIVector2D Size_;
	bool Fullscreen_;
	bool Closed_;
	bool HideCursor_;

	static OSStatus _KeyboardEventHandler_(EventHandlerCallRef NextHandler, EventRef Event, void*);
	static OSErr _QuitEventHandler_(const AppleEvent* appleEvt, AppleEvent *reply, SInt32 refcon);

};

#endif