﻿#ifndef PLATFORMBASE
#define PLATFORMBASE

#include "../Engine/Utilities/SVector2D.h"
#include "../Engine/Utilities/CString.h"

typedef void (TThreadFunction) (void*);

enum FileSystem_RenameDir
{
	FILESYSTEM_RENAMEDIR_SUCCESS, 
	FILESYSTEM_RENAMEDIR_NOTFOUND, 
	FILESYSTEM_RENAMEDIR_EXISTS
};

struct CFileSystemElement
{
	CString Name;
	bool Directory;
};

class CWindowInterface
{
public:
	virtual void Init (bool IsGame, int Port) = 0;
	virtual void Proc (int Time) = 0;
	virtual void KeyDown (unsigned Key) = 0;
	virtual void KeyUp (unsigned Key) = 0;
	virtual void LeftButtonDown() = 0;
	virtual void LeftButtonUp() = 0;
	virtual void RightButtonDown() = 0;
	virtual void RightButtonUp() = 0;
	virtual void MouseWhell (int Delta) = 0;
	virtual void MouseMotion (SIVector2D Position) = 0;
	virtual void Resize() = 0;
	virtual void Unfocus() = 0;
	virtual void Destroy() = 0;
};

#endif
