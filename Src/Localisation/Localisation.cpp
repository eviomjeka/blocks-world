﻿#include "Localisation.h"
#include "../CApplication.h"

#define LOCALISATIONSTRING(Name) CString str_ ## Name (#Name);
#include "StringsList.h"
#undef LOCALISATIONSTRING

CHashTable<CString, CString> CLocalisation::BlocksLocalisation_;

void CLocalisation::Init (const char* Language)
{
	LoadLocalisationStrings_ (Language);
	LoadLocalisationBlocks_ (Language);
}

void CLocalisation::GetBlockLocalisation (CString& Name, CString& Localisation)
{
	CString NameCopy = Name;
	
	Localisation.Clear();
	char* Str = strtok (Name.Data(), " ()1234567890");
	int Size = 0;

	for (;;)
	{
		CString NamePart (Str);
		CString* WordLocalisation = BlocksLocalisation_.Data (NamePart);

		if (!WordLocalisation)
		{
			CString OriginalName;
			OriginalName.Print ("[%s]", NamePart.Data());
			Localisation.Append (OriginalName);

			//WARNING ("Block \"%s\" name's part \"%s\" localization not found", NameCopy.Data(), Str);
		}
		else
		{
			Localisation.Append (*WordLocalisation);
		}
		
		char* NewStr = strtok (NULL, " ()1234567890");
		
		Size = strlen (Str);
		if (NewStr)
			for (int i = Size; i < (int) (NewStr - Name.Data()) && i < NameCopy.Length(); i++)
				Localisation += NameCopy[i];
		
		Size = (NewStr ? ((byte*) (NewStr - Name.Data()) + strlen (NewStr)) : (Size + (byte*) (Str - Name.Data()))) - (byte*) NULL;

		if (!NewStr)
			break;

		Str = NewStr;
	}
	
	for (int i = Size; i < Name.Length(); i++)
		Localisation += NameCopy[i];
}

void CLocalisation::LoadLocalisationStrings_ (const char* Language)
{
	int Line = 1;
	char FileName[MAX_PATH] = "";
	sprintf (FileName, "Localisation/%s/Strings.bwl", Language);
	CFile File (FileName, CFILE_READ);
	
#define LOCALISATIONSTRING(Name) LoadLocalisationString_ (File, str_ ## Name, #Name, Line);
#include "StringsList.h"
#undef LOCALISATIONSTRING
	
	File.Scan (" ");
	assert (File.EndOfFile());
	File.Destroy();
}

void CLocalisation::LoadLocalisationString_ (CFile& File, CString& String, const char* StringName, int& Line)
{
	if (File.EndOfFile())
		error ("Localisation (Strings.bwl:%d): end of file but \"%s\" string required", Line, StringName);
	
	bool EmptyString = true;
	char FileString[256] = "";

	while (EmptyString)
	{
		File.GetString (FileString, 256);

		for (int i = 0; FileString[i]; i++)
			if (FileString[i] != ' ' && FileString[i] != '\t' && FileString[i] != '\n')
			{
				EmptyString = false;
				break;
			}
		if (EmptyString)
			Line++;
	}
	
	char Temp[256] = "";
	char Str[256] = "";

	if (sscanf (FileString, " %s %[^\n]", Temp, Str) != 2)
		error ("Localisation (Strings.bwl:%d): String \"%s\" or localisation string required", Line, StringName);
	String = Str;

	if (strcmp (Temp, StringName) != 0)
		error ("Localisation (Strings.bwl:%d): \"%s\" string read but \"%s\" string required", Line, Temp, StringName);

	Line++;
}

void CLocalisation::LoadLocalisationBlocks_ (const char* Language)
{
	BlocksLocalisation_.Init (CString::Hash, BLOCKS_HASH_ROWS);

	int Line = 1;
	char FileName[MAX_PATH] = "";
	sprintf (FileName, "Localisation/%s/Blocks.bwl", Language);
	CFile File (FileName, CFILE_READ);

	while (!File.EndOfFile())
	{
		char FileString[256] = "";
		File.GetString (FileString, 256);

		bool EmptyString = true;
		for (int i = 0; FileString[i]; i++)
			if (FileString[i] != ' ' && FileString[i] != '\t' && FileString[i] != '\n')
			{
				EmptyString = false;
				break;
			}
		if (EmptyString)
		{
			Line++;
			continue;
		}

		char StrBlockName[256] = "";
		char StrBlockLocalisation[256] = "";
		if (sscanf (FileString, "%s %[^\n]", StrBlockName, StrBlockLocalisation) != 2)
			error ("Localisation (Blocks.bwl:%d): incorrect string", Line);
		
		CString BlockName (StrBlockName);
		CString BlockLocalisation (StrBlockLocalisation);
		BlocksLocalisation_ (BlockName) = BlockLocalisation;

		Line++;
	}

	File.Destroy();
}
