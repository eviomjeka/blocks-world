﻿#ifndef LOCALISATION
#define LOCALISATION

#include <Engine.h>

#define LOCALISATIONSTRING(Name) extern CString str_ ## Name;
#include "StringsList.h"
#undef LOCALISATIONSTRING

#define BLOCKS_HASH_ROWS 256

class CLocalisation
{
public:

	static void Init (const char* Language);

	static void GetBlockLocalisation (CString& Name, CString& Localisation);

private:
	
	static void LoadLocalisationStrings_ (const char* Language);
	static void LoadLocalisationString_ (CFile& Localisation, CString& String, const char* StringName, int& Line);
	static void LoadLocalisationBlocks_ (const char* Language);

	static CHashTable<CString, CString> BlocksLocalisation_;

};

const CString str_key_f1  ("F1");
const CString str_key_f2  ("F2");
const CString str_key_f3  ("F3");
const CString str_key_f4  ("F4");
const CString str_key_f5  ("F5");
const CString str_key_f6  ("F6");
const CString str_key_f7  ("F7");
const CString str_key_f8  ("F8");
const CString str_key_f9  ("F9");
const CString str_key_f10 ("F10");
const CString str_key_f11 ("F11");
const CString str_key_f12 ("F12");

const CString str_key_a ("A");
const CString str_key_b ("B");
const CString str_key_c ("C");
const CString str_key_d ("D");
const CString str_key_e ("E");
const CString str_key_f ("F");
const CString str_key_g ("G");
const CString str_key_h ("H");
const CString str_key_i ("I");
const CString str_key_j ("J");
const CString str_key_k ("K");
const CString str_key_l ("L");
const CString str_key_m ("M");
const CString str_key_n ("N");
const CString str_key_o ("O");
const CString str_key_p ("P");
const CString str_key_q ("Q");
const CString str_key_r ("R");
const CString str_key_s ("S");
const CString str_key_t ("T");
const CString str_key_u ("U");
const CString str_key_v ("V");
const CString str_key_w ("W");
const CString str_key_x ("X");
const CString str_key_y ("Y");
const CString str_key_z ("Z");

const CString str_key_0 ("0");
const CString str_key_1 ("1");
const CString str_key_2 ("2");
const CString str_key_3 ("3");
const CString str_key_4 ("4");
const CString str_key_5 ("5");
const CString str_key_6 ("6");
const CString str_key_7 ("7");
const CString str_key_8	("8");
const CString str_key_9 ("9");

const CString str_key_minus			("-");
const CString str_key_plus			("+");
const CString str_key_tilde		 	("~");
const CString str_key_left_bracket	("[");
const CString str_key_right_bracket	("]");
const CString str_key_colon			(",");
const CString str_key_quote			("\"");
const CString str_key_backslash		("\\");
const CString str_key_comma		 	(",");
const CString str_key_slash		 	("/");
const CString str_key_period		(".");

#endif