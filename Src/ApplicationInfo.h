﻿#ifndef APPLICATIONINFO
#define APPLICATIONINFO

#include "Engine/ApplicationInfoInclude.h"

#define APPLICATION_NAME "Blocks World"

#define ASSERTS ASSERTS_ALL

#if defined WINDOWS_CONFIGURATION
	#define DEVICE DEVICE_COMPUTER
	#define OS OS_WINDOWS

#elif defined __unix
	#define DEVICE DEVICE_COMPUTER
	#define OS OS_LINUX

#elif defined XCODE
	#define DEVICE DEVICE_COMPUTER
	#define OS OS_X

#elif defined ANDROID_CONFIGURATION
	#define DEVICE DEVICE_MOBILE
	#define OS OS_ANDROID

#elif defined MOBILE_TEST_CONFIGURATION
	#define DEVICE DEVICE_MOBILE
	#define OS OS_WINDOWS
#endif

#endif
