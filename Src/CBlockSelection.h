﻿#ifndef CBLOCKSELECTION
#define CBLOCKSELECTION

#include <Engine.h>
#include "BlockItemBase/CBlock.h"
#include "Network/CPlayer.h"

class CClient;

class CBlockSelection
{
public:

	void Init();
	void Destroy();
	
	void InteractWithBlock();
	void RemoveBlock();
	
	void Render();
	void RefreshFocusedBlock();

private:
	
	bool BlockSelection_ (SIVector BlockPosition, CBlockData Block, SFVector PlayerPosition, SFVector Ray);
	bool CollisionAABBWithLine_ (SBlockAABB BoundingBox, SFVector Position, 
								 SFVector Direction, float& SqDistance, byte& Face);
	bool CollisionAABBWithLine1D_ (float Start, float End, float Position, 
								   float Direction, float& Enter, float& Exit);
	SIVector NextFace_ (SFVector RayStart, SIVector CurrentPosition, SFVector Ray);

	void GenerateBoundingBoxMesh_();
	
	bool		BlockSelected_;
	SIVector	BlockPosition_;
	SIVector	SelectionPosition_;
	byte		PreviewFace_;

	CColorMeshConstructor	SelectionMeshConstructor_;
	byte					BlockRotate_;
	CMesh					Selection_;

};

#endif