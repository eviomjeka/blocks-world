﻿#include "CWorldScene.h"
#include "CApplication.h"

void CWorldScene::Init(SIVector2D WorldSize)
{
	ClippingFrustum_.Init (&MATRIXENGINE);
	MeshGenerator_.Init();
	Meshes_.Resize(SIVector(WorldSize.x, SUBWORLD_SECTORS, WorldSize.y));

	MeshQueues_.BlocksMQ.Init (&CShaders::BlocksShader, 1 << 20, 1 << 10);
	MeshQueues_.ComplexBlocksMQ.Init (&CShaders::ComplexBlocksShader, 1 << 20, 1 << 10);
	MeshQueues_.TranslucentComplexBlocksMQ.Init (&CShaders::ComplexBlocksShader, 1 << 20, 1 << 10);

	UseShaderProgram (BlocksShader);
	CShaders::BlocksShader.UniformInt (CShaders::Blocks.Texture(), 0);

	UseShaderProgram (ComplexBlocksShader);
	CShaders::ComplexBlocksShader.UniformInt (CShaders::ComplexBlocks.Texture(), 0);
	
	UseShaderProgram (ShadowBlocksShader);
	CShaders::BlocksShader.UniformInt (CShaders::ShadowBlocks.ShadowMap(), 1);

	CBlocksMeshQueue::GetAttributes (&CShaders::BlocksShader, 
		&BlocksAttributes_, &BlocksElementSize_);
	CComplexBlocksMeshQueue::GetAttributes (&CShaders::ComplexBlocksShader, 
		&ComplexBlocksAttributes_, &ComplexBlocksElementSize_);
	CComplexBlocksMeshQueue::GetPositionAttributes (&CShaders::ComplexBlocksDBShader, 
		&ComplexBlocksPositionAttributes_); // TODO: remove element size!!!
	
	if (SETTINGS.Shadows)
	{
		// TODO: GetShadowAttributes??
		// TODO: rename shader DepthShader or ShadowBlocksShader name or BlocksShadowAttributes_ variable name?
		CBlocksMeshQueue::GetPositionAttributes (&CShaders::DepthShader, &BlocksShadowAttributes_);
		// TODO: manually get attributes?

		glEnable (GL_MULTISAMPLE); // TODO: needed? move from here
		ShadowTextureSize_ = 2048;

		glGenFramebuffers (1, &FBO_.FBO);
		glBindFramebuffer (GL_FRAMEBUFFER, FBO_.FBO);
		glassert();

		glGenTextures (1, &FBO_.ColorBuffer);
		glBindTexture (GL_TEXTURE_2D, FBO_.ColorBuffer);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D (GL_TEXTURE_2D, 0, GL_R16, ShadowTextureSize_, ShadowTextureSize_, 
					  0, GL_RED, GL_UNSIGNED_SHORT, 0);
		glassert();

		glGenRenderbuffers (1, &FBO_.DepthBuffer);
		glBindRenderbuffer (GL_RENDERBUFFER, FBO_.DepthBuffer);
		glRenderbufferStorage (GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, ShadowTextureSize_, ShadowTextureSize_);
		glassert();

		glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, FBO_.ColorBuffer, 0);
		glFramebufferRenderbuffer (GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, FBO_.DepthBuffer);
		assert (glCheckFramebufferStatus (GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
		glBindFramebuffer (GL_FRAMEBUFFER, 0);
		glassert();
	}
}

void CWorldScene::Destroy()
{
	ClippingFrustum_.Destroy();
	
	for (int i = 0; i < Meshes_.Size(); i++)
	{
		Meshes_[i].BlocksMesh.Destroy();
		Meshes_[i].ComplexBlocksMesh.Destroy();
		Meshes_[i].TranslucentComplexBlocksMesh.Destroy();
	}

	if (SETTINGS.Shadows)
	{
		// TODO: delete vbo, fbo, rbo, texture
	}
}

void CWorldScene::GenerateMeshes (CClientSubWorlds3x3& Neighbours)
{
	int Time = TimeMS();

	CClientSubWorld& SubWorld = *Neighbours(0, 0);
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		MeshGenerator_.GenerateMeshes (MeshQueues_, i, Neighbours);
	
	LOG ("Subworld (%d %d) meshes generated: %dms", SubWorld.Position().x, SubWorld.Position().y, TimeMS() - Time);
}

void CWorldScene::UnloadMeshes (SIVector2D Position)
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
	{
		SIVector SectorPosition(Position.x, i, Position.y);
		tSectorMeshes& SectorMeshes = Meshes_(SectorPosition);

		if (SectorMeshes.BlocksMesh)
			SectorMeshes.BlocksMesh.Destroy();
		if (SectorMeshes.ComplexBlocksMesh)
			SectorMeshes.ComplexBlocksMesh.Destroy();
		if (SectorMeshes.TranslucentComplexBlocksMesh)
			SectorMeshes.TranslucentComplexBlocksMesh.Destroy();
	}
}

void CWorldScene::SendMeshes_()
{
	//last loaded mesh type
	while (!MeshQueues_.TranslucentComplexBlocksMQ.Empty())
	{
		assert(!MeshQueues_.ComplexBlocksMQ.Empty());
		assert(!MeshQueues_.BlocksMQ.Empty());

		SIVector Key = MeshQueues_.BlocksMQ.GetPending();
		assert(MeshQueues_.ComplexBlocksMQ.GetPending() == Key);
		assert(MeshQueues_.TranslucentComplexBlocksMQ.GetPending() == Key);

		tSectorMeshes& SectorMeshes = Meshes_(Key);

		MeshQueues_.BlocksMQ.Load(SectorMeshes.BlocksMesh);
		MeshQueues_.ComplexBlocksMQ.Load(SectorMeshes.ComplexBlocksMesh);
		MeshQueues_.TranslucentComplexBlocksMQ.Load(SectorMeshes.TranslucentComplexBlocksMesh);

		SectorMeshes.Position = Key;
		bool Loaded = true;

		for (int i = 0; i < SUBWORLD_SECTORS; i++)
			if (Meshes_(Key.x, i, Key.z).Position != SIVector(Key.x, i, Key.z))
			{
				Loaded = false;
				break;
			}

		for (int i = 0; i < SUBWORLD_SECTORS; i++)
			Meshes_(Key.x, i, Key.z).Loaded = Loaded;
	}
}

//==============================================================================

void CWorldScene::Render()
{
	SendMeshes_();
	ClippingFrustum_.Load();

	if (!SETTINGS.Shadows)
	{
		RenderNotShadowedScene_ (MESHTYPE_BLOCKS);
		RenderNotShadowedScene_ (MESHTYPE_COMPLEX_BLOCKS);
		RenderNotShadowedScene_ (MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB);
		RenderNotShadowedScene_ (MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS);
	}
	else
	{
		RenderToShadowMap_ (MESHTYPE_BLOCKS);
		//RenderToShadowMap_ (MESHTYPE_COMPLEX_BLOCKS);
		//RenderToShadowMap_ (MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS);
		RenderShadowedScene_ (MESHTYPE_BLOCKS);
		//RenderShadowedScene_ (MESHTYPE_COMPLEX_BLOCKS);
		//RenderShadowedScene_ (MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS);
		// TODO: alpha with transulent blocks?
		// TODO: MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB
	}
}

void CWorldScene::RenderNotShadowedScene_ (int MeshType)
{
	PLAYERCONTROLLER.WorldTexture().Bind();

	float TimeCoef = CLIENTWORLD.Time() / (float) DAY_DURATION;
	float DayCoef = 1.0f;
	if		(TimeCoef >= 0.3f && TimeCoef < 0.4f)	DayCoef = (0.4f - TimeCoef) / 0.1f;
	else if (TimeCoef >= 0.4f && TimeCoef < 0.75f)	DayCoef = 0.0f;
	else if (TimeCoef >= 0.75f && TimeCoef < 0.85f)	DayCoef = (TimeCoef - 0.75f) / 0.1f;
	
	SFVector SMColorDay = SFVector (1.0f, 1.0f, 1.0f);
	SFVector SMColorNight = SFVector (0.2f, 0.2f, 0.2f);
	SFVector SMColor = SMColorDay * DayCoef + SMColorNight * (1.0f - DayCoef);

	SFVector FogColorDay = SFVector (0.65f, 0.65f, 0.5f);
	SFVector FogColorNight = SFVector (0.0f, 0.0f, 0.05f);
	SFVector FogColor = FogColorDay * DayCoef + FogColorNight * (1.0f - DayCoef);

	float FogDistance = (float) CLIENT.MapLoader().SightRange() * SUBWORLD_SIZE_XZ;

	CShaderProgram* ShaderProgram = NULL;
	if (MeshType == MESHTYPE_BLOCKS)
		ShaderProgram = &CShaders::BlocksShader;
	else if (MeshType == MESHTYPE_COMPLEX_BLOCKS || 
		MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
		ShaderProgram = &CShaders::ComplexBlocksShader; 
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
		ShaderProgram = &CShaders::ComplexBlocksDBShader;

	UseShaderProgramAddr (ShaderProgram);
	
	if (MeshType == MESHTYPE_BLOCKS)
	{
		ShaderProgram->UniformVector3D (CShaders::Blocks.SMColor(), SMColor);
		ShaderProgram->UniformVector3D (CShaders::Blocks.FogColor(), FogColor);
		ShaderProgram->UniformFloat (CShaders::Blocks.FogDistance(), FogDistance);
		// TODO: calculate FogDistance and send to mesh one time?
	}
	else if (MeshType != MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		ShaderProgram->UniformVector3D (CShaders::ComplexBlocks.SMColor(), SMColor);
		ShaderProgram->UniformVector3D (CShaders::ComplexBlocks.FogColor(), FogColor);
		ShaderProgram->UniformFloat (CShaders::ComplexBlocks.FogDistance(), FogDistance);
	}

	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
	{
		glDisable (GL_CULL_FACE);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		glDisable (GL_CULL_FACE);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glEnable (GL_POLYGON_OFFSET_FILL);
		glPolygonOffset (0.0f, 1.0f);
		glDrawBuffer (GL_NONE);
	}
	
	RenderScene_ (MeshType, RENDERTYPE_NOT_SHADOWED_SCENE);
	
	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
	{
		glDisable (GL_BLEND);
		glEnable (GL_CULL_FACE);
	}
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		glDrawBuffer (GL_BACK);
		glDisable (GL_POLYGON_OFFSET_FILL);

		glDisable (GL_BLEND);
		glEnable (GL_CULL_FACE);
	}
}

void CWorldScene::RenderToShadowMap_ (int MeshType)
{
	glViewport (0, 0, ShadowTextureSize_, ShadowTextureSize_);
	glBindFramebuffer (GL_FRAMEBUFFER, FBO_.FBO);
	
	glClearColor (1.0f, 1.0f, 1.0f, 1.0f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glEnable (GL_POLYGON_OFFSET_FILL);
	//glPolygonOffset (2.0f, 500.0f); // TODO: подобрать значения
	glDisable (GL_CULL_FACE);
	
	MATRIXENGINE.PushProjectionMatrix();
	MATRIXENGINE.ResetProjectionMatrix();
	MATRIXENGINE.Ortho (-10.0f, 10.0f, -10.0f, 10.0f, -500.0f, 500.0f);

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	MATRIXENGINE.LookAt (SFVector (50.0f, 300.0f, 100.0f) + PLAYERCONTROLLER.Position(), 
		PLAYERCONTROLLER.Position(), SFVector (0.0f, 1.0f, 0.0f));
	// TODO:  + SFVector (0.0f, PLAYER_EYES_HEIGHT, 0.0f)

	LightMatrix_ = MATRIXENGINE.ResultingMatrix();

	CShaderProgram* ShaderProgram = &CShaders::DepthShader;
	UseShaderProgramAddr (ShaderProgram);
	
	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
		glDisable (GL_CULL_FACE);

	RenderScene_ (MeshType, RENDERTYPE_TO_SHADOW_MAP);
	
	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
		glEnable (GL_CULL_FACE);
	
	MATRIXENGINE.PopModelViewMatrix();
	MATRIXENGINE.PopProjectionMatrix();

	glEnable (GL_CULL_FACE);
	//glDisable (GL_POLYGON_OFFSET_FILL);
	glBindFramebuffer (GL_FRAMEBUFFER, 0);
	
	glViewport (0, 0, APPLICATION->GetWindowSize().x, APPLICATION->GetWindowSize().y);


#if OS == OS_WINDOWS
	if (GetAsyncKeyState (VK_F3))
	{
		MATRIXENGINE.PushProjectionMatrix();
		MATRIXENGINE.PushModelViewMatrix();
		MATRIXENGINE.ResetProjectionMatrix();
		MATRIXENGINE.ResetModelViewMatrix();
		MATRIXENGINE.Ortho (0, 800, 0, 600, -100, 100);

		CTextureMeshConstructor MC;
		MC.Init (&CShaders::TextureShader, 6);
		CMesh Mesh;
		MC.Begin (&Mesh);
		MC.AddVertex (SFVector (50, 50, 0), SFVector2D (0.0f, 0.0f));
		MC.AddVertex (SFVector (500, 50, 0), SFVector2D (1.0f, 0.0f));
		MC.AddVertex (SFVector (500, 500, 0), SFVector2D (1.0f, 1.0f));
		MC.AddVertex (SFVector (500, 500, 0), SFVector2D (1.0f, 1.0f));
		MC.AddVertex (SFVector (50, 500, 0), SFVector2D (0.0f, 1.0f));
		MC.AddVertex (SFVector (50, 50, 0), SFVector2D (0.0f, 0.0f));
		MC.End (&Mesh);

		UseShaderProgram (TextureShader);
		MATRIXENGINE.Apply (&CShaders::TextureShader);
		
		glBindTexture (GL_TEXTURE_2D, FBO_.ColorBuffer);
		Mesh.Render();
		Mesh.Destroy();

		MATRIXENGINE.PopModelViewMatrix();
		MATRIXENGINE.PopProjectionMatrix();
	}
#endif
}


void CWorldScene::RenderShadowedScene_ (int MeshType)
{
	PLAYERCONTROLLER.WorldTexture().Bind();

	float TimeCoef = CLIENTWORLD.Time() / (float) DAY_DURATION;
	float DayCoef = 1.0f;
	if		(TimeCoef >= 0.3f && TimeCoef < 0.4f)	DayCoef = (0.4f - TimeCoef) / 0.1f;
	else if (TimeCoef >= 0.4f && TimeCoef < 0.75f)	DayCoef = 0.0f;
	else if (TimeCoef >= 0.75f && TimeCoef < 0.85f)	DayCoef = (TimeCoef - 0.75f) / 0.1f;
	
	SFVector SMColorDay = SFVector (1.0f, 1.0f, 1.0f);
	SFVector SMColorNight = SFVector (0.2f, 0.2f, 0.2f);
	SFVector SMColor = SMColorDay * DayCoef + SMColorNight * (1.0f - DayCoef);

	SFVector FogColorDay = SFVector (0.65f, 0.65f, 0.5f);
	SFVector FogColorNight = SFVector (0.0f, 0.0f, 0.05f);
	SFVector FogColor = FogColorDay * DayCoef + FogColorNight * (1.0f - DayCoef);

	float FogDistance = (float) CLIENT.MapLoader().SightRange() * SUBWORLD_SIZE_XZ;

	CShaderProgram* ShaderProgram = NULL;
	if (MeshType == MESHTYPE_BLOCKS)
		ShaderProgram = &CShaders::ShadowBlocksShader;
	/*else if (MeshType == MESHTYPE_COMPLEX_BLOCKS || 
		MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
		ShaderProgram = &CShaders::ComplexBlocksShader; 
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
		ShaderProgram = &CShaders::ComplexBlocksDBShader;*/

	UseShaderProgramAddr (ShaderProgram);
	
	if (MeshType == MESHTYPE_BLOCKS)
	{
		ShaderProgram->UniformVector3D (CShaders::ShadowBlocks.SMColor(), SMColor);
		ShaderProgram->UniformVector3D (CShaders::ShadowBlocks.FogColor(), FogColor);
		ShaderProgram->UniformFloat (CShaders::ShadowBlocks.FogDistance(), FogDistance);
	}
	else if (MeshType != MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		ShaderProgram->UniformVector3D (CShaders::ComplexBlocks.SMColor(), SMColor);
		ShaderProgram->UniformVector3D (CShaders::ComplexBlocks.FogColor(), FogColor);
		ShaderProgram->UniformFloat (CShaders::ComplexBlocks.FogDistance(), FogDistance);
	}

	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
	{
		glDisable (GL_CULL_FACE);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		glDisable (GL_CULL_FACE);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glEnable (GL_POLYGON_OFFSET_FILL);
		glPolygonOffset (0.0f, 1.0f);
		glDrawBuffer (GL_NONE);
	}
	
	//=============================

	glActiveTexture (GL_TEXTURE1);
	glBindTexture (GL_TEXTURE_2D, FBO_.ColorBuffer);
	glActiveTexture (GL_TEXTURE0);
	glassert();

	//=============================

	RenderScene_ (MeshType, RENDERTYPE_SHADOWED_SCENE);
	
	if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
	{
		glDisable (GL_BLEND);
		glEnable (GL_CULL_FACE);
	}
	else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
	{
		glDrawBuffer (GL_BACK);
		glDisable (GL_POLYGON_OFFSET_FILL);

		glDisable (GL_BLEND);
		glEnable (GL_CULL_FACE);
	}
}

void CWorldScene::RenderScene_ (int MeshType, int RenderType)
{
	SIVector2D Position = DivXZ2D(FloorVector(PLAYERCONTROLLER.Position()), SUBWORLD_SIZE_XZ);
	SIVector2D FirstVisibleSubWorld = Position - SIVector2D(CLIENT.MapLoader().SightRange() - 1); 
	SIVector2D LastVisibleSubWorld  = Position + SIVector2D(CLIENT.MapLoader().SightRange() - 1);
	
	for (int x = FirstVisibleSubWorld.x; x <= LastVisibleSubWorld.x; x++)
		for (int y = FirstVisibleSubWorld.y; y <= LastVisibleSubWorld.y; y++)
			for (int i = 0; i < SUBWORLD_SECTORS; i++)
			{
				SIVector CurrentPosition(x, i, y);
				tSectorMeshes& SectorMeshes = Meshes_(CurrentPosition);

				if (!SectorMeshes.Loaded || CurrentPosition != SectorMeshes.Position)
					continue;

				bool MeshExist = false;
				if (MeshType == MESHTYPE_BLOCKS)
					MeshExist = (SectorMeshes.BlocksMesh);
				else if (MeshType == MESHTYPE_COMPLEX_BLOCKS)
					MeshExist = (SectorMeshes.ComplexBlocksMesh);
				else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS || 
						 MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
					MeshExist = (SectorMeshes.TranslucentComplexBlocksMesh);

				if (!MeshExist)
					continue;

				bool Visible = (RenderType == RENDERTYPE_TO_SHADOW_MAP) ? true : 
					(ClippingFrustum_.ContainsAABB(CurrentPosition * SECTOR_SIZE,
						SFVector (SUBWORLD_SIZE_XZ, SECTOR_SIZE, SUBWORLD_SIZE_XZ)));
				if (!Visible)
					continue;

				MATRIXENGINE.PushModelViewMatrix();
				MATRIXENGINE.Translate(CurrentPosition * SECTOR_SIZE);

				if (MeshType == MESHTYPE_BLOCKS)
				{
					if (RenderType == RENDERTYPE_NOT_SHADOWED_SCENE)
					{
						MATRIXENGINE.Apply(&CShaders::BlocksShader); // TODO: get shader from function parameter?
						SectorMeshes.BlocksMesh.Render(NULL, true);
					}
					else if (RenderType == RENDERTYPE_TO_SHADOW_MAP)
					{
						MATRIXENGINE.Apply(&CShaders::DepthShader);
						SectorMeshes.BlocksMesh.Render(&BlocksShadowAttributes_, true);
					}
					else if (RenderType == RENDERTYPE_SHADOWED_SCENE)
					{
						MATRIXENGINE.PushModelViewMatrix();
						MATRIXENGINE.ResetModelViewMatrix();
						MATRIXENGINE.Translate(CurrentPosition * SECTOR_SIZE);
						CMatrix<4, 4> ModelViewMatrix = MATRIXENGINE.ModelViewMatrix();
						MATRIXENGINE.PopModelViewMatrix();
						
						MATRIXENGINE.PushModelViewMatrix(); // TODO: own matrix engine?
						MATRIXENGINE.ResetModelViewMatrix();
						MATRIXENGINE.Translate (0.5f, 0.5f, 0.5f); // TODO: early set LightMatrix_?
						MATRIXENGINE.Scale (0.5f, 0.5f, 0.5f);
						CMatrix<4, 4> LightMatrix = MATRIXENGINE.ModelViewMatrix() * LightMatrix_;
						MATRIXENGINE.PopModelViewMatrix();

						CShaders::ShadowBlocksShader.UniformMatrix4x4 (CShaders::ShadowBlocks.Matrix_ModelView(), ModelViewMatrix);
						CShaders::ShadowBlocksShader.UniformMatrix4x4 (CShaders::ShadowBlocks.LightMatrix(), LightMatrix);

						MATRIXENGINE.Apply(&CShaders::ShadowBlocksShader);
						SectorMeshes.BlocksMesh.Render(NULL, true);
					}
				}
				else if (MeshType == MESHTYPE_COMPLEX_BLOCKS)
				{
					MATRIXENGINE.Apply(&CShaders::ComplexBlocksShader);
					SectorMeshes.ComplexBlocksMesh.Render(NULL, true);
				}
				else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS)
				{
					MATRIXENGINE.Apply(&CShaders::ComplexBlocksShader);
					SectorMeshes.TranslucentComplexBlocksMesh.Render(NULL, true);
				}
				else if (MeshType == MESHTYPE_TRANSLUCENT_COMPLEX_BLOCKS_DB)
				{
					MATRIXENGINE.Apply(&CShaders::ComplexBlocksDBShader);
					SectorMeshes.TranslucentComplexBlocksMesh.Render(NULL, true);
				}

				MATRIXENGINE.PopModelViewMatrix();
			}
}

void CWorldScene::ClearQueues()
{
	MeshQueues_.BlocksMQ.Clear();
	MeshQueues_.ComplexBlocksMQ.Clear();
	MeshQueues_.TranslucentComplexBlocksMQ.Clear();
}
