﻿#include "CBlockUsual.h"
#include "../CApplication.h"

CBlockUsual::CBlockUsual()
{
	SetType_ (BLOCKTYPE_BLOCK);
	SetFaceType_ (FACETYPE_NORMAL);

	SetBlockFaceTextureOffset_ (SIVector2D (0, 1), BLOCK_BLACK);
	SetBlockFaceTextureOffset_ (SIVector2D (1, 1), BLOCK_DARK_GRAY);
	SetBlockFaceTextureOffset_ (SIVector2D (2, 1), BLOCK_LIGHT_GRAY);
	SetBlockFaceTextureOffset_ (SIVector2D (3, 1), BLOCK_WHITE);
	SetBlockFaceTextureOffset_ (SIVector2D (4, 1), BLOCK_RED);
	SetBlockFaceTextureOffset_ (SIVector2D (5, 1), BLOCK_GREEN);
	SetBlockFaceTextureOffset_ (SIVector2D (6, 1), BLOCK_BLUE);
	SetBlockFaceTextureOffset_ (SIVector2D (7, 1), BLOCK_YELLOW);
	SetBlockFaceTextureOffset_ (SIVector2D (8, 1), BLOCK_VIOLET);
	SetBlockFaceTextureOffset_ (SIVector2D (9, 1), BLOCK_CYAN);
	SetBlockFaceTextureOffset_ (SIVector2D (10, 1), BLOCK_ORANGE);
	SetBlockFaceTextureOffset_ (SIVector2D (11, 1), BLOCK_DARK_GREEN);
	SetBlockFaceTextureOffset_ (SIVector2D (12, 1), BLOCK_LIGHT_BLUE);
	SetBlockFaceTextureOffset_ (SIVector2D (13, 1), BLOCK_PURPLE);
	SetBlockFaceTextureOffset_ (SIVector2D (14, 1), BLOCK_PINK);
	SetBlockFaceTextureOffset_ (SIVector2D (15, 1), BLOCK_BROWN);

	SetBlockFaceTextureOffset_ (SIVector2D (0, 0), BLOCK_GRASS);
	SetBlockFaceTextureOffset_ (SIVector2D (1, 0), BLOCK_DIRT);
	SetBlockFaceTextureOffset_ (SIVector2D (2, 0), BLOCK_SAND);
	SetBlockFaceTextureOffset_ (SIVector2D (3, 0), BLOCK_STONE);
	SetBlockFaceTextureOffset_ (SIVector2D (4, 0), BLOCK_WOOD);
	SetBlockFaceTextureOffset_ (SIVector2D (5, 0), BLOCK_LEAVES);
	SetBlockFaceTextureOffset_ (SIVector2D (6, 0), BLOCK_SNOW);
	SetBlockFaceTextureOffset_ (SIVector2D (7, 0), BLOCK_BRIGHT_STONE);
	SetBlockFaceTextureOffset_ (SIVector2D (8, 0), BLOCK_BRICK);
}

void CBlockUsual::GetName (byte Attributes, CString& Name) const
{
	switch (Attributes)
	{
		case BLOCK_GRASS:			Name = "grass";			break;
		case BLOCK_DIRT:			Name = "dirt";			break;
		case BLOCK_SAND:			Name = "sand";			break;
		case BLOCK_STONE:			Name = "stone";			break;
		case BLOCK_WOOD:			Name = "wood";			break;
		case BLOCK_LEAVES:			Name = "leaves";		break;
		case BLOCK_SNOW:			Name = "snow";			break;
		case BLOCK_BRIGHT_STONE:	Name = "bright_stone";	break;
		case BLOCK_BRICK:			Name = "brick";			break;
		default:
			Name.Print ("block (%s)", AttributesToColor (Attributes));
		break;
	}
}

void CBlockUsual::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	for (int i = 0; i < 16; i++)
		InventoryItems.Insert ((byte) i);

	for (int i = 16; i < 16 + 9; i++)
		InventoryItems.Insert ((byte) i);
}