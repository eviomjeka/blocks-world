﻿#ifndef CBLOCKWATER
#define CBLOCKWATER

#include "../BlockItemBase/CBlock.h"

class CBlockWater : public CBlock
{
public:
	
	CBlockWater();
	
	virtual void GetName		(byte Attributes, CString& Name) const;
	virtual void GetAABBList	(byte Attributes, SIVector Position, CArray<SBlockAABB>& AABBList) const;
	virtual void Render			(byte Attributes, CBlockManagerReadOnly* BlockManager, 
								 SIVector Position, CVertexListener* VertexListener) const;
	virtual void OnAdd			(byte Attributes, CBlockManager* BlockManager, 
								 SIVector Position, byte Face, SFVector2D Angle) const;

};

#endif