﻿#ifndef CBLOCKHALF
#define CBLOCKHALF

#include "../BlockItemBase/CBlock.h"

class CBlockHalf : public CBlock
{
public:
	
	CBlockHalf();
	
	virtual void GetName			(byte Attributes, CString& Name) const;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;
	virtual void Render				(byte Attributes, CBlockManagerReadOnly* BlockManager, 
									 SIVector Position, CVertexListener* VertexListener) const;
	virtual bool OnInteraction		(byte Attributes, CBlockManager* BlockManager, SIVector Position, 
									 byte Face, SFVector2D Angle, CItemData Item) const;

};

#endif