﻿#include "CBlockWater.h"
#include "../CApplication.h"

CBlockWater::CBlockWater()
{
	SetType_ (BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK);
	SetLightAttenuation_ (2);
	SetFaceType_ (FACETYPE_NONE);
}

void CBlockWater::GetName (byte, CString& Name) const
{
	Name = "water";
}

void CBlockWater::GetAABBList (byte, SIVector, CArray<SBlockAABB>&) const
{
}

void CBlockWater::Render (byte, CBlockManagerReadOnly* BlockManager, 
						  SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	CBlockData BlockUp = BlockManager->GetBlock (Position + SIVector (0, 1, 0));
	
	float Coef = 0.85f;
	if (BlockUp.ID() == BlocksList.Water.ID())
		Coef = 1.0f;
		
	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		SVector2D<unsigned short> TextureCoord[4] = {};
		GetTextureCoordsForFace (TextureCoord, Face);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = (Face == HYFACE_MASK) ? BlockUp : BlockManager->GetBlock (NeighbourPosition);
		CBlockData BlockNUp = BlockManager->GetBlock (NeighbourPosition + SIVector (0, 1, 0));

		bool MaskNeededWater = (Face != LYFACE_MASK && Face != HYFACE_MASK) && 
			BlockUp.ID() == BlocksList.Water.ID() && BlockNUp.ID() != BlocksList.Water.ID();
		if (!MaskNeededWater && (Face != HYFACE_MASK || (Face == HYFACE_MASK && Block.ID() == BlocksList.Water.ID())) && 
			(Block.ID() == BlocksList.Water.ID() || Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL))
			continue;

		GetFace (Face, v);

		SVector2D<byte> TextureCoord0 (3, 2);

		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		for (int j = 0; j < 6; j++)
		{
			int VertexIndex = VertexIndexes[j];
			SFVector VertexPositon = (SFVector) Position + 
				SFVector ((float) v[VertexIndex].x, (float) v[VertexIndex].y * Coef, (float) v[VertexIndex].z);

			VertexListener->AddVertex (VertexPositon, TextureCoord0, TextureCoord[VertexIndex] * 256);
		}
	}
}

void CBlockWater::OnAdd (byte Attributes, CBlockManager* BlockManager, 
						 SIVector Position, byte, SFVector2D) const
{
	if (CanAdd_ (Attributes, BlockManager, Position, true))
		BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));

	for (int i = 0; i < NUM_FACES; i++)
	{
		SIVector NeighbourPosition = FaceToVector (Position, IndexToFace ((byte) i));
		CBlockData Block = BlockManager->GetBlock (NeighbourPosition);
		if (Block.ID() == BlocksList.Water.ID())
		{
			BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));
			return;
		}
	}
}