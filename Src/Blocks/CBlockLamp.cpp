﻿#include "CBlockLamp.h"
#include "../CApplication.h"

CBlockLamp::CBlockLamp()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);
	SetLightIntensity_ (MAX_LIGHT_INTENSITY);
	SetLightAttenuation_ (0);

	SetFaceType_ (FACETYPE_NONE);
	SetFaceTypeToFace_ (LYFACE_MASK, FACETYPE_NOT_NORMAL);
	
	SetRotate_ (ROTATEX_NONE	, 0);
	SetRotate_ (ROTATEX_180		, 1);
	SetRotate_ (ROTATEZ_CW90	, 2);
	SetRotate_ (ROTATEZ_CCW90	, 3);
	SetRotate_ (ROTATEX_CCW90	, 4);
	SetRotate_ (ROTATEX_CW90	, 5);
}

void CBlockLamp::GetName (byte, CString& Name) const
{
	Name = "lamp";
}

void CBlockLamp::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, 
						 SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};
	SFVector StartPos (0.35f, 0.0f, 0.35f);
	SFVector Size (0.3f, 0.6f, 0.3f);

	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		SVector2D<unsigned short> TextureCoord[4] = {};
		GetTextureCoordsForFace (TextureCoord, Face);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = RotateGetBlock_ (Attributes, BlockManager, Position, NeighbourPosition);
			
		if (Face == LYFACE_MASK && Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
			continue;

		GetFace (Face, v);
		
		SVector2D<byte> TextureCoord0 (2, 2);
		
		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		for (int j = 0; j < 6; j++)
		{
			int VertexIndex = VertexIndexes[j];

			SVector2D<unsigned short> CurrentTextureCoord;
			if (Face == HYFACE_MASK)
				CurrentTextureCoord (128 + TextureCoord[VertexIndex].x * 128, TextureCoord[VertexIndex].y * 128);
			else if (Face == LYFACE_MASK)
				CurrentTextureCoord (128 + TextureCoord[VertexIndex].x * 128, 128 + TextureCoord[VertexIndex].y * 128);
			else
				CurrentTextureCoord (TextureCoord[VertexIndex].x * 128, TextureCoord[VertexIndex].y * 256);

			SFVector VertexPositon = StartPos + ((SFVector) v[VertexIndex]).PerComponentProduct (Size) + Position;
			VertexListener->AddVertex (VertexPositon, TextureCoord0, CurrentTextureCoord);
		}
	}
}

void CBlockLamp::OnAdd (byte Attributes, CBlockManager* BlockManager, SIVector Position, byte Face, SFVector2D) const
{
	if (CanAddOnFace_ (Attributes, BlockManager, Position, Face, false))
	{
		switch (Face)
		{
			case LXFACE_MASK: Attributes = 2; break;
			case HXFACE_MASK: Attributes = 3; break;
			case LYFACE_MASK: Attributes = 0; break;
			case HYFACE_MASK: Attributes = 1; break;
			case LZFACE_MASK: Attributes = 4; break;
			case HZFACE_MASK: Attributes = 5; break;
		}
		BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));
	}
}