﻿#include "CBlockHalf.h"
#include "../CApplication.h"

CBlockHalf::CBlockHalf()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);
	SetLightAttenuation_ (1);

	SetFaceType_ (FACETYPE_NOT_NORMAL);
	SetFaceTypeToFace_ (LYFACE_MASK, FACETYPE_NORMAL);
	SetFaceTypeToFace_ (HYFACE_MASK, FACETYPE_NONE);
}

void CBlockHalf::GetName (byte Attributes, CString& Name) const
{
	Name.Print ("half (%s)", AttributesToColor (Attributes));
}

void CBlockHalf::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	for (int i = 0; i < 16; i++)
		InventoryItems.Insert ((byte) i);
}

void CBlockHalf::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		SVector2D<unsigned short> TextureCoord[4] = {};
		GetTextureCoordsForFace (TextureCoord, Face);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = BlockManager->GetBlock (NeighbourPosition);

		if (Face != HYFACE_MASK && Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
			continue;

		GetFace (Face, v);
				
		SVector2D<byte> TextureCoord0 (Attributes, 1);

		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		for (int j = 0; j < 6; j++)
		{
			int VertexIndex = VertexIndexes[j];
			SFVector VertexPositon = (SFVector) v[VertexIndex];
			VertexPositon.y /= 2.0f;
			VertexPositon += Position;
			VertexListener->AddVertex (VertexPositon, TextureCoord0, TextureCoord[VertexIndex] * 256);
		}
	}
}

bool CBlockHalf::OnInteraction (byte Attributes, CBlockManager* BlockManager, SIVector Position, 
								byte Face, SFVector2D, CItemData Item) const
{
	if (Face != LYFACE_MASK || Item.ID() != BlocksList.Half.ID())
		return false;

	CBlockData Block = BlockDataEx (DoubleHalf, Attributes + 16 * Item.Attributes());
	BlockManager->SetBlock (Position, Block);
	return true;
}