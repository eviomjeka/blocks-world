﻿#ifndef CBLOCKDOUBLEHALF
#define CBLOCKDOUBLEHALF

#include "../BlockItemBase/CBlock.h"

class CBlockDoubleHalf : public CBlock
{
public:
	
	CBlockDoubleHalf();
	
	virtual void GetName			(byte Attributes, CString& Name) const;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;
	virtual void GetAABBList		(byte Attributes, SIVector Position, CArray<SBlockAABB>& AABBList) const;
	virtual void Render				(byte Attributes, CBlockManagerReadOnly* BlockManager, 
									 SIVector Position, CVertexListener* VertexListener) const;

};

#endif