﻿#include "CBlockDoor.h"
#include "../CApplication.h"

CBlockDoor::CBlockDoor()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);

	SetFaceType_ (FACETYPE_NONE);
	SetLightAttenuation_ (1);
	
	SetRotate_ (ROTATEX_NONE	, 0 , 6);
	SetRotate_ (ROTATEY_CW90	, 6 , 12);
	SetRotate_ (ROTATEY_180		, 12, 18);
	SetRotate_ (ROTATEY_CCW90	, 18, 24);
	
	SetRotate_ (ROTATEY_CW90	, 24, 30);
	SetRotate_ (ROTATEY_180		, 30, 36);
	SetRotate_ (ROTATEY_CCW90	, 36, 42);
	SetRotate_ (ROTATEX_NONE	, 42, 48);
}

void CBlockDoor::GetName (byte Attributes, CString& Name) const
{
	if (Attributes < 24)
		Name = "door";
	else
		Name = "opened_door";
}

void CBlockDoor::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};
	SFVector Size (1.0f, 1.0f, 1.0f / 8.0f);

	SIVector DoorPosition ((Attributes / 3) % 2, Attributes % 3, 0);

	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		if (Face != LZFACE_MASK && Face != HZFACE_MASK && 
			(!(DoorPosition.x == 0 && Face == LXFACE_MASK) && !(DoorPosition.x == 1 && Face == HXFACE_MASK) && 
			 !(DoorPosition.y == 0 && Face == LYFACE_MASK) && !(DoorPosition.y == 2 && Face == HYFACE_MASK)))
			continue;

		SVector2D<unsigned short> TextureCoord[4] = {};
		GetTextureCoordsForFace (TextureCoord, Face, Face == HZFACE_MASK);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = RotateGetBlock_ (Attributes, BlockManager, Position, NeighbourPosition);
			
		if (Face != LZFACE_MASK && Face != HZFACE_MASK && Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
			continue;

		GetFace (Face, v);
		
		SVector2D<byte> TextureCoordSide (2, 3);
		SVector2D<byte> TextureCoordUpDown (3, 3);
		SVector2D<byte> TextureCoordMain (1 - (Attributes / 3) % 2, 3 + 2 - Attributes % 3);

		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		for (int j = 0; j < 6; j++)
		{
			int VertexIndex = VertexIndexes[j];

			SVector2D<unsigned short> CurrentTextureCoord;
			
			if (Face == LXFACE_MASK)
				CurrentTextureCoord ((TextureCoord[VertexIndex].x + 8 - Attributes % 3) * 256 / 8, 
									  TextureCoord[VertexIndex].y * 256);
			else if (Face == HXFACE_MASK)
				CurrentTextureCoord ((TextureCoord[VertexIndex].x + 2 - Attributes % 3) * 256 / 8, 
									  TextureCoord[VertexIndex].y * 256);
			else if (Face == HYFACE_MASK)
				CurrentTextureCoord (TextureCoord[VertexIndex].x * 256, 
									(TextureCoord[VertexIndex].y + 1 - (Attributes / 3) % 2) * 256 / 8);
			else if (Face == LYFACE_MASK)
				CurrentTextureCoord (TextureCoord[VertexIndex].x * 256, 
									 (TextureCoord[VertexIndex].y + 7 - (Attributes / 3) % 2) * 256 / 8);
			else
				CurrentTextureCoord (TextureCoord[VertexIndex].x * 256, TextureCoord[VertexIndex].y * 256);
			
			SVector2D<byte> TextureCoordReady;
			if (Face == LZFACE_MASK || Face == HZFACE_MASK)
				TextureCoordReady = TextureCoordMain;
			else if (Face == LXFACE_MASK || Face == HXFACE_MASK)
				TextureCoordReady = TextureCoordSide;
			else
				TextureCoordReady = TextureCoordUpDown;
		
			SFVector VertexPositon = ((SFVector) v[VertexIndex]).PerComponentProduct (Size);
			if (Attributes >= 24)
				VertexPositon.z = 7.0f / 8.0f + VertexPositon.z;
			VertexPositon += Position;

			VertexListener->AddVertex (VertexPositon, TextureCoordReady, CurrentTextureCoord);
		}
	}
}

bool CBlockDoor::RenderPreview (byte, SVector2D<byte>& TextureOffset) const
{
	TextureOffset = SVector2D<byte> (4, 3);
	return true;
}

void CBlockDoor::OnAdd (byte Attributes, CBlockManager* BlockManager, SIVector Position, byte, SFVector2D) const
{
	if (CanAddOnFace_ (Attributes, BlockManager, Position, LYFACE_MASK, false))
	{
		bool CanAdd = true;
		for (int x = 0; x < 2 && CanAdd; x++)
			for (int y = 0; y < 3; y++)
			{
				if (BlockManager->GetBlock (Position + SIVector (x, y, 0)).ID())
				{
					CanAdd = false;
					break;
				}
			}

		if (CanAdd)
		{
			for (int x = 0; x < 2 && CanAdd; x++)
				for (int y = 0; y < 3; y++)
				{
					BlockManager->SetBlock (Position + SIVector (x, y, 0), CBlockData (ID(), (byte) (3*x + y)));
				}
		}
	}
}

void CBlockDoor::OnRemove (byte Attributes, CBlockManager* BlockManager, SIVector Position) const
{
	SIVector StartPosition (-(Attributes / 3) % 2, -Attributes % 3, 0);

	for (int x = 0; x < 2; x++)
		for (int y = 0; y < 3; y++)
		{
			if (BlockManager->GetBlock (Position + SIVector (x, y, 0)).ID() == BlocksList.Door.ID())
				BlockManager->SetBlock (Position + SIVector (x, y, 0) + StartPosition, CBlockData());
		}
}

bool CBlockDoor::OnInteraction (byte Attributes, CBlockManager* BlockManager, SIVector Position, 
								byte Face, SFVector2D, CItemData) const
{
	if (Face != LZFACE_MASK && Face != HZFACE_MASK)
		return false;
	
	SIVector StartPosition (-(Attributes / 3) % 2, -Attributes % 3, 0);
	bool CanAdd = true;
	for (int y = 0; y < 3; y++)
	{
		if (BlockManager->GetBlock (Position + StartPosition + SIVector (0, y, 1)).ID())
		{
			CanAdd = false;
			break;
		}
	}

	if (CanAdd)
	{
		for (int y = 0; y < 3; y++)
		{
			BlockManager->SetBlock (Position + StartPosition + SIVector (1, y, 0), CBlockData());
			BlockManager->SetBlock (Position + StartPosition + SIVector (0, y, 0), 
									BlockDataEx (Door, 24 + 0 + Attributes / 6 + y % 3));
			BlockManager->SetBlock (Position + StartPosition + SIVector (0, y, 1), 
									BlockDataEx (Door, 24 + 3 + Attributes / 6 + y % 3));
		}
	}

	return true;
}