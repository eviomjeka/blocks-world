﻿#ifndef CBLOCKGLASS
#define CBLOCKGLASS

#include "../BlockItemBase/CBlock.h"

class CBlockGlass : public CBlock
{
public:

	CBlockGlass();

	virtual void GetName	(byte Attributes, CString& Name) const;

};

#endif