﻿#ifndef CBLOCKICE
#define CBLOCKICE

#include "../BlockItemBase/CBlock.h"

class CBlockIce : public CBlock
{
public:
	
	CBlockIce();
	
	virtual void GetName	(byte Attributes, CString& Name) const;
	virtual void Render		(byte Attributes, CBlockManagerReadOnly* BlockManager, 
							 SIVector Position, CVertexListener* VertexListener) const;
	virtual void OnAdd		(byte Attributes, CBlockManager* BlockManager, 
							 SIVector Position, byte Face, SFVector2D Angle) const;

};

#endif