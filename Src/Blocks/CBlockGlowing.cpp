﻿#include "CBlockGlowing.h"
#include "../CApplication.h"

CBlockGlowing::CBlockGlowing()
{
	SetType_ (BLOCKTYPE_BLOCK);
	SetLightIntensity_ (MAX_LIGHT_INTENSITY);
	SetFaceType_ (FACETYPE_NORMAL);
	SetBlockFaceTextureOffset_ (SVector2D<byte> (1, 2));
}

void CBlockGlowing::GetName (byte, CString& Name) const
{
	Name = "glowing";
}