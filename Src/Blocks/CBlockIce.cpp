﻿#include "CBlockIce.h"
#include "../CApplication.h"

CBlockIce::CBlockIce()
{
	SetType_ (BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK);
	SetLightAttenuation_ (6);
	SetFaceType_ (FACETYPE_NOT_NORMAL);
}

void CBlockIce::GetName (byte, CString& Name) const
{
	Name = "ice";
}

void CBlockIce::Render (byte, CBlockManagerReadOnly* BlockManager, SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		SVector2D<unsigned short> TextureCoord[4] = {};
		GetTextureCoordsForFace (TextureCoord, Face);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = BlockManager->GetBlock (NeighbourPosition);

		if (Block.ID() == BlocksList.Ice.ID() || Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
			continue;

		GetFace (Face, v, Position);
				
		SVector2D<byte> TextureCoord0 (4, 2);

		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		for (int j = 0; j < 6; j++)
		{
			int VertexIndex = VertexIndexes[j];
			SFVector VertexPositon = (SFVector) v[VertexIndex];
			VertexListener->AddVertex (VertexPositon, TextureCoord0, TextureCoord[VertexIndex] * 256);
		}
	}
}

void CBlockIce::OnAdd (byte Attributes, CBlockManager* BlockManager, SIVector Position, byte, SFVector2D) const
{
	if (CanAdd_ (Attributes, BlockManager, Position, true))
		BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));

	for (int i = 0; i < NUM_FACES; i++)
	{
		SIVector NeighbourPosition = FaceToVector (Position, IndexToFace ((byte) i));
		CBlockData Block = BlockManager->GetBlock (NeighbourPosition);
		if (Block.ID() == BlocksList.Water.ID())
		{
			BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));
			return;
		}
	}
}