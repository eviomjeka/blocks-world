﻿#ifndef BLOCKSINCLUDE
#define BLOCKSINCLUDE

#include "CBlockAir.h"
#include "CBlockUsual.h"
#include "CBlockGlass.h"
#include "CBlockGlowing.h"
#include "CBlockLamp.h"
#include "CBlockWater.h"
#include "CBlockIce.h"
#include "CBlockHalf.h"
#include "CBlockDoubleHalf.h"
#include "CBlockStairs.h"
#include "CBlockDoor.h"
#include "CBlockCabel.h"

#endif