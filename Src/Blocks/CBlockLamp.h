﻿#ifndef CBLOCKLAMP
#define CBLOCKLAMP

#include "../BlockItemBase/CBlock.h"

class CBlockLamp : public CBlock
{
public:

	CBlockLamp();
	
	virtual void GetName	(byte Attributes, CString& Name) const;
	virtual void Render		(byte Attributes, CBlockManagerReadOnly* BlockManager, 
							 SIVector Position, CVertexListener* VertexListener) const;
	virtual void OnAdd		(byte Attributes, CBlockManager* BlockManager, SIVector Position, byte Face, SFVector2D Angle) const;

};

#endif