﻿#ifndef CBLOCKGLOWING
#define CBLOCKGLOWING

#include "../BlockItemBase/CBlock.h"

class CBlockGlowing : public CBlock
{
public:

	CBlockGlowing();

	virtual void GetName	(byte Attributes, CString& Name) const;

};

#endif