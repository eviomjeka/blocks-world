﻿#include "CBlockHalf.h"
#include "../CApplication.h"

CBlockDoubleHalf::CBlockDoubleHalf()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);
	SetRotate_ (ROTATEX_NONE | ROTATEY_NONE | ROTATEZ_NONE);
	SetFaceType_ (FACETYPE_NORMAL);
}

void CBlockDoubleHalf::GetName (byte, CString& Name) const
{
	Name = "double_half";
}

void CBlockDoubleHalf::GetInventoryItems (CArray<byte>&) const
{
}

void CBlockDoubleHalf::GetAABBList (byte, SIVector, CArray<SBlockAABB>& AABBList) const
{
	SBlockAABB AABB = {};
	AABB.Type = BLOCK_AABB_SELECTION_AND_PHYSICS;

	AABB.Start	= SFVector (0.0f, 0.0f, 0.0f);
	AABB.End	= SFVector (1.0f, 0.5f, 1.0f);
	AABBList.Insert (AABB);
	
	AABB.Start	= SFVector (0.0f, 0.5f, 0.0f);
	AABB.End	= SFVector (1.0f, 1.0f, 1.0f);
	AABBList.Insert (AABB);
}

void CBlockDoubleHalf::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, 
							   SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	for (int n = 0; n < 2; n++)
	{
		for (int i = 0; i < NUM_FACES; i++)
		{
			byte Face = IndexToFace ((byte) i);

			SVector2D<unsigned short> TextureCoord[4] = {};
			GetTextureCoordsForFace (TextureCoord, Face);

			SIVector NeighbourPosition = FaceToVector (Position, Face);
			CBlockData Block = BlockManager->GetBlock (NeighbourPosition);

			if (Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL)
				continue;
			if ((n == 0 && Face == HYFACE_MASK) || (n == 1 && Face == LYFACE_MASK))
				continue;

			GetFace (Face, v);
				
			SVector2D<byte> TextureCoord0 ((n == 0) ? (Attributes % 16) : (Attributes / 16), 1);

			const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
			for (int j = 0; j < 6; j++)
			{
				int VertexIndex = VertexIndexes[j];
				SFVector VertexPositon = (SFVector) v[VertexIndex];
				VertexPositon.y /= 2.0f;
				if (n == 1) VertexPositon += SFVector (0.0f, 0.5f, 0.0f);
				VertexPositon += Position;
				VertexListener->AddVertex (VertexPositon, TextureCoord0, TextureCoord[VertexIndex] * 256);
			}
		}
	}
}