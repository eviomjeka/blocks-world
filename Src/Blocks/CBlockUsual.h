﻿#ifndef CBLOCKUSUAL
#define CBLOCKUSUAL

#include "../BlockItemBase/CBlock.h"

enum EUsualBlockAttributes
{
	BLOCK_GRASS = 16, 
	BLOCK_DIRT, 
	BLOCK_SAND, 
	BLOCK_STONE, 
	BLOCK_WOOD, 
	BLOCK_LEAVES, 
	BLOCK_SNOW, 
	BLOCK_BRIGHT_STONE, 
	BLOCK_BRICK
};

class CBlockUsual : public CBlock
{
public:

	CBlockUsual();
	
	virtual void GetName			(byte Attributes, CString& Name) const;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;

};

#endif