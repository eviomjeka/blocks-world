﻿#ifndef CBLOCKDOOR
#define CBLOCKDOOR

#include "../BlockItemBase/CBlock.h"

class CBlockDoor : public CBlock
{
public:

	CBlockDoor();
	
	virtual void GetName		(byte Attributes, CString& Name) const;
	virtual void Render			(byte Attributes, CBlockManagerReadOnly* BlockManager, 
								 SIVector Position, CVertexListener* VertexListener) const;
	virtual bool RenderPreview	(byte Attributes, SVector2D<byte>& TextureOffset) const;
	virtual void OnAdd			(byte Attributes, CBlockManager* BlockManager, 
								 SIVector Position, byte Face, SFVector2D Angle) const;
	virtual void OnRemove		(byte Attributes, CBlockManager* BlockManager, SIVector Position) const;
	virtual bool OnInteraction	(byte Attributes, CBlockManager* BlockManager, SIVector Position, 
								 byte Face, SFVector2D Angle, CItemData Item) const;

};

#endif