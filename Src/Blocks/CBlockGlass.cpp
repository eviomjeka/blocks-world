﻿#include "CBlockGlass.h"
#include "../CApplication.h"

CBlockGlass::CBlockGlass()
{
	SetType_ (BLOCKTYPE_BLOCK);
	SetLightAttenuation_ (0);
	SetFaceType_ (FACETYPE_NOT_NORMAL);
	SetBlockFaceTextureOffset_ (SVector2D<byte> (0, 2));
}

void CBlockGlass::GetName (byte, CString& Name) const
{
	Name = "glass";
}