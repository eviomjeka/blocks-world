﻿#include "CBlockStairs.h"
#include "../CApplication.h"

CBlockStairs::CBlockStairs()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);
	SetLightAttenuation_ (1);

	SetFaceType_ (FACETYPE_NOT_NORMAL);
	SetFaceTypeToFace_ (LYFACE_MASK, FACETYPE_NORMAL);
	SetFaceTypeToFace_ (HXFACE_MASK, FACETYPE_NORMAL);
	
	SetRotate_ (ROTATEY_NONE, 0, 16);
	SetRotate_ (ROTATEY_CW90, 16, 32);
	SetRotate_ (ROTATEY_180, 32, 48);
	SetRotate_ (ROTATEY_CCW90, 48, 64);
}

void CBlockStairs::GetName (byte Attributes, CString& Name) const
{
	Name.Print ("stairs (%s)", AttributesToColor (Attributes % 16));
}

void CBlockStairs::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	for (int i = 0; i < 16; i++)
		InventoryItems.Insert ((byte) i);
}

void CBlockStairs::GetAABBList (byte, SIVector, CArray<SBlockAABB>& AABBList) const
{
	SBlockAABB AABB = {};
	AABB.Type = BLOCK_AABB_SELECTION_AND_PHYSICS;

	AABB.Start = SFVector (0.0f, 0.0f, 0.0f);
	AABB.End = SFVector (0.5f, 0.5f, 1.0f);
	AABBList.Insert (AABB);

	AABB.Start = SFVector (0.5f, 0.0f, 0.0f);
	AABB.End = SFVector (1.0f, 0.5f, 1.0f);
	AABBList.Insert (AABB);

	AABB.Start = SFVector (0.5f, 0.5f, 0.0f);
	AABB.End = SFVector (1.0f, 1.0f, 1.0f);
	AABBList.Insert (AABB);
}

void CBlockStairs::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, 
						   SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	const SFVector Shift[3] = { SFVector (0.0f, 0.0f, 0.0f), SFVector (0.5f, 0.0f, 0.0f), SFVector (0.5f, 0.5f, 0.0f) };
	for (int n = 0; n < 3; n++)
	{
		for (int i = 0; i < NUM_FACES; i++)
		{
			byte Face = IndexToFace ((byte) i);

			SVector2D<unsigned short> TextureCoord[4] = {};
			GetTextureCoordsForFace (TextureCoord, Face);

			if (n == 0 && Face == HXFACE_MASK)
				continue;
			if (n == 1 && (Face == LXFACE_MASK || Face == HYFACE_MASK))
				continue;
			if (n == 2 && Face == LYFACE_MASK)
				continue;

			SIVector NeighbourPosition = FaceToVector (Position, Face);
			CBlockData Block = RotateGetBlock_ (Attributes, BlockManager, Position, NeighbourPosition);
			
			if (Block.FaceType (InvertFace (Face)) == FACETYPE_NORMAL && 
				!(n == 0 && Face == HYFACE_MASK) && !(n == 2 && Face == LXFACE_MASK))
				continue;

			GetFace (Face, v);
			
			SVector2D<byte> TextureCoord0 (Attributes % 16, 1);

			const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
			for (int j = 0; j < 6; j++)
			{
				int VertexIndex = VertexIndexes[j];
				SFVector VertexPositon = (SFVector) v[VertexIndex];
				VertexPositon.x /= 2.0f;
				VertexPositon.y /= 2.0f;
				VertexPositon += Shift[n] + Position;
				VertexListener->AddVertex (VertexPositon, TextureCoord0, TextureCoord[VertexIndex] * 256);
			}
		}
	}
}

void CBlockStairs::OnAdd (byte Attributes, CBlockManager* BlockManager, 
						  SIVector Position, byte, SFVector2D Rotate) const
{
	if (CanAdd_ (Attributes, BlockManager, Position, true))
	{
		byte RotateFace = GetRotateYFace (Rotate);
		byte RotateBlock = 0;

		switch (RotateFace)
		{
			case LXFACE_MASK: RotateBlock = 0; break;
			case HXFACE_MASK: RotateBlock = 2; break;
			case LZFACE_MASK: RotateBlock = 3; break;
			case HZFACE_MASK: RotateBlock = 1; break;
		}

		BlockManager->SetBlock (Position, CBlockData (ID(), Attributes + 16 * RotateBlock));
	}
}