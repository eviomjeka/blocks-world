﻿#include "CBlockCabel.h"
#include "../CApplication.h"

CBlockCabel::CBlockCabel()
{
	SetType_ (BLOCKTYPE_COMPLEX_BLOCK);
	SetRotate_ (ROTATEX_NONE | ROTATEY_NONE | ROTATEZ_NONE);
	SetFaceType_ (FACETYPE_NOT_NORMAL);
	SetLightAttenuation_ (1);
	SetLightIntensity_ (0, 0);
	SetLightIntensity_ (12, 1);
}

void CBlockCabel::GetName (byte Attributes, CString& Name) const
{
	if (Attributes == 0)
		Name = "cabel";
	else
		Name = "cabel_with_electric_current";
}

void CBlockCabel::Render (byte Attributes, CBlockManagerReadOnly* BlockManager, 
						  SIVector Position, CVertexListener* VertexListener) const
{
	SIVector v[4] = {};

	byte Mask = 0;

	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);

		SIVector NeighbourPosition = FaceToVector (Position, Face);
		CBlockData Block = BlockManager->GetBlock (NeighbourPosition);
		if (Block.ID() == BlocksList.Cabel.ID())
			Mask |= Face;
	}

	for (int n = 0; n < 7; n++)
	{
		byte CabelFace = (n == 6) ? 0 : IndexToFace ((byte) n);

		if (!(Mask & CabelFace) && n != 6)
			continue;

		for (int i = 0; i < NUM_FACES; i++)
		{
			byte Face = IndexToFace ((byte) i);

			SVector2D<unsigned short> TextureCoord[4] = {};
			GetTextureCoordsForFace (TextureCoord, Face);

			SIVector NeighbourPosition = FaceToVector (Position, Face);
			CBlockData Block = BlockManager->GetBlock (NeighbourPosition);

			GetFace (Face, v);
				
			SVector2D<byte> TextureCoord0 (6, 2);

			const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
			for (int j = 0; j < 6; j++)
			{
				int VertexIndex = VertexIndexes[j];
				SFVector VertexPositon = (SFVector) v[VertexIndex];
				SIVector IVertexPositon = v[VertexIndex];
				VertexPositon /= 8.0f;
				VertexPositon += SFVector ((16.0f - 2.0f) / 2.0f / 16.0f);

				
#define CABEL_LOW_FACE(Face,Coord)										\
	if (CabelFace == Face)												\
	{																	\
		if (IVertexPositon.Coord == 0 && (Mask & Face))					\
			VertexPositon.Coord = 0.0f;									\
		else if (Mask & Face)											\
			VertexPositon.Coord = (16.0f - 2.0f) / 2.0f / 16.0f;		\
	}

#define CABEL_HIGH_FACE(Face,Coord)										\
	if (CabelFace == Face)												\
	{																	\
		if (IVertexPositon.Coord == 0 && Mask & Face)					\
			VertexPositon.Coord = (16.0f + 2.0f) / 2.0f / 16.0f;		\
		else if (Mask & Face)											\
			VertexPositon.Coord = 1.0f;									\
	}
				if (n != 6)
				{
					CABEL_LOW_FACE (LXFACE_MASK, x);
					CABEL_LOW_FACE (LYFACE_MASK, y);
					CABEL_LOW_FACE (LZFACE_MASK, z);
					
					CABEL_HIGH_FACE (HXFACE_MASK, x);
					CABEL_HIGH_FACE (HYFACE_MASK, y);
					CABEL_HIGH_FACE (HZFACE_MASK, z);
				}

#undef CABEL_LOW_FACE
#undef CABEL_HIGH_FACE

				VertexPositon += Position;

				SVector2D<unsigned short> ReadyTextureCoord = TextureCoord[VertexIndex] * 256 / 4;
				if (Attributes != 0)
					ReadyTextureCoord.y += 256 / 2;

				VertexListener->AddVertex (VertexPositon, TextureCoord0, ReadyTextureCoord);
			}
		}
	}
}