﻿#ifndef CBLOCKAIR
#define CBLOCKAIR

#include "../BlockItemBase/CBlock.h"

class CBlockAir : public CBlock
{
public:
	
	CBlockAir();

	virtual void GetName	(byte Attributes, CString& Name) const;

};

#endif