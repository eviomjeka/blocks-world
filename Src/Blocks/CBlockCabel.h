﻿#ifndef CBLOCKCABEL
#define CBLOCKCABEL

#include "../BlockItemBase/CBlock.h"

class CBlockCabel : public CBlock
{
public:
	
	CBlockCabel();
	
	virtual void GetName			(byte Attributes, CString& Name) const;
	virtual void Render				(byte Attributes, CBlockManagerReadOnly* BlockManager, 
									 SIVector Position, CVertexListener* VertexListener) const;

};

#endif