﻿#include "CSky.h"
#include "CApplication.h"

const float CSky::TURBIDITY = 10.0f;
const float CSky::COLOR_EXP = 20.0f;

void CSky::Init()
{
	CPngImage::Instance.LoadFromFile ((const char*) "Sun.png", &SunTexture_);
	CPngImage::Instance.LoadFromFile ((const char*) "Moon.png", &MoonTexture_);
	CPngImage::Instance.LoadFromFile ((const char*) "Star.png", &StarTexture_);

	CSkyMeshConstructor SkyMeshConstructor;
	SkyMeshConstructor.Init (&CShaders::SkydomeShader, SKYBOX_MESH_SIZE);
	SunMoonMeshConstructor_.Init (&CShaders::TextureShader, 6);
	StarsMeshConstructor_.Init (&CShaders::TextureShader, 6 * NUM_STARS);

	SkyMeshConstructor.Begin (&SkydomeMesh_);

	float R = 1800, s1 = 50, s2 = 50;
	
	SFVector Position[2] = {};

	for (int i = 0; i < s1; i++)
	{
		SkyMeshConstructor.BeginTriangleStrip();		

		for (int j = 0; j <= s2; j++)
		{
			Position[0] = SFVector (
				R * sin(-2.0f * PI * j / s2) * sin(PI * i / s1), 
				R * cos(-2.0f * PI * j / s2) * sin(PI * i / s1), 
				R * cos(PI * i / s1));

			Position[1] = SFVector (
				R * sin(-2.0f * PI * j / s2) * sin(PI * (i + 1) / s1), 
				R * cos(-2.0f * PI * j / s2) * sin(PI * (i + 1) / s1), 
				R * cos(PI * (i + 1) / s1));
			
			SkyMeshConstructor.AddTriangleStrip (Position[0]);
			SkyMeshConstructor.AddTriangleStrip (Position[1]);
		}
	}

	SkyMeshConstructor.End (&SkydomeMesh_);

	CRandom Random;
	StarsPosition_.Resize (NUM_STARS);
	int j = 0;
	for (int i = 0; i < NUM_STARS; i++, j++)
	{
		SIVector RandVector (Random (-1000, 1000), Random (-1000, 1000), Random (-1000, 1000));
		float SqRandVectorSize = (float) RandVector.SqLen();
		if (SqRandVectorSize > 1000.0f * 1000.0f)
		{
			i--;
			continue;
		}
		float RandVectorSize = sqrt (SqRandVectorSize);
		SFVector StarPosition (1680.0f * RandVector.x / RandVectorSize, 
							   1680.0f * RandVector.y / RandVectorSize, 
							   1680.0f * RandVector.z / RandVectorSize);
		if (StarPosition.y >= 1660 || StarPosition.y <= -1660)
		{
			i--;
			continue;
		}
		StarsPosition_.Insert (StarPosition);
	}
	
	UseShaderProgram (TextureShader);
	CShaders::TextureShader.UniformInt (CShaders::Texture.Texture(), 0);

	UseShaderProgram (SkydomeShader);
	CShaders::SkydomeShader.UniformFloat (CShaders::Skydome.Turbidity(), TURBIDITY);
	CShaders::SkydomeShader.UniformFloat (CShaders::Skydome.ColorExp(), COLOR_EXP);
}

void CSky::Destroy()
{
	SunTexture_.Destroy();
	MoonTexture_.Destroy();
	StarTexture_.Destroy();

	SkydomeMesh_.Destroy();
	StarsMesh_.Destroy();
	SunMoonMesh_.Destroy();
}

SFVector CSky::SunPosition()
{
	return SunPosition_;
}

void CSky::Render (int Time)
{
	float TimeCoef = Time / (float) DAY_DURATION;
	float DayCoef = 1.0f;
	if		(TimeCoef >= 0.25f && TimeCoef < 0.35f) DayCoef = 1.0f - (TimeCoef - 0.25f) / 0.1f;
	else if (TimeCoef >= 0.35f && TimeCoef < 0.75f) DayCoef = 0.0f;
	else if (TimeCoef >= 0.75f && TimeCoef < 0.85f) DayCoef = 1.0f - (0.85f - TimeCoef) / 0.1f;

	float SunCoef = 1.0f;
	if		(TimeCoef >= 0.15f && TimeCoef < 0.25f) SunCoef = 1.0f - (TimeCoef - 0.15f) / 0.1f;
	else if (TimeCoef >= 0.25f && TimeCoef < 0.85f) SunCoef = 0.0f;
	else if (TimeCoef >= 0.85f && TimeCoef < 0.95f) SunCoef = 1.0f - (0.95f - TimeCoef) / 0.1f;

	float StarsCoef = 1.0f;
	if		(TimeCoef >= 0.35f && TimeCoef < 0.45f) StarsCoef = 1.0f - (TimeCoef - 0.35f) / 0.1f;
	else if (TimeCoef >= 0.4f && TimeCoef < 0.67f)  StarsCoef = 0.0f;
	else if (TimeCoef >= 0.67f && TimeCoef < 0.77f) StarsCoef = 1.0f - (0.77f - TimeCoef) / 0.1f;

	float SunTime = Time - DAY_DURATION / 16.0f;
	if (SunTime < 0) SunTime += DAY_DURATION;
	float WorldAngle = SunTime * 2.0f * PI / DAY_DURATION;
	SFVector SunPosition (1500.0f * sin (WorldAngle), 1500.0f * cos (WorldAngle), 0.0f);
	SFVector MoonPosition (1500.0f * sin (WorldAngle + PI), 1500.0f * cos (WorldAngle + PI), 0.0f);

	float SunTimeForSkydome = SunTime;
	if (SunTimeForSkydome / DAY_DURATION >= 0.21f && SunTimeForSkydome / DAY_DURATION <= 0.29f)
		SunTimeForSkydome = DAY_DURATION * 0.21f;
	else if (SunTimeForSkydome / DAY_DURATION >= 0.29f && SunTimeForSkydome / DAY_DURATION <= 0.34f)
		SunTimeForSkydome -= DAY_DURATION * 0.08f;

	if (SunTimeForSkydome / DAY_DURATION >= 0.7f && SunTimeForSkydome / DAY_DURATION <= 0.75f)
		SunTimeForSkydome += DAY_DURATION * 0.05f;
	else if (SunTimeForSkydome / DAY_DURATION >= 0.75f && SunTimeForSkydome / DAY_DURATION <= 0.80f)
		SunTimeForSkydome = DAY_DURATION * 0.8f;

	float WorldAngleForSkydome = SunTimeForSkydome * 2.0f * PI / DAY_DURATION;
	SunPosition_ (1500.0f * sin (WorldAngleForSkydome), 0, 1600.0f * cos (WorldAngleForSkydome));
	
	UseShaderProgram (TextureShader);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	PLAYERCONTROLLER.SetPlayerModelViewMatrix (false);
	UseShaderProgram (SkydomeShader);
	MATRIXENGINE.Apply (&CShaders::SkydomeShader);
	CShaders::SkydomeShader.UniformVector3D (CShaders::Skydome.SunPosition(), SunPosition_);
	glDisable (GL_CULL_FACE);
	
	SkydomeMesh_.Render();

	glEnable (GL_CULL_FACE);
	MATRIXENGINE.PopModelViewMatrix();

	if (DayCoef < 1.0f)
	{
		MATRIXENGINE.PushModelViewMatrix();
		MATRIXENGINE.ResetModelViewMatrix();
		PLAYERCONTROLLER.SetPlayerModelViewMatrix (false);
		MATRIXENGINE.Translate (0, -100, 0);
		MATRIXENGINE.RotateZ (-WorldAngle * 180.0f / PI);

		StarsMeshConstructor_.Begin (&StarsMesh_);
		for (int i = 0; i < NUM_STARS; i++)
		{
			CSprite::ToMeshConstructor (&MATRIXENGINE, &StarsMeshConstructor_, StarsPosition_[i], 2.0f, 
										SColor (255, 255, 255, (byte) (255.0f * (1.0f - StarsCoef))), 
										SFVector2D (0.0f, 0.0f), SFVector2D (1.0f, 1.0f));
		}
		StarsMeshConstructor_.End (&StarsMesh_);

		UseShaderProgram (TextureShader);
		MATRIXENGINE.Apply (&CShaders::TextureShader);
		StarTexture_.Bind();

		StarsMesh_.Render();
		
		MATRIXENGINE.PopModelViewMatrix();
	}

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	MATRIXENGINE.RotateX (90);
	MATRIXENGINE.Translate (0, -100, 0);
	MATRIXENGINE.RotateZ ((WorldAngle) * 180.0f / PI + 180.0f);
	SunMoonMeshConstructor_.Begin (&SunMoonMesh_);
	SColor SunColor = SColor (255, (byte) (140.0f + (255.0f - 140.0f) * SunCoef), (byte) (70.0f + (255.0f - 70.0f) * SunCoef));

	CSprite::ToMeshConstructor (&MATRIXENGINE, &SunMoonMeshConstructor_, SunPosition, 400.0f, 
								SunColor, SFVector2D (0.0f, 0.0f), SFVector2D (1.0f, 1.0f));

	SunMoonMeshConstructor_.End (&SunMoonMesh_);
	MATRIXENGINE.PopModelViewMatrix();
	
	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	PLAYERCONTROLLER.SetPlayerModelViewMatrix (false);
	UseShaderProgram (TextureShader);
	MATRIXENGINE.Apply (&CShaders::TextureShader);
	SunTexture_.Bind();

	SunMoonMesh_.Render();
	
	MATRIXENGINE.PopModelViewMatrix();

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	MATRIXENGINE.RotateX (90);
	MATRIXENGINE.Translate (0, -100, 0);
	MATRIXENGINE.RotateZ ((WorldAngle) * 180.0f / PI);
	SunMoonMeshConstructor_.Begin (&SunMoonMesh_);

	CSprite::ToMeshConstructor (&MATRIXENGINE, &SunMoonMeshConstructor_, MoonPosition, 100.0f, 
					SColor (255, 255, 255), SFVector2D (0.0f, 0.0f), SFVector2D (1.0f, 1.0f));

	SunMoonMeshConstructor_.End (&SunMoonMesh_);
	MATRIXENGINE.PopModelViewMatrix();
	
	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	PLAYERCONTROLLER.SetPlayerModelViewMatrix (false);
	MATRIXENGINE.Apply (&CShaders::TextureShader);
	MoonTexture_.Bind();

	SunMoonMesh_.Render();
	
	MATRIXENGINE.PopModelViewMatrix();

	glDisable (GL_BLEND);
}