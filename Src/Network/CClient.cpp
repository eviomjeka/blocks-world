﻿#include "CClient.h"
#include "../CApplication.h"

#define JOIN_NO_SERVER		1
#define JOIN_SERVER_FULL	2

CClient::CClient() :
Buffer_(CLIENT_SERVER_BUFFER_SIZE),
PlayerStatePacketNumber_(-1),
LaunchTime_(-1),
LastServerPacket_(-1),
Initialized_(false)
{
}

void CClient::Init(const char* IP, unsigned short TCPPort)
{
	PlayerStatePacketNumber_ = 0;
	LastServerPacket_ = -1;
	LaunchTime_ = 0;

	TCPSocket_.Init(IP, TCPPort);
	UDPSocket_.InitClient();

	SClientJoinPacket ClientJoinPacket;
	ClientJoinPacket.ID = -1;
	Buffer_.Clear();
	Buffer_ << ClientJoinPacket;
	TCPSocket_.Send(Buffer_);

	SServerJoinPacket ServerJoinPacket;
	TCPSocket_ >> ServerJoinPacket;
	PlayerID_ = ServerJoinPacket.PlayerID;

	World_.Init(ServerJoinPacket.WorldParams);

	TCPSocket_ >> World_.Players_;
	SSetTimePacket SetTimePacket;
	TCPSocket_ >> SetTimePacket;
	PLAYERCONTROLLER.SetTime (SetTimePacket.PlayerID, SetTimePacket.Time);

	ServerAddress_ = CreateTSocketAddress(IP, ServerJoinPacket.UDPPort);

	int Controlled = -1;
	for (int i = 0; i < World_.Players_.Size(); i++)
		if (World_.Players_[i].ID_ == PlayerID_)
			Controlled = i;

	assert (Controlled != -1);
	PLAYERCONTROLLER.SetControlled (Controlled);

	char MapDirectory[MAX_PATH] = "";
	sprintf(MapDirectory, "WorldsCache/%s", ServerJoinPacket.ServerName.Data());
	CFileSystem::CreateDir(MapDirectory);
	MapLoader_.Init(IP, ServerJoinPacket.MapLoaderPort, &World_, MapDirectory, SIVector2D(0), SETTINGS.SightRange + 2);

	Initialized_ = true;
}

void CClient::Destroy()
{
	if (!Initialized_)
		return;

	MapLoader_.Destroy();
	World_.Destroy();
	UDPSocket_.Destroy();
	Initialized_ = false;
}

void CClient::Step()
{
	assert(Initialized_);

	PacketStep_();
	MapLoader_.SetPosition(DivXZ2D(FloorVector(PLAYERCONTROLLER.Position()), SUBWORLD_SIZE_XZ));
}

bool CClient::Initialized()
{
	return Initialized_;
}

void CClient::PacketStep_()
{
	ReceiveUDPPackets_();
	ReceiveTCPPackets_();
	SendPlayerState_();
}

//==============================================================================

void CClient::ReceiveUDPPackets_()
{
	TSocketAddress Address;
	
	Buffer_.Clear();
	while (UDPSocket_.Receive(&Address, Buffer_))
	{
		SPlayerStatesPacket PlayerStatesPacket;
		Buffer_ >> PlayerStatesPacket;
		assert(Address == ServerAddress_);
		ProcessPlayerStatesPacket_(PlayerStatesPacket);
		Buffer_.Clear();
	}
}

void CClient::ProcessPlayerStatesPacket_(SPlayerStatesPacket& PlayerStatesPacket)
{
	CPlayer PlayerStateBackup = *PLAYERCONTROLLER.Controlled();

	if (PlayerStatesPacket.Number > LastServerPacket_)
		World_.Players_ = PlayerStatesPacket.PlayerStates;
	
	for (int i = 0; i < World_.Players_.Size(); i++)
		if (World_.Players_[i].ID_ == PlayerID_)
		{
			CPlayer& PlayerState = World_.Players_[i];
			PlayerState = PlayerStateBackup;
			PLAYERCONTROLLER.SetControlled(i);
		}
}

void CClient::SendPlayerState_()
{
	assert(PLAYERCONTROLLER.Controlled());
	SPlayerStatePacket PlayerStatePacket;
	PlayerStatePacket.Number		= PlayerStatePacketNumber_;
	PlayerStatePacket.PlayerState	= *PLAYERCONTROLLER.Controlled();

	Buffer_.Clear();
	Buffer_ << PlayerStatePacket;
	UDPSocket_.Send(&ServerAddress_, Buffer_);
	PlayerStatePacketNumber_++;
}

//==============================================================================

void CClient::ReceiveTCPPackets_()
{
	int PacketType = 0;
	while (TCPSocket_.Receive((byte*) &PacketType, sizeof PacketType))
	{
		switch (PacketType)
		{
		case TCP1_CHAT_MESSAGE_PACKET:
			ChatMessagePacket_();
		break;

		case TCP1_SET_TIME_PACKET:
			SetTimePacket_();
		break;

		case TCP1_FLY_PLAYER_PACKET:
			FlyPlayerPacket_();
		break;

		default:
			error("Unknown packet id");
		break;
		}
	}
}

void CClient::ChatMessagePacket_()
{
	SChatMessagePacket ChatMessagePacket = {};
	TCPSocket_ >> ChatMessagePacket;
	PLAYERCONTROLLER.ChatMessage (ChatMessagePacket.PlayerID, ChatMessagePacket.Message);
}

void CClient::SetTimePacket_()
{
	SSetTimePacket SetTimePacket = {};
	TCPSocket_ >> SetTimePacket;
	PLAYERCONTROLLER.SetTime (SetTimePacket.PlayerID, SetTimePacket.Time);
}

void CClient::FlyPlayerPacket_()
{
	SFlyPlayerPacket FlyPlayerPacket = {};
	TCPSocket_ >> FlyPlayerPacket;
	PLAYERCONTROLLER.FlyPlayer (FlyPlayerPacket.PlayerID, FlyPlayerPacket.FlyMode);
}

//==============================================================================

void CClient::InteractWithBlock(SInteractWithBlockInfo InteractWithBlockInfo)
{
	int PacketType = TCP1_INTERACT_WITH_BLOCK_PACKET;
	SInteractWithBlockPacket InteractWithBlockPacket;
	InteractWithBlockPacket.InteractWithBlockInfo = InteractWithBlockInfo;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << InteractWithBlockPacket;
	TCPSocket_.Send(Buffer_);
}

void CClient::RemoveBlock(SIVector Position)
{
	int PacketType = TCP1_REMOVEBLOCK_PACKET;
	SRemoveBlockPacket RemoveBlockPacket;
	RemoveBlockPacket.Position	= Position;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << RemoveBlockPacket;
	TCPSocket_.Send(Buffer_);
}

void CClient::ChatMessage(CString& Message)
{
	int PacketType = TCP1_CHAT_MESSAGE_PACKET;
	SChatMessagePacket ChatMessagePacket;
	ChatMessagePacket.PlayerID = PlayerID_;
	ChatMessagePacket.Message = Message;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << ChatMessagePacket;
	TCPSocket_.Send(Buffer_);
}

void CClient::SetTime(int Time)
{
	int PacketType = TCP1_SET_TIME_PACKET;
	SSetTimePacket SetTimePacket;
	SetTimePacket.PlayerID = PlayerID_;
	SetTimePacket.Time = Time;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << SetTimePacket;
	TCPSocket_.Send(Buffer_);
}

void CClient::FlyPlayer(int PlayerID, bool FlyMode)
{
	int PacketType = TCP1_FLY_PLAYER_PACKET;
	SFlyPlayerPacket FlyPlayerPacket;
	FlyPlayerPacket.PlayerID = PlayerID;
	FlyPlayerPacket.FlyMode = FlyMode;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << FlyPlayerPacket;
	TCPSocket_.Send(Buffer_);
}

CMapLoader& CClient::MapLoader()
{
	return MapLoader_;
}
