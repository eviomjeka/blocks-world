﻿#ifndef CPLAYER
#define CPLAYER

#include <Engine.h>
#include "../BlockItemBase/BlockItemInclude.h"
#include "../World/CSubWorld.h"

#define PLAYER_MAX_Y_SPEED			50.0f
#define PLAYER_SPEED				7.0f
#define PLAYER_FLY_SPEED			20.0f
#define PLAYER_RADIUS				0.35f // must be <= 0.5f
#define PLAYER_JUMP_VELOCITY		19.0f
#define PLAYER_EYES_HEIGHT			2.6f
#define PLAYER_HEIGHT_FLOAT			2.8f
#define PLAYER_HEIGHT				3
#define PLAYER_INTERACTION_RANGE	6
#define PLAYER_CLIMP_WITHOUT_JUMP	0.5f // must be < 1.0f
#define PLAYER_STUCK_SIZE()			((fabs (Velocity_.y) * PHYSICS_TIME_STEP + 1) / 1000.0f)
// PLAYER_STUCK_SIZE(PLAYER_MAX_Y_SPEED) + MAX_STUCK_SIZE must be <= PLAYER_HEIGHT_FLOAT

class CPlayerModel;

#define NUM_MOVE_DIRECTIONS 6
enum MoveDirection
{
	MOVE_DIRECTION_LOW_X, 
	MOVE_DIRECTION_HIGH_X, 
	MOVE_DIRECTION_LOW_Z, 
	MOVE_DIRECTION_HIGH_Z, 
	MOVE_DIRECTION_LOW_Y, 
	MOVE_DIRECTION_HIGH_Y
};

class CPlayer
{
public://private: TODO: fix

	int				ID_;
	SFVector		Position_;
	SFVector		Velocity_;
	SFVector2D		Orientation_;
	bool			FlyMode_;
	bool			BlockUnder_;
	bool			PrevBlockUnder_;
	bool			IsStuck_;
	bool			CanJump_;
	CItemData		ItemData_;
	bool			Interacted_;
	bool			Removed_;
	
	inline bool CheckCollision_ (bool IsMoveY);
	inline bool CheckLowYCollision_ (bool IsMoveY, bool* IsNewPosition, float* NewPositionY);
	inline bool CheckHighYCollision_ (bool IsMoveY);

	inline bool IsCollisionAtHeight_(int Height, bool IsHighCollisionY = true, float* CollisionY = NULL);
	inline void MoveCheckCollision_(SFVector Delta);
	inline void MoveCheckCollisionY_(float y);
	inline bool CollisionWithAABB_ (SFVector Start, SFVector End, SIVector Delta, bool IsHighCollisionY, float* CollisionY);

public:

	void Init(int ID,  SFVector Position = SFVector (0.0f, 2 * SUBWORLD_SIZE_Y + 20, 0.0f) /* TODO: remove this */);
	void SetFlyMode(bool FlyMode);
	void Rotate(SFVector2D Delta);
	void PhysicsStep(int Time);
	void Jump();

	friend class CPlayerController;
	friend CStream& operator <<(CStream& Stream, CPlayer& Player);
	friend CStream& operator >>(CStream& Stream, CPlayer& Player);
};

CStream& operator <<(CStream& Stream, CPlayer& Player);
CStream& operator >>(CStream& Stream, CPlayer& Player);

#endif
