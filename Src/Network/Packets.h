﻿#ifndef PACKETS
#define PACKETS

#include <Engine.h>
#include "../BlockItemBase/BlockItemInclude.h"
#include "../World/CWorld.h"
#include "CPlayer.h"

#define CLIENT_SERVER_BUFFER_SIZE				(2 << 20)
#define CLIENT_SERVER_SUBWORLD_VERSIONS_ROWS	256

enum UDPPackets
{
	UDP_POSITION_PACKET = 1
};

enum TCP1Packets
{
	TCP1_INTERACT_WITH_BLOCK_PACKET = 1, 
	TCP1_REMOVEBLOCK_PACKET, 
	TCP1_CHAT_MESSAGE_PACKET, 
	TCP1_SET_TIME_PACKET, 
	TCP1_FLY_PLAYER_PACKET
};

enum TCP2Packets
{
	TCP2_WORLDPATCH_PACKET = 1, 
	TCP2_SUBWORLD_PACKET
};

struct SServerJoinPacket
{
	int					PlayerID;
	unsigned short		UDPPort;
	unsigned short		MapLoaderPort;
	SWorldParams		WorldParams;
	CString				ServerName;
};

struct SClientJoinPacket
{
	int					ID;
};

struct SPlayerStatePacket
{
	int					Number;
	CPlayer				PlayerState;
};

struct SClientMapLoaderJoinPacket
{
	int					PlayerID;
	int					SightRange;
	CArray<int>			PlayerSubWorldVersions;
};

struct SInteractWithBlockPacket
{
	SInteractWithBlockInfo	InteractWithBlockInfo;
};

struct SRemoveBlockPacket
{
	SIVector			Position;
};

struct SPositionPacket
{
	CArray<int>			PlayerSubWorldVersions;
	SIVector2D			Position;
};

struct SPlayerStatesPacket
{
	int					Number;
	CArray<CPlayer>		PlayerStates;
};

struct SSubWorldPacketHeader
{
	SIVector2D			Position;
	int					Version;
	int					Size;
};

struct SWorldPatchPacket
{
	CArray<SInteractWithBlockInfo>	InteractWithBlockInfoItems;
	CArray<SIVector>				RemovedBlocks;
};

struct SChatMessagePacket
{
	int					PlayerID;
	CString				Message;
};

struct SSetTimePacket
{
	int					PlayerID;
	int					Time;
};

struct SFlyPlayerPacket
{
	int					PlayerID;
	bool				FlyMode;
};

CStream& operator >>(CStream& Stream, SServerJoinPacket&			ServerJoinPacket);
CStream& operator >>(CStream& Stream, SClientJoinPacket&			ClientJoinPacket);
CStream& operator >>(CStream& Stream, SPlayerStatePacket&			PlayerStatePacket);
CStream& operator >>(CStream& Stream, SInteractWithBlockPacket&		InteractWithBlockPacket);
CStream& operator >>(CStream& Stream, SClientMapLoaderJoinPacket&	ClientMapLoaderJoinPacket);
CStream& operator >>(CStream& Stream, SRemoveBlockPacket&			RemoveBlockPacket);
CStream& operator >>(CStream& Stream, SPositionPacket&				PositionPacket);
CStream& operator >>(CStream& Stream, SPlayerStatesPacket&			PlayerStatesPacket);
CStream& operator >>(CStream& Stream, SSubWorldPacketHeader&		SubWorldPacketHeader);
CStream& operator >>(CStream& Stream, SWorldPatchPacket&			WorldPatchPacket);
CStream& operator >>(CStream& Stream, SChatMessagePacket&			ChatMessagePacket);
CStream& operator >>(CStream& Stream, SSetTimePacket&				SetTimePacket);
CStream& operator >>(CStream& Stream, SFlyPlayerPacket&				FlyPlayerPacket);

CStream& operator <<(CStream& Stream, SServerJoinPacket&			ServerJoinPacket);
CStream& operator <<(CStream& Stream, SClientJoinPacket&			ClientJoinPacket);
CStream& operator <<(CStream& Stream, SPlayerStatePacket&			PlayerStatePacket);
CStream& operator <<(CStream& Stream, SInteractWithBlockPacket&		InteractWithBlockPacket);
CStream& operator <<(CStream& Stream, SClientMapLoaderJoinPacket&	ClientMapLoaderJoinPacket);
CStream& operator <<(CStream& Stream, SRemoveBlockPacket&			RemoveBlockPacket);
CStream& operator <<(CStream& Stream, SPositionPacket&				PositionPacket);
CStream& operator <<(CStream& Stream, SPlayerStatesPacket&			PlayerStatesPacket);
CStream& operator <<(CStream& Stream, SSubWorldPacketHeader&		SubWorldPacketHeader);
CStream& operator <<(CStream& Stream, SWorldPatchPacket&			WorldPatchPacket);
CStream& operator <<(CStream& Stream, SChatMessagePacket&			ChatMessagePacket);
CStream& operator <<(CStream& Stream, SSetTimePacket&				SetTimePacket);
CStream& operator <<(CStream& Stream, SFlyPlayerPacket&				FlyPlayerPacket);

#endif