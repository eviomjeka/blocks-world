﻿#include "Packets.h"

CStream& operator >>(CStream& Stream, SServerJoinPacket&			ServerJoinPacket)
{ Stream >> ServerJoinPacket.PlayerID >> ServerJoinPacket.UDPPort >> ServerJoinPacket.WorldParams >> ServerJoinPacket.MapLoaderPort >> ServerJoinPacket.ServerName; return Stream; }

CStream& operator >>(CStream& Stream, SClientJoinPacket&			ClientJoinPacket)
{ Stream >> ClientJoinPacket.ID; return Stream;}

CStream& operator >>(CStream& Stream, SPlayerStatePacket&			PlayerStatePacket)
{ Stream >> PlayerStatePacket.Number >> PlayerStatePacket.PlayerState; return Stream; }

CStream& operator >>(CStream& Stream, SInteractWithBlockPacket&		InteractWithBlockPacket)
{ Stream >> InteractWithBlockPacket.InteractWithBlockInfo; return Stream; }

CStream& operator >>(CStream& Stream, SClientMapLoaderJoinPacket&	ClientMapLoaderJoinPacket)
{ Stream >> ClientMapLoaderJoinPacket.PlayerID >> ClientMapLoaderJoinPacket.SightRange >> ClientMapLoaderJoinPacket.PlayerSubWorldVersions; return Stream; }

CStream& operator >>(CStream& Stream, SRemoveBlockPacket&			RemoveBlockPacket)
{ Stream >> RemoveBlockPacket.Position; return Stream; }

CStream& operator >>(CStream& Stream, SPositionPacket&				PositionPacket)
{ Stream >> PositionPacket.Position >> PositionPacket.PlayerSubWorldVersions; return Stream; }

CStream& operator >>(CStream& Stream, SPlayerStatesPacket&			PlayerStatesPacket)
{ Stream >> PlayerStatesPacket.Number >> PlayerStatesPacket.PlayerStates; return Stream; }

CStream& operator >>(CStream& Stream, SSubWorldPacketHeader&		SubWorldPacketHeader)
{ Stream  >> SubWorldPacketHeader.Position >> SubWorldPacketHeader.Version >> SubWorldPacketHeader.Size; return Stream; }

CStream& operator >>(CStream& Stream, SWorldPatchPacket&			WorldPatchPacket)
{ Stream >> WorldPatchPacket.InteractWithBlockInfoItems >> WorldPatchPacket.RemovedBlocks; return Stream; }

CStream& operator >>(CStream& Stream, SChatMessagePacket&			ChatMessagePacket)
{ Stream >> ChatMessagePacket.PlayerID >> ChatMessagePacket.Message; return Stream; }

CStream& operator >>(CStream& Stream, SSetTimePacket&			SetTimePacket)
{ Stream >> SetTimePacket.PlayerID >> SetTimePacket.Time; return Stream; }

CStream& operator >>(CStream& Stream, SFlyPlayerPacket&				FlyPlayerPacket)
{ Stream >> FlyPlayerPacket.PlayerID >> FlyPlayerPacket.FlyMode; return Stream; }

//===================================

CStream& operator <<(CStream& Stream, SServerJoinPacket&			ServerJoinPacket)
{ Stream << ServerJoinPacket.PlayerID << ServerJoinPacket.UDPPort << ServerJoinPacket.WorldParams << ServerJoinPacket.MapLoaderPort << ServerJoinPacket.ServerName; return Stream; }

CStream& operator <<(CStream& Stream, SClientJoinPacket&			ClientJoinPacket)
{ Stream << ClientJoinPacket.ID; return Stream; }

CStream& operator <<(CStream& Stream, SPlayerStatePacket&			PlayerStatePacket)
{ Stream << PlayerStatePacket.Number << PlayerStatePacket.PlayerState; return Stream; }

CStream& operator <<(CStream& Stream, SInteractWithBlockPacket&		InteractWithBlockPacket)
{ Stream << InteractWithBlockPacket.InteractWithBlockInfo; return Stream; }

CStream& operator <<(CStream& Stream, SClientMapLoaderJoinPacket&	ClientMapLoaderJoinPacket)
{ Stream << ClientMapLoaderJoinPacket.PlayerID << ClientMapLoaderJoinPacket.SightRange << ClientMapLoaderJoinPacket.PlayerSubWorldVersions; return Stream; }

CStream& operator <<(CStream& Stream, SRemoveBlockPacket&			RemoveBlockPacket)
{ Stream << RemoveBlockPacket.Position; return Stream; }

CStream& operator <<(CStream& Stream, SPositionPacket&				PositionPacket)
{ Stream << PositionPacket.Position << PositionPacket.PlayerSubWorldVersions; return Stream; }

CStream& operator <<(CStream& Stream, SPlayerStatesPacket&			PlayerStatesPacket)
{ Stream << PlayerStatesPacket.Number << PlayerStatesPacket.PlayerStates; return Stream; }

CStream& operator <<(CStream& Stream, SSubWorldPacketHeader&		SubWorldPacketHeader)
{ Stream << SubWorldPacketHeader.Position << SubWorldPacketHeader.Version << SubWorldPacketHeader.Size; return Stream; }

CStream& operator <<(CStream& Stream, SWorldPatchPacket&			WorldPatchPacket)
{ Stream << WorldPatchPacket.InteractWithBlockInfoItems << WorldPatchPacket.RemovedBlocks; return Stream; }

CStream& operator <<(CStream& Stream, SChatMessagePacket&			ChatMessagePacket)
{ Stream << ChatMessagePacket.PlayerID << ChatMessagePacket.Message; return Stream; }

CStream& operator <<(CStream& Stream, SSetTimePacket&				SetTimePacket)
{ Stream << SetTimePacket.PlayerID << SetTimePacket.Time; return Stream; }

CStream& operator <<(CStream& Stream, SFlyPlayerPacket&				FlyPlayerPacket)
{ Stream << FlyPlayerPacket.PlayerID << FlyPlayerPacket.FlyMode; return Stream; }