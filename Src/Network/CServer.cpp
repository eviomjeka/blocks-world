﻿#include "CServer.h"

CServer::CServer() :
Buffer_(CLIENT_SERVER_BUFFER_SIZE),
CompressionBuffer_(1 << 20),
PlayerStatesNumber_(-1),
TCPPort_(0),
MapLoaderPort_(0),
UDPPort_(0),
Time_(0),
NPlayers_(-1),
Initialized_(false),
PlayerSaves_(SERVER_MAX_PLAYERS_REGISTERED),
SubWorldCache_(HashSIVector2D, SERVER_SUBWORLD_CACHE_ROWS),
OpenedChunks_(HashSIVector2D, 1/*WTF?????????????????????????????*/)
{
}

void CServer::Init(const char* Name, const char* MapDirectory, 
				   unsigned short TCPPort, unsigned short MapLoaderPort, unsigned short UDPPort)
{
	PlayerStatesNumber_ = 0;
	NPlayers_			= 0;
	TCPPort_			= TCPPort;
	UDPPort_			= UDPPort;
	MapLoaderPort_		= MapLoaderPort;
	ServerName_			= Name;
	Time_				= 0;
	strcpy(MapDirectory_, MapDirectory);

	LoadState_();

	Players_.Clear();

	CRandom::Srand();
	WorldParams_.Seed = CRandom::RandomIntFromRand();

	WorldGenerator_.Init (WorldParams_);

	TCPSocket_.Init(TCPPort, SERVER_MAX_PLAYERS);
	MapLoaderServer_.Init(MapLoaderPort, SERVER_MAX_PLAYERS);
	UDPSocket_.InitServer(UDPPort);

	Initialized_ = true;
}

bool CServer::Accept_()
{
	CTCPClient Client = TCPSocket_.AcceptClient();
	if (Client.Disconnected())
		return false;
	
	SClientJoinPacket ClientJoinPacket;
	Client >> ClientJoinPacket;

	tPlayer& Player = Players_.Insert()();
	Player.LastPlayerState	= -1;
	Player.TCPSocket		= Client;
	Player.ValidAddress		= false;
	Player.SightRange		= 0;

	if (ClientJoinPacket.ID == -1)
	{
		Player.PlayerState.Init(PlayerSaves_.Size());
		tPlayerSave PlayerSave;
		PlayerSave.Position = Player.PlayerState.Position_;
		PlayerSaves_.Insert(PlayerSave);
	}
	else
	{
		tPlayerSave& PlayerSave = PlayerSaves_[ClientJoinPacket.ID];
		Player.PlayerState.Init(ClientJoinPacket.ID, PlayerSave.Position);
	}

	Player.Position = DivXZ2D(Player.PlayerState.Position_, SUBWORLD_SIZE_XZ);

	SServerJoinPacket ServerJoinPacket;
	ServerJoinPacket.UDPPort		= UDPPort_;
	ServerJoinPacket.MapLoaderPort	= MapLoaderPort_;
	ServerJoinPacket.PlayerID		= Player.PlayerState.ID_;
	ServerJoinPacket.WorldParams	= WorldParams_;
	ServerJoinPacket.ServerName		= ServerName_;

	Buffer_.Clear();
	Buffer_ << ServerJoinPacket;
	Client.Send(Buffer_);

	NPlayers_++;
	
	SSetTimePacket SetTimePacket;
	SetTimePacket.PlayerID = -1;
	SetTimePacket.Time = Time_;

	Buffer_.Clear();
	Buffer_ << NPlayers_;
	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
		Buffer_ << i().PlayerState;

	Buffer_ << SetTimePacket;
	Client.Send(Buffer_);

	LOG ("Client #%d joined the server", Player.PlayerState.ID_ + 1);

	return true;
}

bool CServer::AcceptMapLoaders_()
{
	CTCPClient Client = MapLoaderServer_.AcceptClient();
	if (Client.Disconnected())
		return false;

	SClientMapLoaderJoinPacket ClientMapLoaderJoinPacket;
	Client >> ClientMapLoaderJoinPacket;
	tPlayerIterator i = Players_.End();
	assert (FindPlayer_(ClientMapLoaderJoinPacket.PlayerID, i));
	tPlayer& Player = i();
	Player.MapLoader  = Client;
	Player.SightRange = ClientMapLoaderJoinPacket.SightRange;
	LoadChunks_(Player);
	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
			InitSubWorld_(Player.Position + SIVector2D(x, y));
	SendSubWorlds_(Player, ClientMapLoaderJoinPacket.PlayerSubWorldVersions);

	return true;
}

void CServer::Step (int Time)
{
	assert(Initialized_);
	
	bool SyncTime = Time_ % DAY_DURATION > (Time_ + Time) % DAY_DURATION;
	Time_ += Time;
	while (Time_ > DAY_DURATION) Time_ -= DAY_DURATION;
	
	while (Accept_()) ;
	while (AcceptMapLoaders_()) ;

	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
	{
		if (i().TCPSocket.Disconnected())
		{
			tPlayerIterator j = i;
			i--;
			Disconnect_(j);
		}
		else
		{
			ReceiveTCPPackets_(i());
			ReceiveMapLoaderPackets_(i());
		}
	}

	ReceiveUDPPackets_();
	SendPlayerStates_();
	
	if (SyncTime)
		SyncTime_();
	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
		SendWorldPatch_(i());

	InteractWithBlockInfoItems_.Clear();
	RemovedBlocks_.Clear();
}

void CServer::SendPlayerStates_()
{
	SPlayerStatesPacket PlayerStatesPacket;
	PlayerStatesPacket.Number = PlayerStatesNumber_;
	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
		PlayerStatesPacket.PlayerStates.Insert(i().PlayerState, true);

	Buffer_.Clear();
	Buffer_ << PlayerStatesPacket;

	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
	{
		tPlayer& Player = i();

		if (Player.ValidAddress)
			UDPSocket_.Send(&Player.Address, Buffer_);
	}

	PlayerStatesNumber_++;
}

bool CServer::FindPlayer_(int ID, tPlayerIterator& i)
{
	for (i = Players_.First(); i != Players_.End(); i++)
	{
		if (i().PlayerState.ID_ == ID)
			return true;
	}

	return false;
}

void CServer::Disconnect_(tPlayerIterator PlayerI)
{
	tPlayer& Player = PlayerI();
	LOG ("Client #%d disconnected", Player.PlayerState.ID_ + 1);
	UnloadSubWorlds_(Player);
	UnloadChunks_(Player);
	Player.TCPSocket.Destroy();
	Player.MapLoader.Destroy();
	tPlayerSave PlayerSave;
	PlayerSave.Position = Player.PlayerState.Position_;
	PlayerSaves_[Player.PlayerState.ID_] = PlayerSave;
	Players_.Remove(PlayerI);
	NPlayers_--;
}

void CServer::Destroy()
{
	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i = Players_.First())
		Disconnect_(i);

	TCPSocket_.Destroy();
	UDPSocket_.Destroy();
	MapLoaderServer_.Destroy();

	Initialized_ = false;
}

void CServer::MovePlayer_(tPlayer& Player, SPositionPacket& PositionPacket)
{
		SIVector2D Translation = PositionPacket.Position - Player.Position;
		assert(Translation.SqLen() == 1);

		// TODO: smth faster
		LoadNewChunks_(Player, Player.Position, PositionPacket.Position);

		assert(PositionPacket.PlayerSubWorldVersions.Size() == 2 * Player.SightRange + 1);

		if (!Translation.x)
		{
			for (int x = -1; x <= 1; x++)
			{
				UnloadSubWorld_(PositionPacket.Position + SIVector2D(x, 0) - Translation * 2);
				InitSubWorld_  (PositionPacket.Position + SIVector2D(x, 0) + Translation);
			}
			
			int FirstSubWorld = PositionPacket.Position.x - Player.SightRange;
			int LastSubWorld  = PositionPacket.Position.x + Player.SightRange;
			int y = PositionPacket.Position.y + Player.SightRange * Translation.y;

			for (int x = FirstSubWorld, i = 0; x <= LastSubWorld; x++)
			{
				LOG("PlayerVersion: %d", PositionPacket.PlayerSubWorldVersions[i]);
				SendSubWorld_(Player, SIVector2D(x, y), PositionPacket.PlayerSubWorldVersions[i++]);
			}
		}
		else
		{
			for (int y = -1; y <= 1; y++)
			{
				UnloadSubWorld_(PositionPacket.Position + SIVector2D(0, y) - Translation * 2);
				InitSubWorld_  (PositionPacket.Position + SIVector2D(0, y) + Translation);
			}
			
			int FirstSubWorld = PositionPacket.Position.y - Player.SightRange;
			int LastSubWorld  = PositionPacket.Position.y + Player.SightRange;
			int x = PositionPacket.Position.x + Player.SightRange * Translation.x;

			for (int y = FirstSubWorld, i = 0; y <= LastSubWorld; y++)
			{
				LOG("PlayerVersion %d", PositionPacket.PlayerSubWorldVersions[i]);
				SendSubWorld_(Player, SIVector2D(x, y), PositionPacket.PlayerSubWorldVersions[i++]);
			}
		}
		UnloadOldChunks_(Player, Player.Position, PositionPacket.Position);

		Player.Position = PositionPacket.Position;
}

void CServer::LoadNewChunks_(tPlayer& Player, SIVector2D PrevPosition, SIVector2D NewPosition)
{
	SIVector2D PrevFirstChunk = Div2D(PrevPosition - SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D PrevLastChunk  = Div2D(PrevPosition + SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D NewFirstChunk  = Div2D(NewPosition  - SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D NewLastChunk   = Div2D(NewPosition  + SIVector2D(Player.SightRange), CHUNK_SIZE);

	for (int x = NewFirstChunk.x; x <= NewLastChunk.x; x++)
		for (int y = NewFirstChunk.y; y <= NewLastChunk.y; y++)
		{
			SIVector2D Position(x, y);
			if (!Position.InsideOrOnAABB(PrevFirstChunk, PrevLastChunk))
				LoadChunk_(Position);
		}
}

void CServer::UnloadOldChunks_(tPlayer& Player, SIVector2D PrevPosition, SIVector2D NewPosition)
{
	SIVector2D PrevFirstChunk = Div2D(PrevPosition - SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D PrevLastChunk  = Div2D(PrevPosition + SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D NewFirstChunk  = Div2D(NewPosition  - SIVector2D(Player.SightRange), CHUNK_SIZE);
	SIVector2D NewLastChunk   = Div2D(NewPosition  + SIVector2D(Player.SightRange), CHUNK_SIZE);

	for (int x = PrevFirstChunk.x; x <= PrevLastChunk.x; x++)
		for (int y = PrevFirstChunk.y; y <= PrevLastChunk.y; y++)
		{
			SIVector2D Position(x, y);
			if (!Position.InsideOrOnAABB(NewFirstChunk, NewLastChunk))
				UnloadChunk_(Position);
		}
}

void CServer::SendSubWorld_(tPlayer& Player, SIVector2D Position, int PlayerVersion)
{
	SIVector2D ChunkPosition = Div2D(Position, SUBWORLD_SIZE_XZ);
	CChunk& Chunk = OpenedChunks_(ChunkPosition, true).Data;

	if (Chunk.SubWorldLoaded(Position))
		SendCachedSubWorld_(Player, Position, PlayerVersion);
	else if (Chunk.SubWorldSaved(Position))
		SendSavedSubWorld(Player, Chunk, Position, PlayerVersion);
	else
	{
		assert(!PlayerVersion);
		SendEmptySubWorld_(Player, Position, 0);
	}
}

void CServer::SendCachedSubWorld_(tPlayer& Player, SIVector2D Position, int PlayerVersion)
{
	CSubWorld& SubWorld = SubWorldCache_(Position, true).Data;

	if (PlayerVersion == SubWorld.Version())
	{
		SendEmptySubWorld_(Player, Position, PlayerVersion);
		return;
	}

	Buffer_.Clear();
	Buffer_ << SubWorld;
	CCompressor::Compress(Buffer_, CompressionBuffer_);

	int PacketType = TCP2_SUBWORLD_PACKET;
	SSubWorldPacketHeader SubWorldPacketHeader;
	SubWorldPacketHeader.Position	= Position;
	SubWorldPacketHeader.Version	= SubWorld.Version();
	SubWorldPacketHeader.Size		= CompressionBuffer_.WritePosition();

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << SubWorldPacketHeader;
	Buffer_ << CompressionBuffer_;
	Player.MapLoader.Send(Buffer_);
}

void CServer::SendSavedSubWorld(tPlayer& Player, CChunk& Chunk, SIVector2D Position, int PlayerVersion)
{
	int PacketType = TCP2_SUBWORLD_PACKET;
	SSubWorldPacketHeader SubWorldPacketHeader;
	SubWorldPacketHeader.Position	= Position;
	SubWorldPacketHeader.Version	= Chunk.SubWorldVersion(Position);
	SubWorldPacketHeader.Size		= Chunk.SubWorldDataSize(Position);
	
	if (PlayerVersion == Chunk.SubWorldVersion(Position))
	{
		SendEmptySubWorld_(Player, Position, PlayerVersion);
		return;
	}

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << SubWorldPacketHeader;
	Chunk.LoadSubWorldData(Buffer_, Position);
	Player.MapLoader.Send(Buffer_);
}

void CServer::SendEmptySubWorld_(tPlayer& Player, SIVector2D Position, int Version)
{
	int PacketType = TCP2_SUBWORLD_PACKET;
	SSubWorldPacketHeader SubWorldPacketHeader;
	SubWorldPacketHeader.Position	= Position;
	SubWorldPacketHeader.Version	= Version;
	SubWorldPacketHeader.Size		= 0;

	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << SubWorldPacketHeader;
	Player.MapLoader.Send(Buffer_);
}

void CServer::LoadChunks_(tPlayer& Player)
{
	SIVector2D FirstSubWorld = Player.Position - SIVector2D(Player.SightRange);
	SIVector2D LastSubWorld	 = Player.Position + SIVector2D(Player.SightRange);
	SIVector2D FirstChunk	 = Div2D(FirstSubWorld, CHUNK_SIZE);
	SIVector2D LastChunk	 = Div2D(LastSubWorld,  CHUNK_SIZE);

	for (int x = FirstChunk.x; x <= LastChunk.x; x++)
		for (int y = FirstChunk.y; y <= LastChunk.y; y++)
			LoadChunk_(SIVector2D(x, y));
}

void CServer::UnloadChunks_(tPlayer& Player)
{
	SIVector2D FirstSubWorld = Player.Position - SIVector2D(Player.SightRange);
	SIVector2D LastSubWorld	 = Player.Position + SIVector2D(Player.SightRange);
	SIVector2D FirstChunk	 = Div2D(FirstSubWorld, CHUNK_SIZE);
	SIVector2D LastChunk	 = Div2D(LastSubWorld,  CHUNK_SIZE);

	for (int x = FirstChunk.x; x <= LastChunk.x; x++)
		for (int y = FirstChunk.y; y <= LastChunk.y; y++)
			UnloadChunk_(SIVector2D(x, y));
}

void CServer::UnloadSubWorlds_(tPlayer& Player)
{
	SIVector2D FirstSubWorld = Player.Position - SIVector2D(1);
	SIVector2D LastSubWorld  = Player.Position + SIVector2D(1);

	for (int x = FirstSubWorld.x; x <= LastSubWorld.x; x++)
		for (int y = FirstSubWorld.y; y <= LastSubWorld.y; y++)
			UnloadSubWorld_(SIVector2D(x, y));
}

void CServer::SendSubWorlds_(tPlayer& Player, CArray<int>& PlayerSubWorldVersions)
{
	SIVector2D FirstSubWorld = Player.Position - SIVector2D(Player.SightRange);
	SIVector2D LastSubWorld  = Player.Position + SIVector2D(Player.SightRange);

	int i = 0;
	for (int x = FirstSubWorld.x; x <= LastSubWorld.x; x++)
		for (int y = FirstSubWorld.y; y <= LastSubWorld.y; y++)
			SendSubWorld_(Player, SIVector2D(x, y), PlayerSubWorldVersions[i++]);
}

void CServer::ReceiveMapLoaderPackets_(tPlayer& Player)
{
	int PacketType = 0;
	int BytesReceived = Player.MapLoader.Receive((byte*) &PacketType, sizeof PacketType);
	
	while (BytesReceived)
	{
		assert(BytesReceived == sizeof PacketType);
		assert(PacketType == UDP_POSITION_PACKET);
		
		SPositionPacket PositionPacket;
		Player.MapLoader >> PositionPacket;
		MovePlayer_(Player, PositionPacket);

		BytesReceived = Player.MapLoader.Receive((byte*) &PacketType, sizeof PacketType);
	}
}

void CServer::ReceiveTCPPackets_(tPlayer& Player)
{
	int PacketType = 0;
	int BytesReceived = Player.TCPSocket.Receive((byte*) &PacketType, sizeof PacketType);
	
	while (BytesReceived)
	{
		assert(BytesReceived == sizeof PacketType);

		switch (PacketType)
		{
			case TCP1_INTERACT_WITH_BLOCK_PACKET:
			{
				SInteractWithBlockPacket InteractWithBlockPacket;
				Player.TCPSocket >> InteractWithBlockPacket;
				InteractWithBlock_(InteractWithBlockPacket.InteractWithBlockInfo);
			}
			break;
			case TCP1_REMOVEBLOCK_PACKET:
			{
				SRemoveBlockPacket RemoveBlockPacket;
				Player.TCPSocket >> RemoveBlockPacket;
				RemoveBlock_(RemoveBlockPacket.Position);
			}
			break;
			case TCP1_CHAT_MESSAGE_PACKET:
			{
				SChatMessagePacket ChatMessagePacket;
				Player.TCPSocket >> ChatMessagePacket;
				ChatMessagePacket.PlayerID = Player.PlayerState.ID_;
				ChatMessage_(ChatMessagePacket);
			}
			break;
			case TCP1_SET_TIME_PACKET:
			{
				SSetTimePacket SetTimePacket;
				Player.TCPSocket >> SetTimePacket;
				SetTimePacket.PlayerID = Player.PlayerState.ID_;
				SetTime_(SetTimePacket);
			}
			break;
			case TCP1_FLY_PLAYER_PACKET:
			{
				SFlyPlayerPacket FlyPlayerPacket;
				Player.TCPSocket >> FlyPlayerPacket;
				FlyPlayer_(FlyPlayerPacket);
			}
			break;
			default:
				error("Invalid packet");
			break;
		}

		BytesReceived = Player.TCPSocket.Receive((byte*) &PacketType, sizeof PacketType);
	}
}

void CServer::ReceiveUDPPackets_()
{
	Buffer_.Clear();
	TSocketAddress SenderAddress;
	while (UDPSocket_.Receive(&SenderAddress, Buffer_))
	{
		SPlayerStatePacket PlayerStatePacket;
		Buffer_ >> PlayerStatePacket;
		bool PlayerFound = true;
		for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
		{
			tPlayer& Player = i();

			if (Player.PlayerState.ID_ == PlayerStatePacket.PlayerState.ID_)
			{
				if (!Player.ValidAddress)
				{
					Player.Address = SenderAddress;
					Player.ValidAddress = true;
				}
				else
					assert(Player.Address == SenderAddress);

				if (PlayerStatePacket.Number > Player.LastPlayerState)
				{
					Player.PlayerState = PlayerStatePacket.PlayerState;
				}
				PlayerFound = true;
				break;
			}
		}

		assert(PlayerFound);		
		Buffer_.Clear();
	}
}

void CServer::InteractWithBlock_(SInteractWithBlockInfo& InteractWithBlockInfo)
{
	//TODO: check the possibility
	if (InteractWithBlockInfo.Block != GetBlock (InteractWithBlockInfo.BlockPosition))
		return;

	InteractWithBlockInfoItems_.Insert(InteractWithBlockInfo, true);

	bool Interaction = BlocksList (InteractWithBlockInfo.Block.ID())->OnInteraction 
		(InteractWithBlockInfo.Block.Attributes(), this, InteractWithBlockInfo.BlockPosition, 
		 InteractWithBlockInfo.Face, InteractWithBlockInfo.Angle, InteractWithBlockInfo.Item);

	if (!Interaction && !GetBlock (InteractWithBlockInfo.SelectionPosition).ID())
		ItemsList (InteractWithBlockInfo.Item.ID())->OnAdd (InteractWithBlockInfo.Item.Attributes(), this, 
			InteractWithBlockInfo.SelectionPosition, InteractWithBlockInfo.Face, InteractWithBlockInfo.Angle);
}

void CServer::RemoveBlock_(SIVector Position)
{
	//TODO: check the possibility
	RemovedBlocks_.Insert(Position, true);

	CBlockData Block = GetBlock (Position);
	BlocksList (Block.ID())->OnRemove (Block.Attributes(), this, Position);
}

void CServer::ChatMessage_(SChatMessagePacket& ChatMessagePacket)
{
	if (ChatMessagePacket.PlayerID != -1)
		LOG ("Server: client #%d sent the message \"%s\"", ChatMessagePacket.PlayerID, ChatMessagePacket.Message.Data());
	else
		LOG ("Server: server sent message \"%s\"", ChatMessagePacket.Message.Data());
	
	int PacketType = TCP1_CHAT_MESSAGE_PACKET;
	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << ChatMessagePacket;

	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
	{
		tPlayer& Player = i();
		Player.TCPSocket.Send(Buffer_);
	}
}

void CServer::SendMessageToChat_(CString& String)
{
	SChatMessagePacket ChatMessagePacket;
	ChatMessagePacket.PlayerID = -1;
	ChatMessagePacket.Message = String;

	ChatMessage_ (ChatMessagePacket);
}

void CServer::SetTime_ (SSetTimePacket& SetTimePacket)
{
	LOG ("Server: server sent time to clients");
	
	Time_ = SetTimePacket.Time;

	int PacketType = TCP1_SET_TIME_PACKET;
	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << SetTimePacket;

	for (tPlayerIterator i = Players_.First(); i != Players_.End(); i++)
	{
		tPlayer& Player = i();
		Player.TCPSocket.Send(Buffer_);
	}
}

void CServer::SyncTime_()
{
	SSetTimePacket SetTimePacket;
	SetTimePacket.PlayerID = -1;
	SetTimePacket.Time = Time_;
	SetTime_ (SetTimePacket);
}

void CServer::FlyPlayer_(SFlyPlayerPacket& FlyPlayerPacket)
{
	tPlayerIterator i = Players_.First();
	if (!FindPlayer_ (FlyPlayerPacket.PlayerID, i))
		return;

	LOG ("Server: server sent player fly info to clients");
	
	tPlayer& Player = i();
	Player.PlayerState.SetFlyMode (FlyPlayerPacket.FlyMode);

	int PacketType = TCP1_FLY_PLAYER_PACKET;
	Buffer_.Clear();
	Buffer_ << PacketType;
	Buffer_ << FlyPlayerPacket;

	for (i = Players_.First(); i != Players_.End(); i++)
	{
		tPlayer& Player = i();
		Player.TCPSocket.Send(Buffer_);
	}
}

void CServer::SendWorldPatch_(tPlayer& Player)
{
	int PacketType = TCP2_WORLDPATCH_PACKET;
	SWorldPatchPacket WorldPatchPacket;

	SIVector2D PlayerLo = Player.Position - SIVector2D(Player.SightRange);
	SIVector2D PlayerHi = Player.Position + SIVector2D(Player.SightRange);

	for (int i = 0; i < InteractWithBlockInfoItems_.Size(); i++)
	{
		SInteractWithBlockInfo InteractWithBlockInfo = InteractWithBlockInfoItems_[i];
		SIVector2D BlockSubWorldPosition = DivXZ2D(InteractWithBlockInfo.BlockPosition, SUBWORLD_SIZE_XZ);

		if (BlockSubWorldPosition.InsideOrOnAABB(PlayerLo, PlayerHi))
			WorldPatchPacket.InteractWithBlockInfoItems.Insert(InteractWithBlockInfo, true);
	}
	for (int i = 0; i < RemovedBlocks_.Size(); i++)
	{
		SIVector RemovedBlock = RemovedBlocks_[i];
		SIVector2D BlockSubWorldPosition = DivXZ2D(RemovedBlock, SUBWORLD_SIZE_XZ);

		if (BlockSubWorldPosition.InsideOrOnAABB(PlayerLo, PlayerHi))
			WorldPatchPacket.RemovedBlocks.Insert(RemovedBlock, true);
	}

	if (WorldPatchPacket.InteractWithBlockInfoItems.Size() || WorldPatchPacket.RemovedBlocks.Size())
	{
		Buffer_.Clear();
		Buffer_ << PacketType;
		Buffer_ << WorldPatchPacket;
		Player.MapLoader.Send(Buffer_);
	}
}

void CServer::InitSubWorld_(SIVector2D Position)
{
	tCachedSubWorld& SubWorld = SubWorldCache_(Position);
	if (!SubWorld.References)
	{
		SIVector2D ChunkPosition = Div2D(Position, CHUNK_SIZE);
		CChunk& Chunk = OpenedChunks_(ChunkPosition, true).Data;

		Chunk.LoadSubWorld(WorldGenerator_, SubWorld.Data, Position, Buffer_, CompressionBuffer_);
	}

	SubWorld.References++;
}

void CServer::UnloadSubWorld_(SIVector2D Position)
{
	tCachedSubWorld& SubWorld = SubWorldCache_(Position, true);
	if (!--SubWorld.References)
	{
		//TODO: save only if changes exist
		SaveSubWorld_(SubWorld.Data);
		SubWorld.Data.Destroy();
		SubWorldCache_.Remove(Position);
	}
}

void CServer::SaveSubWorld_(CSubWorld& SubWorld)
{
	SIVector2D ChunkPosition = Div2D(SubWorld.Position(), CHUNK_SIZE);
	
	CChunk& Chunk = OpenedChunks_(ChunkPosition, true).Data;
	Chunk.SaveSubWorld(SubWorld, Buffer_, CompressionBuffer_);
}

void CServer::LoadState_()
{
	char PlayerSavesFileName[MAX_PATH];
	sprintf(PlayerSavesFileName, "%s/PlayerSaves.bwsp", MapDirectory_);

	CFile PlayerSavesFile(PlayerSavesFileName, CFILE_READ | CFILE_BINARY, false);
	if (PlayerSavesFile)
	{
		PlayerSavesFile >> PlayerSaves_;
		PlayerSavesFile.Destroy();
	}
}

void CServer::SaveState_()
{
	char PlayerSavesFileName[MAX_PATH];
	sprintf(PlayerSavesFileName, "%s/PlayerSaves.bwsp", MapDirectory_);

	CFile PlayerSavesFile(PlayerSavesFileName, CFILE_WRITE | CFILE_BINARY | CFILE_CREATE);
	PlayerSavesFile << PlayerSaves_;
	PlayerSavesFile.Destroy();
}

void CServer::LoadChunk_(SIVector2D Position)
{
	LOG("Server opened chunk (%d %d)", Position.x, Position.y);

	tOpenedChunk& Chunk = OpenedChunks_(Position);

	if (!Chunk.References)
		Chunk.Data.Init(MapDirectory_, Position);

	Chunk.References++;
}

void CServer::UnloadChunk_(SIVector2D Position)
{
	LOG("Server closed chunk (%d %d)", Position.x, Position.y);

	tOpenedChunk& Chunk = OpenedChunks_(Position, true);

	if (!--Chunk.References)
	{
		Chunk.Data.Destroy(MapDirectory_);
		OpenedChunks_.Remove(Position);
	}
}

CBlockData CServer::GetBlock(SIVector Position)
{
	assert (Position.y >= 0 && Position.y <= SUBWORLD_SIZE_Y);

	SIVector2D SubWorldPosition	= DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	SIVector   BlockPosition	= ModXZ  (Position, SUBWORLD_SIZE_XZ);

	CSubWorld& SubWorld = SubWorldCache_(SubWorldPosition, true).Data;

	return SubWorld.GetBlock(BlockPosition);
}

void CServer::SetBlock(SIVector Position, CBlockData Block)
{
	assert (Position.y >= 0 && Position.y < SUBWORLD_SIZE_Y);

	SIVector2D SubWorldPosition	= DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	SIVector   BlockPosition	= ModXZ  (Position, SUBWORLD_SIZE_XZ);

	CSubWorld& SubWorld = SubWorldCache_(SubWorldPosition, true).Data;
	SubWorld.SetBlock(BlockPosition, Block);
}

CStream& operator <<(CStream& Stream, CServer::tPlayerSave& PlayerSave)
{
	Stream << PlayerSave.Position;
	return Stream;
}

CStream& operator >>(CStream& Stream, CServer::tPlayerSave& PlayerSave)
{
	Stream >> PlayerSave.Position;
	return Stream;
}
