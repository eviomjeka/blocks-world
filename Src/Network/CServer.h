﻿#ifndef CSERVER
#define CSERVER

#define SERVER_MAX_PLAYERS				6
#define SERVER_MAX_PLAYERS_REGISTERED	6

#include <Engine.h>
#include "../World/CWorld.h"
#include "CPlayer.h"
#include "Packets.h"

#define SERVER_SUBWORLD_CACHE_ROWS			256
#define SERVER_SENT_SUBWORLDS_ROWS			256
#define SERVER_SUBWORLD_RECEIVERS_ROWS		256

class CServer : public CBlockManager
{
private:
	struct tOpenedChunk
	{
		int References;
		CChunk Data;

		tOpenedChunk() : References(0) {}
	};

	struct tPlayer
	{
		TSocketAddress					Address;
		CPlayer							PlayerState;
		SIVector2D						Position;
		int								LastPlayerState;
		int								SightRange;
		CTCPClient						TCPSocket;
		CTCPClient						MapLoader;
		bool							ValidAddress;

		tPlayer() :
			Position(-1, -1),
			LastPlayerState(-1),
			SightRange(-1),
			ValidAddress(false)
		{}

	};
public:
	struct tPlayerSave
	{
		SFVector			Position;
	};
	struct tCachedSubWorld
	{
		CSubWorld			Data;
		int					References;

		tCachedSubWorld() : References(0) {}
	};
	typedef CList<tPlayer>::tIterator tPlayerIterator;

	CTCPServer									TCPSocket_;
	CTCPServer									MapLoaderServer_;
	CUDPSocket									UDPSocket_;
	CBuffer										Buffer_;
	CBuffer										CompressionBuffer_;
	int											PlayerStatesNumber_;
	char										MapDirectory_[MAX_PATH];
	CString										ServerName_;

	CList<tPlayer>								Players_;
	unsigned short								TCPPort_;
	unsigned short								MapLoaderPort_;
	unsigned short								UDPPort_;
	SWorldParams								WorldParams_;
	int											NPlayers_;
	bool										Initialized_;
	int											Time_;
	CWorldGenerator								WorldGenerator_;

	CArray<tPlayerSave>							PlayerSaves_;
	CHashTable<SIVector2D, tCachedSubWorld>		SubWorldCache_;
	CHashTable<SIVector2D, tOpenedChunk>		OpenedChunks_;
	CArray<SInteractWithBlockInfo>				InteractWithBlockInfoItems_;
	CArray<SIVector>							RemovedBlocks_;

	bool FindPlayer_(int ID, tPlayerIterator& i);
	bool Accept_();

	void LoadState_();
	void SaveState_();
	bool AcceptMapLoaders_();
	void Disconnect_(tPlayerIterator PlayerI);
	void MovePlayer_(tPlayer& Player, SPositionPacket& PositionPacket);
	void ReceiveTCPPackets_(tPlayer& Player);
	void ReceiveMapLoaderPackets_(tPlayer& Player);
	void ReceiveUDPPackets_();
	void SendWorldPatch_(tPlayer& Player);
	void SendSubWorlds_(tPlayer& Player, CArray<int>& PlayerSubWorldVersions);
	void InteractWithBlock_(SInteractWithBlockInfo& InteractWithBlockInfo);
	void RemoveBlock_(SIVector Position);
	void ChatMessage_(SChatMessagePacket& ChatMessagePacket);
	void SendMessageToChat_(CString& String);
	void SetTime_(SSetTimePacket& SetTimePacket);
	void FlyPlayer_(SFlyPlayerPacket& FlyPlayerPacket);
	void SyncTime_();
	void SaveSubWorld_(CSubWorld& SubWorld);
	void SendPlayerStates_();
	
	void InitSubWorld_(SIVector2D Position);
	void UnloadSubWorld_(SIVector2D Position);
	void UnloadOldChunks_(tPlayer& Player, SIVector2D PrevPosition, SIVector2D NewPosition);
	void LoadNewChunks_(tPlayer& Player, SIVector2D PrevPosition, SIVector2D NewPosition);

	void SendSubWorld_(tPlayer& Player, SIVector2D Position, int PlayerVersion);
	void SendCachedSubWorld_(tPlayer& Player, SIVector2D Position, int PlayerVersion);
	void SendSavedSubWorld(tPlayer& Player, CChunk& Chunk, SIVector2D Position, int PlayerVersion);
	void SendEmptySubWorld_(tPlayer& Player, SIVector2D Position, int Version);
	void UnloadChunk_(SIVector2D Position);
	void LoadChunk_(SIVector2D Position);
	void LoadChunks_(tPlayer& Player);
	void UnloadChunks_(tPlayer& Player);
	void UnloadSubWorlds_(tPlayer& Player);

public:
	CServer();

	void Init(const char* Name, const char* MapDirectory, 
		unsigned short TCPPort, unsigned short MapLoaderPort, unsigned short UDPPort);
	void Destroy();

	virtual CBlockData GetBlock(SIVector Position);
	virtual void SetBlock(SIVector Position, CBlockData Block);

	void Step (int Time);

	friend CStream& operator <<(CStream& Stream, CServer::tPlayerSave& PlayerSave);
	friend CStream& operator >>(CStream& Stream, CServer::tPlayerSave& PlayerSave);

};


#endif