﻿#ifndef CCLIENTMAPLOADER
#define CCLIENTMAPLOADER

#include <Engine.h>
#include "../MapLoader/CMapLoader.h"

#define MAPLOADER_QUEUE_SIZE 256

class CClientMapLoader :
	public CMapLoader
{
private:

	CBuffer								Buffer_;
	CBuffer								CompressionBuffer_;
	CTCPClient							Client_;
	
	CQueue<SIVector2D>					PositionQueue_;
	bool								Initialized_;
	int									SightRange_;
	volatile bool						LoaderThreadReady_;

	SIVector2D							OldPosition_;

	CMutex								Mutex_;
	
	static void _LoaderThread_(void* This);
	void LoaderThread_();

	void ProcessWorldPatchPacket_();
	void ProcessSubWorldPacket_();




	
	void SendPosition_(SIVector2D Position, int Tx, int Ty);

	SIVector2D ProcessNewPositions_();
	void ProcessNewPosition_(SIVector2D PreviousPosition, SIVector2D Position);


public:

	CClientMapLoader();
	~CClientMapLoader();

	void Init(const char* IP, unsigned short Port, CWorld* World, 
		const char* MapDirectory, SIVector2D Position, int SightRange);
	void Destroy();

	void SetPosition(SIVector2D Position);



	//TODO: to CMapLoader
	virtual int LoadingSteps_();
	virtual float PercentsLoaded();
	virtual bool GameStarted();
	virtual int SightRange();
	virtual bool Valid();


};

#endif
