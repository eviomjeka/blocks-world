﻿#include "CClientMapLoader.h"
#include "Packets.h"
#include "../CApplication.h"

CClientMapLoader::CClientMapLoader() :
	Buffer_(2 << 20),
	CompressionBuffer_(2 << 20),
	Initialized_(false),
	PositionQueue_(256)
{}

CClientMapLoader::~CClientMapLoader()
{
	assert(!Initialized_);
}

void CClientMapLoader::Init(const char* IP, unsigned short Port, CWorld* World, 
							const char* MapDirectory, SIVector2D Position, int SightRange)
{
	SightRange_				  = SightRange;
	LoadingStepsPassed_		  = 0;
	LoaderThreadReady_		  = false;


	CMapLoader::Init(MapDirectory, 6 * Square(2 * SightRange + 1), World);
	Client_.Init(IP, Port);

	SClientMapLoaderJoinPacket ClientMapLoaderJoinPacket;
	ClientMapLoaderJoinPacket.PlayerID   = CLIENT.PlayerID();
	ClientMapLoaderJoinPacket.SightRange = SightRange;

	for (int x = Position.x - SightRange_; x <= Position.x + SightRange; x++)
		for (int y = Position.y - SightRange_; y <= Position.y + SightRange; y++)
		{
			SIVector2D SubWorldPosition(x, y);
			SIVector2D ChunkPosition = Div2D(SubWorldPosition, CHUNK_SIZE);

			ClientMapLoaderJoinPacket.PlayerSubWorldVersions.Insert(GetChunk_(ChunkPosition, 0).SubWorldVersion(SubWorldPosition), true);
		}

	Buffer_.Clear();
	Buffer_ << ClientMapLoaderJoinPacket;
	Client_ << Buffer_;

	OldPosition_ = Position;


	LaunchThread(_LoaderThread_, (void*) this);
	while (!LoaderThreadReady_)
		Sleep (1);
	
	Initialized_ = true;
}

void CClientMapLoader::Destroy()
{
	assert(Initialized_);
	LoaderThreadReady_ = false;
	while (!LoaderThreadReady_)
		Sleep (1);

	CMapLoader::Destroy();
	Client_.Destroy();
	Initialized_ = false;
}

void CClientMapLoader::SetPosition(SIVector2D Position)
{
	Mutex_.Lock();
	if (Position != OldPosition_ && (PositionQueue_.Empty() || PositionQueue_.Tail() != Position))
		PositionQueue_.Push(Position);
	Mutex_.Unlock();
}

SIVector2D CClientMapLoader::ProcessNewPositions_()
{	
	SIVector2D Position;

	for (;;)
	{
		Mutex_.Lock();
		Position = OldPosition_;

		if (PositionQueue_.Empty())
		{
			Mutex_.Unlock();
			break;
		}
		else
		{		
			Position = PositionQueue_.Pop();
			SIVector2D OldPosition = OldPosition_;
			OldPosition_ = Position;
			Mutex_.Unlock();
			ProcessNewPosition_(OldPosition, Position);
		}
	}

	return Position;
}

void CClientMapLoader::ProcessNewPosition_(SIVector2D PreviousPosition, SIVector2D Position)
{
	SIVector2D CurrentPosition	= PreviousPosition;
	SIVector2D TranslationSign	= Sign(Position - PreviousPosition);
	Buffer_.Clear();
/*
	for (int x = CurrentPosition.x; x != Position.x; x += TranslationSign.x)
	{
		for (int y = -SightRange_; y <= SightRange_; y++)
			PLAYERCONTROLLER.WorldScene()->UnloadMeshes (SIVector2D (x + TranslationSign.x * SightRange_, y + CurrentPosition.y));
	}
	for (int y = CurrentPosition.y; y != Position.y; y += TranslationSign.y)
	{
		for (int x = -SightRange_; x <= SightRange_; x++)
			PLAYERCONTROLLER.WorldScene()->UnloadMeshes (SIVector2D (x + CurrentPosition.x, y + TranslationSign.y * SightRange_));
	}
*/
	while (CurrentPosition.x != Position.x)
	{
		CurrentPosition.x += TranslationSign.x;
		SendPosition_(CurrentPosition, TranslationSign.x, 0);
	}
	while (CurrentPosition.y != Position.y)
	{
		CurrentPosition.y += TranslationSign.y;
		SendPosition_(CurrentPosition, 0, TranslationSign.y);
	}

	if (Buffer_.WritePosition())
		Client_.Send(Buffer_);
}

void CClientMapLoader::LoaderThread_()
{
	LoaderThreadReady_ = true;
	SIVector2D CurrentLoadedPosition;

	while (LoaderThreadReady_)
	{
		ProcessNewPositions_();

		int PacketType = 0;
		int BytesReceived = Client_.Receive((byte*) &PacketType, sizeof PacketType);
		if (!BytesReceived)
		{
			Sleep(1);
			continue;
		}
		assert(BytesReceived == sizeof PacketType);

		switch (PacketType)
		{
		case TCP2_WORLDPATCH_PACKET:
			ProcessWorldPatchPacket_();
			break;

		case TCP2_SUBWORLD_PACKET:
			ProcessSubWorldPacket_();
			break;

		default:
			error("Unknown packet id");
			break;
		}
	}

	LoaderThreadReady_ = true;
}

void CClientMapLoader::ProcessWorldPatchPacket_()
{
	SWorldPatchPacket WorldPatchPacket;
	Client_ >> WorldPatchPacket;

	//TODO: optimize for multiple block items
	for (int i = 0; i < WorldPatchPacket.InteractWithBlockInfoItems.Size(); i++)
	{
		World_->InteractWithBlock(WorldPatchPacket.InteractWithBlockInfoItems[i]);
	}

	for (int i = 0; i < WorldPatchPacket.RemovedBlocks.Size(); i++)
	{
		World_->RemoveBlock(WorldPatchPacket.RemovedBlocks[i]);
	}
}

void CClientMapLoader::ProcessSubWorldPacket_()
{
	SSubWorldPacketHeader SubWorldPacketHeader;
	Client_ >> SubWorldPacketHeader;

	SIVector2D Position = ProcessNewPositions_();
	SubWorldCache_.SetPriorityDecider(Position);
	bool Load = SubWorldCache_.IsSufficientPriority(SubWorldPacketHeader.Position);
	
	LOG ("Initializing subworld at (%d %d)", SubWorldPacketHeader.Position.x, SubWorldPacketHeader.Position.y);

	CClientSubWorld* ExistingSubWorld = SubWorldCache_(SubWorldPacketHeader.Position);

	if (ExistingSubWorld)
	{
		assert(ExistingSubWorld->Version() == SubWorldPacketHeader.Version);

		if (!ExistingSubWorld->Prepared_)
		{
			LOG("Continued loading subworld (%d %d)", SubWorldPacketHeader.Position.x, SubWorldPacketHeader.Position.y);
			if (!ExistingSubWorld->FullyGenerated_)
				GenerateObjects_(*ExistingSubWorld);
			PrepareSubWorld_(*ExistingSubWorld);
			//LoadingStepsPassed_++;
		}

		return;
	}

	//TODO: some asserts?
	if (SubWorldPacketHeader.Size)
	{
		Buffer_.Clear();
		assert(Client_.Receive(Buffer_, SubWorldPacketHeader.Size, true));

		if (Load)
		{	
			CClientSubWorld& SubWorld = SubWorldCache_.Allocate(SubWorldPacketHeader.Position);
			UnloadSubWorld_(SubWorld);

			CompressionBuffer_.Clear();
			CCompressor::Decompress(Buffer_, CompressionBuffer_);
			SubWorld.Init(SubWorldPacketHeader.Position, CompressionBuffer_, SubWorldPacketHeader.Version);
			SubWorldCache_.ReportReady(&SubWorld);
			GetChunk_(Div2D(SubWorldPacketHeader.Position, CHUNK_SIZE), +1);
			PrepareSubWorld_(SubWorld);
		}
		else
			SaveSubWorldData_(SubWorldPacketHeader.Position, Buffer_, SubWorldPacketHeader.Version);
	}
	else if (Load)
	{
		CClientSubWorld& SubWorld = SubWorldCache_.Allocate(SubWorldPacketHeader.Position);
		UnloadSubWorld_(SubWorld);
		LoadSubWorld_(SubWorld, SubWorldPacketHeader.Position);
		SubWorldCache_.ReportReady(&SubWorld);
		assert(SubWorldCache_(SubWorldPacketHeader.Position));
		PrepareSubWorld_(SubWorld);
	}		

	LoadingStepsPassed_++;
}

void CClientMapLoader::SendPosition_(SIVector2D Position, int Tx, int Ty)
{
	assert(Tx * Tx + Ty * Ty == 1);

	int PacketType = UDP_POSITION_PACKET;
	SPositionPacket PositionPacket;
	PositionPacket.Position = Position;

	if (Tx)
	{
		int x = Position.x + Tx * SightRange_;

		for (int y = Position.y - SightRange_; y <= Position.y + SightRange_; y++)
		{
			SIVector2D SubWorldPosition(x, y);
			SIVector2D ChunkPosition = Div2D(SubWorldPosition, SUBWORLD_SIZE_XZ);
			CChunk& Chunk = GetChunk_(ChunkPosition, 0);

			PositionPacket.PlayerSubWorldVersions.Insert(Chunk.SubWorldVersion(SubWorldPosition), true);
		}
	}
	else
	{
		int y = Position.y + Ty * SightRange_;

		for (int x = Position.x - SightRange_; x <= Position.x + SightRange_; x++)
		{
			SIVector2D SubWorldPosition(x, y);
			SIVector2D ChunkPosition = Div2D(SubWorldPosition, SUBWORLD_SIZE_XZ);
			CChunk& Chunk = GetChunk_(ChunkPosition, 0);
			assert(Chunk.Valid(ChunkPosition));

			PositionPacket.PlayerSubWorldVersions.Insert(Chunk.SubWorldVersion(SubWorldPosition), true);
		}
	}

	Buffer_ << PacketType;
	Buffer_ << PositionPacket;
}

void CClientMapLoader::_LoaderThread_(void* This)
{
	((CClientMapLoader*) This)->LoaderThread_();
}

//========================================================

int CClientMapLoader::LoadingSteps_()
{
	int InitializationSteps	= (SightRange_ * 2 + 1) * (SightRange_ * 2 + 1);
	int LightingSteps		= 0 * (SightRange_ * 2 - 1) * (SightRange_ * 2 - 1);

	return InitializationSteps + LightingSteps;
}	

float CClientMapLoader::PercentsLoaded()
{
	return LoadingStepsPassed_ / float(LoadingSteps_()) * 100.0f;
}

bool CClientMapLoader::GameStarted()
{
	return 1;
	//LoadingStepsPassed_ >= LoadingSteps_();
}

bool CClientMapLoader::Valid()
{
	return Client_.Disconnected();
}

int CClientMapLoader::SightRange()
{
	return SightRange_;
}
