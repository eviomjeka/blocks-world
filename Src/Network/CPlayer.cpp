﻿#include "CPlayer.h"
#include "../CApplication.h"

void CPlayer::Init(int ID, SFVector Position)
{
	ID_				= ID;
	Position_		= Position;
	Velocity_		= SFVector(0.0f);
	Orientation_	= SFVector2D (0.0f, 0.0f);
	FlyMode_		= false;
	BlockUnder_		= false;
	IsStuck_		= false;
	ItemData_		= CItemData();
	Interacted_		= false;
	Removed_		= false;
}


void CPlayer::SetFlyMode(bool FlyMode)
{
	Velocity_ = SFVector (0.0f);
	FlyMode_ = FlyMode;
}

bool CPlayer::CheckCollision_ (bool IsMoveY)
{
	bool IsNewPosition = false;
	float NewPositionY = 0.0f;

	bool Low  = CheckLowYCollision_ (IsMoveY, &IsNewPosition, &NewPositionY);
	bool High = CheckHighYCollision_ (IsMoveY);
	if (IsNewPosition)
		Position_.y = NewPositionY;
	return Low || High;
}

bool CPlayer::CheckLowYCollision_ (bool IsMoveY, bool* IsNewPosition, float* NewPositionY)
{
	bool IsCollision[3] = {};
	float CollisionY[3] = {};

	for (int i = 0; i < 3; i++)
		IsCollision[i] = IsCollisionAtHeight_ (i - 1, true, &CollisionY[i]);

	float InBlockY = Position_.y - floor (Position_.y);
	
	BlockUnder_ = false;
	if (IsMoveY)
		IsStuck_ = false;

	(*IsNewPosition) = false;

	if (IsCollision[2])
	{
		if (1.0f - InBlockY + CollisionY[2] <= PLAYER_CLIMP_WITHOUT_JUMP + PLAYER_STUCK_SIZE())
		{
			if (IsMoveY || PrevBlockUnder_)
			{
				(*NewPositionY) = floor (Position_.y);
				(*NewPositionY) += 1.0f + CollisionY[2];
				(*IsNewPosition) = true;
				Velocity_.y = max (Velocity_.y, 0.0f);
				BlockUnder_ = true;

				return false;
			}
			else
				return true;
		}
		else
		{
			if (IsMoveY)
			{
				IsStuck_ = true;
				Velocity_.y = 0.0f;
			}
			return true;
		}
	}
	else if (IsCollision[1])
	{
		if (CollisionY[1] - InBlockY <= PLAYER_CLIMP_WITHOUT_JUMP + PLAYER_STUCK_SIZE())
		{
			if (IsMoveY || PrevBlockUnder_)
			{
				(*NewPositionY) = floor (Position_.y);
				(*NewPositionY) += CollisionY[1];
				(*IsNewPosition) = true;
				Velocity_.y = max (Velocity_.y, 0.0f);
				BlockUnder_ = true;

				return false;
			}
			else
				return true;
		}
		else
		{
			if (IsMoveY)
			{
				IsStuck_ = true;
				Velocity_.y = 0.0f;
			}
			return true;
		}
	}
	else if (IsCollision[0] && InBlockY == 0.0f && CollisionY[0] == 1.0f)
	{
		(*NewPositionY) = Position_.y;
		(*IsNewPosition) = true;
		Velocity_.y = max (Velocity_.y, 0.0f);
		BlockUnder_ = true;

		return false;
	}

	return false;
}

bool CPlayer::CheckHighYCollision_ (bool IsMoveY)
{
	bool IsCollision[3] = {};
	float CollisionY[3] = {};

	for (int i = 0; i < 3; i++)
		IsCollision[i] = IsCollisionAtHeight_ (i + 2, false, &CollisionY[i]);

	float InBlockY = Position_.y - floor (Position_.y);
	
	if (IsCollision[0])
	{
		if (1.0f - (PLAYER_HEIGHT - PLAYER_HEIGHT_FLOAT) + InBlockY <= CollisionY[0] + PLAYER_STUCK_SIZE())
		{
			Position_.y = floor (Position_.y);
			Position_.y += CollisionY[0];
			Velocity_.y = min (Velocity_.y, 0.0f);

			return false;
		}
		else
		{
			if (IsMoveY)
			{
				IsStuck_ = true;
				Velocity_.y = 0.0f;
			}
			return true;
		}
	}
	/*else if (IsCollision[1] && InBlockY <= CollisionY[1])
	{
		if (CollisionY[1] - InBlockY <= PLAYER_CLIMP_WITHOUT_JUMP)
		{
			Position_.y = floor (Position_.y);
			Position_.y += CollisionY[1];
			Velocity_.y = min (Velocity_.y, 0.0f);

			return false;
		}
		else
		{
			if (IsMoveY)
				IsStuck_ = true;
			return true;
		}
	}*/
	/*else if (IsCollision[0] && InBlockY == 0.0f && CollisionY[0] == 1.0f)
	{
		Velocity_.y = max (Velocity_.y, 0.0f);
		BlockUnder_ = true;

		return false;
	}*/

	return false;
}

bool CPlayer::IsCollisionAtHeight_ (int Height, bool IsHighCollisionY, float* CollisionY)
{
	SFVector Position = Position_;
	SIVector Pos = FloorVector(Position);
	Pos.y += Height;
	SFVector2D InBlockPos = SFVector2D(Position_.x - Pos.x, Position_.z - Pos.z);

	if (Pos.y <= -10)
		return true;
	if (Pos.y < 0 || Pos.y > SUBWORLD_SIZE_Y)
		return false;
	
	float CurrentY = 0.0f;
	bool IsCollision = false;

	if (CollisionY)
		(*CollisionY) = (IsHighCollisionY ? -1 : 1) * 1000.0f;

	for (int x = -1; x <= 1; x++)
		for (int z = -1; z <= 1; z++)
		{
			SIVector BlockPosition = Pos + SIVector (x, 0, z);

			CBlockData Block = CLIENT.MapLoader().GetBlock (BlockPosition); // TODO: it is not normal, fix
			CArray<SBlockAABB> AABBList (MAX_BLOCK_BOUNDING_BOXES);
			BlocksList (Block.ID())->GetAABBList (Block.Attributes(), BlockPosition, AABBList);
			
			for (int i = 0; i < AABBList.Size(); i++)
			{
				if ((AABBList[i].Type == BLOCK_AABB_PHYSICS || 
					AABBList[i].Type == BLOCK_AABB_SELECTION_AND_PHYSICS))
				{
					RotateBlockAABB (AABBList[i], Block.Rotate());
					if (CollisionWithAABB_ (AABBList[i].Start, AABBList[i].End, SIVector (x, Pos.y, z), IsHighCollisionY, &CurrentY))
					{
						if (CollisionY)
						{
							IsCollision = true;
							if (( IsHighCollisionY && CurrentY > (*CollisionY)) || 
								(!IsHighCollisionY && CurrentY < (*CollisionY)))
								(*CollisionY) = CurrentY;
						}
						else
							return true;
					}
				}
			}
		}

	return IsCollision;
}

bool CPlayer::CollisionWithAABB_ (SFVector Start, SFVector End, SIVector Delta, bool IsHighCollisionY, float* CollisionY)
{
	SFVector Pos = Position_ - SFVector (floor (Position_.x), 0, floor (Position_.z));
	
	bool IsInXZ = (Pos.x + PLAYER_RADIUS > min (Start.x, End.x) + Delta.x && 
				   Pos.x - PLAYER_RADIUS < max (Start.x, End.x) + Delta.x) && 
				  (Pos.z + PLAYER_RADIUS > min (Start.z, End.z) + Delta.z && 
				   Pos.z - PLAYER_RADIUS < max (Start.z, End.z) + Delta.z);
	if (!IsInXZ)
		return false;

	float StartY = (float) Pos.y;
	float EndY   = (float) Pos.y + PLAYER_HEIGHT_FLOAT;

	float MaxStart	= max (StartY, Delta.y + min (Start.y, End.y));
	float MinEnd	= min (EndY, Delta.y + max (Start.y, End.y));

	if (MaxStart <= MinEnd)
	{
		if (CollisionY)
		{
			if (IsHighCollisionY)
				(*CollisionY) = max (Start.y, End.y);
			else
				(*CollisionY) = min (Start.y, End.y);
		}
		
		return true;
	}

	return false;
}

void CPlayer::MoveCheckCollision_ (SFVector Delta)
{
	SFVector PrevousPosition = Position_;
	Position_ += Delta;

	if (CheckCollision_ (false))
		Position_ = PrevousPosition;
}

void CPlayer::MoveCheckCollisionY_ (float y)
{
	float PrevousY = Position_.y;
	Position_.y += y;

	if (CheckCollision_ (true))
		Position_.y = PrevousY;
}

void CPlayer::PhysicsStep (int Time)
{
	float Alpha = Orientation_.y * PI / 180.0f;
	float dX = (cos(Alpha) * Velocity_.x + sin(Alpha) * Velocity_.z) * (Time / 1000.0f);
	float dZ = (sin(Alpha) * Velocity_.x - cos(Alpha) * Velocity_.z) * (Time / 1000.0f);

	PrevBlockUnder_ = BlockUnder_;

	MoveCheckCollision_(SFVector(dX, 0.0f, 0.0f));
	MoveCheckCollision_(SFVector(0.0f, 0.0f, dZ));
	
	if (!FlyMode_)
	{
		if (!IsStuck_ && (!BlockUnder_ || Velocity_.y > 0.0f))
		{
			float dYVelocity = -WORLD_GRAVITY * (Time / 1000.0f);
			float AverageVelocityY = max(min(Velocity_.y + dYVelocity / 2.0f, PLAYER_MAX_Y_SPEED), -PLAYER_MAX_Y_SPEED);

			Position_.y += AverageVelocityY * (Time / 1000.0f);
			Velocity_.y += dYVelocity;
			Velocity_.y = max(min(Velocity_.y, PLAYER_MAX_Y_SPEED), -PLAYER_MAX_Y_SPEED);
			
			CheckCollision_ (true);
		}
	}
	else
		MoveCheckCollisionY_ (Velocity_.y * (Time / 1000.0f));
}

void CPlayer::Jump()
{
	if (FlyMode_)
		return;

	SIVector Position = FloorVector (Position_);
	Position.y = (int) (Position_.y + PLAYER_EYES_HEIGHT);
	CBlockData Block0 = (Position.y >= 0 && Position.y < SUBWORLD_SIZE_Y) ? 
		CLIENT.MapLoader().GetBlock (Position) : CBlockData();
	Position.y++;
	CBlockData BlockY = (Position.y >= 0 && Position.y < SUBWORLD_SIZE_Y) ? 
		CLIENT.MapLoader().GetBlock (Position) : CBlockData();

	if (BlockUnder_ && !IsStuck_ && true)
	{
		Velocity_.y = PLAYER_JUMP_VELOCITY;
		return;
	}
}

CStream& operator <<(CStream& Stream, CPlayer& Player)
{
	Stream << Player.ID_;
	Stream << Player.Position_;
	Stream << Player.Velocity_;
	Stream << Player.Orientation_;
	Stream << Player.FlyMode_;
	Stream << Player.IsStuck_;
	Stream << Player.ItemData_;
	Stream << Player.Interacted_;
	Stream << Player.Removed_;

	return Stream;
}

CStream& operator >>(CStream& Stream, CPlayer& Player)
{
	Stream >> Player.ID_;
	Stream >> Player.Position_;
	Stream >> Player.Velocity_;
	Stream >> Player.Orientation_;
	Stream >> Player.FlyMode_;
	Stream >> Player.IsStuck_;
	Stream >> Player.ItemData_;
	Stream >> Player.Interacted_;
	Stream >> Player.Removed_;

	// TODO: check parameters
	
	return Stream;
}
