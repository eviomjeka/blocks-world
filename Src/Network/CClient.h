﻿#ifndef CCLIENT
#define CCLIENT

#include <Engine.h>
#include "../CPlayerController.h"
#include "../World/CWorld.h"
#include "CPlayer.h"
#include "Packets.h"
#include "../CGame.h"

class CClient :
	public CGame
{
private:
	CUDPSocket						UDPSocket_;
	CTCPClient						TCPSocket_;
	CBuffer							Buffer_;
	int								PlayerStatePacketNumber_;
	int								LaunchTime_;
public: // TODO: fix

	TSocketAddress					ServerAddress_;
	int								LastServerPacket_;
	bool							Initialized_;
	CClientMapLoader				MapLoader_;
	
	void ProcessPlayerStatesPacket_(SPlayerStatesPacket& PlayerStatesPacket);
	void ReceiveUDPPackets_();
	void ReceiveTCPPackets_();
	void SendPlayerState_();
	void PacketStep_();

	void ChatMessagePacket_();
	void SetTimePacket_();
	void FlyPlayerPacket_();

public:
	CClient();

	void Init(const char* IP, unsigned short TCPPort);

	virtual CMapLoader& MapLoader();
	virtual bool Initialized();
	virtual void Step();
	virtual void Destroy();
	virtual void InteractWithBlock(SInteractWithBlockInfo InteractWithBlockInfo);
	virtual void RemoveBlock(SIVector Position);
	virtual void ChatMessage(CString& Message);
	virtual void SetTime(int Time);
	virtual void FlyPlayer(int PlayerID, bool FlyMode);

};

#endif