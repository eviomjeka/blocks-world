﻿#ifndef CSKY
#define CSKY

#include <Engine.h>
#include "MeshConstructors/CSkyMeshConstructor.h"

#define SKYBOX_MESH_SIZE 15000
#define NUM_STARS 200

class CSky
{
public:
	
	static const float TURBIDITY;
	static const float COLOR_EXP;

	void Init();
	void Destroy();
	SFVector SunPosition();

	void Render (int Time);

private:
	
	CTextureMeshConstructor SunMoonMeshConstructor_;
	CTextureMeshConstructor StarsMeshConstructor_;
	CArray<SFVector > StarsPosition_;
	SFVector SunPosition_;
	
	CMesh SkydomeMesh_;
	CMesh StarsMesh_;
	CMesh SunMoonMesh_;

	CTexture SunTexture_;
	CTexture MoonTexture_;
	CTexture StarTexture_;

};

#endif