#include <Engine.h>
#include "World/CChunk.h"

class CServerSubWorldCache :
	public CCache<SIVector2D, CSubWorld, SIVector2D, false>
{
public:
	virtual int CalculatePriority(SIVector2D& /*Key*/){return 0;}
};