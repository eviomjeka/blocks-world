#include "CSingleMapLoader.h"
#include "../CApplication.h"

CSingleMapLoader::CSingleMapLoader() :
	BlockChanges_(256)
{
}

void CSingleMapLoader::Init(CWorld* World, const char* MapDirectory, SIVector2D Position, int SightRange)
{
	LoadingStepsPassed_		  = 0;
	LoaderThreadReady_		  = false;
	SightRange_				  = SightRange;

	CMapLoader::Init(MapDirectory, Square(2 * SightRange + 1), World);

	Position_ = Position;
	OldPosition_ = Position + SIVector2D(344234, 32434234);

	LaunchThread(_LoaderThread_, (void*) this);
	while (!LoaderThreadReady_)
		Sleep (1);

	Initialized_ = true;
}

void CSingleMapLoader::Destroy()
{
	assert(Initialized_);
	LoaderThreadReady_ = false;
	while (!LoaderThreadReady_)
		Sleep (1);

	CMapLoader::Destroy();

	Initialized_ = false;
}

void CSingleMapLoader::SetPosition(SIVector2D Position)
{
	Mutex_.Lock();
	Position_ = Position;
	Mutex_.Unlock();
}

void CSingleMapLoader::LoaderThread_()
{
	LoaderThreadReady_ = true;

	while (LoaderThreadReady_)
	{
		ProcessBlockChanges_();
		ProcessNewPosition_();
	}

	LoaderThreadReady_ = true;
}

void CSingleMapLoader::ProcessNewPosition_()
{
	Mutex_.Lock();
	SIVector2D Position = Position_;
	Mutex_.Unlock();


	if (Position == OldPosition_)
	{
		Sleep(1);
		return;
	}

	SubWorldCache_.SetPriorityDecider(Position);

	for (int x = -SightRange_; x <= +SightRange_; x++)
		for (int y = -SightRange_; y <= +SightRange_; y++)
		{
			if (!LoaderThreadReady_)
				return;

			SIVector2D CurrentPosition = Position + SIVector2D(x, y);
			CClientSubWorld* Existing = SubWorldCache_(CurrentPosition);

			if (Existing)
			{
				if (!Existing->Prepared_)
				{
					LOG("Continued loading subworld (%d %d)", CurrentPosition.x, CurrentPosition.y);
					if (!Existing->FullyGenerated_)
						GenerateObjects_(*Existing);
					PrepareSubWorld_(*Existing);
					LoadingStepsPassed_++;
				}
			}
			else if (SubWorldCache_.IsSufficientPriority(CurrentPosition))
			{
				LOG("Loading subworld (%d %d)", CurrentPosition.x, CurrentPosition.y);
				CClientSubWorld& SubWorld = SubWorldCache_.Allocate(CurrentPosition);
				UnloadSubWorld_(SubWorld);
				LoadSubWorld_(SubWorld, CurrentPosition);
				SubWorldCache_.ReportReady(&SubWorld);
				assert(SubWorldCache_(CurrentPosition));
				PrepareSubWorld_(SubWorld);
				LoadingStepsPassed_++;
			}
			else
				error("Subworld cache full. Consider making it larger.");
		}

		OldPosition_ = Position;
}

void CSingleMapLoader::InteractWithBlock(SInteractWithBlockInfo AddedItem)
{
	if (!BlockChanges_.Full())
	{
		SBlockChange BlockChange;
		BlockChange.Item						= true;
		BlockChange.InteractWithBlockInfoItem	= AddedItem;
		BlockChanges_.Push(BlockChange);
	}
	else
		WARNING ("block change queue full");
}

void CSingleMapLoader::RemoveBlock(SIVector RemovedBlock)
{
	if (!BlockChanges_.Full())
	{
		SBlockChange BlockChange;
		BlockChange.Item			= false;
		BlockChange.RemovedBlock	= RemovedBlock;
		BlockChanges_.Push(BlockChange);
	}
	else
		WARNING ("block change queue full");
}

void CSingleMapLoader::_LoaderThread_(void* This)
{
	((CSingleMapLoader*) This)->LoaderThread_();
}


void CSingleMapLoader::ProcessBlockChanges_()
{
	while (!BlockChanges_.Empty())
	{
		SBlockChange BlockChange = BlockChanges_.Pop();

		if (BlockChange.Item)
			World_->InteractWithBlock(BlockChange.InteractWithBlockInfoItem);
		else
			World_->RemoveBlock(BlockChange.RemovedBlock);
	}
}



//============================

int CSingleMapLoader::LoadingSteps_()
{
	int InitializationSteps	= (SightRange_ * 2 + 1) * (SightRange_ * 2 + 1);
	int LightingSteps		= 0 * (SightRange_ * 2 - 1) * (SightRange_ * 2 - 1);

	return InitializationSteps + LightingSteps;
}	

float CSingleMapLoader::PercentsLoaded()
{
	return LoadingStepsPassed_ / float(LoadingSteps_()) * 100.0f;
}

bool CSingleMapLoader::GameStarted()
{
	return LoadingStepsPassed_ >= LoadingSteps_();
}

int CSingleMapLoader::SightRange()
{
	return SightRange_;
}

bool CSingleMapLoader::Valid()
{
	return Initialized_;
}
