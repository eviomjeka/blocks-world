#include "CSingle.h"
#include "../CApplication.h"

CSingle::CSingle() :
	Initialized_(false)
{
}

CSingle::~CSingle()
{
	assert(!Initialized_);
}

void CSingle::Init(const char* WorldName)
{
	/*load settings*/
	SWorldParams WorldParams = {};
	CRandom::Srand();
	WorldParams.Seed = CRandom::RandomIntFromRand();

	SFVector PlayerPosition (0, 300, 0);
	World_.Init(WorldParams);
	World_.Players_.Insert(CPlayer(), true);
	World_.Players_[0].Init(0, PlayerPosition);
	PlayerID_ = 0;

	PLAYERCONTROLLER.SetControlled(0);

	char MapDirectory[MAX_PATH] = "";
	sprintf(MapDirectory, "Worlds/%s", WorldName);
	CFileSystem::CreateDir(MapDirectory);

	MapLoader_.Init(&World_, MapDirectory, DivXZ2D(FloorVector(PlayerPosition), SUBWORLD_SIZE_XZ), SETTINGS.SightRange + 2);

	Initialized_ = true;
}

CMapLoader& CSingle::MapLoader()
{
	return MapLoader_;
}

bool CSingle::Initialized()
{
	return Initialized_;
}

void CSingle::Step()
{
	MapLoader_.SetPosition(DivXZ2D(FloorVector(PLAYERCONTROLLER.Position()), SUBWORLD_SIZE_XZ));
}

void CSingle::Destroy()
{
	assert(Initialized_);

	MapLoader_.Destroy();
	World_.Destroy();

	Initialized_ = false;
}

void CSingle::InteractWithBlock(SInteractWithBlockInfo InteractWithBlockInfo)
{
	MapLoader_.InteractWithBlock(InteractWithBlockInfo);
}

void CSingle::RemoveBlock(SIVector Position)
{
	MapLoader_.RemoveBlock(Position);
}

void CSingle::ChatMessage(CString& Message)
{
	PLAYERCONTROLLER.ChatMessage (PlayerID(), Message);
}

void CSingle::SetTime(int Time)
{
	PLAYERCONTROLLER.SetTime (PlayerID(), Time);
}

void CSingle::FlyPlayer(int PlayerID, bool FlyMode)
{
	PLAYERCONTROLLER.FlyPlayer (PlayerID, FlyMode);
}
