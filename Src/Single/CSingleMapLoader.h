#ifndef CSINGLEMAPLOADER
#define CSINGLEMAPLOADER

#include <Engine.h>
#include "../MapLoader/CMapLoader.h"

struct SBlockChange
{
	bool Item;

	SInteractWithBlockInfo		InteractWithBlockInfoItem;
	SIVector					RemovedBlock;
};


class CSingleMapLoader :
	public CMapLoader
{
private:
	SIVector2D							Position_;
	SIVector2D							OldPosition_;

	int									SightRange_;
	volatile bool						LoaderThreadReady_;
	bool								Initialized_;
	CMutex								Mutex_;
	CQueue<SBlockChange>				BlockChanges_;

	static void _LoaderThread_(void* This);
	void LoaderThread_();

	void ProcessBlockChanges_();
	void ProcessNewPosition_();


public:

	CSingleMapLoader();
	//~CSingleMapLoader();

	void Init(CWorld* World, const char* MapDirectory, SIVector2D Position, int SightRange);
	void Destroy();

	void SetPosition(SIVector2D Position);

	void InteractWithBlock(SInteractWithBlockInfo AddedItem);
	void RemoveBlock(SIVector RemovedBlock);


	//TODO: to CMapLoader
	virtual int LoadingSteps_();
	virtual float PercentsLoaded();
	virtual bool GameStarted();
	virtual int SightRange();
	virtual bool Valid();

};

#endif
