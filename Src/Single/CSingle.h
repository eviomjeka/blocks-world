#ifndef CSINGLE
#define CSINGLE

#include "../CGame.h"
#include "CSingleMapLoader.h"

class CSingle :
	public CGame
{
private:
	CSingleMapLoader	MapLoader_;
	bool				Initialized_;

public:
	CSingle();
	~CSingle();

	void Init(const char* WorldName);

	virtual CMapLoader& MapLoader();
	virtual bool Initialized();
	virtual void Step();
	virtual void Destroy();
	virtual void InteractWithBlock(SInteractWithBlockInfo InteractWithBlockInfo);
	virtual void RemoveBlock(SIVector Position);
	virtual void ChatMessage(CString& Message);
	virtual void SetTime(int Time);
	virtual void FlyPlayer(int PlayerID, bool FlyMode);

};

#endif