﻿#ifndef CBLOCKSLIST
#define CBLOCKSLIST

#include "CBlock.h"
#include "../Blocks/BlocksInclude.h"

class CBlocksList
{
private:

	friend class CBlock;

	CSArray<CBlock*, MAX_BLOCKS> Blocks_;
	void RegisterBlock_ (CBlock* Block);

public:

	static CBlocksList Instance;
	void Init();
	inline CBlock* operator () (byte ID);

	CBlockAir			Air;
	CBlockUsual			Usual;
	CBlockGlass			Glass;
	CBlockGlowing		Glowing;
	CBlockLamp			Lamp;
	CBlockWater			Water;
	CBlockIce			Ice;
	CBlockHalf			Half;
	CBlockDoubleHalf	DoubleHalf;
	CBlockStairs		Stairs;
	CBlockDoor			Door;
	CBlockCabel			Cabel;

};

inline CBlock* CBlocksList::operator () (byte ID)
{
	return Blocks_[ID];
}

#endif