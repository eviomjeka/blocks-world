﻿#include "CBlock.h"
#include "../CApplication.h"

CBlock::CBlock() :
	ID_ (0)
{
	SetType_ (BLOCKTYPE_VOID);
	SetRotate_ (ROTATEX_NONE | ROTATEY_NONE | ROTATEZ_NONE);
	SetCanTick_ (0);
	SetLightIntensity_ (0);
	SetLightAttenuation_ (LIGHT_NOT_PASS);
	SetFaceType_ (FACETYPE_NONE);
	SetBlockFaceTextureOffset_ (SVector2D<byte> (0));
}

void CBlock::SetID (const byte ID)
{
	ID_ = ID;

	for (int i = 0; i < MAX_ATTRIBUTES; i++)
	{
		SBlockAttributeInfo BlockInfo = BlockInfo_[i];

		byte Rotate = BlockInfo_[i].Rotate;

		for (int j = 0; j < NUM_FACES; j++)
		{
			byte Face = IndexToFace ((byte) j);
			SIVector Vector = FaceToVector (SIVector (0), Face);
			Vector = RotateVector (Vector, Rotate);
			Face = VectorToFace (Vector);
			byte Index = FaceToIndex (Face);
			
			BlockInfo_[i].LightAttenuation[Index]		= BlockInfo.LightAttenuation[j];
			BlockInfo_[i].FaceType[Index]				= BlockInfo.FaceType[j];
			BlockInfo_[i].BlockFaceTextureOffset[Index]	= BlockInfo.BlockFaceTextureOffset[j];

			if (BlockInfo_[i].FaceType[Index] == FACETYPE_NORMAL)
				BlockInfo_[i].LightAttenuation[Index] = LIGHT_NOT_PASS;
		}
	}

	BlocksList.RegisterBlock_ (this);
}

void CBlock::SetType_ (const byte Type)
{
	assert (Type == BLOCKTYPE_VOID || Type == BLOCKTYPE_BLOCK || 
			Type == BLOCKTYPE_COMPLEX_BLOCK || Type == BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK);

	Type_ = Type;
}

void CBlock::SetRotate_ (const byte Rotate, const byte AttributesStart, const byte AttributesEnd)
{
	assert (Rotate <= MAX_ROTATE);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		BlockInfo_[i].Rotate = Rotate;
}

void CBlock::SetCanTick_ (const byte IsInteractive, const byte AttributesStart, const byte AttributesEnd)
{
	assert (IsInteractive <= 1);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		BlockInfo_[i].CanTick = IsInteractive;
}

void CBlock::SetLightIntensity_ (const byte LightIntensity, const byte AttributesStart, const byte AttributesEnd)
{
	assert (LightIntensity <= MAX_LIGHT_INTENSITY);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		BlockInfo_[i].LightIntensity = LightIntensity;
}

void CBlock::SetLightAttenuation_ (const byte LightAttenuation, const byte AttributesStart, const byte AttributesEnd)
{
	assert (LightAttenuation <= LIGHT_NOT_PASS);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		for (int j = 0; j < NUM_FACES; j++)
			BlockInfo_[i].LightAttenuation[j] = LightAttenuation;
}

void CBlock::SetLightAttenuationToFace_ (const byte Face, const byte LightAttenuation, 
										 const byte AttributesStart, const byte AttributesEnd)
{
	assert (LightAttenuation <= LIGHT_NOT_PASS);
	assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;
	
	byte Index = FaceToIndex (Face);
	for (int i = Start; i < End; i++)
		BlockInfo_[i].LightAttenuation[Index] = LightAttenuation;
}

void CBlock::SetFaceType_ (const byte FaceType, const byte AttributesStart, const byte AttributesEnd)
{
	assert (FaceType == FACETYPE_NORMAL || FaceType == FACETYPE_NOT_NORMAL || FaceType == FACETYPE_NONE);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		for (int j = 0; j < NUM_FACES; j++)
			BlockInfo_[i].FaceType[j] = FaceType;
}

void CBlock::SetFaceTypeToFace_	(const byte Face, const byte FaceType, 
								 const byte AttributesStart, const byte AttributesEnd)
{
	assert (FaceType == FACETYPE_NORMAL || FaceType == FACETYPE_NOT_NORMAL || FaceType == FACETYPE_NONE);
	assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;
	
	byte Index = FaceToIndex (Face);
	for (int i = Start; i < End; i++)
		BlockInfo_[i].FaceType[Index] = FaceType;
}

void CBlock::SetBlockFaceTextureOffset_	(const SVector2D<byte> BlockFaceTextureOffset, 
										 const byte AttributesStart, const byte AttributesEnd)
{
	assert (BlockFaceTextureOffset.x < 16 && BlockFaceTextureOffset.y < 16);

	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;

	for (int i = Start; i < End; i++)
		for (int j = 0; j < NUM_FACES; j++)
			BlockInfo_[i].BlockFaceTextureOffset[j] = BlockFaceTextureOffset;
}

void CBlock::SetBlockFaceTextureOffsetToFace_ (const byte Face, const SVector2D<byte> BlockFaceTextureOffset, 
											   const byte AttributesStart, const byte AttributesEnd)
{
	assert (BlockFaceTextureOffset.x < 16 && BlockFaceTextureOffset.y < 16);
	assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);
	
	byte Start = AttributesStart, End = AttributesEnd;
	if (AttributesStart == 255 && AttributesEnd == 0)
	{
		Start = 0;
		End = 255;
	}
	else if (AttributesEnd == 0)
		End = AttributesStart + 1;
	
	byte Index = FaceToIndex (Face);
	for (int i = Start; i < End; i++)
		BlockInfo_[i].BlockFaceTextureOffset[Index] = BlockFaceTextureOffset;
}

//==============================================================================

CBlockData CBlock::RotateGetBlock_ (byte Attributes, CBlockManagerReadOnly* BlockManager, 
									SIVector Position, SIVector NeighbourPosition) const
{
	byte Rotate = CBlockData (ID(), Attributes).Rotate();
	SIVector BlockPosition = Position + RotateVector (NeighbourPosition - Position, Rotate);

	return BlockManager->GetBlock (BlockPosition);
}

bool CBlock::CanAddOnFace_ (byte Attributes, CBlockManagerReadOnly* BlockManager, SIVector Position, 
							byte Face, bool AddOnNotNormalFace) const
{
	SIVector NeighbourPosition = FaceToVector (Position, Face);
	CBlockData Block = RotateGetBlock_ (Attributes, BlockManager, Position, NeighbourPosition);

	byte FaceType = Block.FaceType (InvertFace (Face));

	return FaceType == FACETYPE_NORMAL || (AddOnNotNormalFace && FaceType == FACETYPE_NOT_NORMAL);
}

bool CBlock::CanAdd_ (byte Attributes, CBlockManagerReadOnly* BlockManager, 
					  SIVector Position, bool AddOnNotNormalFace) const
{
	for (int i = 0; i < NUM_FACES; i++)
	{
		if (CanAddOnFace_ (Attributes, BlockManager, Position, IndexToFace ((byte) i), AddOnNotNormalFace))
			return true;
	}

	return false;
}

//==============================================================================

void CBlock::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	InventoryItems.Insert (0);
}

class CAABBListVertexListener : public CVertexListener
{
public:

	CAABBListVertexListener() :
		Initialized_ (false)
	{}

	virtual void AddVertex (SFVector VertexPosition, SVector2D<byte>, SVector2D<unsigned short>)
	{
		VertexPosition -= Position;

		if (!Initialized_)
		{
			Initialized_ = true;
			Start = VertexPosition;
			End = VertexPosition;
			return;
		}
		
		if (VertexPosition.x < Start.x) Start.x = VertexPosition.x;
		if (VertexPosition.y < Start.y) Start.y = VertexPosition.y;
		if (VertexPosition.z < Start.z) Start.z = VertexPosition.z;

		if (VertexPosition.x > End.x) End.x = VertexPosition.x;
		if (VertexPosition.y > End.y) End.y = VertexPosition.y;
		if (VertexPosition.z > End.z) End.z = VertexPosition.z;
	}

	SFVector Start;
	SFVector End;
	SFVector Position;

private:

	bool Initialized_;

};

void CBlock::GetAABBList (byte Attributes, SIVector Position, CArray<SBlockAABB>& AABBList) const
{
	if (Type_ == BLOCKTYPE_BLOCK)
	{
		SBlockAABB AABB = {};
		AABB.Start	= SFVector (0.0f, 0.0f, 0.0f);
		AABB.End	= SFVector (1.0f, 1.0f, 1.0f);
		AABB.Type	= BLOCK_AABB_SELECTION_AND_PHYSICS;

		AABBList.Insert (AABB);
	}
	else if (Type_ == BLOCKTYPE_COMPLEX_BLOCK || 
			 Type_ == BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK)
	{
		CAABBListVertexListener Listener;
		Listener.Position = Position;
		BlocksList (ID())->Render (Attributes, &CLIENT.MapLoader(), Position, &Listener);

		SBlockAABB AABB = {};
		AABB.Start	= Listener.Start;
		AABB.End	= Listener.End;
		AABB.Type	= BLOCK_AABB_SELECTION_AND_PHYSICS;

		AABBList.Insert (AABB);
	}
}

void CBlock::Render (byte, CBlockManagerReadOnly*, SIVector, CVertexListener*) const
{
}

bool CBlock::RenderPreview (byte, SVector2D<byte>&) const
{
	return false;
}

void CBlock::OnAdd (byte Attributes, CBlockManager* BlockManager, SIVector Position, byte, SFVector2D) const
{
	if (CanAdd_ (Attributes, BlockManager, Position, true))
		BlockManager->SetBlock (Position, CBlockData (ID(), Attributes));
}

void CBlock::OnRemove (byte, CBlockManager* BlockManager, SIVector Position) const
{
	BlockManager->SetBlock (Position, CBlockData());
}

bool CBlock::OnInteraction (byte, CBlockManager*, SIVector, byte, SFVector2D, CItemData) const
{
	return false;
}