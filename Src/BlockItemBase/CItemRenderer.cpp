﻿#include "CItemRenderer.h"
#include "../CApplication.h"

CItemRenderer CItemRenderer::Instance;

CItemRenderer::CItemRenderer()
{
	CMatrixEngine MatrixEngine;
	MatrixEngine.ResetModelViewMatrix();
	MatrixEngine.RotateX (30.0f);
	MatrixEngine.RotateY (30.0f);
	Matrix_ = MatrixEngine.ModelViewMatrix();
}

SFVector CItemRenderer::Render (SIVector Position, CItemData Item, CComplexBlocksMeshQueue* MQ, float& MaxSize, bool RotateBlock)
// TODO: split this function
{
	MinCoord_ = SFVector (1000.0f);
	MaxCoord_ = SFVector (-1000.0f);

	SVector2D<byte> TextureOffset;
	bool UseItemIcon = ItemsList (Item.ID())->Render (Item.Attributes(), TextureOffset);
	const SVector2D<byte> LightVector = SVector2D<byte> (MAX_LIGHT_INTENSITY, MAX_LIGHT_INTENSITY);
	if (UseItemIcon)
	{
		const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
		const SVector2D<byte> FacePosition[4] = { SVector2D<byte> (0, 0), SVector2D<byte> (1, 0), 
												  SVector2D<byte> (1, 1), SVector2D<byte> (0, 1) };
		for (int i = 0; i < 6; i++)
		{
			int VertexIndex = VertexIndexes[i];

			MQ->AddVertex ((Position + SVector<byte> (FacePosition[VertexIndex].x, FacePosition[VertexIndex].y, 0)) * (2*256), 
						   (SVector2D<unsigned short> (TextureOffset) * 2 + 
						    SVector2D<unsigned short> (FacePosition[VertexIndex])) * 256, LightVector);
		}

		MaxSize = 1.0f;
		return SFVector (0.0f, 0.0f, 0.0f);
	}
	else
	{
		assert (Item.ID() < 256);
		CBlockData Block = CBlockData ((byte) Item.ID(), Item.Attributes());
		if (Block.Type() == BLOCKTYPE_BLOCK)
		{
			SIVector FaceVertices[4];
			SVector2D<unsigned short> TextureCoord[4];

			for (int i = 0; i < NUM_FACES; i++)
			{
				byte Face = IndexToFace ((byte) i);
				GetFace (Face, FaceVertices);
				
				TextureCoord[0] = Block.BlockFaceTextureOffset (Face);
				assert (TextureCoord[0].x < 16 && TextureCoord[0].y < 16);
				TextureCoord[0] *= 2;

				GetTextureCoordsForFace (TextureCoord, Face);
				
				byte FoggingLighting = 0;
				if (Face == LYFACE_MASK || Face == HYFACE_MASK)
					FoggingLighting = 0;
				else if (Face == LXFACE_MASK || Face == HXFACE_MASK)
					FoggingLighting = 4;
				else
					FoggingLighting = 6;

				const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 }; // TODO: constant array
				for (int j = 0; j < 6; j++)
				{
					int VertexIndex = VertexIndexes[j];

					SFVector VertexPosition = (SFVector) Position;

					if (RotateBlock)
						VertexPosition += Matrix_ * FaceVertices[VertexIndex];
					else
						VertexPosition += FaceVertices[VertexIndex];

					
					SFVector v = VertexPosition;

					if (v.x < MinCoord_.x) MinCoord_.x = v.x;
					if (v.y < MinCoord_.y) MinCoord_.y = v.y;
					if (v.z < MinCoord_.z) MinCoord_.z = v.z;

					if (v.x > MaxCoord_.x) MaxCoord_.x = v.x;
					if (v.y > MaxCoord_.y) MaxCoord_.y = v.y;
					if (v.z > MaxCoord_.z) MaxCoord_.z = v.z;

					MQ->AddVertex ((SVector<short>) (VertexPosition * (2.0f*256.0f)), TextureCoord[VertexIndex] * 256, 
								   LightVector - SVector2D<byte> (FoggingLighting));
				}
			}
		}
		else
		{
			MQ_ = MQ;
			Position_ = Position;
			CBlockData Block ((byte) Item.ID(), Item.Attributes());
			Rotate_ = Block.Rotate();
			NumVertices_ = 0;
			BlocksList ((byte) Item.ID())->Render (Item.Attributes(), &CBasicBlockManager::Instance, SIVector (0), this);
		}
	}

	SFVector Size = MaxCoord_ - MinCoord_;

	SFVector Translate = Size / 2.0f - MaxCoord_ + SFVector (0.5f, 0.5f, 0.0f);
	MaxSize = max (Size.x, Size.y);

	return Translate;
}

void CItemRenderer::AddVertex (SFVector VertexPosition, SVector2D<byte> TextureShift, SVector2D<unsigned short> TextureCoord)
{
	if (Rotate_ != (ROTATEX_NONE | ROTATEY_NONE | ROTATEZ_NONE))
		VertexPosition = RotateVector (VertexPosition - SFVector (0.5f), Rotate_) + SFVector (0.5f);

	VertexPositions_[NumVertices_ % 3] = VertexPosition;
	TextureCoords_[NumVertices_ % 3] = TextureCoord;
	
	NumVertices_++;
	if (NumVertices_ % 3 != 0)
		return;
	
	for (int i = 0; i < 3; i++)
	{
		VertexPositions_[i] = (SFVector) Position_ + Matrix_ * VertexPositions_[i];
		
		SFVector v = VertexPositions_[i];

		if (v.x < MinCoord_.x) MinCoord_.x = v.x;
		if (v.y < MinCoord_.y) MinCoord_.y = v.y;
		if (v.z < MinCoord_.z) MinCoord_.z = v.z;

		if (v.x > MaxCoord_.x) MaxCoord_.x = v.x;
		if (v.y > MaxCoord_.y) MaxCoord_.y = v.y;
		if (v.z > MaxCoord_.z) MaxCoord_.z = v.z;
	}

	SFVector N = ((VertexPositions_[1] - VertexPositions_[0]) * (VertexPositions_[2] - VertexPositions_[0])).Normalize();
	byte Light = MAX_LIGHT_INTENSITY - (byte) (fabs (N.x) * 5.0f + fabs (N.z) * 10.0f) + 5;
	if (Light > MAX_LIGHT_INTENSITY)
		Light = MAX_LIGHT_INTENSITY;

	for (int i = 0; i < 3; i++)
	{
		MQ_->AddVertex ((SVector<short>) (VertexPositions_[i] * (2.0f*256.0f)), 
			SVector2D<unsigned short> (TextureShift) * 2 * 256 + TextureCoords_[i], SVector2D<byte> (Light, 0));
	}
}
