﻿#include "CBlocksList.h"
#include "../CApplication.h"

CBlocksList CBlocksList::Instance;

void CBlocksList::RegisterBlock_ (CBlock* Block)
{
	assert (Block);

	byte ID = Block->ID();
	assert (ID < MAX_BLOCKS);
	assert (!Blocks_[ID]);
	if (ID != 0)
		ItemsList.Blocks_[ID].SetID (ID);

	Blocks_[ID] = Block;
}

void CBlocksList::Init()
{
	Air			.SetID (0);
	Usual		.SetID (1);
	Glass		.SetID (2);
	Glowing		.SetID (3);
	Lamp		.SetID (4);
	Water		.SetID (5);
	Ice			.SetID (6);
	Half		.SetID (7);
	DoubleHalf	.SetID (8);
	Stairs		.SetID (9);
	Door		.SetID (10);
	Cabel		.SetID (11);
}