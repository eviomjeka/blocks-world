﻿#include "CItemsList.h"
#include "../CApplication.h"

CItemsList CItemsList::Instance;

void CItemsList::RegisterItem_ (CItem* Item)
{
	assert (Item);

	unsigned short ID = Item->ID();
	assert (ID < MAX_ITEMS);
	assert (!Items_[ID]);

	Items_[ID] = Item;
}

void CItemsList::Init()
{
	Void.SetID (0);
}