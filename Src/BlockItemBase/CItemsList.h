﻿#ifndef CITEMSLIST
#define CITEMSLIST

#include "CItem.h"
#include "../Items/ItemsInclude.h"

class CItemsList
{
private:

	friend class CItem;
	friend class CBlocksList;

	CSArray<CItem*, MAX_ITEMS> Items_;
	CSArray<CItemBlock, MAX_BLOCKS> Blocks_;

	void RegisterItem_ (CItem* Item);

public:
	
	static CItemsList Instance;
	void Init();
	inline CItem* operator () (unsigned short ID);
	inline CItemBlock& Block (byte ID);

	CItemVoid	Void;

};

CItem* CItemsList::operator () (unsigned short ID)
{
	return Items_[ID];
}

CItemBlock& CItemsList::Block (byte ID)
{
	__assert (ID != 0);
	__assert (Items_[ID]);

	return Blocks_[ID];
}

#endif