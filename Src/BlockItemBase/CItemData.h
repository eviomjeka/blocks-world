﻿#ifndef CITEMDATA
#define CITEMDATA

#include "CItemsList.h"

class CItemData
{
private:

	unsigned short ID_;
	byte Attributes_;
	
	friend CStream& operator << (CStream& Stream, CItemData& Item);
	friend CStream& operator >> (CStream& Stream, CItemData& Item);

public:

	inline CItemData (unsigned short ID = 0, byte Attributes = 0);
	
	inline bool operator == (const CItemData Item) const;
	inline bool operator != (const CItemData Item) const;
	inline unsigned short ID() const;
	inline bool Void() const;
	inline byte Attributes() const;

	inline void GetName (CString& String) const;

};

CItemData::CItemData (unsigned short ID, byte Attributes)
	: ID_ (ID), Attributes_ (Attributes)
{}

bool CItemData::operator == (const CItemData Item) const
{
	return ID_ == Item.ID_ && Attributes_ == Item.Attributes_;
}

bool CItemData::operator != (const CItemData Item) const
{
	return ID_ != Item.ID_ || Attributes_ != Item.Attributes_;
}

unsigned short CItemData::ID() const
{
	return ID_;
}

bool CItemData::Void() const
{
	return ID_ == 0;
}

byte CItemData::Attributes() const
{
	return Attributes_;
}

//==============================================================================

void CItemData::GetName (CString& String) const
{
	ItemsList (ID_)->GetName (Attributes_, String);
}

#endif