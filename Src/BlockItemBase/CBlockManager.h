﻿#ifndef CBLOCKMANAGER
#define CBLOCKMANAGER

#include <Engine.h>

class CBlockData;

class CBlockManagerReadOnly
{
public:
	virtual CBlockData GetBlock (SIVector Position) = 0;
};

class CBlockManager : public CBlockManagerReadOnly
{
public:
	virtual void SetBlock (SIVector Position, CBlockData Block) = 0;
};

class CBasicBlockManager : public CBlockManager
{
public:

	static CBasicBlockManager Instance;

	virtual void SetBlock (SIVector Position, CBlockData Block);
	virtual CBlockData GetBlock (SIVector Position);

};

#endif