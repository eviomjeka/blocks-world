﻿#include "CItemData.h"

CStream& operator << (CStream& Stream, CItemData& Item)
{
	Stream << Item.ID_;
	Stream << Item.Attributes_;

	return Stream;
}

CStream& operator >> (CStream& Stream, CItemData& Item)
{
	Stream >> Item.ID_;
	Stream >> Item.Attributes_;

	return Stream;
}
