﻿#ifndef CBLOCK
#define CBLOCK

#include "BlockItem.h"

class CBlockData;
class CItemData;

struct SBlockAttributeInfo
{
	byte Rotate;
	byte LightIntensity;
	byte CanTick;
	byte LightAttenuation[NUM_FACES];
	byte FaceType[NUM_FACES];
	SVector2D<byte> BlockFaceTextureOffset[NUM_FACES];
};

class CBlock
{
public:
	
	CBlock();
	
	void SetID (const byte ID);
	inline byte ID() const;
	
	virtual void GetName			(byte Attributes, CString& Name) const = 0;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;
	virtual void GetAABBList		(byte Attributes, SIVector Position, CArray<SBlockAABB>& AABBList) const;
	virtual void Render				(byte Attributes, CBlockManagerReadOnly* BlockManager, 
									 SIVector Position, CVertexListener* VertexListener) const;
	virtual bool RenderPreview		(byte Attributes, SVector2D<byte>& TextureOffset) const;

	virtual void OnAdd				(byte Attributes, CBlockManager* BlockManager, SIVector Position, 
									 byte Face, SFVector2D Angle) const;
	virtual void OnRemove			(byte Attributes, CBlockManager* BlockManager, SIVector Position) const;
	virtual bool OnInteraction		(byte Attributes, CBlockManager* BlockManager, SIVector Position, 
									 byte Face, SFVector2D Angle, CItemData Item) const;

protected:

	void SetType_							(const byte Type);

	void SetRotate_							(const byte Rotate, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetCanTick_						(const byte CanTick, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetLightIntensity_					(const byte LightIntensity, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetLightAttenuation_				(const byte LightAttenuation, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetLightAttenuationToFace_			(const byte Face, const byte LightAttenuation, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetFaceType_						(const byte FaceType, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetFaceTypeToFace_					(const byte Face, const byte FaceType, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetBlockFaceTextureOffset_			(const SVector2D<byte> BlockFaceTextureOffset, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	void SetBlockFaceTextureOffsetToFace_	(const byte Face, const SVector2D<byte> BlockFaceTextureOffset, 
											 const byte AttributesStart = 255, const byte AttributesEnd = 0);
	
	CBlockData RotateGetBlock_	(byte Attributes, CBlockManagerReadOnly* BlockManager, 
								 SIVector Position, SIVector NeighbourPosition) const;
	bool CanAddOnFace_			(byte Attributes, CBlockManagerReadOnly* BlockManager,  // TODO: to block/item api
								 SIVector Position, byte Face, bool AddOnNotNormalFace) const;
	bool CanAdd_				(byte Attributes, CBlockManagerReadOnly* BlockManager, 
								 SIVector Position, bool AddOnNotNormalFace) const;
	
private:
	
	friend class CBlockData;

	byte ID_;
	byte Type_;
	SBlockAttributeInfo BlockInfo_[MAX_ATTRIBUTES];

};

byte CBlock::ID() const
{
	return ID_;
}

#endif