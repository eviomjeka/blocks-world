﻿#ifndef CBLOCKDATA
#define CBLOCKDATA

#include "CBlocksList.h"

class CBlockData
{
public:

	inline CBlockData (byte ID = 0, byte Attributes = 0);
	
	inline byte ID() const;
	inline byte Attributes() const;
	inline bool operator == (CBlockData Block) const;
	inline bool operator != (CBlockData Block) const;

	inline byte				Type					() const;
	inline byte				Rotate					() const;
	inline byte				CanTick					() const;
	inline byte				LightIntensity			() const;
	inline byte				LightAttenuation		(const byte Face) const;
	inline byte				FaceType				(const byte Face) const;
	inline SVector2D<byte>	BlockFaceTextureOffset	(const byte Face) const;
	
private:

	byte ID_;
	byte Attributes_;

	friend CStream& operator << (CStream& Stream, CBlockData& Block);
	friend CStream& operator >> (CStream& Stream, CBlockData& Block);

};

CBlockData::CBlockData (byte ID, byte Attributes)
	: ID_ (ID), Attributes_ (Attributes)
{
}

byte CBlockData::ID() const
{
	return ID_;
}

byte CBlockData::Attributes() const
{
	return Attributes_;
}

bool CBlockData::operator == (CBlockData Block) const
{
	return ID_ == Block.ID_ && Attributes_ == Block.Attributes_;
}

bool CBlockData::operator != (CBlockData Block) const
{
	return ID_ != Block.ID_ || Attributes_ != Block.Attributes_;
}

//==============================================================================

byte CBlockData::Type() const
{
	return BlocksList (ID_)->Type_; // TODO: performance problem with pointer
}

byte CBlockData::Rotate() const
{
	return BlocksList (ID_)->BlockInfo_[Attributes_].Rotate;
}

byte CBlockData::CanTick() const
{
	return BlocksList (ID_)->BlockInfo_[Attributes_].CanTick;
}

byte CBlockData::LightIntensity() const
{
	return BlocksList (ID_)->BlockInfo_[Attributes_].LightIntensity;
}

byte CBlockData::LightAttenuation (const byte Face) const
{
	__assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			  Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);
	
	byte Index = FaceToIndex (Face);
	return BlocksList (ID_)->BlockInfo_[Attributes_].LightAttenuation[Index];
}

byte CBlockData::FaceType (const byte Face) const
{
	__assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			  Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);
	
	byte Index = FaceToIndex (Face);
	return BlocksList (ID_)->BlockInfo_[Attributes_].FaceType[Index];
}

SVector2D<byte> CBlockData::BlockFaceTextureOffset (const byte Face) const
{
	__assert (Face == LXFACE_MASK || Face == HXFACE_MASK || Face == LYFACE_MASK || 
			  Face == HYFACE_MASK || Face == LZFACE_MASK || Face == HZFACE_MASK);
	
	byte Index = FaceToIndex (Face);
	return BlocksList (ID_)->BlockInfo_[Attributes_].BlockFaceTextureOffset[Index];
}

#endif