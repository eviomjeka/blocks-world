﻿#include "BlockItem.h"

byte FaceToIndex (byte Face)
{
	switch (Face)
	{
		case LXFACE_MASK: return 0;
		case HXFACE_MASK: return 1;
		case LYFACE_MASK: return 2;
		case HYFACE_MASK: return 3;
		case LZFACE_MASK: return 4;
		case HZFACE_MASK: return 5;
		default: error ("Unknown face"); break;
	};

	return 0;
}

byte IndexToFace (byte Index)
{
	__assert (Index >= 0 && Index < 6);
	return 1 << Index;
}

byte InvertFace (byte Face)
{
	switch (Face)
	{
		case LXFACE_MASK: return HXFACE_MASK;
		case HXFACE_MASK: return LXFACE_MASK;
		case LYFACE_MASK: return HYFACE_MASK;
		case HYFACE_MASK: return LYFACE_MASK;
		case LZFACE_MASK: return HZFACE_MASK;
		case HZFACE_MASK: return LZFACE_MASK;
		default: error ("Unknown face"); break;
	};

	return 0;
}

SIVector FaceToVector (SIVector Vector, byte Face)
{
	switch (Face)
	{
		case HXFACE_MASK: Vector.x++; break;
		case LXFACE_MASK: Vector.x--; break;
		case HYFACE_MASK: Vector.y++; break;
		case LYFACE_MASK: Vector.y--; break;
		case HZFACE_MASK: Vector.z++; break;
		case LZFACE_MASK: Vector.z--; break;
		default: error ("Unknown face"); break;
	}

	return Vector;
}

byte VectorToFace (SIVector Vector)
{
	if		(Vector == SIVector (-1, 0, 0)) return LXFACE_MASK;
	else if	(Vector == SIVector (+1, 0, 0)) return HXFACE_MASK;
	else if	(Vector == SIVector (0, -1, 0)) return LYFACE_MASK;
	else if	(Vector == SIVector (0, +1, 0)) return HYFACE_MASK;
	else if	(Vector == SIVector (0, 0, -1)) return LZFACE_MASK;
	else if	(Vector == SIVector (0, 0, +1)) return HZFACE_MASK;

	error ("Incorrect vector");
	return 0;
}

void GetFace (byte Face, SIVector v[4], SIVector Position)
{	
	SIVector Lo = Position;
	SIVector Hi = Lo + SIVector (1);

	switch (Face)
	{
	case LXFACE_MASK:
		v[0] (Lo.x, Lo.y, Lo.z);
		v[1] (Lo.x, Lo.y, Hi.z);
		v[2] (Lo.x, Hi.y, Hi.z);
		v[3] (Lo.x, Hi.y, Lo.z);
	break;

	case HXFACE_MASK:
		v[0] (Hi.x, Lo.y, Lo.z);
		v[1] (Hi.x, Hi.y, Lo.z);
		v[2] (Hi.x, Hi.y, Hi.z);
		v[3] (Hi.x, Lo.y, Hi.z);
	break;

	case LYFACE_MASK:
		v[0] (Lo.x, Lo.y, Lo.z);
		v[1] (Hi.x, Lo.y, Lo.z);
		v[2] (Hi.x, Lo.y, Hi.z);
		v[3] (Lo.x, Lo.y, Hi.z);
	break;

	case HYFACE_MASK:
		v[0] (Lo.x, Hi.y, Lo.z);
		v[1] (Lo.x, Hi.y, Hi.z);
		v[2] (Hi.x, Hi.y, Hi.z);
		v[3] (Hi.x, Hi.y, Lo.z);
	break;

	case LZFACE_MASK:
		v[0] (Lo.x, Lo.y, Lo.z);
		v[1] (Lo.x, Hi.y, Lo.z);
		v[2] (Hi.x, Hi.y, Lo.z);
		v[3] (Hi.x, Lo.y, Lo.z);
	break;

	case HZFACE_MASK:
		v[0] (Lo.x, Lo.y, Hi.z);
		v[1] (Hi.x, Lo.y, Hi.z);
		v[2] (Hi.x, Hi.y, Hi.z);
		v[3] (Lo.x, Hi.y, Hi.z);
	break;

	default:
		error ("Unknown face");
	break;
	};
}

SFVector RotateToRotationVector (byte Rotate)
{
	SFVector RotationVector (0.0f, 0.0f, 0.0f);
	if		(Rotate % 4 == ROTATE_CW90)		RotationVector.x = 90.0f;
	else if	(Rotate % 4 == ROTATE_180)		RotationVector.x = 180.0f;
	else if	(Rotate % 4 == ROTATE_CCW90)	RotationVector.x = -90.0f;
	Rotate /= 4;

	if		(Rotate % 4 == ROTATE_CW90)		RotationVector.y = 90.0f;
	else if	(Rotate % 4 == ROTATE_180)		RotationVector.y = 180.0f;
	else if	(Rotate % 4 == ROTATE_CCW90)	RotationVector.y = -90.0f;
	Rotate /= 4;
		
	if		(Rotate % 4 == ROTATE_CW90)		RotationVector.z = 90.0f;
	else if	(Rotate % 4 == ROTATE_180)		RotationVector.z = 180.0f;
	else if	(Rotate % 4 == ROTATE_CCW90)	RotationVector.z = -90.0f;

	return RotationVector;
}

byte GetRotateYFace (SFVector2D Rotate)
{
	byte Face = 0;

	if (Rotate.y <= 45.0f || Rotate.y >= 360.0f - 45.0f)
		Face = LZFACE_MASK;
	else if (Rotate.y >= 90.0f + -45.0f && Rotate.y <= 90.0f + 45.0f)
		Face = LXFACE_MASK;
	else if (Rotate.y >= 180.0f + -45.0f && Rotate.y <= 180.0f + 45.0f)
		Face = HZFACE_MASK;
	else if (Rotate.y >= 270.0f + -45.0f && Rotate.y <= 270.0f + 45.0f)
		Face = HXFACE_MASK;
	else
		error ("Incorrect rotate parameter");

	return Face;
}

void RotateBlockAABB (SBlockAABB& BlockAABB, byte Rotate)
{
	BlockAABB.Start	= RotateVector (BlockAABB.Start - SFVector (0.5f), Rotate) + SFVector (0.5f);
	BlockAABB.End	= RotateVector (BlockAABB.End - SFVector (0.5f), Rotate) + SFVector (0.5f);
}

const char* AttributesToColor (byte Attributes)
{
	switch (Attributes)
	{
		case BLOCK_BLACK:		return "black";
		case BLOCK_DARK_GRAY:	return "dark_gray";
		case BLOCK_LIGHT_GRAY:	return "light_gray";
		case BLOCK_WHITE:		return "white";
		case BLOCK_RED:			return "red";
		case BLOCK_GREEN:		return "green";
		case BLOCK_BLUE:		return "blue";
		case BLOCK_YELLOW:		return "yellow";
		case BLOCK_VIOLET:		return "violet";
		case BLOCK_CYAN:		return "cyan";
		case BLOCK_ORANGE:		return "orange";
		case BLOCK_DARK_GREEN:	return "dark_green";
		case BLOCK_LIGHT_BLUE:	return "light_blue";
		case BLOCK_PURPLE:		return "purple";
		case BLOCK_PINK:		return "pink";
		case BLOCK_BROWN:		return "brown";
		default:				error ("Unknown block color");
	}

	return 0;
}
