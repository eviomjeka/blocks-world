﻿#ifndef BLOCKITEM
#define BLOCKITEM

#include <Engine.h>
#include "CBlockManager.h"
#include "../MeshConstructors/CBlocksMeshQueue.h"
#include "../MeshConstructors/CComplexBlocksMeshQueue.h"

#define BlocksList	CBlocksList::Instance
#define ItemsList	CItemsList::Instance

#define BlockData(Block)					CBlockData (BlocksList.Block.ID())
#define BlockDataEx(Block, Attributes)		CBlockData (BlocksList.Block.ID(), Attributes)

#define ItemData(Item)						CItemData (ItemsList.Item.ID())
#define ItemDataEx(Item, Attributes)		CItemData (ItemsList.Item.ID(), Attributes)

#define ItemDataBlock(Block)				CItemData (BlocksList.Block.ID())
#define ItemDataBlockEx(Block, Attributes)	CItemData (BlocksList.Block.ID(), Attributes)

//==============================================================================

#define MAX_BLOCKS					256
#define MAX_ITEMS					(256*256)
#define MAX_ATTRIBUTES				256

#define MAX_BLOCK_MESH_SIZE			256
#define MAX_ITEM_PREVIEW_MESH_SIZE	256
#define MAX_BLOCK_BOUNDING_BOXES	16

#define LIGHT_NOT_PASS				(MAX_LIGHT_INTENSITY + 1)

//==============================================================================

#define NUM_FACES		6
#define MAX_FACE		63

#define LXFACE_MASK 1
#define HXFACE_MASK 2
#define LYFACE_MASK 4
#define HYFACE_MASK 8
#define LZFACE_MASK 16
#define HZFACE_MASK 32

//==============================================================================

#define MAX_ROTATE		63

#define ROTATE_NONE		0
#define ROTATE_CW90		1
#define ROTATE_180		2
#define ROTATE_CCW90	3

#define ROTATEX_NONE	(ROTATE_NONE	<< 0)
#define ROTATEX_CW90	(ROTATE_CW90	<< 0)
#define ROTATEX_180		(ROTATE_180		<< 0)
#define ROTATEX_CCW90	(ROTATE_CCW90	<< 0)

#define ROTATEY_NONE	(ROTATE_NONE	<< 2)
#define ROTATEY_CW90	(ROTATE_CW90	<< 2)
#define ROTATEY_180		(ROTATE_180		<< 2)
#define ROTATEY_CCW90	(ROTATE_CCW90	<< 2)

#define ROTATEZ_NONE	(ROTATE_NONE	<< 4)
#define ROTATEZ_CW90	(ROTATE_CW90	<< 4)
#define ROTATEZ_180		(ROTATE_180		<< 4)
#define ROTATEZ_CCW90	(ROTATE_CCW90	<< 4)

//==============================================================================

enum EBlockType
{
	BLOCKTYPE_VOID = 0, 
	BLOCKTYPE_BLOCK, 
	BLOCKTYPE_COMPLEX_BLOCK, 
	BLOCKTYPE_COMPLEX_TRANSLUCENT_BLOCK
};

enum EFaceType
{
	FACETYPE_NORMAL = 0, 
	FACETYPE_NOT_NORMAL, 
	FACETYPE_NONE
};

enum EBoundingBoxType
{
	BLOCK_AABB_SELECTION_AND_PHYSICS = 0, 
	BLOCK_AABB_SELECTION = 1, 
	BLOCK_AABB_PHYSICS = 2
};

enum EBlockColorAttributes
{
	BLOCK_BLACK = 0, 
	BLOCK_DARK_GRAY, 
	BLOCK_LIGHT_GRAY, 
	BLOCK_WHITE, 
	BLOCK_RED, 
	BLOCK_GREEN, 
	BLOCK_BLUE, 
	BLOCK_YELLOW, 
	BLOCK_VIOLET, 
	BLOCK_CYAN, 
	BLOCK_ORANGE, 
	BLOCK_DARK_GREEN, 
	BLOCK_LIGHT_BLUE, 
	BLOCK_PURPLE, 
	BLOCK_PINK, 
	BLOCK_BROWN
};

//==============================================================================

class CVertexListener
{
public:
	virtual void AddVertex (SFVector VertexPosition, SVector2D<byte> TextureShift, SVector2D<unsigned short> TextureCoord) = 0;
};

struct SBlockAABB
{
	SFVector Start;
	SFVector End;
	byte	 Type;
};

//==============================================================================

byte FaceToIndex				(byte Face);
byte IndexToFace				(byte Index);
byte InvertFace					(byte Face);
SIVector FaceToVector			(SIVector Vector, byte Face);
byte VectorToFace				(SIVector Vector);
void GetFace					(byte Face, SIVector v[4], SIVector Position = SIVector (0));
SFVector RotateToRotationVector	(byte Rotate);
byte GetRotateYFace				(SFVector2D Rotate);
void RotateBlockAABB			(SBlockAABB& BlockAABB, byte Rotate);
const char* AttributesToColor	(byte Attributes);

template <typename T>
SVector<T> RotateVector			(SVector<T> Position, byte Rotate);
template <typename T>
void GetTextureCoordsForFace	(SVector2D<T> v[4], byte Face, bool Mirror = false);

//==============================================================================

template <typename T>
SVector<T> RotateVector (SVector<T> Vector, byte Rotate)
{
	T A = Vector.y; T* PA = &Vector.y;
	T B = Vector.z; T* PB = &Vector.z;
	if		(Rotate % 4 == ROTATE_CW90)		{ (*PA) = +B; (*PB) = -A; }
	else if	(Rotate % 4 == ROTATE_180)		{ (*PA) = -A; (*PB) = -B; }
	else if	(Rotate % 4 == ROTATE_CCW90)	{ (*PA) = -B; (*PB) = +A; }
	Rotate /= 4;
	
	A = Vector.z; PA = &Vector.z;
	B = Vector.x; PB = &Vector.x;
	if		(Rotate % 4 == ROTATE_CW90)		{ (*PA) = +B; (*PB) = -A; }
	else if	(Rotate % 4 == ROTATE_180)		{ (*PA) = -A; (*PB) = -B; }
	else if	(Rotate % 4 == ROTATE_CCW90)	{ (*PA) = -B; (*PB) = +A; }
	Rotate /= 4;

	A = Vector.x; PA = &Vector.x;
	B = Vector.y; PB = &Vector.y;
	if		(Rotate % 4 == ROTATE_CW90)		{ (*PA) = +B; (*PB) = -A; }
	else if	(Rotate % 4 == ROTATE_180)		{ (*PA) = -A; (*PB) = -B; }
	else if	(Rotate % 4 == ROTATE_CCW90)	{ (*PA) = -B; (*PB) = +A; }
	
	return Vector;
}

template <typename T>
void GetTextureCoordsForFace (SVector2D<T> v[4], byte Face, bool Mirror)
{
	SVector2D<T> v0 = v[0];

	if (Face == LXFACE_MASK || Face == HZFACE_MASK || Face == HYFACE_MASK)
	{
		if (Mirror)
		{
			v[1] = v0 + SVector2D<T> (0, 1);
			v[0] = v0 + SVector2D<T> (1, 1);
			v[3] = v0 + SVector2D<T> (1, 0);
			v[2] = v0;
		}
		else
		{
			v[0] = v0 + SVector2D<T> (0, 1);
			v[1] = v0 + SVector2D<T> (1, 1);
			v[2] = v0 + SVector2D<T> (1, 0);
			v[3] = v0;
		}
	}
	else if (Face == HXFACE_MASK || Face == LZFACE_MASK || Face == LYFACE_MASK)
	{
		if (Mirror)
		{
			v[0] = v0 + SVector2D<T> (0, 1);
			v[3] = v0 + SVector2D<T> (1, 1);
			v[2] = v0 + SVector2D<T> (1, 0);
			v[1] = v0;
		}
		else
		{
			v[3] = v0 + SVector2D<T> (0, 1);
			v[0] = v0 + SVector2D<T> (1, 1);
			v[1] = v0 + SVector2D<T> (1, 0);
			v[2] = v0;
		}
	}
}

#endif
