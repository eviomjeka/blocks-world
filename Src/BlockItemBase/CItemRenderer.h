﻿#ifndef CITEMRENDERER
#define CITEMRENDERER

#include "BlockItem.h"
#include "CItemData.h"

class CItemRenderer : private CVertexListener
{
private:
	
	CComplexBlocksMeshQueue* MQ_;
	SIVector Position_;
	byte Rotate_;
	CMatrix<4,4> Matrix_;
	SFVector VertexPositions_[3];
	SVector2D<unsigned short> TextureCoords_[3];
	int NumVertices_;

	SFVector MinCoord_;
	SFVector MaxCoord_;

	virtual void AddVertex (SFVector VertexPosition, SVector2D<byte> TextureShift, SVector2D<unsigned short> TextureCoord);

public:
	
	static CItemRenderer Instance;
	CItemRenderer();

	SFVector Render (SIVector Position, CItemData Item, CComplexBlocksMeshQueue* MQ, float& MaxSize, bool RotateBlock = true);

};

#endif
