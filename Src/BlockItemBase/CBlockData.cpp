﻿#include "CBlockData.h"

CStream& operator << (CStream& Stream, CBlockData& Block)
{
	Stream << Block.ID_;
	Stream << Block.Attributes_;

	return Stream;
}

CStream& operator >> (CStream& Stream, CBlockData& Block)
{
	Stream >> Block.ID_;
	Stream >> Block.Attributes_;

	return Stream;
}