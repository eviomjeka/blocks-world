﻿#include "CItem.h"
#include "../CApplication.h"

CItem::CItem() :
	ID_ (0)
{}

void CItem::SetID (const unsigned short ID)
{
	ID_ = ID;
	ItemsList.RegisterItem_ (this);
}

//==============================================================================

void CItem::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	InventoryItems.Insert (0);
}

void CItem::OnAdd (byte, CBlockManager*, SIVector, byte, SFVector2D) const
{
}