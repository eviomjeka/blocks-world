﻿#ifndef CITEM
#define CITEM

#include "BlockItem.h"

class CItemData;

class CItem
{
public:
	
	CItem();

	void SetID (const unsigned short ID);
	inline unsigned short ID() const;
	
	virtual void GetName			(byte Attributes, CString& Name) const = 0;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;
	virtual bool Render				(byte Attributes, SVector2D<byte>& TextureOffset) const = 0;
	virtual void OnAdd				(byte Attributes, CBlockManager* BlockManager, 
									 SIVector Position, byte Face, SFVector2D Angle) const;
	
private:
	
	friend class CItemData;

	unsigned short ID_;

};

unsigned short CItem::ID() const
{
	return ID_;
}

#endif