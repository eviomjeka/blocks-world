﻿#include "CChatWindow.h"
#include "../CApplication.h"

CChatWindow::CChatWindow() : 
	Text_ (ALL_STRINGS_CHAT), 
	TextHistory_ (TEXT_HISTORY_SIZE), 
	TextHistoryStart_ (0), 
	CurrentHistoryText_ (0), 
	ChatOpened_ (false), 
	Start_ (0), 
	Cursor_ (0), 
	CursorStep_ (0), 
	AnimationStep_ (0), 
	SliderStep_ (CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP), 
	Shift_ (0), 
	NumStrings_ (0), 
	NumStringsInMessage_ (1)
{
	for (int i = 0; i < ALL_STRINGS_CHAT; i++)
	{
		SChatMessage ChatMessage = {};
		ChatMessage.Step = CHAT_MESSAGE_NOT_HIDE_STEP + CHAT_MESSAGE_HIDE_STEP;
		Text_.Insert (ChatMessage);
	}
	for (int i = 0; i < TEXT_HISTORY_SIZE; i++)
		TextHistory_.Insert (CString::EmptyString);

	KeyToCharacter_.Init (this, true);
}

CChatWindow::~CChatWindow()
{}

void CChatWindow::AddString (CString& String, SColor Color)
{
	int Start = 0;
	int End = String.Length();
	for (;;)
	{
		bool EndOfString = true;
		int WordStart = -1;
		for (int i = Start; i < End; i++)
		{
			if (String[i] != ' ' && WordStart == -1)
				WordStart = i;
			else if (String[i] == ' ')
				WordStart = -1;

			if (String[i] == '\t')
				String[i] = ' ';

			if (FONT.GetLength (String, 1.6f, Start, i + 1) > CHAT_SIZE_X)
			{
				if (WordStart == -1)
					WordStart = i;
				
				int TempEnd = 0;

				if (FONT.GetLength (String, 1.6f, WordStart, i + 1) > CHAT_SIZE_X)
					TempEnd = i;
				else
				{
					TempEnd = WordStart;
					i = WordStart - 1;
				}
				
				CString String2 (String, Start, TempEnd);
				AddString_ (String2, Color);

				Start = TempEnd;
				EndOfString = false;
				break;
			}
		}
		if (EndOfString)
		{
			CString String2 (String, Start, End);
			AddString_ (String2, Color);
			break;
		}
	}
}

void CChatWindow::AddString_ (CString& String, SColor Color)
{
	NumStrings_ = min ((NumStrings_ + 1), ALL_STRINGS_CHAT);
	Start_--;
	if (Start_ < 0) Start_ += ALL_STRINGS_CHAT;
	Text_[Start_].Text  = String;
	Text_[Start_].Color = Color;
	Text_[Start_].Step  = 0.0f;
	if (Shift_ != 0)
	{
		Shift_++;
		if (Shift_ > ALL_STRINGS_CHAT - NUM_STRINGS_CHAT)
			Shift_ = ALL_STRINGS_CHAT - NUM_STRINGS_CHAT;
	}
}

void CChatWindow::Render (int Time)
{
	KeyToCharacter_.Update (Time);

	GUIRENDERER.PushMatrix();
	
	GUIRENDERER.Translate (0.5f + 0.5f, 11.0f, -5.0f);
	byte Alpha = (byte) (AnimationStep_ / CHAT_MAX_ANIMATION_STEP * 255.0f);
	
	GUIRENDERER.SetColor (0, 0, 0, (byte) (Alpha * 0.6f));
	GUIRENDERER.AddRect (SFVector (-0.5f, -0.8f, -0.1f), 
						 SFVector (0.5f + CHAT_SIZE_X, 2.2f * NumStringsInMessage_ - 0.2f, -0.1f));

	CursorStep_ += 1 * Time / 10.0f;
	while (CursorStep_ > 2 * CURSOR_STEP)
		CursorStep_ -= 2 * CURSOR_STEP;
	
	if (ChatOpened_ && AnimationStep_ != CHAT_MAX_ANIMATION_STEP)
	{
		AnimationStep_ += 1 * Time / 10.0f;
		if (AnimationStep_ > CHAT_MAX_ANIMATION_STEP)
			AnimationStep_ = CHAT_MAX_ANIMATION_STEP;
	}
	else if (!ChatOpened_ && AnimationStep_ != 0)
	{
		AnimationStep_ -= 1 * Time / 10.0f;
		if (AnimationStep_ <= 0.0f)
		{
			AnimationStep_ = 0.0f;
			
			CurrentText_.Clear();
			Cursor_ = 0;
			CursorStep_ = 0;
			SliderStep_ = CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP;
			Shift_ = 0;
		}
	}
	if (Shift_ == 0 && SliderStep_ != CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP)
	{
		SliderStep_ += 1 * Time / 10.0f;
		if (SliderStep_ > CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP)
			SliderStep_ = CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP;
	}
	else if (Shift_ != 0 && SliderStep_ != 0)
		SliderStep_ = 0;

	GUIRENDERER.SetColor (255, 255, 255, Alpha);
	GUIRENDERER.PushMatrix();
	for (int i = 0; CurrentText_.Length() != 0 && i < NumStringsInMessage_; i++)
	{
		int j = NumStringsInMessage_ - i - 1;
		int Start = (j == 0) ? 0 : Delimiter_[j - 1];
		int End = (j == NumStringsInMessage_ - 1) ? CurrentText_.Length() : Delimiter_[j];
		CurrentText_.Render (0, 1.8f, Start, End);
		GUIRENDERER.Translate (0.0f, 2.2f, 0.0f);
	}
	GUIRENDERER.PopMatrix();

	if (ChatOpened_ && CursorStep_ < CURSOR_STEP)
	{
		int i = 0;
		for (; i < NumStringsInMessage_; i++)
			if (i == NumStringsInMessage_ - 1 || Delimiter_[i] >= Cursor_)
				break;
		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (((Cursor_ != 0) ? FONT.GetLength (CurrentText_, 1.8f, 
			(i != 0) ? Delimiter_[i - 1] : 0, Cursor_) : 0.0f) - 
			2.1f / 10.0f, -1.5f / 32.0f + 2.2f * (NumStringsInMessage_ - i - 1), 0.1f);
		
		CString Str("|");
		FONT.Render(Str, 2.1f);
		GUIRENDERER.PopMatrix();
	}

	GUIRENDERER.Translate (0, 0.8f + 2.2f * NumStringsInMessage_, 0);
	
	bool Opened = ChatOpened_ && AnimationStep_ == CHAT_MAX_ANIMATION_STEP;
	for (int i = 0; i < ALL_STRINGS_CHAT; i++)
	{
		if (Text_[i].Step != CHAT_MESSAGE_NOT_HIDE_STEP + CHAT_MESSAGE_HIDE_STEP)
		{
			if (!Opened)
			{
				Text_[i].Step += 1 * Time / 10.0f;
				if (Text_[i].Step > CHAT_MESSAGE_NOT_HIDE_STEP + CHAT_MESSAGE_HIDE_STEP)
					Text_[i].Step = CHAT_MESSAGE_NOT_HIDE_STEP + CHAT_MESSAGE_HIDE_STEP;
			}
			else
				Text_[i].Step = CHAT_MESSAGE_NOT_HIDE_STEP + CHAT_MESSAGE_HIDE_STEP;
		}
	}
	for (int i = 0; i < NUM_STRINGS_CHAT; i++)
	{
		int j = (i + Start_ + Shift_) % ALL_STRINGS_CHAT;
		
		byte Alpha2 = (Text_[j].Step < CHAT_MESSAGE_NOT_HIDE_STEP) ? 255 : 
			(byte) (255.0f - (Text_[j].Step - CHAT_MESSAGE_NOT_HIDE_STEP) * 255.0f / CHAT_MESSAGE_HIDE_STEP);

		GUIRENDERER.SetColor (0, 0, 0, (byte) (max (Alpha, Alpha2) * 0.6f));
		GUIRENDERER.AddRect (SFVector (-0.5f, -0.4f, -0.1f), 
							 SFVector (0.5f + CHAT_SIZE_X, 1.4f, -0.1f));
		
		GUIRENDERER.SetColor (SColor (Text_[j].Color.r, Text_[j].Color.g, Text_[j].Color.b, max (Alpha, Alpha2)));
		Text_[j].Text.Render (0, 1.6f);
		GUIRENDERER.Translate (0, 1.8f, 0);
	}
	GUIRENDERER.PopMatrix();

	float ChatSizeY = NUM_STRINGS_CHAT * 1.8f;
	float Size = ChatSizeY / sqrt ((float) (NumStrings_ - NUM_STRINGS_CHAT + 1));
	float Position = 0.8f + 0.4f + 2.2f * NumStringsInMessage_ + (ChatSizeY - Size / 2.0f) - 
		(ChatSizeY - Size) * (NumStrings_ - NUM_STRINGS_CHAT - Shift_) / (NumStrings_ - NUM_STRINGS_CHAT);
	byte Alpha2 = (SliderStep_ < CHAT_SLIDER_NOT_HIDE_STEP) ? 255 : 
		(byte) (255.0f - (SliderStep_ - CHAT_SLIDER_NOT_HIDE_STEP) * 255.0f / CHAT_SLIDER_HIDE_STEP);
	
	GUIRENDERER.PushMatrix();
	GUIRENDERER.SetColor (0, 0, 0, (byte) (min (Alpha, Alpha2) * 0.6f));
	GUIRENDERER.Translate (0.5f + CHAT_SIZE_X + 1.4f, 11.0f - 0.8f, 0.0f);
	GUIRENDERER.AddRect (SFVector (0.0f, -Size / 2.0f + Position, 0.0f), SFVector (1.0f, Size / 2.0f + Position, 0.0f));
	GUIRENDERER.PopMatrix();
}

bool CChatWindow::KeyDown (unsigned Key)
{
	KeyToCharacter_.KeyDown (Key);
	return true;
}

void CChatWindow::KeyUp (unsigned Key)
{
	KeyToCharacter_.KeyUp (Key);
}

void CChatWindow::MouseWhell (char Delta)
{
	bool CanShift = Delta < 0 || NumStrings_ > NUM_STRINGS_CHAT + Shift_;
	if (CanShift)
		Shift_ += Delta;
	if (Shift_ > ALL_STRINGS_CHAT - NUM_STRINGS_CHAT)
		Shift_ = ALL_STRINGS_CHAT - NUM_STRINGS_CHAT;
	else if (Shift_ < 0)
		Shift_ = 0;
}

void CChatWindow::Open()
{
	ChatOpened_ = true;

	RecalculateStrings_();
}

void CChatWindow::Close (bool Animation)
{
	ChatOpened_ = false;
	KeyToCharacter_.StopKeyRepetition();

	if (!Animation)
	{
		AnimationStep_ = 0.0f;
		
		CurrentText_.Clear();
		Cursor_ = 0;
		CursorStep_ = 0;
		SliderStep_ = CHAT_SLIDER_NOT_HIDE_STEP + CHAT_SLIDER_HIDE_STEP;
		CurrentHistoryText_ = 0;
		Shift_ = 0;
	}
}

bool CChatWindow::IsOpened()
{
	return ChatOpened_;
}

void CChatWindow::Character (char Char)
{
	if (!ChatOpened_)
		return;

	if (CurrentText_.Length() == CHAT_MAX_MESSAGE_SIZE - 1)
		return;

	CurrentText_ += 0;
	if (Cursor_ != CurrentText_.Length())
	{
		for (int i = CurrentText_.Length() - 2; i >= Cursor_; i--)
			CurrentText_[i + 1] = CurrentText_[i];
	}
	CurrentText_[Cursor_] = Char;
	
	Cursor_++;
	CursorStep_ = 0;
	
	RecalculateStrings_();
}

void CChatWindow::SpecialKey (unsigned Key)
{
	if (!ChatOpened_ && Key == KEY_UP)
		Open();

	if (!ChatOpened_)
		return;
	// TODO: switch
	if (Key == KEY_ENTER && CurrentText_.Length() != 0)
	{
		CString Text = CurrentText_;
		PLAYERCONTROLLER.SendChatMessage (Text);

		if (strcmp (CurrentText_.Data(), TextHistory_[Mod (TextHistoryStart_ - 1, TEXT_HISTORY_SIZE)].Data()) != 0)
		{
			TextHistory_[TextHistoryStart_] = CurrentText_;
			TextHistoryStart_ = (TextHistoryStart_ + 1) % TEXT_HISTORY_SIZE;
		}
		CurrentHistoryText_ = 0;
		
		CurrentText_.Clear();
		Cursor_ = 0;
		CursorStep_ = 0;

		Close();
	}
	else if (Key == KEY_ENTER)
	{
		Close();
	}
	else if (Key == KEY_UP)
	{
		if (CurrentHistoryText_ != TEXT_HISTORY_SIZE - 1 && 
			TextHistory_[Mod (TextHistoryStart_ - CurrentHistoryText_ - 1, TEXT_HISTORY_SIZE)].Length() > 0)
		{
			if (CurrentHistoryText_ == 0)
				TextHistory_[TextHistoryStart_] = CurrentText_;

			CurrentHistoryText_ = Mod (CurrentHistoryText_ + 1, TEXT_HISTORY_SIZE);
			CurrentText_ = TextHistory_[Mod (TextHistoryStart_ - CurrentHistoryText_, TEXT_HISTORY_SIZE)];
			Cursor_ = CurrentText_.Length();
		}
	}
	else if (Key == KEY_DOWN)
	{
		if (CurrentHistoryText_ != 0)
		{
			CurrentHistoryText_ = Mod (CurrentHistoryText_ - 1, TEXT_HISTORY_SIZE);
			CurrentText_ = TextHistory_[Mod (TextHistoryStart_ - CurrentHistoryText_, TEXT_HISTORY_SIZE)];
			Cursor_ = CurrentText_.Length();
		}
	}
	else if (Key == KEY_LEFT)
	{
		Cursor_--;
		if (Cursor_ < 0)
			Cursor_ = 0;
		else
			CursorStep_ = 0;
	}
	else if (Key == KEY_RIGHT)
	{
		Cursor_++;
		if (Cursor_ > CurrentText_.Length())
			Cursor_ = CurrentText_.Length();
		else
			CursorStep_ = 0;
	}
	else if (Key == KEY_DELETE)
	{
		if (Cursor_ < CurrentText_.Length())
		{
			for (int i = Cursor_; i < CurrentText_.Length(); i++)
			{
				if (i > 0 && i != Cursor_)
					CurrentText_[i - 1] = CurrentText_[i];
			}
			CurrentText_.Shorten (CurrentText_.Length() - 1);

			CursorStep_ = 0;
		}
	}
	else if (Key == KEY_BACKSPACE)
	{
		Cursor_--;
		if (Cursor_ < 0)
			Cursor_ = 0;
		else
		{
			for (int i = Cursor_ + 1; i < CurrentText_.Length(); i++)
			{
				if (i > 0)
					CurrentText_[i - 1] = CurrentText_[i];
			}
			CurrentText_.Shorten (CurrentText_.Length() - 1);

			CursorStep_ = 0;
		}
	}
	
	RecalculateStrings_();
}

void CChatWindow::RecalculateStrings_()
{
	NumStringsInMessage_ = 1;

	int Start = 0;
	int End = CurrentText_.Length();
	for (;;)
	{
		bool EndOfString = true;
		int WordStart = -1;
		for (int i = Start; i < End; i++)
		{
			if (CurrentText_[i] != ' ' && WordStart == -1)
				WordStart = i;
			else if (CurrentText_[i] == ' ')
				WordStart = -1;

			if (FONT.GetLength (CurrentText_, 1.8f, Start, i + 1) > CHAT_SIZE_X)
			{
				if (WordStart == -1)
					WordStart = i;

				if (FONT.GetLength (CurrentText_, 1.8f, WordStart, i + 1) > CHAT_SIZE_X)
					Start = i;
				else
				{
					Start = WordStart;
					i = WordStart - 1;
				}
				EndOfString = false;
				Delimiter_[NumStringsInMessage_ - 1] = Start;
				NumStringsInMessage_++;
				break;
			}
		}
		if (EndOfString)
			break;
	}
}
