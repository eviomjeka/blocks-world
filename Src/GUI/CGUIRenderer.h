﻿#ifndef CGUIRENDERER
#define CGUIRENDERER

#include <Engine.h>

class CGUIRenderer
{
public:
	
	void Init();
	void Destroy();
	void Begin();
	void End();
	void Render();
	void PushMatrix();
	void PopMatrix();
	void Translate (float x, float y, float z);
	SFVector GetTranslateVector();
	SColor GetCurrentColor();
	CMatrixEngine* GetMatrixEngine();
	void Resize (float ortho_gui_size_x);
	float GetTexureCoef();

	void SetColor(SColor Color);
	void SetColor(byte r, byte g, byte b, byte a);
	void AddVertex(SFVector Vertex);
	void AddVertex(float x, float y, float z);
	void AddQuad(SFVector v0, SFVector v1, SFVector v2, SFVector v3);
	void AddTriangle(SFVector v0, SFVector v1, SFVector v2);
	void RemoveQuad(SFVector v0, SFVector v1, SFVector v2, SFVector v3);
	void AddRect(SFVector v0, SFVector v1);
	void AddCircle(SFVector P, float R, int Quality);
	void AddHorizontalLine(float x, float y, float l, float w);
	void AddVerticalLine(float x, float y, float l, float w);

	void AddTextureVertex (SFVector Vertex, SFVector2D TexCoord);

private:

	static const int MESH_SIZE;
	static const int FONT_MESH_SIZE;
	static const int TEXTURE_MESH_SIZE;

	CColorMeshConstructor MeshConstructor_;
	CTextureMeshConstructor FontMeshConstructor_;
	CTextureMeshConstructor TextureMeshConstructor_;
	CMesh Mesh_;
	CMesh FontMesh_;
	CMesh TextureMesh_;

	CTexture Texture_;
	float TexureCoef_;

	CArray<SFVector > Translate_;
	CMatrixEngine MatrixEngine_;

};

#endif