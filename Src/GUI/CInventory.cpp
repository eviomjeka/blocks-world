﻿#include "CInventory.h"
#include "../CApplication.h"

void CInventory::Init()
{
	MeshQueue.Init (&CShaders::ComplexBlocksShader, MAX_ITEM_PREVIEW_MESH_SIZE, 2);
	UseShaderProgram (ComplexBlocksShader);
	CShaders::ComplexBlocksShader.UniformFloat (CShaders::Blocks.FogDistance(), 100.0f); // TODO: fix?

	Opened_ = false;
	Current_ = 0;
	PrevFocused_ = SIVector2D (-1, 0);
	Selected_ = false;
	AirSelected_ = false;
	SelectedCell_.Init (0);
	BlockSelectionState_ = 0.0f;
	State_ = ITEMCELL_MAX_STATE;
	BlockSelection_.Init (200);
	BlockSelection_.SetSelected (true, false);
	
	for (int x = 0; x < NUM_CELLS_X; x++)
		for (int y = 0; y < NUM_CELLS_Y + 1; y++)
		{
			if (y != BOTTOMCELLSY)
			{
				Cells_[x][y].Init (200);
				Cells_[x][y].SetSelected (false, false);
			}
			else
			{
				Cells_[x][y].Init (200);
				if (x == Current_)
					Cells_[x][y].SetSelected (true, false);
				else
					Cells_[x][y].SetSelected (false, false);
			}
		}

	StartBottomCells_[0] = ItemDataBlock (Door); //ItemDataBlockEx (Usual, BLOCK_GRASS);
	StartBottomCells_[1] = ItemDataBlockEx (Usual, BLOCK_DIRT);
	StartBottomCells_[2] = ItemDataBlockEx (Usual, BLOCK_BLACK);
	StartBottomCells_[3] = ItemDataBlock (Glass);
	StartBottomCells_[4] = ItemDataBlock (Glowing);
	StartBottomCells_[5] = ItemDataBlock (Lamp);
	StartBottomCells_[6] = ItemDataBlock (Water);
	StartBottomCells_[7] = ItemDataBlock (Ice);
	StartBottomCells_[8] = ItemDataBlockEx (Half, BLOCK_YELLOW);
	StartBottomCells_[9] = ItemDataBlockEx (Stairs, BLOCK_LIGHT_GRAY);

	int x = 0, y = 0;
	CArray<byte> Items (MAX_ATTRIBUTES);
	for (int i = 1; i <= MAX_ITEMS; i++)
	{
		if (!ItemsList ((unsigned short) i))
			continue;
		Items.Clear();
		ItemsList ((unsigned short) i)->GetInventoryItems (Items);
		for (int j = 0; j < Items.Size(); j++)
		{
			Cells_[x][y].SetItem (CItemData ((unsigned short) i, Items[j]));
			x++;
			if (x == NUM_CELLS_X)
			{
				x = 0;
				y++;
			}
		}
	}
}

void CInventory::Destroy()
{
	for (int x = 0; x < NUM_CELLS_X; x++)
		for (int y = 0; y < NUM_CELLS_Y + 1; y++)
			Cells_[x][y].Destroy();

	SelectedCell_.Destroy();
	BlockSelection_.Destroy();
}

void CInventory::New()
{
	Current_ = 0;

	for (int i = 0; i < NUM_CELLS_X; i++)
		Cells_[i][BOTTOMCELLSY].SetItem (StartBottomCells_[i]);
}

void CInventory::Load (CStream& Stream)
{
	Current_ = 0;
	
	for (int i = 0; i < NUM_CELLS_X; i++)
	{
		short ID = 0; byte Attributes = 0;
		Stream >> ID; Stream >> Attributes;
		Cells_[i][BOTTOMCELLSY].SetItem (CItemData (ID, Attributes));
	}
}

void CInventory::Save (CStream& Stream)
{
	for (int i = 0; i < NUM_CELLS_X; i++)
	{
		unsigned short ID = Cells_[i][BOTTOMCELLSY].Item().ID();
		byte Attributes = Cells_[i][BOTTOMCELLSY].Item().Attributes();
		Stream << ID;
		Stream << Attributes;
	}
}

void CInventory::OpenClose (bool Open)
{
	bool CurrentSelected = (Opened_ && PrevFocused_.x == Current_ && PrevFocused_.y == NUM_CELLS_Y);

	Opened_ = Open;
	if (!Open)
	{
		Selected_ = false;

		if (PrevFocused_.x != -1)
		{
			if (!CurrentSelected)
				Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (false, false);
			PrevFocused_.x = -1;
		}
	}
	if (!CurrentSelected)
		Cells_[Current_][BOTTOMCELLSY].SetSelected (!Opened_);
	if (CurrentSelected)
		State_ = ITEMCELL_MAX_STATE;
}

bool CInventory::IsOpened()
{
	return Opened_;
}

void CInventory::Render (int Time)
{
	if (!Opened_)
	{
		if (State_ < ITEMCELL_MAX_STATE)
			State_ += 1 * Time / 10.0f;
		if (State_ > ITEMCELL_MAX_STATE)
			State_ = ITEMCELL_MAX_STATE;
	}
	else
	{
		if (State_ > 0)
			State_ -= 1 * Time / 10.0f;
		if (State_ < 0)
			State_ = 0.0f;
	}

	if (!Opened_ && BlockSelection_.Item().ID() && !AirSelected_)
		BlockSelectionState_ = 50.0f;
	else if (!Opened_ && BlockSelection_.Item().ID() && AirSelected_)
	{
		if (BlockSelectionState_ > 0.0f)
			BlockSelectionState_ -= 1 * Time / 10.0f;
		if (BlockSelectionState_ < 0.0f)
			BlockSelectionState_ = 0.0f;

		if (BlockSelectionState_ == 0.0f && AirSelected_)
		{
			BlockSelection_.SetItem (ItemData (Void));
			AirSelected_ = false;
		}
	}

	if (!Opened_ && BlockSelectionState_ != 0.0f)
	{
		float x = (CApplication::GUI_SIZE_X + (CApplication::GUI_SIZE_X + NUM_CELLS_X * 
			(ITEMCELL_BACK_SIZE + CELLS_DISTANCE) - CELLS_DISTANCE) / 2) / 2.0f - ITEMCELL_BACK_SIZE / 2.0f + 0.5f;

		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (x, 8.5f, -0.1f);
		GUIRENDERER.SetColor (0, 0, 0, 100);
		GUIRENDERER.AddRect (SFVector (-8.0f, -5.5f, 0.0f), SFVector (ITEMCELL_BACK_SIZE + 8.0f, 4.5f, 0.0f));
		GUIRENDERER.PopMatrix();

		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (x, 5.0f, 55.0f);
		BlockSelection_.Render (Time);
		GUIRENDERER.PopMatrix();
		
		RenderBlockName_ (SFVector2D (x + ITEMCELL_BACK_SIZE / 2.0f, 12.0f + ITEMCELL_BACK_SIZE), BlockSelection_);
	}

	float CurrentX = 0.0f;
	for (int i = 0; i < NUM_CELLS_X; i++)
	{
		float BottomCellX = (CApplication::GUI_SIZE_X - NUM_CELLS_X * (ITEMCELL_BACK_SIZE +
			  CELLS_DISTANCE) + CELLS_DISTANCE) / 2 +
			  i * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE);
		float BottomCellY = 4.0f;

		if (Current_ == i)
			CurrentX = BottomCellX;

		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (BottomCellX, BottomCellY, 0.0f);
		Cells_[i][BOTTOMCELLSY].Render (Time);
		GUIRENDERER.PopMatrix();
	}

	if (Cells_[Current_][BOTTOMCELLSY].Item().ID())
		RenderBlockName_ (SFVector2D (CurrentX + ITEMCELL_BACK_SIZE / 2.0f, 6.0f), Cells_[Current_][BOTTOMCELLSY], 
			(byte) (State_ * 255.0f / ITEMCELL_MAX_STATE));

	if (!Opened_)
		return;

	float SizeX = CELLS_DISTANCE * (NUM_CELLS_X - 1) + ITEMCELL_BACK_SIZE * NUM_CELLS_X + 2.0f * 5.0f;
	float SizeY = CELLS_DISTANCE * (NUM_CELLS_Y - 1) + ITEMCELL_BACK_SIZE * NUM_CELLS_Y + 2.0f * 5.0f;
	float x0 = (CApplication::GUI_SIZE_X - SizeX) / 2.0f;
	float y0 = (CApplication::GUI_SIZE_Y - SizeY) / 2.0f;

	GUIRENDERER.SetColor (128, 128, 128, 128);
	GUIRENDERER.AddRect (SFVector (x0, y0, 0.0f), SFVector (x0 + SizeX, y0 + SizeY, 0.0f));

	for (int x = 0; x < NUM_CELLS_X; x++)
		for (int y = 0; y < NUM_CELLS_Y; y++)
		{
			GUIRENDERER.PushMatrix();
			GUIRENDERER.Translate (5.0f + x0 + x * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE), 
								   5.0f + y0 + y * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE), 0.1f);
			Cells_[x][NUM_CELLS_Y - (1 + y)].Render (Time);
			GUIRENDERER.PopMatrix();
		}

	if (Selected_)
	{
		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (SelectedCellPos_.x, SelectedCellPos_.y, 55.0f);
		SelectedCell_.Render (Time);
		GUIRENDERER.PopMatrix();
	}
	if (!Selected_ && PrevFocused_.x != -1 && Cells_[PrevFocused_.x][PrevFocused_.y].Item().ID())
		RenderBlockName_ (PrevCellPosition_, Cells_[PrevFocused_.x][PrevFocused_.y]);

	if (Selected_)
		RenderBlockName_ (SFVector2D (SelectedCellPos_.x + ITEMCELL_BACK_SIZE / 2.0f, 
									  SelectedCellPos_.y + ITEMCELL_BACK_SIZE / 2.0f), SelectedCell_);
}

void CInventory::RenderBlockName_ (SFVector2D Position, CItemCell& Cell, byte Alpha)
{
	if (Alpha == 0)
		return;

	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (Position.x, Position.y - 5.3f, 2.0f * 55.0f);

	CString Name;
	Cell.Item().GetName (Name);
	CLocalisation::GetBlockLocalisation (Name, Text_);

	Text_.RecalculateSize();
	float Size = Text_.Size();
		
	GUIRENDERER.SetColor (0, 0, 0, (byte) ((float) Alpha * 200.0f / 255.0f));
	GUIRENDERER.AddRect (SFVector (-0.4f - 1.4f * Size / 2.0f, -0.2f, -0.5f), 
						 SFVector (+0.4f + 1.4f * Size / 2.0f, +1.6f, +0.5f));
	GUIRENDERER.Translate (0.0f, 0.3f, 0.1f);
	GUIRENDERER.SetColor (255, 255, 255, Alpha);

	Text_.Render (1, 1.4f);

	GUIRENDERER.PopMatrix();
}

CItemData CInventory::GetCurrentItem()
{
	return Cells_[Current_][BOTTOMCELLSY].Item();
}

void CInventory::SetSelectedItem (CItemData Item)
{
	AirSelected_ = (Item.ID() == 0);
	if (!(BlockSelection_.Item().ID() && !Item.ID()))
		BlockSelection_.SetItem (Item);
}

void CInventory::Left()
{
	Cells_[Current_][BOTTOMCELLSY].SetSelected (false);

	Current_++;

	if (Current_ > NUM_CELLS_X - 1)
		Current_ = 0;

	Cells_[Current_][BOTTOMCELLSY].SetSelected (true);
	State_ = 0.0f;
}

void CInventory::Right()
{
	Cells_[Current_][BOTTOMCELLSY].SetSelected (false);

	Current_--;

	if (Current_ < 0)
		Current_ = NUM_CELLS_X - 1;

	Cells_[Current_][BOTTOMCELLSY].SetSelected (true);
	State_ = 0.0f;
}

void CInventory::SetCurrentCell (int ID)
{
	Cells_[Current_][BOTTOMCELLSY].SetSelected (false);

	Current_ = ID;

	Cells_[Current_][BOTTOMCELLSY].SetSelected (true);
}

void CInventory::LeftButtonDown()
{
	if (!Opened_)
		return;

	if (!Selected_ && PrevFocused_.x != -1 && Cells_[PrevFocused_.x][PrevFocused_.y].Item().ID() != 0)
	{
		Selected_ = true;
		Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (false);
		SelectedBottomCellNum_ = (PrevFocused_.y != BOTTOMCELLSY) ? -1 : PrevFocused_.x;
		SelectedCell_.SetItem (Cells_[PrevFocused_.x][PrevFocused_.y].Item());
		if (PrevFocused_.y == BOTTOMCELLSY)
			Cells_[PrevFocused_.x][PrevFocused_.y].SetItem (CItemData());
		SelectedCell_.SetSelected (true, false);
		SelectedCell_.SetSelected (false);
	}
	else if (Selected_)
	{
		if (PrevFocused_.x != -1 && PrevFocused_.y == BOTTOMCELLSY)
		{
			if (SelectedBottomCellNum_ != -1)
			{
				Cells_[SelectedBottomCellNum_][BOTTOMCELLSY].SetItem 
					(Cells_[PrevFocused_.x][PrevFocused_.y].Item());
			}

			Cells_[PrevFocused_.x][PrevFocused_.y].SetItem (SelectedCell_.Item());
			Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (true);
		}
		else if (PrevFocused_.x != -1)
			Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (true);
		Selected_ = false;
	}
}

void CInventory::LeftButtonUp()
{
}

void CInventory::MouseMotion (SFVector2D Position)
{
	if (!Opened_)
		return;

	SelectedCellPos_ = SFVector2D (Position.x, CApplication::GUI_SIZE_Y - Position.y) - 
					   SFVector2D (ITEMCELL_BACK_SIZE / 2.0f);

	for (int i = 0; i < NUM_CELLS_X; i++)
	{
		float BottomCellX = (CApplication::GUI_SIZE_X - NUM_CELLS_X * (ITEMCELL_BACK_SIZE +
			  CELLS_DISTANCE) + CELLS_DISTANCE) / 2 +
			  i * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE);
		float BottomCellY = CApplication::GUI_SIZE_Y - 4.0f - ITEMCELL_BACK_SIZE;
		if ((Position.x >= BottomCellX && Position.x <= BottomCellX + ITEMCELL_BACK_SIZE) && 
			(Position.y >= BottomCellY && Position.y <= BottomCellY + ITEMCELL_BACK_SIZE))
		{
			if (PrevFocused_.x == i  && PrevFocused_.y == -1)
				return;

			if (PrevFocused_.x != -1)
				Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (false);

			PrevFocused_ = SIVector2D (i, BOTTOMCELLSY);
			PrevCellPosition_ = SFVector2D (BottomCellX + ITEMCELL_BACK_SIZE / 2.0f, 
				CApplication::GUI_SIZE_Y - (BottomCellY + ITEMCELL_BACK_SIZE / 2.0f));

			if (!Selected_)
				Cells_[i][BOTTOMCELLSY].SetSelected (true);

			return;
		}
	}

	float SizeX = CELLS_DISTANCE * (NUM_CELLS_X - 1.0f) + 
		ITEMCELL_BACK_SIZE * NUM_CELLS_X + 2.0f * 5.0f;
	float SizeY = CELLS_DISTANCE * (NUM_CELLS_Y - 1) + 
		ITEMCELL_BACK_SIZE * NUM_CELLS_Y + 2.0f * 5.0f;
	float x0 = (CApplication::GUI_SIZE_X - SizeX) / 2.0f;
	float y0 = (CApplication::GUI_SIZE_Y - SizeY) / 2.0f;

	for (int x = 0; x < NUM_CELLS_X; x++)
		for (int y = 0; y < NUM_CELLS_Y; y++)
		{
			float x1 = 5.0f + x0 + x * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE);
			float y1 = 5.0f + y0 + y * (ITEMCELL_BACK_SIZE + CELLS_DISTANCE);
			if ((Position.x >= x1 && Position.x <= x1 + ITEMCELL_BACK_SIZE) && 
				(Position.y >= y1 && Position.y <= y1 + ITEMCELL_BACK_SIZE))
			{
				if (PrevFocused_.x == x && PrevFocused_.y == y)
					return;

				if (PrevFocused_.x != -1)
					Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (false);

				PrevFocused_ = SIVector2D (x, y);
				PrevCellPosition_ = SFVector2D (x1 + ITEMCELL_BACK_SIZE / 2.0f, 
					CApplication::GUI_SIZE_Y - (y1 + ITEMCELL_BACK_SIZE / 2.0f));

				if (!Selected_)
					Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (true);

				return;
			}
		}

	if (PrevFocused_.x != -1)
	{
		Cells_[PrevFocused_.x][PrevFocused_.y].SetSelected (false);
		PrevFocused_.x = -1;
	}
}
