﻿#ifndef CCHATWINDOW
#define CCHATWINDOW

#include <Engine.h>
#include "../CKeysUtilities.h"
#include "CGUIRenderer.h"
#include "CText.h"

#define CHAT_MAX_MESSAGE_SIZE 100

#define MAX_CHAT_STRINGS 10
#define NUM_STRINGS_CHAT 15
#define ALL_STRINGS_CHAT 100
#define TEXT_HISTORY_SIZE 100
#define CHAT_SIZE_X 45.0f
#define CHAT_MAX_ANIMATION_STEP 15.0f
#define CHAT_MESSAGE_NOT_HIDE_STEP 800.0f
#define CHAT_MESSAGE_HIDE_STEP 40.0f
#define CHAT_SLIDER_NOT_HIDE_STEP 200.0f
#define CHAT_SLIDER_HIDE_STEP 20.0f

class CChatWindow : public CKeyToCharacter::CCharacterListener
{
public:
	
	struct SChatMessage
	{
		float Step;
		SColor Color;
		CText Text;
	};

private:
	
	CArray<CString> TextHistory_;
	int TextHistoryStart_;
	int CurrentHistoryText_;

	CArray<SChatMessage> Text_;
	int Start_;
	SFVector2D Size_;
	CText CurrentText_;
	bool ChatOpened_;
	float CursorStep_;
	int Cursor_;
	float AnimationStep_;
	int Shift_;
	float SliderStep_;
	int NumStrings_;
	int NumStringsInMessage_;
	int Delimiter_[MAX_CHAT_STRINGS - 1];
	CKeyToCharacter KeyToCharacter_;
	
	void AddString_ (CString& String, SColor Color);
	void RecalculateStrings_();

public:

	CChatWindow();
	virtual ~CChatWindow();
					
	void AddString (CString& String, SColor Color);

	void Render (int Time);
	void Open();
	void Close (bool Animation = true);
	bool IsOpened();
	bool KeyDown (unsigned Key);
	void KeyUp (unsigned Key);
	void MouseWhell (char Delta);

	virtual void Character (char Char);
	virtual void SpecialKey (unsigned Key);

};

#endif