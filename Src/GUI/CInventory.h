﻿#ifndef CINVENTORY
#define CINVENTORY

#include <Engine.h>
#include "CGUIRenderer.h"
#include "CItemCell.h"
#include "CText.h"

#define NUM_CELLS_X		 10
#define NUM_CELLS_Y		 7
#define CELLS_DISTANCE	 1.5f
#define BOTTOMCELLSY	 NUM_CELLS_Y

class CInventory
{
public:
	
	void Init();
	void Destroy();

	void New();
	void Load (CStream& Stream);
	void Save (CStream& Stream);
	void OpenClose (bool Open);
	bool IsOpened();
	CItemData GetCurrentItem();
	void SetSelectedItem (CItemData Item);
	void Left();
	void Right();
	void SetCurrentCell (int ID);
	void Render (int Time);
	void LeftButtonDown();
	void LeftButtonUp();
	void MouseMotion (SFVector2D Position);

	CComplexBlocksMeshQueue MeshQueue;

private:
	
	void RenderBlockName_ (SFVector2D Position, CItemCell& Cell, byte Alpha = 255);

	CItemData StartBottomCells_[NUM_CELLS_X];
	
	CItemCell Cells_[NUM_CELLS_X][NUM_CELLS_Y + 1];
	int Current_;
	bool Opened_;
	SIVector2D PrevFocused_;
	SFVector2D PrevCellPosition_;
	CText Text_;
	bool Selected_;

	CItemCell SelectedCell_;
	SFVector2D SelectedCellPos_;
	int SelectedBottomCellNum_;
	float State_;

	CItemCell BlockSelection_;
	bool AirSelected_;
	float BlockSelectionState_;

};

#endif
