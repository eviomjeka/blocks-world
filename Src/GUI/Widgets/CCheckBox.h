﻿#ifndef CCHECKBOX
#define CCHECKBOX

#include "CWidget.h"

class CCheckBox : public CWidget
{
private:

	CText str_;
	float size_;
	bool checked_;
	bool focused_;
	float str_size_;

public:

	CCheckBox (SFVector2D position, bool from_top, 
			   CString& str, float size, bool checked);
	virtual ~CCheckBox();
	
	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

	bool IsChecked();

};

#endif