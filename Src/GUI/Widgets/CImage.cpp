﻿#include "CImage.h"
#include "../../CApplication.h"

CImage::CImage (SFVector2D position, bool from_top, bool image_from_top, 
	float size_x, float size_coef)
{
	position_ = position;
	size_ = SFVector2D (size_x, size_x * size_coef);
	from_top_ = from_top;
	image_from_top_ = image_from_top;
}

CImage::~CImage()
{}

void CImage::Render (int)
{
	SFVector2D TexCoords[4] = { SFVector2D (0, 0), SFVector2D (1, 0), 
		SFVector2D (1, 1), SFVector2D (0, 1) };
	SFVector Verticles[4] = {};

	if (image_from_top_)
	{
		Verticles[0] = SFVector (position_.x, position_.y - size_.y, 0.0f);
		Verticles[1] = SFVector (position_.x + size_.x, position_.y - size_.y, 0.0f);
		Verticles[2] = SFVector (position_.x + size_.x, position_.y, 0.0f);
		Verticles[3] = SFVector (position_.x, position_.y, 0.0f);
	}
	else
	{
		Verticles[0] = SFVector (position_.x, position_.y, 0.0f);
		Verticles[1] = SFVector (position_.x + size_.x, position_.y, 0.0f);
		Verticles[2] = SFVector (position_.x + size_.x, position_.y + size_.y, 0.0f);
		Verticles[3] = SFVector (position_.x, position_.y + size_.y, 0.0f);
	}

	GUIRENDERER.AddTextureVertex (Verticles[0], TexCoords[0]);
	GUIRENDERER.AddTextureVertex (Verticles[1], TexCoords[1]);
	GUIRENDERER.AddTextureVertex (Verticles[2], TexCoords[2]);

	GUIRENDERER.AddTextureVertex (Verticles[0], TexCoords[0]);
	GUIRENDERER.AddTextureVertex (Verticles[2], TexCoords[2]);
	GUIRENDERER.AddTextureVertex (Verticles[3], TexCoords[3]);
}

void CImage::MouseMotion (SFVector2D)	{}
bool CImage::KeyDown (unsigned)			{ return false; }
void CImage::KeyUp (unsigned)			{}
void CImage::LeftButtonDown()			{}
void CImage::LeftButtonUp()				{}
void CImage::RightButtonDown()			{}
void CImage::RightButtonUp()			{}
void CImage::FocusChange (bool)			{}
bool CImage::IsCursorIn (SFVector2D)	{ return false; }
bool CImage::IsFocusing()				{ return false; }
