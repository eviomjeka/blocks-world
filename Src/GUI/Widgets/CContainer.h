﻿#ifndef CCONTAINER
#define CCONTAINER

#include "CWidget.h"
#include "CEditText.h"
#include "CMessageBox.h"

class CContainer : public CWidget
{
private:

	CMessageBox* message_box_;
	bool message_box_actived_;
	bool ignorekeyup_;
	SFVector2D prev_mousemotion_;
	bool elements_added_; 
	float GuiShift_;

	SFVector2D size_;
	int focused_;
	int  edit_text_focused_;
	bool mouse_focused_;
	bool horizontal_;
	bool main_;

public:
	
	CArray<CWidget*> widgets_;
	
	CContainer (bool horizontal, bool from_top, bool main, SFVector2D position, SFVector2D size);
	virtual ~CContainer();

	void AddWidget (CWidget* widget);
	void RemoveAllWidgets();
	CWidget* GetWidget (int i);

	void SetSize (SFVector2D size, float gui_shift);
	SFVector2D GetSize();

	void CreateMessageBox();
	void OpenMessageBox (CString& str, bool isbutton = true);
	void CloseMessageBox();

	int GetFocused();
	void SetFocused (int focused);
	void SetNextFocused();
	void SetPrevFocused();

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();
};

#endif