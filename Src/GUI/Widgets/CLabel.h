﻿#ifndef CLABEL
#define CLABEL

#include "CWidget.h"

#define MAX_TEXT_SIZE 256

class CLabel : public CWidget
{
private:

	CText str_;
	float size_;
	byte pos_;
	SColor Color_;
	
public:
	CLabel (SFVector2D position, bool from_top, byte pos, CString& str,
			float size, SColor Color = SColor (0, 0, 0));

	virtual ~CLabel();
			
	void SetStr (CString& str);
	CString& GetStr();

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

	void SetColor (SColor Color);

};

#endif