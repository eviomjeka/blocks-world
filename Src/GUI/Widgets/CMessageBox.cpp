﻿#include "CMessageBox.h"
#include "../../CApplication.h"

CMessageBox::CMessageBox (SFVector2D position)
	: size_ (SFVector2D (35, 10)), button_focused_ (false),
	  is_button_ (false), visible_ (false)
{
	from_top_ = false;
	position_ = position;
	label_  = new CLabel (SFVector2D (0, 0), false, 1, CString::EmptyString, 1.5f);
	button_ = new CButton (SFVector2D (0, 0), false, SFVector2D (25.0f, 2.5f), 
						   1, str_ok, CMESSAGEBOX_BUTTONID, this, SColor (255, 0, 0), false);
}

CMessageBox::~CMessageBox()
{
	delete label_;
	delete button_;
}

void CMessageBox::SetStr (CString& str)
{
	label_->SetStr (str);
	size_.x = max (35, FONT.GetLength (str, 1.5f) + 10);
}

void CMessageBox::SetIsButton (bool is_button)
{
	is_button_ = is_button;
}

void CMessageBox::SetVisible (bool visible, SFVector2D cursorposition)
{
	visible_ = visible;
	if (!visible_)
		button_->EraseStep();
	else
	{
		MouseMotion (cursorposition);
	}
}

bool CMessageBox::IsVisible()
{
	return visible_;
}

void CMessageBox::SetPosition (SFVector2D position)
{
	position_ = position;
}

void CMessageBox::CButtonOnClick (int)
{
	button_focused_ = false;
	SetVisible (false);
}

void CMessageBox::Render (int time)
{
	GUIRENDERER.PushMatrix();

	GUIRENDERER.Translate (position_.x - size_.x / 2.0f, position_.y - size_.y / 2.0f, 0.6f);
	GUIRENDERER.SetColor (204, 212, 217, 255);
	GUIRENDERER.AddRect (SFVector (0, 0, 0), SFVector (size_.x, size_.y, 0));

	if (is_button_)
	{
		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (size_.x / 2.0f, size_.y * 2.0f / 3.0f, 0.1f);
		label_->Render (time);
		GUIRENDERER.PopMatrix();
		GUIRENDERER.Translate (size_.x / 2.0f, size_.y / 5.0f, 0.2f);
		button_->Render (time);
	}
	else
	{
		GUIRENDERER.Translate (size_.x / 2.0f, (size_.y - size_.y / 6.5f / 2.0f) / 2.0f, 0.1f);
		label_->Render (time);
	}

	GUIRENDERER.PopMatrix();
}

void CMessageBox::MouseMotion (SFVector2D position)
{
	if (is_button_ && button_->IsCursorIn (SFVector2D (position.x - position_.x, 
										   position.y - (position_.y + size_.y / 2.0f - 2.5f - size_.y / 5.0f))))
	{
		button_->MouseMotion (position);
		if (!button_focused_)
		{
			button_->FocusChange (true);
			button_focused_ = true;
		}
	}
	else if (is_button_ && button_focused_)
	{
		button_->FocusChange (false);
		button_focused_ = false;
	}
}

bool CMessageBox::KeyDown (unsigned key)
{
	if (is_button_ && !button_focused_ && (key == KEY_LEFT || key == KEY_UP || key == KEY_RIGHT || key == KEY_DOWN))
	{
		button_focused_ = true;
		button_->FocusChange (true);
		return true;
	}
	else if (is_button_ && button_focused_ && (key == KEY_LEFT || key == KEY_UP || key == KEY_RIGHT || key == KEY_DOWN))
	{
		button_focused_ = false;
		button_->FocusChange (false);
		return true;
	}
	else if (button_focused_ && is_button_)
	{
		button_->KeyDown (key);
		return true;
	}
	return false;
}

void CMessageBox::KeyUp (unsigned)			{}
void CMessageBox::LeftButtonDown()
{
	if (is_button_ && button_focused_)
		button_->LeftButtonDown();
}
void CMessageBox::LeftButtonUp()			{}
void CMessageBox::RightButtonDown()			{}
void CMessageBox::RightButtonUp()			{}
void CMessageBox::FocusChange (bool)		{}
bool CMessageBox::IsCursorIn (SFVector2D)	{ return true; }
bool CMessageBox::IsFocusing()				{ return true; }