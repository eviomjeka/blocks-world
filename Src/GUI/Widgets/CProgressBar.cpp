﻿#include "CProgressBar.h"
#include "../../CApplication.h"

CProgressBar::CProgressBar (SFVector2D position, bool from_top, SFVector2D size) :
	percents_ (0),
	size_ (size)
{
	position_ = position;
	from_top_ = from_top;
}

CProgressBar::~CProgressBar() {}

void CProgressBar::SetPercents (float percents)
{
	percents_ = percents;
	if (percents_ < 0.0f)
		percents_ = 0.0f;
	if (percents_ > 100.0f)
		percents_ = 100.0f;
}

void CProgressBar::SetInfo (CString& info)
{
	info_ = info;
}

void CProgressBar::Render (int)
{
	GUIRENDERER.Translate (position_.x, position_.y, 0);
	GUIRENDERER.SetColor (255, 0, 0, 255);

	GUIRENDERER.AddRect (SFVector (0.3f, 0.3f, 0.0f), 
		SFVector (0.3f + (size_.x - 0.6f) * percents_ / 100.0f, size_.y - 0.3f, 0.0f));
	
	GUIRENDERER.SetColor (0, 0, 0, 255);
	GUIRENDERER.Translate (size_.x / 2.0f, size_.y / 4.0f, 0.1f);
	CText str;
	str.Print ("%d%%", (int) percents_);
	
	str.Render(1, size_.y * 2.0f / 3.0f);

	GUIRENDERER.Translate (0, size_.y, 0.1f);
	info_.Render(1, size_.y / 3.0f);
}

void CProgressBar::MouseMotion (SFVector2D)	{}
bool CProgressBar::KeyDown (unsigned)		{ return false; }
void CProgressBar::KeyUp (unsigned)			{}
void CProgressBar::LeftButtonDown()			{}
void CProgressBar::LeftButtonUp()			{}
void CProgressBar::RightButtonDown()		{}
void CProgressBar::RightButtonUp()			{}
void CProgressBar::FocusChange (bool)		{}
bool CProgressBar::IsCursorIn (SFVector2D)	{ return false; }
bool CProgressBar::IsFocusing()				{ return false; }