﻿#include "CCheckBox.h"
#include "../../CApplication.h"

CCheckBox::CCheckBox (SFVector2D position, bool from_top, 
					  CString& str, float size, bool checked)
	:
		size_ (size), 
		checked_ (checked),
		focused_ (false), 
		str_ (str)
{
	str_size_ = FONT.GetLength (str_, size) + size * 1.5f;
	position_ = position;
	from_top_ = from_top;
}

CCheckBox::~CCheckBox() {}

bool CCheckBox::IsChecked()
{
	return checked_;
}

void CCheckBox::Render (int)
{
	GUIRENDERER.Translate (position_.x, position_.y - size_ / 10, 0);
	
	if (focused_)
		GUIRENDERER.SetColor (255, 128, 64, 255);
	else
		GUIRENDERER.SetColor (255, 255, 255, 255);
	GUIRENDERER.AddRect (SFVector (0, 0, 0), SFVector (size_, size_, 0));

	if (checked_)
	{
		GUIRENDERER.SetColor (50, 50, 50, 255);
		GUIRENDERER.AddQuad (SFVector (0, size_ / 2.0f, 0.1f), SFVector (size_ / 2.0f - size_ / 10.0f, 0, 0.1f), 
			SFVector (size_ / 2.0f + size_ / 10.0f, 0, 0.1f), SFVector (size_ / 5.0f, size_ / 2.0f, 0.1f));

		GUIRENDERER.AddQuad (SFVector (size_ / 2.0f - size_ / 10.0f, 0, 0.1f), 
			SFVector (size_ / 2.0f + size_ / 10.0f, 0, 0.1f), 
			SFVector (size_ * 6.0f / 5.0f + size_ / 5.0f, size_ * 5.0f / 4.0f, 0.1f), 
			SFVector (size_ * 6.0f / 5.0f, size_ * 5.0f / 4.0f, 0.1f));
	}

	GUIRENDERER.SetColor (0, 0, 0, 255);
	GUIRENDERER.Translate (size_ * 1.5f, size_ / 10, 0);
	str_.Render (0, size_);
}

void CCheckBox::MouseMotion (SFVector2D) {}

bool CCheckBox::KeyDown (unsigned key)
{
	if (key == KEY_ENTER || key == KEY_SPACE)
	{
		checked_ = !checked_;
		return true;
	}

	return false;
}

void CCheckBox::KeyUp (unsigned)	{}

void CCheckBox::LeftButtonDown()
{
	checked_ = !checked_;
}

void CCheckBox::LeftButtonUp()		{}
void CCheckBox::RightButtonDown()	{}
void CCheckBox::RightButtonUp()		{}

void CCheckBox::FocusChange (bool focus)
{
	focused_ = focus;
}

bool CCheckBox::IsCursorIn (SFVector2D cursor)
{
	return (cursor.x >= position_.x && cursor.x <= position_.x + str_size_) && 
		   (cursor.y >= position_.y && cursor.y <= position_.y + size_);
}

bool CCheckBox::IsFocusing() { return true; }
