﻿#ifndef CPROGRESSBAR
#define CPROGRESSBAR

#include "CWidget.h"

#define NUM_PERCENTS 100

class CProgressBar : public CWidget
{
private:

	float percents_;
	SFVector2D size_;
	CText info_;

public:

	CProgressBar (SFVector2D position, bool from_top, SFVector2D size);
	virtual ~CProgressBar();
   
	void SetPercents (float percents);
	void SetInfo (CString& info);

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

};

#endif