﻿#include "CSliderBar.h"
#include "../../CApplication.h"

CSliderBar::CSliderBar (SFVector2D position, bool from_top, float size,
						float min, float max, float step, float current, float viewcoef, int keymove) :
	size_ (size),
	min_ (min),
	max_ (max),
	step_ (step),
	current_ (current),
	viewcoef_ (viewcoef), 
	keymove_ (keymove),
	state_ (0),
	onfocus_step_ (0)
{
	position_ = position;
	from_top_ = from_top;
	focused_ = false;
}

CSliderBar::~CSliderBar() {}

void CSliderBar::Render (int time)
{
	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (position_.x, position_.y, -0.1f);

	GUIRENDERER.SetColor (0, 255, 0, 255);
	GUIRENDERER.AddRect (SFVector (0, 1.0f, 0), SFVector (size_, 1.5f, 0));

	GUIRENDERER.SetColor (0, 0, 0, 255);
	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (-4.0f, 0.7f, 0.0f);
	CText str;
	str.Print("%g", min_ * viewcoef_);
	str.Render(1, 1.8f);
	GUIRENDERER.PopMatrix();

	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (size_ + 4.0f, 0.7f, 0.0f);
	str.Print("%g", max_ * viewcoef_);
	str.Render(1, 1.8f);
	GUIRENDERER.PopMatrix();

	float coef = 1 + onfocus_step_ / 75.0f;
	float pos = size_ * (current_ - min_) / (max_ - min_);

	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (pos, 0.7f - (coef - 1.0f) * 0.5f, 1.0f);
	GUIRENDERER.SetColor (0, 0, 0, 255);
	str.Print("%g", current_ * viewcoef_);
	str.Render(1, 1.8f + 0.8f * (coef - 1.0f));
	GUIRENDERER.PopMatrix();

	GUIRENDERER.Translate (0.0f, -1.1f * (coef - 1), 0.1f);

	int g = 0, b = 0;
	g += (int) (onfocus_step_ * 180 / SLIDER_BAR_MAX_STEP); if (g > 255) g = 255;
	b += (int) (onfocus_step_ * 180 / SLIDER_BAR_MAX_STEP); if (b > 255) b = 255;
	GUIRENDERER.SetColor (255, (byte) g, (byte) b, 255);

	GUIRENDERER.AddRect (SFVector (pos - 1.8f * coef, 0.15f * coef, 0), 
		SFVector (pos + 1.8f * coef, 2.35f * coef, 0));

	GUIRENDERER.PopMatrix();

	if (focused_ && onfocus_step_ < SLIDER_BAR_MAX_STEP - 1)
		onfocus_step_ += 1 * time / 10.0f;
	if (focused_ && onfocus_step_ > SLIDER_BAR_MAX_STEP - 1)
		onfocus_step_ = SLIDER_BAR_MAX_STEP - 1;
	if (!focused_ && onfocus_step_ > 0)
		onfocus_step_ -= 1 * time / 10.0f;
	if (!focused_ && onfocus_step_ < 0)
		onfocus_step_ = 0;
}

float CSliderBar::GetCurrent()
{
	return current_;
}

float CSliderBar::GetMin()
{
	return min_;
}

float CSliderBar::GetMax()
{
	return max_;
}

void CSliderBar::SetMin (float min)
{
	min_ = min;
}

void CSliderBar::SetMax (float max)
{
	max_ = max;
}

void CSliderBar::MouseMotion (SFVector2D position)
{
	if (state_ == 1)
	{
		float pos = position.x - position_.x;
		if (pos < 0)
			pos = 0;
		else if (pos > size_)
			pos = size_;
		current_ = pos * (max_ - min_) / size_ + min_;
		float current_step = ((int) (current_ / step_)) * step_;
		if (current_ - current_step > 0.5f)
			current_ = current_step + 1.0f;
		else
			current_ = current_step;
	}
	mouse_pos_ = position;
}

bool CSliderBar::KeyDown (unsigned key)
{
	if (key == KEY_LEFT)
	{
		current_ -= step_ * keymove_;
		if (current_ < min_)
			current_ = min_;
	}
	else if (key == KEY_RIGHT)
	{
		current_ += step_ * keymove_;
		if (current_ > max_)
			current_ = max_;
	}

	return false;
}
void CSliderBar::KeyUp (unsigned) {}

void CSliderBar::LeftButtonDown()
{
	state_ = 1;
}
void CSliderBar::LeftButtonUp()
{
	state_ = 0;
	if (!IsCursorIn (SFVector2D (mouse_pos_.x, -mouse_pos_.y)))
	{
		focused_ = false;
	}
}

void CSliderBar::RightButtonDown() {}
void CSliderBar::RightButtonUp() {}

void CSliderBar::FocusChange (bool focus)
{
	focused_ = focus;
}

bool CSliderBar::IsCursorIn (SFVector2D cursor)
{
	float pos = size_ * (current_ - min_) / (max_ - min_);
	return state_ == 1 || (
		   cursor.x >= position_.x + pos - 1.8f &&
		   cursor.x <= position_.x + pos + 1.8f &&
		   cursor.y >= position_.y + 0.0f &&
		   cursor.y <= position_.y + 2.5f);
}

bool CSliderBar::IsFocusing() { return true; }
