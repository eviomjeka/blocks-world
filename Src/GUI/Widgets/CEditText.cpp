﻿#include "CEditText.h"
#include "../../CApplication.h"

CEditText::CEditText (SFVector2D position, bool from_top, SFVector2D size, 
					  int text_size, bool additional_symbols, CString& str)
	:
		size_ (size), 
		cursor_step_ (0),
		cursor_(0), 
		left_click_ (false), 
		max_str_size_ (text_size)
{
	key_to_character_.Init (this, additional_symbols);
	position_ = position;
	from_top_ = from_top;
	focused_ = false;
	
	SetStr (str);
}

CEditText::~CEditText() {}

void CEditText::Render (int time)
{
	key_to_character_.Update (time);

	GUIRENDERER.SetColor (255, 255, 255, 255);
	GUIRENDERER.AddRect (SFVector (position_.x, position_.y, -0.1f), 
						   SFVector (position_.x + size_.x, position_.y + size_.y, -0.1f));

	GUIRENDERER.PushMatrix();
	GUIRENDERER.SetColor (0, 0, 0, 255);
	GUIRENDERER.Translate (position_.x + size_.y / 5.0f, position_.y + size_.y / 4.0f, 0);
	FONT.Render(str_, size_.y / 4.0f * 3.0f);

	if (cursor_step_ < CURSOR_STEP && focused_)
	{
		char c = cursor_ < str_.Length() ? str_[cursor_] : 0;
		if (cursor_ < str_.Length())
			str_[cursor_] = 0;
		GUIRENDERER.Translate (FONT.GetLength (str_, size_.y / 4.0f * 3.0f) - size_.y / 11.0f, -size_.y / 32.0f, 0.1f);
		if (cursor_ < str_.Length())
			str_[cursor_] = c;

		CString Str("|");
		FONT.Render(Str, size_.y);
	}
	
	GUIRENDERER.PopMatrix();

	if (focused_)
	{
		cursor_step_ += 1 * time / 10.0f;
		while (cursor_step_ > 2 * CURSOR_STEP)
			cursor_step_ -= 2 * CURSOR_STEP;
	}

	if (focused_)
		GUIRENDERER.SetColor (200, 100, 50, 255);
	else
		GUIRENDERER.SetColor (0, 0, 0, 255);

	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (0.0f, 0.0f, 0.2f);
	GUIRENDERER.AddHorizontalLine (position_.x, position_.y, size_.x, 0.2f);
	GUIRENDERER.AddHorizontalLine (position_.x, position_.y + size_.y, size_.x, 0.2f);
	GUIRENDERER.AddVerticalLine   (position_.x, position_.y, size_.y, 0.2f);
	GUIRENDERER.AddVerticalLine   (position_.x + size_.x, position_.y, size_.y, 0.2f);
	GUIRENDERER.PopMatrix();
}

void CEditText::MouseMotion (SFVector2D) {}

bool CEditText::KeyDown (unsigned key)
{
	if (key == KEY_UP || key == KEY_DOWN)
		return false;

	key_to_character_.KeyDown (key);
	return true;
}
void CEditText::KeyUp (unsigned key)
{
	key_to_character_.KeyUp (key);
}

void CEditText::LeftButtonDown()
{
	left_click_ = true;
}
void CEditText::LeftButtonUp()
{
	left_click_ = false;
}

void CEditText::RightButtonDown() {}
void CEditText::RightButtonUp()   {}
void CEditText::FocusChange (bool focus)
{
	if (focus)
	{
		cursor_ = str_.Length();
		cursor_step_ = 0;
	}
}

void CEditText::SetEditBoxFocus (bool focus)
{
	if (!focus && focused_)
		key_to_character_.StopKeyRepetition();
	if (focus)
		cursor_step_ = 0.0f;
	
	focused_ = focus;
}

bool CEditText::IsCursorIn (SFVector2D cursor)
{
	return cursor.x >= position_.x &&
		   cursor.x <= position_.x + size_.x &&
		   cursor.y >= position_.y &&
		   cursor.y <= position_.y + size_.y;
}

bool CEditText::IsFocusing() { return true; }

void CEditText::Character (char character)
{
	if (!focused_)
		return;
	if (str_.Length() >= max_str_size_ - 1)
		return;
	
	str_ += 0;
	if (cursor_ != str_.Length())
	{
		for (int i = str_.Length() - 2; i >= cursor_; i--)
			str_[i + 1] = str_[i];
	}
	str_[cursor_] = character;
	
	cursor_++;
	cursor_step_ = 0;
}

void CEditText::SpecialKey (unsigned key)
{
	if (key == KEY_LEFT)
	{
		cursor_--;
		if (cursor_ < 0)
			cursor_ = 0;
		else
			cursor_step_ = 0;
	}
	else if (key == KEY_RIGHT)
	{
		cursor_++;
		if (cursor_ > str_.Length())
			cursor_ = str_.Length();
		else
			cursor_step_ = 0;
	}
	else if (key == KEY_DELETE)
	{
		if (cursor_ < str_.Length())
		{
			for (int i = cursor_; i < str_.Length(); i++)
			{
				if (i > 0 && i != cursor_)
					str_[i - 1] = str_[i];
			}
			str_.Shorten (str_.Length() - 1);

			cursor_step_ = 0;
		}
	}
	else if (key == KEY_BACKSPACE)
	{
		cursor_--;
		if (cursor_ < 0)
			cursor_ = 0;
		else
		{
			for (int i = cursor_ + 1; i < str_.Length(); i++)
			{
				if (i > 0)
					str_[i - 1] = str_[i];
			}
			str_.Shorten (str_.Length() - 1);

			cursor_step_ = 0;
		}
	}
}

CString& CEditText::GetStr()
{
	return str_;
}

void CEditText::SetStr (CString& str)
{
	str_ = str;
	cursor_ = str.Length();
}