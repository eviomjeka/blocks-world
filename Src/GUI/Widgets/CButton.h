﻿#ifndef CBUTTON
#define CBUTTON

#include "CWidget.h"

#define BUTTON_MAX_STEP 12

class CButton : public CWidget
{
public:

	class CButtonOnClickListeter
	{
	public:
		virtual void CButtonOnClick (int id) = 0;
	};

private:
	CText str_;
	CText additionalstr_;
	SFVector2D size_;
	SColor Color_;
	bool focused_;
	float step_;
	int id_;
	byte pos_;
	CButtonOnClickListeter* listener_;
	bool sticking_;
	bool pressed_;
	bool can_press_;
	bool actived_;

public:

	CButton (SFVector2D position, bool from_top, SFVector2D size, byte pos,
			 CString& str, int id, CButtonOnClickListeter* listener,
			 SColor Color, bool sticking = false, bool actived = true, 
			 CString& additionalstr = CString::EmptyString);

	virtual ~CButton();
	
	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

	void SetText (CString& Text);
	CString& GetText();
	CString& GetAdditionalText();
	void Press (bool animation);
	void Unpress();
	void CanPress (bool press);
	void EraseStep();
	void SetActived (bool actived);
	void Update (int time);

};

#endif