﻿#ifndef CMESSAGEBOX
#define CMESSAGEBOX

#include "CWidget.h"
#include "CLabel.h"
#include "CButton.h"

#define CMESSAGEBOX_BUTTONID 50

class CMessageBox : public CWidget, CButton::CButtonOnClickListeter
{
private:

	CButton* button_;
	CLabel* label_;
	SFVector2D size_;
	bool button_focused_;
	bool is_button_;
	bool visible_;

public:

	CMessageBox (SFVector2D position);
	virtual ~CMessageBox();

	void SetStr (CString& str);
	void SetIsButton (bool is_button);
	void SetVisible (bool visible, SFVector2D cursorposition = SFVector2D (0, 0));
	bool IsVisible();
	void SetPosition (SFVector2D position);
	virtual void CButtonOnClick (int id);

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();
};

#endif