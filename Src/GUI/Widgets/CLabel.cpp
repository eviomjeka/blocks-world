﻿#include "CLabel.h"
#include "../../CApplication.h"

CLabel::CLabel (SFVector2D position, bool from_top, byte pos, CString& str,
				 float size, SColor Color)
	:
	str_(str),
	size_ (size),
	pos_ (pos),
	Color_ (Color)
{
	position_ = position;
	from_top_ = from_top;
}

CLabel::~CLabel() {}

void CLabel::SetStr (CString& str)
{
	str_ = str;
}

CString& CLabel::GetStr()
{
	return str_;
}

void CLabel::Render (int)
{
	GUIRENDERER.Translate (position_.x, position_.y, 0);
	GUIRENDERER.SetColor (Color_);

	str_.Render(pos_, size_);
}

void CLabel::MouseMotion (SFVector2D)	{}
bool CLabel::KeyDown (unsigned)			{ return false; }
void CLabel::KeyUp (unsigned)			{}
void CLabel::LeftButtonDown()			{}
void CLabel::LeftButtonUp()				{}
void CLabel::RightButtonDown()			{}
void CLabel::RightButtonUp()			{}
void CLabel::FocusChange (bool)			{}
bool CLabel::IsCursorIn (SFVector2D)	{ return false; }
bool CLabel::IsFocusing()				{ return false; }

void CLabel::SetColor (SColor Color)
{
	Color_ = Color;
}
