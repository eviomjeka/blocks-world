﻿#ifndef CCHOOSEELEMENTBAR
#define CCHOOSEELEMENTBAR

#include "CWidget.h"
#include "CButton.h"

#define CHOOSEELEMENT_BUTTONIDSTART 100

class CChooseElementBar : public CWidget, CButton::CButtonOnClickListeter
{
public:

	class CChooseElementListener
	{
	public:
		virtual void CChooseElementBarOnSelectFirstTime() = 0;
	};

private:
	
	CArray<CButton*> elements_;
	int max_str_size_;
	int max_additional_str_size_;
	int focused_num_;
	bool focused_;
	bool mouse_focused_;
	int selected_;
	SFVector2D size_;
	SFVector2D prev_position_;
	float button_sizey_;
	int slider_pos_;
	float push_y_;
	bool cursor_in_;
	bool leftrightscrool_;
	byte state_;
	CChooseElementListener* listener_;
	SColor Color1_;
	SColor Color2_;

public:

	CChooseElementBar (SFVector2D position, bool from_top, bool leftrightscrool, SFVector2D size, 
					   int max_str_size, int max_additional_str_size, 
					   float button_sizey, CChooseElementListener* listener, SColor Color1, SColor Color2);
	virtual ~CChooseElementBar();
					
	void AddElement (CString& name, CString& additionalstr = CString::EmptyString);
	int GetSelectedElementID();
	CString& GetSelectedElementName();
	CString& GetSelectedElementAdditionalText();
	void SetSize (SFVector2D size);
	void AddSliderPos (int pos);

	virtual void CButtonOnClick (int id);

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

};

#endif