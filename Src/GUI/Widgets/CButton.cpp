﻿#include "CButton.h"
#include "../../CApplication.h"

CButton::CButton (SFVector2D position, bool from_top, SFVector2D size, byte pos,
				  CString& str, int id, CButton::CButtonOnClickListeter* listener,
				  SColor Color, bool sticking, bool actived, CString& additionalstr)
	:
		str_(str),
		additionalstr_(additionalstr),
		size_ (size),
		Color_ (Color), 
		step_ (0),
		id_ (id),
		pos_ (pos),
		listener_ (listener), 
		sticking_ (sticking), 
		pressed_ (false), 
		can_press_ (true), 
		actived_ (actived)
{
	position_ = position;
	from_top_ = from_top;
	focused_ = false;
}

CButton::~CButton() {}

void CButton::Render (int time)
{
	GUIRENDERER.PushMatrix();
	
	float shift = 0;
	if (pos_ == 0)
		shift = 0.0f + step_ / (90.0f * 2.0f);
	else if (pos_ == 1)
		shift = 0.5f + step_ / (90.0f * 2.0f);
	else
		shift = 1.0f + step_ / (90.0f * 2.0f);
	
	GUIRENDERER.Translate (position_.x - shift * size_.x,
				  position_.y - size_.y * step_ / (90.0f * 2.0f), -0.1f);
	
	if (actived_)
	{
		int r = Color_.r, g = Color_.g, b = Color_.b;
		r += (int) (step_ * 180 / BUTTON_MAX_STEP); if (r > 255) r = 255;
		g += (int) (step_ * 180 / BUTTON_MAX_STEP); if (g > 255) g = 255;
		b += (int) (step_ * 180 / BUTTON_MAX_STEP); if (b > 255) b = 255;
		
		GUIRENDERER.SetColor ((byte) r, (byte) g, (byte) b, Color_.a);
	}
	else
	{
		int r = 180, g = 180, b = 180;
		r += (int) (step_ * 180 / BUTTON_MAX_STEP / 4.0f); if (r > 255) r = 255;
		g += (int) (step_ * 180 / BUTTON_MAX_STEP / 4.0f); if (g > 255) g = 255;
		b += (int) (step_ * 180 / BUTTON_MAX_STEP / 4.0f); if (b > 255) b = 255;
		
		GUIRENDERER.SetColor ((byte) r, (byte) g, (byte) b, Color_.a);
	}

	GUIRENDERER.AddRect (SFVector (0, 0, 0), 
		SFVector (size_.x + size_.x * step_ / 90.0f, size_.y + size_.y * step_ / 90.0f, 0));
	
	GUIRENDERER.PopMatrix();

	GUIRENDERER.SetColor (0, 0, 0, 255);
	if (!additionalstr_.Empty())
	{
		GUIRENDERER.PushMatrix();
		GUIRENDERER.Translate (position_.x - (pos_ / 2.0f - 0.5f) * size_.x, position_.y + size_.y * 0.5f + 
							   size_.y * 0.52f * step_ / 90.0f / 2.0f, 0.0f);
		str_.Render(1, size_.y * 0.52f * (1 + step_ / 100.0f));
		GUIRENDERER.PopMatrix();
		GUIRENDERER.Translate (position_.x - (pos_ / 2.0f - 0.5f) * size_.x, position_.y + size_.y * 0.1f - 
							   size_.y * 0.3f * step_ / 90.0f / 2.0f, 0.0f);
		additionalstr_.Render(1, size_.y * 0.3f * (1 + step_ / 90.0f));
	}
	else
	{
		GUIRENDERER.Translate (position_.x - (pos_ / 2.0f - 0.5f) * size_.x, position_.y + size_.y * 0.3f, 0.0f);
		str_.Render(1, size_.y * 0.65f * (1 + step_ / 90.0f));
	}

	Update (time);
}

void CButton::Update (int time)
{
	if (actived_ && !sticking_)
	{
		if (focused_ && step_ < BUTTON_MAX_STEP)
			step_ += 1 * time / 12.0f;
		if (focused_ && step_ > BUTTON_MAX_STEP)
			step_ = BUTTON_MAX_STEP;
		if (!focused_ && step_ > 0)
			step_ -= 1 * time / 12.0f;
		if (!focused_ && step_ < 0)
			step_ = 0;
	}
	else if (actived_ && sticking_)
	{
		if (pressed_)
		{
			if (focused_ && step_ < BUTTON_MAX_STEP)
				step_ += 1 * time / 12.0f;
			if (focused_ && step_ > BUTTON_MAX_STEP)
				step_ = BUTTON_MAX_STEP;
		}
		else
		{
			if (focused_ && step_ < BUTTON_MAX_STEP * 4.0f / 9.0f)
				step_ += 1 * time / 12.0f;
			if (focused_ && step_ > BUTTON_MAX_STEP * 4.0f / 9.0f)
				step_ = BUTTON_MAX_STEP * 4.0f / 9.0f;
		}
		if (!focused_ && ((!pressed_ && step_ > 0) || (pressed_ && step_ > BUTTON_MAX_STEP * 3.0f / 5.0f)))
			step_ -= 1 * time / 12.0f;
		if (!focused_ && (pressed_ && step_ < BUTTON_MAX_STEP * 3.0f / 5.0f))
			step_ = BUTTON_MAX_STEP * 3.0f / 5.0f;
		if (!focused_ && !pressed_ && step_ < 0)
			step_ = 0;
	}
	else
	{
		if (focused_ && step_ < BUTTON_MAX_STEP / 4.0f)
			step_ += 1 * time / 12.0f;
		if (focused_ && step_ > BUTTON_MAX_STEP / 4.0f)
			step_ = BUTTON_MAX_STEP / 4.0f;
		if (!focused_ && step_ > 0)
			step_ -= 1 * time / 12.0f;
		if (!focused_ && step_ < 0)
			step_ = 0;
	}
}

void CButton::MouseMotion (SFVector2D) {}

bool CButton::KeyDown (unsigned key)
{
	if (key == KEY_ENTER || key == KEY_SPACE)
	{
		if (can_press_ && actived_ && ((sticking_ && !pressed_) || !sticking_))
		{
			pressed_ = true;
			listener_->CButtonOnClick (id_);
		}
		return true;
	}
	return false;
}
void CButton::KeyUp (unsigned)	{}

void CButton::LeftButtonDown()
{
	if (can_press_ && actived_ && ((sticking_ && !pressed_) || !sticking_))
	{
		pressed_ = true;
		listener_->CButtonOnClick (id_);
	}
}

void CButton::LeftButtonUp()	{}
void CButton::RightButtonDown()	{}
void CButton::RightButtonUp()	{}
void CButton::FocusChange (bool focus)
{
	focused_ = focus;
}

bool CButton::IsCursorIn (SFVector2D cursor)
{
	return cursor.x >= position_.x - size_.x * (pos_ / 2.0f) &&
		   cursor.x <= position_.x - size_.x * ((pos_ - 2) / 2.0f) &&
		   cursor.y >= position_.y &&
		   cursor.y <= position_.y + size_.y;
}

bool CButton::IsFocusing()		{ return true; }

void CButton::Press (bool animation)
{
	pressed_ = true;
	if (!animation)
	{
		if (!sticking_)
			step_ = BUTTON_MAX_STEP;
		else
			step_ = BUTTON_MAX_STEP * 3.0f / 5.0f;
	}
}

void CButton::Unpress()
{
	pressed_ = false;
}

void CButton::SetText (CString& Text)
{
	str_ = Text;
}

CString& CButton::GetText()
{
	return str_;
}

CString& CButton::GetAdditionalText()
{
	return additionalstr_;
}

void CButton::CanPress (bool press)
{
	can_press_ = press;
}

void CButton::EraseStep()
{
	step_ = 0;
	focused_ = false;
}

void CButton::SetActived (bool actived)
{
	actived_ = actived;
}
