﻿#ifndef CSLIDERBAR
#define CSLIDERBAR

#include "CWidget.h"

#define SLIDER_BAR_MAX_STEP 20

class CSliderBar : public CWidget
{
private:

	float size_;
	float min_;
	float max_;
	float step_;
	float current_;
	float viewcoef_;
	int keymove_;
	bool focused_;
	byte state_;
	float onfocus_step_;
	SFVector2D mouse_pos_;

public:

	CSliderBar (SFVector2D position, bool from_top, float size,
				float min, float max, float step, float current, float viewcoef = 1.0f, int keymove = 1);
   virtual ~CSliderBar();
   
	float GetCurrent();
	float GetMin();
	float GetMax();
	void SetMin (float min);
	void SetMax (float max);

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

};

#endif