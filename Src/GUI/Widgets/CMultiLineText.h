﻿#ifndef CMULTILINETEXT
#define CMULTILINETEXT

#include "CWidget.h"

class CMultiLineText : public CWidget
{
private:

	struct CMultiLineString
	{
		CMultiLineString() : strlen(-1), big(false){}

		CMultiLineString(CString _str, bool isbig)
		{
			big = isbig;
			str = _str;
			strlen = -1;
		}

		CText str;
		int strlen;
		bool big;
	};

	CArray<CMultiLineString> text_;
	SFVector2D size_;
	SFVector2D prev_position_;
	int slider_pos_;
	float push_y_;
	bool cursor_in_;
	bool leftrightscrool_;
	byte state_;
	SColor Color1_;
	SColor Color2_;

public:

	CMultiLineText (SFVector2D position, bool from_top, bool leftrightscrool, SFVector2D size,
					SColor Color1, SColor Color2);

	virtual ~CMultiLineText();
					
	void AddStr (CString str, bool big = false);
	void AddStr (const char* str, bool big = false);
	void SetSize (SFVector2D size);
	void AddSliderPos (int pos);

	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();

};

#endif