﻿#include "CContainer.h"
#include "../../CApplication.h"

CContainer::CContainer (bool horizontal, bool from_top, bool main, 
						SFVector2D position, SFVector2D size) :
	message_box_ (NULL),
	message_box_actived_ (false), 
	ignorekeyup_ (false), 
	prev_mousemotion_ (0, 0),
	GuiShift_(-1),
	size_ (size), 
	focused_ (-1), 
	edit_text_focused_ (-1), 
	mouse_focused_ (false), 
	horizontal_ (horizontal), 
	main_ (main)
{
	position_ = position;
	from_top_ = from_top;
	message_box_ = NULL;
	elements_added_ = false;
}

CContainer::~CContainer()
{
	if (message_box_)
		delete message_box_;
	for (int i = 0; i < widgets_.Size(); i++)
		delete widgets_[i];

	widgets_.Clear();
}

void CContainer::AddWidget (CWidget* widget)
{
	widgets_.Insert (widget, true);
	elements_added_ = true;
}

void CContainer::RemoveAllWidgets()
{
	for (int i = 0; i < widgets_.Size(); i++)
		delete widgets_[i];
	
	widgets_.Clear();
	focused_ = -1;
}

void CContainer::CreateMessageBox()
{
	assert (main_);
	assert (!message_box_);

	message_box_ = new CMessageBox (SFVector2D (size_.x / 2.0f, size_.y / 2.0f));
}

void CContainer::OpenMessageBox (CString& str, bool isbutton)
{
	assert (main_);

	if (focused_ != -1)
	{
		widgets_[focused_]->FocusChange (false);
		focused_ = -1;
	}
	if (edit_text_focused_ != -1)
	{
		CEditText* edittext = dynamic_cast <CEditText*> (widgets_[edit_text_focused_]);
		assert (edittext);
		edittext->SetEditBoxFocus (false);
		edit_text_focused_ = -1;
	}

	message_box_->SetStr (str);
	message_box_->SetIsButton (isbutton);
	message_box_->SetVisible (true, prev_mousemotion_);
	message_box_actived_ = true;
}

void CContainer::CloseMessageBox()
{
	assert (main_);

	message_box_->SetVisible (false);
	message_box_actived_ = false;
	MouseMotion (prev_mousemotion_);
}

CWidget* CContainer::GetWidget (int i)
{
	assert (i >= 0 && i < widgets_.Size());
	return widgets_[i];
}

void CContainer::SetSize (SFVector2D size, float gui_shift)
{
	size_ = size;
	if (main_ && message_box_)
	{
		message_box_->SetPosition (SFVector2D (size_.x / 2.0f, size_.y / 2.0f));
	}
	GuiShift_ = gui_shift;
}

SFVector2D CContainer::GetSize()
{
	return size_;
}

int CContainer::GetFocused()
{
	return focused_;
}

void CContainer::SetFocused (int focused)
{
	if (focused_ != -1)
		widgets_[focused_]->FocusChange (false);
	focused_ = focused;
	if (focused_ != -1)
		widgets_[focused_]->FocusChange (true);
}

void CContainer::SetNextFocused()
{
	for (int i = focused_ + 1; i < widgets_.Size(); i++)
	{
		if (widgets_[i]->IsFocusing())
		{
			widgets_[i]->FocusChange (true);
			if (focused_ != -1)
			{
				CEditText* edittext = dynamic_cast <CEditText*> (widgets_[focused_]);
				if (edittext)
					edittext->SetEditBoxFocus (false);

				CContainer* prev_focused_container = dynamic_cast <CContainer*> (widgets_[focused_]);
				CContainer* current_focused_container = dynamic_cast <CContainer*> (widgets_[i]);

				if ((prev_focused_container != NULL && current_focused_container != NULL) && 
					(prev_focused_container->horizontal_ == current_focused_container->horizontal_ && 
					 current_focused_container->horizontal_ != horizontal_))
					current_focused_container->SetFocused (prev_focused_container->GetFocused());
				widgets_[focused_]->FocusChange (false);
			}
			focused_ = i;
			if (edit_text_focused_ != -1 && focused_ != edit_text_focused_)
			{
				CEditText* edittext = dynamic_cast <CEditText*> (widgets_[edit_text_focused_]);
				assert (edittext);
				edittext->SetEditBoxFocus (false);
				edit_text_focused_ = -1;
			}

			CEditText* edittext = dynamic_cast <CEditText*> (widgets_[focused_]);
			if (edittext)
			{
				edittext->SetEditBoxFocus (true);
				edit_text_focused_ = focused_;
			}
			else
				edit_text_focused_ = -1;
			
			mouse_focused_ = false;
			break;
		}
	}
}

void CContainer::SetPrevFocused()
{
	for (int i = focused_ - 1; i >= 0; i--)
	{
		if (widgets_[i]->IsFocusing())
		{
			widgets_[i]->FocusChange (true);
			if (focused_ != -1)
			{
				CEditText* edittext = dynamic_cast <CEditText*> (widgets_[focused_]);
				if (edittext)
					edittext->SetEditBoxFocus (false);

				CContainer* prev_focused_container = dynamic_cast <CContainer*> (widgets_[focused_]);
				CContainer* current_focused_container = dynamic_cast <CContainer*> (widgets_[i]);

				if (prev_focused_container != NULL && current_focused_container != NULL)
					current_focused_container->SetFocused (prev_focused_container->GetFocused());
				widgets_[focused_]->FocusChange (false);
			}
			focused_ = i;

			if (edit_text_focused_ != -1 && focused_ != edit_text_focused_)
			{
				CEditText* edittext = dynamic_cast <CEditText*> (widgets_[edit_text_focused_]);
				assert (edittext);
				edittext->SetEditBoxFocus (false);
				edit_text_focused_ = -1;
			}

			CEditText* edittext = dynamic_cast <CEditText*> (widgets_[focused_]);
			if (edittext)
			{
				edittext->SetEditBoxFocus (true);
				edit_text_focused_ = focused_;
			}
			else
				edit_text_focused_ = -1;
			
			mouse_focused_ = false;
			break;
		}
	}
}

void CContainer::Render (int time)
{
	if (elements_added_)
	{
		MouseMotion (prev_mousemotion_);
		elements_added_ = false;
	}

	for (int i = 0; i < widgets_.Size(); i++)
	{
		GUIRENDERER.PushMatrix();
		if (main_ && widgets_[i]->IsFromTop())
			GUIRENDERER.Translate (0, size_.y, 0);
		widgets_[i]->Render (time);
		GUIRENDERER.PopMatrix();
	}

	if (message_box_actived_)
	{
		message_box_->Render (time);
	}
}

void CContainer::MouseMotion (SFVector2D position)
{
	prev_mousemotion_ = position;

	if (message_box_actived_)
	{
		message_box_->MouseMotion (position);
		return;
	}

	for (int i = 0; i < (int) widgets_.Size(); i++)
	{
		if (widgets_[i]->IsCursorIn (SFVector2D (position.x,
			((widgets_[i]->IsFromTop() || !main_) ? (-position.y) : (-position.y + size_.y)))))
		{
			if (i != focused_)
			{
				if (focused_ != -1)
					widgets_[focused_]->FocusChange (false);

				focused_ = i;
				mouse_focused_ = true;

				widgets_[focused_]->FocusChange (true);
				if (!main_ || widgets_[focused_]->IsFromTop())
					widgets_[focused_]->MouseMotion (position);
				else
					widgets_[focused_]->MouseMotion (SFVector2D (position.x, position.y - size_.y));
			}
			else
			{
				if (!main_ || widgets_[focused_]->IsFromTop())
					widgets_[focused_]->MouseMotion (position);
				else
					widgets_[focused_]->MouseMotion (SFVector2D (position.x, position.y - size_.y));
			}

			return;
		}
	}

	if (focused_ != -1)
	{
		widgets_[focused_]->FocusChange (false);
		focused_ = -1;
	}
}

bool CContainer::KeyDown (unsigned key)
{
	if (message_box_actived_)
	{
		message_box_->KeyDown (key);
		if (!message_box_->IsVisible())
		{
			message_box_actived_ = false;
			MouseMotion (prev_mousemotion_);
		}
		return true;
	}

	if (focused_ == -1 && main_ && (key == KEY_LEFT || key == KEY_UP || key == KEY_RIGHT || key == KEY_DOWN))
	{
		SetNextFocused();
		return true;
	}
	
	if (edit_text_focused_ != -1 && !(focused_ != -1 && key == KEY_ENTER) && 
		widgets_[edit_text_focused_]->KeyDown (key))
		return true;
	else if (focused_ == -1)
		return false;
	else if (widgets_[focused_]->KeyDown (key))
	{
		if (key == KEY_ENTER)
			ignorekeyup_ = true;
		return true;
	}

	if (((horizontal_ && key == KEY_LEFT) || (!horizontal_ && key == KEY_UP)) && focused_ != 0)
	{
		SetPrevFocused();
		return true;
	}
	else if (((horizontal_ && key == KEY_RIGHT) || (!horizontal_ && key == KEY_DOWN)) &&
			 focused_ != (int) widgets_.Size() - 1)
	{
		SetNextFocused();
		return true;
	}
	return false;
}

void CContainer::KeyUp (unsigned key)
{
	if (ignorekeyup_)
	{
		ignorekeyup_ = false;
		return;
	}

	if (edit_text_focused_ != -1)
	{
		widgets_[edit_text_focused_]->KeyUp (key);
		return;
	}
	if (focused_ != -1)
		widgets_[focused_]->KeyUp (key);
}

void CContainer::LeftButtonDown()
{
	if (message_box_actived_)
	{
		message_box_->LeftButtonDown();
		if (!message_box_->IsVisible())
		{
			message_box_actived_ = false;
			MouseMotion (prev_mousemotion_);
		}
		return;
	}

	if (edit_text_focused_ != -1 && focused_ != edit_text_focused_)
	{
		CEditText* edittext = dynamic_cast <CEditText*> (widgets_[edit_text_focused_]);
		assert (edittext);
		edittext->SetEditBoxFocus (false);

		CEditText* edittextfocused = focused_ == -1 ? NULL : (dynamic_cast <CEditText*> (widgets_[focused_]));
		if (edittextfocused)
		{
			edit_text_focused_ = focused_;
			edittextfocused->SetEditBoxFocus (true);
		}
		else
			edit_text_focused_ = -1;
	}
	else if (focused_ != -1)
	{
		CEditText* edittext = dynamic_cast <CEditText*> (widgets_[focused_]);
		if (edittext)
		{
			edittext->SetEditBoxFocus (true);
			edit_text_focused_ = focused_;
			return;
		}
	}

	if (focused_ != -1 && (!main_ || mouse_focused_))
		widgets_[focused_]->LeftButtonDown();
}

void CContainer::LeftButtonUp()
{
	if (focused_ != -1 && (!main_ || mouse_focused_))
		widgets_[focused_]->LeftButtonUp();
}

void CContainer::RightButtonDown()
{
	if (focused_ != -1 && (!main_ || mouse_focused_))
		widgets_[focused_]->RightButtonDown();
}

void CContainer::RightButtonUp()
{
	if (focused_ != -1 && (!main_ || mouse_focused_))
		widgets_[focused_]->RightButtonUp();
}

void CContainer::FocusChange (bool focus)
{
	if (focus)
	{
		focused_ = 0;
		widgets_[focused_]->FocusChange (true);
	}
	else
	{
		if (focused_ != -1)
			widgets_[focused_]->FocusChange (false);

		focused_ = -1;
	}
}

bool CContainer::IsCursorIn (SFVector2D cursor)
{
	for (int i = 0; i < (int) widgets_.Size(); i++)
	{
		if (widgets_[i]->IsCursorIn (cursor))
			return true;
	}

	return false;
}

bool CContainer::IsFocusing()
{
	return true;
}
