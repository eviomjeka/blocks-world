﻿#include "CChooseElementBar.h"
#include "../../CApplication.h"

CChooseElementBar::CChooseElementBar (SFVector2D position, bool from_top, bool leftrightscrool, 
									  SFVector2D size, int max_str_size, 
									  int max_additional_str_size, float button_sizey, 
									  CChooseElementListener* listener, SColor Color1, SColor Color2) :
	max_str_size_ (max_str_size), 
	max_additional_str_size_ (max_additional_str_size), 
	focused_num_ (-1),
	focused_ (false), 
	mouse_focused_ (false), 
	selected_ (-1), 
	size_ (size),
	prev_position_ (-1, -1),
	button_sizey_ (button_sizey),
	slider_pos_ (0),
	push_y_ (0),
	cursor_in_ (false),
	leftrightscrool_ (leftrightscrool), 
	state_ (0),
	listener_ (listener), 
	Color1_ (Color1), 
	Color2_ (Color2)
{
	position_ = position;
	from_top_ = from_top;
}

CChooseElementBar::~CChooseElementBar()
{
	for (int i = 0; i < (int) elements_.Size(); i++)
		delete elements_[i];

	elements_.Clear();
}

void CChooseElementBar::AddElement (CString& name, CString& additionalstr)
{
	elements_.Insert (new CButton (SFVector2D (0, 0), true, 
		SFVector2D (size_.x / 2.0f, button_sizey_), 1, name, 
		CHOOSEELEMENT_BUTTONIDSTART + elements_.Size(), this, 
		Color1_, true, true, additionalstr), true);
}

void CChooseElementBar::SetSize (SFVector2D size)
{
	size_ = size;
	if (slider_pos_ + (int) (size_.y / (button_sizey_ + 1.5f)) > (int) elements_.Size())
		slider_pos_ = elements_.Size() - (int) (size_.y / (button_sizey_ + 1.5f));
	if (slider_pos_ < 0)
		slider_pos_ = 0;
}

void CChooseElementBar::AddSliderPos (int pos)
{
	if ((int) (size_.y / (button_sizey_ + 1.5f)) >= (int) elements_.Size())
		return;
	if (state_ == 2)
		return;

	int size = elements_.Size() - (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));
	
	slider_pos_ += pos;

	if (mouse_focused_ && focused_num_ != -1)
	{
		elements_[focused_num_]->FocusChange (false);
		focused_num_ += pos;
		if (slider_pos_ < 0)
			focused_num_ -= slider_pos_;
		else if (slider_pos_ > size)
			focused_num_ -= slider_pos_ - size;
		elements_[focused_num_]->FocusChange (true);
	}
	else if (!mouse_focused_)
	{
		if (focused_num_ != -1)
			elements_[focused_num_]->FocusChange (false);
		focused_num_ = -1;
	}

	if (slider_pos_ < 0)
		slider_pos_ = 0;
	else if (slider_pos_ > size)
		slider_pos_ = size;

	SFVector2D PrevPosition = prev_position_;
	if (from_top_)
		PrevPosition.y = -PrevPosition.y;
	IsCursorIn (PrevPosition);
	if (state_ == 0 && cursor_in_)
		state_ = 1;
	else if (state_ == 1 && !cursor_in_)
		state_ = 0;
}

int CChooseElementBar::GetSelectedElementID()
{
	return (elements_.Size() == 0 || selected_ == -1) ? -1 : selected_;
}

CString& CChooseElementBar::GetSelectedElementName()
{
	return (elements_.Size() == 0 || selected_ == -1) ? CString::EmptyString : elements_[selected_]->GetText();
}

CString& CChooseElementBar::GetSelectedElementAdditionalText()
{
	return (elements_.Size() == 0 || selected_ == -1) ? 
		CString::EmptyString : elements_[selected_]->GetAdditionalText();
}

void CChooseElementBar::CButtonOnClick (int id)
{
	if (selected_ != -1)
		elements_[selected_]->Unpress();

	selected_ = id - CHOOSEELEMENT_BUTTONIDSTART;

	listener_->CChooseElementBarOnSelectFirstTime();
}

void CChooseElementBar::Render (int time)
{
	GUIRENDERER.Translate (position_.x, position_.y, 0);

	GUIRENDERER.PushMatrix();

	GUIRENDERER.SetColor (255, 255, 255, 255);
	GUIRENDERER.AddHorizontalLine (0, 0, size_.x, 0.3f);
	GUIRENDERER.AddHorizontalLine (0, -size_.y, size_.x, 0.3f);
	GUIRENDERER.AddVerticalLine (0, -size_.y, size_.y, 0.3f);
	GUIRENDERER.AddVerticalLine (size_.x, -size_.y, size_.y, 0.3f);

	if ((int) size_.y < (int) elements_.Size() * (button_sizey_ + 1.5f))
	{
		GUIRENDERER.Translate (size_.x - 2.8f, 0, -0.1f);
		GUIRENDERER.AddVerticalLine (0, -size_.y, size_.y, 0.3f);

		GUIRENDERER.Translate (0.1f, -0.1f, 0.0f);

		GUIRENDERER.AddRect (SFVector (0, -size_.y + 0.2f, 0), SFVector (2.8f, 0, 0));

		GUIRENDERER.Translate (0, 0, 0.1f);

		int see = (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));
		float slider_size = see * size_.y / (float) elements_.Size();
		if (slider_size < 2.8f) slider_size = 2.8f;
		float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
						 ((float) (elements_.Size() - see));
		if (state_ == 0)
			GUIRENDERER.SetColor (Color1_);
		else
			GUIRENDERER.SetColor (Color2_);

		GUIRENDERER.AddRect (SFVector (0, -slider_pos - slider_size, 0), 
			SFVector (2.8f, -slider_pos, 0));
	}

	GUIRENDERER.PopMatrix();

	int see = (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));

	float elements_size = (min ((int) elements_.Size(), slider_pos_ + see) - slider_pos_ - 1) * 
							(button_sizey_ + 1.5f) + button_sizey_;
	GUIRENDERER.Translate (size_.x / 2.0f, -0.5f - ((size_.y - 1.0f) - elements_size) / 2.0f - button_sizey_, 0.0f);
	for (int i = 0; i < slider_pos_; i++)
		elements_[i]->Update (time);
	for (int i = min ((int) elements_.Size(), slider_pos_ + see); i < (int) elements_.Size(); i++)
		elements_[i]->Update (time);
	for (int i = slider_pos_; i < (int) elements_.Size() && i < slider_pos_ + see; i++)
	{
		GUIRENDERER.PushMatrix();
		char* str = elements_[i]->GetText().Data();
		char character = str[max_str_size_];
		str[max_str_size_] = 0;
		char* additional_str = elements_[i]->GetAdditionalText().Data();
		char character2 = additional_str[max_additional_str_size_];
		additional_str[max_additional_str_size_] = 0;
		elements_[i]->Render (time);
		str[max_str_size_] = character;
		additional_str[max_additional_str_size_] = character2;

		GUIRENDERER.PopMatrix();
		GUIRENDERER.Translate (0, -(button_sizey_ + 1.5f), 0);
	}
}

void CChooseElementBar::MouseMotion (SFVector2D position)
{
	prev_position_ = position;

	int see = (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));

	bool found_focused = false;
	float elements_size = (min ((int) elements_.Size(), slider_pos_ + see) - slider_pos_ - 1) * 
							(button_sizey_ + 1.5f) + button_sizey_;
	
	for (int i = slider_pos_; i < (int) elements_.Size() && i < slider_pos_ + see; i++)
	{
		if (elements_[i]->IsCursorIn (SFVector2D (position.x - (position_.x + size_.x / 2.0f), 
			position.y + (position_.y - 0.5f - ((size_.y - 1.0f) - elements_size) / 2.0f - (i - slider_pos_) * (button_sizey_ + 1.5f)))))
		{
			if (focused_num_ != -1)
				elements_[focused_num_]->FocusChange (false);
			focused_num_ = i;
			elements_[focused_num_]->FocusChange (true);
			mouse_focused_ = true;
			found_focused = true;
			break;
		}
	}
	if (!found_focused && focused_num_ != -1)
	{
		elements_[focused_num_]->FocusChange (false);
		focused_num_ = -1;
		mouse_focused_ = false;
	}

	if (focused_num_ != -1 && elements_.Size() != 0)
	{
		elements_[focused_num_]->MouseMotion (SFVector2D (position.x - 
			(position_.x + size_.x), position.y - (position_.y - (button_sizey_ + 1.5f) - focused_num_ * (button_sizey_ + 1.5f))));
		return;
	}
	
	if ((int) size_.y >= (int) elements_.Size() * (button_sizey_ + 1.5f))
		return;

	float slider_size = see * size_.y / (float) elements_.Size();
	if (slider_size < 2.8f) slider_size = 2.8f;
	float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
						((float) (elements_.Size() - see));

	if (!((position.x >= position_.x + size_.x - 2.8f &&
		   position.x <= position_.x + size_.x - 2.8f + 2.8f) &&
		  (position.y >= -position_.y + slider_pos &&
		   position.y <= -position_.y + slider_pos + slider_size)) && state_ != 2)
	{
		state_ = 0;
		return;
	}

	if (state_ == 0)
		state_ = 1;
	if (state_ == 1)
	{
		push_y_ = position.y - slider_pos;
		return;
	}
	else if (state_ == 2)
	{
		slider_pos_ = (int) ((position.y - push_y_) *
					  (elements_.Size() - see) / (size_.y - slider_size));

		if (slider_pos_ < 0)
			slider_pos_ = 0;
		else if (slider_pos_ > (int) (elements_.Size() - see))
			slider_pos_ = elements_.Size() - see;

		return;
	}
}

bool CChooseElementBar::KeyDown (unsigned key)
{
	if (elements_.Size() == 0)
		return false;

	if ((leftrightscrool_ && key == KEY_LEFT) || (!leftrightscrool_ && key == KEY_UP))
	{
		mouse_focused_ = false;
		if (focused_num_ > 0)
		{
			elements_[focused_num_]->FocusChange (false);
			focused_num_--;
			elements_[focused_num_]->FocusChange (true);

			if (focused_num_ - slider_pos_ < 0)
				slider_pos_--;

			return true;
		}
	}
	else if ((leftrightscrool_ && key == KEY_RIGHT) || (!leftrightscrool_ && key == KEY_DOWN))
	{
		mouse_focused_ = false;
		if (focused_num_ < (int) elements_.Size() - 1 && focused_num_ != -1)
		{
			elements_[focused_num_]->FocusChange (false);
			focused_num_++;
			elements_[focused_num_]->FocusChange (true);

			int see = (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));
			if (focused_num_ - slider_pos_ >= see)
				slider_pos_++;

			return true;
		}
	}
	else if (focused_num_ != -1)
		elements_[focused_num_]->KeyDown (key);

	return false;
}

void CChooseElementBar::KeyUp (unsigned) {}

void CChooseElementBar::LeftButtonDown()
{
	if (state_ == 1)
	{
		state_ = 2;
		return;
	}

	if (focused_num_ != -1 && selected_ != focused_num_)
		elements_[focused_num_]->LeftButtonDown();
}

void CChooseElementBar::LeftButtonUp()
{
	if (state_ == 2)
	{
		state_ = cursor_in_ ? 1 : 0;
		MouseMotion (prev_position_);
	}
}

void CChooseElementBar::RightButtonDown()	{}
void CChooseElementBar::RightButtonUp()		{}

void CChooseElementBar::FocusChange (bool focus)
{
	focused_ = focus;

	if (!focus)
	{
		state_ = 0;
		mouse_focused_ = false;
	}
	if (!focus && focused_num_ != -1)
	{
		elements_[focused_num_]->FocusChange (false);
		focused_num_ = -1;
		return;
	}
	if (focus && !mouse_focused_)
	{
		if (focused_num_ == -1 && elements_.Size() > 0)
		{
			focused_num_ = slider_pos_;
			elements_[focused_num_]->FocusChange (true);
		}
	}
}

bool CChooseElementBar::IsCursorIn (SFVector2D cursor)
{
	prev_position_ = SFVector2D (cursor.x, -cursor.y);

	int see = (int) ((size_.y - 1.0f) / (button_sizey_ + 1.5f));
	if ((int) size_.y < (int) elements_.Size() * (button_sizey_ + 1.5f))
	{
		float slider_size = see * size_.y / (float) elements_.Size();
		if (slider_size < 2.8f) slider_size = 2.8f;
		float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
							((float) (elements_.Size() - see));

		if ((cursor.x >= position_.x + size_.x - 2.8f &&
			 cursor.x <= position_.x + size_.x - 2.8f + 2.8f) &&
			(-cursor.y >= -position_.y + slider_pos &&
			 -cursor.y <= -position_.y + slider_pos + slider_size))
		{
			cursor_in_ = true;
			return true;
		}
		else
			cursor_in_ = false;
	}
	else
		cursor_in_ = false;

	if (state_ == 2)
		return true;	

	float elements_size = (min ((int) elements_.Size(), slider_pos_ + see) - slider_pos_ - 1) * 
							(button_sizey_ + 1.5f) + button_sizey_;
	
	for (int i = slider_pos_; i < (int) elements_.Size() && i < slider_pos_ + see; i++)
	{
		if (elements_[i]->IsCursorIn (SFVector2D (cursor.x - (position_.x + size_.x / 2.0f), 
			-cursor.y + (position_.y - 0.5f - ((size_.y - 1.0f) - elements_size) / 2.0f - 
			(i - slider_pos_) * (button_sizey_ + 1.5f)))))
		{
			return true;
		}
	}

	return false;
}

bool CChooseElementBar::IsFocusing()
{
	return elements_.Size() != 0;
}
