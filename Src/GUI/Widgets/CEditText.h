﻿#ifndef CEDITTEXT
#define CEDITTEXT

#include "CWidget.h"
#include "../../CKeysUtilities.h"

#define CURSOR_STEP 60

class CEditText : public CWidget, CKeyToCharacter::CCharacterListener
{
private:

	SFVector2D size_;
	bool focused_;
	CKeyToCharacter key_to_character_;
	float cursor_step_;

	CString str_;
	int cursor_;
	bool left_click_;
	int max_str_size_;

public:

	CEditText (SFVector2D position, bool from_top, 
			   SFVector2D size, int text_size, 
			   bool additional_symbols, CString& str = CString::EmptyString);
	virtual ~CEditText();
   
	virtual void Render (int time);
	virtual void MouseMotion (SFVector2D position);
	virtual bool KeyDown (unsigned key);
	virtual void KeyUp (unsigned key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void FocusChange (bool focus);
	virtual bool IsCursorIn (SFVector2D cursor);
	virtual bool IsFocusing();
	
	virtual void Character (char character);
	virtual void SpecialKey (unsigned key);

	void SetEditBoxFocus (bool focus);

	CString& GetStr();
	void SetStr (CString& str);

};

#endif