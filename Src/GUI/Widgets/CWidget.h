﻿#ifndef CWIDGET
#define CWIDGET

#include <Engine.h>
#include "../CGUIRenderer.h"
#include "../CText.h"

class CWidget
{
protected:

	SFVector2D position_;
	bool from_top_;

public:
   
   virtual ~CWidget() {}
   
	void SetPosition (SFVector2D position)
	{
		position_ = position;
	}
	SFVector2D GetPosition()
	{
		return position_;
	}
	bool IsFromTop()
	{
		return from_top_;
	}

	virtual void Render (int time) = 0;
	virtual void MouseMotion (SFVector2D position) = 0;
	virtual bool KeyDown (unsigned key) = 0;
	virtual void KeyUp (unsigned key) = 0;
	virtual void LeftButtonDown() = 0;
	virtual void LeftButtonUp() = 0;
	virtual void RightButtonDown() = 0;
	virtual void RightButtonUp() = 0;
	virtual void FocusChange (bool focus) = 0;
	virtual bool IsCursorIn (SFVector2D cursor) = 0;
	virtual bool IsFocusing() = 0;

};

#endif