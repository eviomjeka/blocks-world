﻿#include "CMultiLineText.h"
#include "../../CApplication.h"

CMultiLineText::CMultiLineText (SFVector2D position, bool from_top, 
	bool leftrightscrool, SFVector2D size, SColor Color1, SColor Color2) :
	size_ (size),
	prev_position_ (-1),
	slider_pos_ (0),
	push_y_ (0),
	cursor_in_ (false),
	leftrightscrool_ (leftrightscrool),
	state_ (0),
	Color1_ (Color1), 
	Color2_ (Color2)
{
	position_ = position;
	from_top_ = from_top;
}

CMultiLineText::~CMultiLineText() {}

void CMultiLineText::AddStr (const char* str, bool big)
{
	text_.Insert (CMultiLineString (str, big), true);
	if (big)
		text_.Insert (CMultiLineString ((char*) "", false), true);
}

void CMultiLineText::SetSize (SFVector2D size)
{
	size_ = size;
	if (slider_pos_ + (int) (size_.y / 2.2f) > (int) text_.Size())
		slider_pos_ = text_.Size() - (int) (size_.y / 2.2f);
	if (slider_pos_ < 0)
		slider_pos_ = 0;
}

void CMultiLineText::AddSliderPos (int pos)
{
	if ((int) (size_.y / 2.2f) >= (int) text_.Size())
		return;
	if (state_ == 2)
		return;

	slider_pos_ += pos;
	int size = text_.Size() - (int) ((size_.y - 1.0f) / 2.2f);

	if (slider_pos_ < 0)
		slider_pos_ = 0;
	else if (slider_pos_ > size)
		slider_pos_ = size;

	SFVector2D PrevPosition = prev_position_;
	if (from_top_)
		PrevPosition.y = -PrevPosition.y;
	IsCursorIn (PrevPosition);
	if (state_ == 0 && cursor_in_)
		state_ = 1;
	else if (state_ == 1 && !cursor_in_)
		state_ = 0;
}

void CMultiLineText::Render (int)
{
	GUIRENDERER.Translate (position_.x, position_.y, 0);
	GUIRENDERER.PushMatrix();

	GUIRENDERER.SetColor (255, 255, 255, 255);
	GUIRENDERER.AddHorizontalLine (0, 0, size_.x, 0.3f);
	GUIRENDERER.AddHorizontalLine (0, -size_.y, size_.x, 0.3f);
	GUIRENDERER.AddVerticalLine (0, -size_.y, size_.y, 0.3f);
	GUIRENDERER.AddVerticalLine (size_.x, -size_.y, size_.y, 0.3f);

	if ((int) (size_.y / 2.2f) < (int) text_.Size())
	{
		GUIRENDERER.Translate (size_.x - 3.0f, 0, -0.1f);
		GUIRENDERER.AddVerticalLine (0, -size_.y, size_.y, 0.3f);

		GUIRENDERER.Translate (0.1f, -0.1f, 0.0f);

		GUIRENDERER.AddRect (SFVector (0, -size_.y + 0.2f, 0), SFVector (2.8f, 0, 0));

		GUIRENDERER.Translate (0, 0, 0.1f);

		int see_text = (int) ((size_.y - 1.0f) / 2.2f);
		float slider_size = see_text * size_.y / (float) text_.Size();
		if (slider_size < 2.8f) slider_size = 2.8f;
		float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
						 ((float) (text_.Size() - see_text));
		if (state_ == 0)
			GUIRENDERER.SetColor (Color1_);
		else
			GUIRENDERER.SetColor (Color2_);

		GUIRENDERER.AddRect (SFVector (0, -slider_pos - slider_size, 0), 
			SFVector (2.8f, -slider_pos, 0));
	}

	GUIRENDERER.PopMatrix();

	GUIRENDERER.SetColor (0, 0, 0, 255);

	int see_text = (int) ((size_.y - 1.0f) / 2.2f);

	float elements_size = (min ((int) text_.Size(), slider_pos_ + see_text) - slider_pos_ - 1) * 2.2f + 0.9f;
	GUIRENDERER.Translate (1.5f, -0.5f - ((size_.y - 1.0f) - elements_size) / 2.0f - 0.9f, 0.0f);
	for (int i = slider_pos_; i < (int) text_.Size() && i < slider_pos_ + see_text; i++)
	{
		GUIRENDERER.PushMatrix();
		if (text_[i].big)
			GUIRENDERER.Translate (0, -0.25f, 0);
		text_[i].str.Render (0, text_[i].big ? 2.1f : 1.6f);
		GUIRENDERER.PopMatrix();
		GUIRENDERER.Translate (0, -2.2f, 0);
	}
}

void CMultiLineText::MouseMotion (SFVector2D position)
{
	prev_position_ = position;
	if (state_ == 1)
	{
		int see_text = (int) ((size_.y - 1.0f) / 2.2f);
		float slider_size = see_text * size_.y / (float) text_.Size();
		if (slider_size < 2.8f) slider_size = 2.8f;
		float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
							((float) (text_.Size() - see_text));

		push_y_ = position.y - slider_pos;
	}
	else if (state_ == 2)
	{
		int see_text = (int) ((size_.y - 1.0f) / 2.2f);
		float slider_size = see_text * size_.y / (float) text_.Size();
		if (slider_size < 2.8f) slider_size = 2.8f;

		slider_pos_ = (int) ((position.y - push_y_) *
					  (text_.Size() - see_text) / (size_.y - slider_size));

		if (slider_pos_ < 0)
			slider_pos_ = 0;
		else if (slider_pos_ > (int) text_.Size() - see_text)
			slider_pos_ = text_.Size() - see_text;
	}
}

bool CMultiLineText::KeyDown (unsigned key)
{
	int size = text_.Size() - (int) ((size_.y - 1.0f) / 2.2f);

	if (((leftrightscrool_ && key == KEY_LEFT) || (!leftrightscrool_ && key == KEY_UP)) && slider_pos_ > 0)
	{
		slider_pos_ -= 3;
		if (slider_pos_ < 0)
			slider_pos_ = 0;
		return true;
	}
	else if (((leftrightscrool_ && key == KEY_RIGHT) || (!leftrightscrool_ && key == KEY_DOWN)) && slider_pos_ < size)
	{
		slider_pos_ += 3;
		if (slider_pos_ > size)
			slider_pos_ = size;
		return true;
	}
	return false;
}

void CMultiLineText::KeyUp (unsigned) {}

void CMultiLineText::LeftButtonDown()
{
	if (state_ == 1)
		state_ = 2;
}

void CMultiLineText::LeftButtonUp()
{
	state_ = cursor_in_ ? 1 : 0;
	MouseMotion (prev_position_);
}

void CMultiLineText::RightButtonDown() {}
void CMultiLineText::RightButtonUp() {}

void CMultiLineText::FocusChange (bool focus)
{
	if (focus)
		state_ = 1;
	else
		state_ = 0;
}

bool CMultiLineText::IsCursorIn (SFVector2D cursor)
{
	prev_position_ = SFVector2D (cursor.x, -cursor.y);

	int see_text = (int) ((size_.y - 1.0f) / 2.2f);
	float slider_size = see_text * size_.y / (float) text_.Size();
	if (slider_size < 2.8f) slider_size = 2.8f;
	float slider_pos = ((float) slider_pos_) * (size_.y - slider_size - 0.2f) /
						((float) (text_.Size() - see_text));

	if ((cursor.x >= position_.x + size_.x - 2.8f &&
		 cursor.x <= position_.x + size_.x - 2.8f + 2.8f) &&
		(cursor.y <= position_.y - slider_pos &&
		 cursor.y >= position_.y - slider_pos - slider_size))
	{
		cursor_in_ = true;
		return true;
	}

	cursor_in_ = false;

	if ((int) (size_.y / 2.2f) >= (int) text_.Size())
		return false;
	if (state_ == 2)
		return true;

	return false;
}

bool CMultiLineText::IsFocusing()
{
	return (int) ((size_.y - 1.0f) / 2.2f) < (int) text_.Size();
}
