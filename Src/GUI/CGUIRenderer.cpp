﻿#include "CGUIRenderer.h"
#include "../CApplication.h"

const int CGUIRenderer::MESH_SIZE			= 2 << 18;
const int CGUIRenderer::FONT_MESH_SIZE		= 2 << 18;
const int CGUIRenderer::TEXTURE_MESH_SIZE	= 64;

void CGUIRenderer::Init()
{
	MeshConstructor_.Init (&CShaders::ColorShader, MESH_SIZE);
	FontMeshConstructor_.Init (&CShaders::TextureShader, FONT_MESH_SIZE);
	TextureMeshConstructor_.Init (&CShaders::TextureShader, TEXTURE_MESH_SIZE);

	UseShaderProgram (TextureShader);
	CShaders::TextureShader.UniformInt (CShaders::Texture.Texture(), 0);
	
	UseShaderProgram (FontShader);
	CShaders::FontShader.UniformInt (CShaders::Font.Texture(), 0);

	Translate_.Insert(SFVector (0, 0, 0), true);
	MatrixEngine_.ResetModelViewMatrix();

	FONT.SetMeshConstructor (&FontMeshConstructor_);

	SIVector2D Size = CPngImage::Instance.LoadFromFile ("Logo.png", &Texture_);

	TexureCoef_ = Size.y / (float) Size.x;
}

void CGUIRenderer::Destroy()
{
	Texture_.Destroy();

	Mesh_.Destroy();
	FontMesh_.Destroy();
	TextureMesh_.Destroy();
}

void CGUIRenderer::PushMatrix()
{
	SFVector translate = Translate_.Top();
	Translate_.Insert(translate, true);
}

void CGUIRenderer::PopMatrix()
{
	assert (Translate_.Size() > 1);
	Translate_.Pop();
}

void CGUIRenderer::Translate (float x, float y, float z)
{
	SFVector Position = Translate_.Top() + SFVector (x, y, z);
	Translate_.Pop();
	Translate_.Insert(Position, true);
	FONT.Translate (Position);
}

SFVector CGUIRenderer::GetTranslateVector()
{
	return Translate_.Top();
}

SColor CGUIRenderer::GetCurrentColor()
{
	return MeshConstructor_.GetCurrentColor();
}

CMatrixEngine* CGUIRenderer::GetMatrixEngine()
{
	return &MatrixEngine_;
}

float CGUIRenderer::GetTexureCoef()
{
	return TexureCoef_;
}

void CGUIRenderer::Resize (float ortho_gui_size_x)
{
	MatrixEngine_.ResetProjectionMatrix();
	MatrixEngine_.Ortho (0, ortho_gui_size_x, 0, CApplication::GUI_SIZE_Y, -1000, 1000);
}

void CGUIRenderer::Begin()
{
	MeshConstructor_.Begin (&Mesh_);
	FontMeshConstructor_.Begin (&FontMesh_);
	TextureMeshConstructor_.Begin (&TextureMesh_);
}

void CGUIRenderer::End()
{
	MeshConstructor_.End (&Mesh_);
	FontMeshConstructor_.End (&FontMesh_);
	TextureMeshConstructor_.End (&TextureMesh_);

	assert (Translate_.Size() == 1);
	Translate_.Pop();
	Translate_.Insert (SFVector (0, 0, 0), true);
}

void CGUIRenderer::Render()
{
	UseShaderProgram (TextureShader);
	Texture_.Bind();
	MatrixEngine_.Apply (&CShaders::TextureShader);
	TextureMesh_.Render();

	UseShaderProgram (ColorShader);
	MatrixEngine_.Apply (&CShaders::ColorShader);
	Mesh_.Render();
	
	UseShaderProgram (FontShader);
	FONT.Texture().Bind();
	MatrixEngine_.Apply (&CShaders::FontShader);
	FontMesh_.Render();
}

void CGUIRenderer::AddTextureVertex(SFVector Vertex, SFVector2D TexCoord)
{
	TextureMeshConstructor_.AddVertex (Vertex + Translate_.Top(), TexCoord);
}

void CGUIRenderer::SetColor(SColor Color)
{
	MeshConstructor_.SetColor (Color);
	FontMeshConstructor_.SetColor (Color);
}

void CGUIRenderer::SetColor(byte r, byte g, byte b, byte a)
{
	SetColor (SColor (r, g, b, a));
}

void CGUIRenderer::AddVertex(SFVector Vertex)
{
	MeshConstructor_.AddVertex (Vertex + Translate_.Top());
}

void CGUIRenderer::AddVertex(float x, float y, float z)
{
	MeshConstructor_.AddVertex (SFVector (x, y, z) + Translate_.Top());
}

void CGUIRenderer::AddQuad(SFVector v0, SFVector v1, SFVector v2, SFVector v3)
{
	MeshConstructor_.AddQuad (v0 + Translate_.Top(), v1 + Translate_.Top(), 
							  v2 + Translate_.Top(), v3 + Translate_.Top());
}

void CGUIRenderer::AddTriangle(SFVector v0, SFVector v1, SFVector v2)
{
	MeshConstructor_.AddTriangle (v0 + Translate_.Top(), v1 + Translate_.Top(), v2 + Translate_.Top());
}

void CGUIRenderer::AddRect(SFVector v0, SFVector v1)
{
	MeshConstructor_.AddRect (v0 + Translate_.Top(), v1 + Translate_.Top());
}

void CGUIRenderer::AddCircle(SFVector P, float R, int Quality)
{
	MeshConstructor_.AddCircle (P + Translate_.Top(), R, Quality);
}

void CGUIRenderer::AddHorizontalLine(float x, float y, float l, float w)
{
	MeshConstructor_.AddHorizontalLine (x + Translate_.Top().x, y + Translate_.Top().y, l, w);
}

void CGUIRenderer::AddVerticalLine(float x, float y, float l, float w)
{
	MeshConstructor_.AddVerticalLine (x + Translate_.Top().x, y + Translate_.Top().y, l, w);
}