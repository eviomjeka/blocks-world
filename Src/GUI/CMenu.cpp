﻿#include "CMenu.h"
#include "../CApplication.h"

CMenu::CMenu()
	:
		MenuNum_ (MAIN_MENU), 
		choose_button_id_ (-1),
		servernum_(-1),
		texturecoef_(-1),
		buttonid_ (-1), 
		MenuContainer_ (false, true, true, SFVector2D (0, 0), SFVector2D (0, 0)), 
		ingame_ (false)
{
}

CMenu::~CMenu()
{
	if (MenuNum_ == SETTINGS_MENU)
		SaveSettings_();
}

void CMenu::Init (bool IsGame)
{
	texturecoef_ = GUIRENDERER.GetTexureCoef();
	MenuContainer_.CreateMessageBox();

	if (IsGame)
		MainMenu_();
	else
		ServerMenu_();
}

void CMenu::FocusLost()
{
	MenuContainer_.SetFocused (-1);
}

void CMenu::Render (int time)
{
	if (MenuNum_ == LOADING_MENU)
		((CProgressBar*) MenuContainer_.GetWidget (0))->SetPercents (CLIENT.MapLoader().PercentsLoaded());
	if (buttonid_ != -1)
		CButtonOnClick_();

	if (MenuNum_ == SETTINGS_MENU && MenuContainer_.widgets_.Size() >= 11)
	{
		CContainer* Container = (CContainer*) MenuContainer_.GetWidget (10);
		if (Container->widgets_.Size() >= 1)
		{
			bool Checked = ((CCheckBox*) Container->GetWidget (1))->IsChecked();
			if (SETTINGS.ShowFPS != Checked)
				SETTINGS.ShowFPS = Checked;
		}
	}

	MenuContainer_.Render (time);
	glassert();
}

void CMenu::KeyDown (unsigned key)
{
	if (!APPMODULE.IsGameMode())
	{
		MenuContainer_.KeyDown (key);
		return;
	}

	if (choose_button_id_ != -1)
	{
		SettingsSetKey_ (key);
		return;
	}

	if (MenuNum_ != MAIN_MENU && MenuNum_ != GAME_MENU && MenuNum_ != LOADING_MENU && 
		key == KEY_ESCAPE && APPLICATION->IsMenu() && !ingame_)
	{
		if (MenuNum_ == SETTINGS_MENU)
			SaveSettings_();
		MenuContainer_.RemoveAllWidgets();
		MainMenu_();
	}
	else if (MenuNum_ == GAME_MENU && key == KEY_ESCAPE)
	{
		MenuContainer_.RemoveAllWidgets();
		APPLICATION->ResumeGame();
	}
	else if (key == KEY_ESCAPE && ingame_)
	{
		if (MenuNum_ == SETTINGS_MENU)
			SaveSettings_();
		MenuContainer_.RemoveAllWidgets();
		GameMenu();
	}
	else
		MenuContainer_.KeyDown (key);
}

void CMenu::KeyUp (unsigned key)
{
	MenuContainer_.KeyUp (key);
}

void CMenu::LeftButtonDown()
{
	if (choose_button_id_ != -1)
	{
		SettingsSetKey_ (MOUSE_LEFT_BUTTON);
		return;
	}

	MenuContainer_.LeftButtonDown();
}

void CMenu::LeftButtonUp()
{
	MenuContainer_.LeftButtonUp();
}

void CMenu::RightButtonDown()
{
	if (choose_button_id_ != -1)
	{
		SettingsSetKey_ (MOUSE_RIGHT_BUTTON);
		return;
	}

	MenuContainer_.RightButtonDown();
}

void CMenu::RightButtonUp()
{
	MenuContainer_.RightButtonUp();
}

void CMenu::MouseWhell (int delta)
{
	if (delta < 0 && choose_button_id_ != -1)
	{
		SettingsSetKey_ (MOUSE_WHELL_UP);
		return;
	}
	else if (delta > 0 && choose_button_id_ != -1)
	{
		SettingsSetKey_ (MOUSE_WHELL_DOWN);
		return;
	}

	if (MenuNum_ == MENU_VERSION_HISTORY || MenuNum_ == PT_SANS_LICENSE_MENU)
		((CMultiLineText*) MenuContainer_.GetWidget (1))->AddSliderPos (delta);
	else if (MenuNum_ == WORLDS_LIST || MenuNum_ == SERVERS_LIST)
		((CChooseElementBar*) MenuContainer_.GetWidget (1))->AddSliderPos (delta);
}

void CMenu::MouseMotion (SFVector2D mouse)
{
	MenuContainer_.MouseMotion (mouse);
}

void CMenu::Resize (float gui_shift)
{
	MenuContainer_.SetSize (SFVector2D (CApplication::GUI_SIZE_X, CApplication::GUI_SIZE_Y), gui_shift);

	if (MenuNum_ == MENU_VERSION_HISTORY || MenuNum_ == PT_SANS_LICENSE_MENU)
		((CMultiLineText*) MenuContainer_.GetWidget (1))->SetSize (
			SFVector2D (CApplication::GUI_SIZE_X - 6, CApplication::GUI_SIZE_Y - 20));
	else if (MenuNum_ == LOADING_MENU)
		MenuContainer_.GetWidget (0)->SetPosition (SFVector2D
							((CApplication::GUI_SIZE_X - 50) / 2, (CApplication::GUI_SIZE_Y - 5) / 2));
	else if (MenuNum_ == WORLDS_LIST)
		((CChooseElementBar*) MenuContainer_.GetWidget (1))->SetSize (
			SFVector2D (CApplication::GUI_SIZE_X - 6, CApplication::GUI_SIZE_Y - 27));
	else if (MenuNum_ == SERVERS_LIST)
		((CChooseElementBar*) MenuContainer_.GetWidget (1))->SetSize (
			SFVector2D (CApplication::GUI_SIZE_X - 6, CApplication::GUI_SIZE_Y - 27));
}

void CMenu::CChooseElementBarOnSelectFirstTime()
{
	CContainer* container = (CContainer*) MenuContainer_.GetWidget (2);
	((CButton*) container->GetWidget (1))->SetActived (true);
	((CButton*) container->GetWidget (2))->SetActived (true);
	((CButton*) container->GetWidget (3))->SetActived (true);
}

void CMenu::ServerMenu_()
{
	MenuNum_ = SERVER_MENU;
	
	float PlayersListShift = CApplication::GUI_SIZE_X / 4.0f;
	MenuContainer_.AddWidget (new CImage (SFVector2D (10, -3), true, true, 
							   CApplication::GUI_SIZE_X - 20, texturecoef_));
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (PlayersListShift + 3, -18), true, 0,
							   str_server_bw, 2.5f));
	CString Version, StrVersion;
	APPMODULE.Version (StrVersion);
	Version.Print ("%s %s", str_version.Data(), StrVersion.Data());
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 3, -18), 
							  true, 2, Version, 2.5f));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (3, -18), true, 0,
							  str_players_list, 2.5f));
	CMultiLineText* PlayersList = new CMultiLineText (SFVector2D (3, -20), true, true, 
								SFVector2D (PlayersListShift - 3.0f, 
								MenuContainer_.GetSize().y - 21), SColor (255, 0, 0), SColor (255, 100, 100));
	MenuContainer_.AddWidget (PlayersList);

	CMultiLineText* ServerLog = new CMultiLineText (SFVector2D (PlayersListShift + 3, -20), true, true, 
								SFVector2D (CApplication::GUI_SIZE_X - PlayersListShift - 6,
								MenuContainer_.GetSize().y - 31), SColor (255, 0, 0), SColor (255, 100, 100));
	MenuContainer_.AddWidget (ServerLog);

	MenuContainer_.AddWidget (new CEditText (SFVector2D (PlayersListShift + 3, 1), 
							   false, SFVector2D (CApplication::GUI_SIZE_X - 
							   PlayersListShift - 6, 8), CHAT_MAX_MESSAGE_SIZE, false));
}

void CMenu::MainMenu_()
{
	MenuNum_ = MAIN_MENU;

	//MenuContainer_.AddWidget (new CImage (SFVector2D (3, -3), true, true, CApplication::GUI_SIZE_X - 6, texturecoef_));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -30), true, SFVector2D (36, 6), 1,
											str_singleplayer, WORLDS_LIST_BUTTON, this, SColor (255, 0, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -37), true, SFVector2D (36, 6), 1,
											str_multiplayer, SERVERS_LIST_BUTTON, this, SColor (100, 100, 255)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -44), true, SFVector2D (36, 6), 1,
											str_settings, SETTINGS_BUTTON, this, SColor (255, 128, 64)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -51), true, SFVector2D (36, 6), 1,
											str_about_game, ABOUT_GAME_BUTTON, this, SColor (255, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -58), true, SFVector2D (36, 6), 1,
											str_quit, EXIT_BUTTON, this, SColor (255, 0, 255)));


	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -23), true, SFVector2D (80, 20), 1,
											CString::EmptyString, RUN_GAME_BUTTON, this, SColor (255, 50, 120)));
	
	CString Version, StrVersion;
	APPMODULE.Version (StrVersion);
	Version.Print ("%s %s", str_version.Data(), StrVersion.Data());
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (1.0f, 1.0f), false, 0,
							   Version, 2.0f));
}

void CMenu::WorldsList_()
{
	MenuNum_ = WORLDS_LIST;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_singleplayer, 5));

	CChooseElementBar* chooseworld = new CChooseElementBar (SFVector2D (3, -10), true, true, 
						SFVector2D (CApplication::GUI_SIZE_X - 6, MenuContainer_.GetSize().y - 27), 
						WORLD_NAME_SIZE - 1, 0, 4, this, SColor (255, 0, 0), SColor (255, 100, 100));
	CArray<CFileSystemElement> elements;
	CFileSystem::FileList (&elements, "Worlds");
	
	for (int i = 0; i < (int) elements.Size(); i++)
		if (elements[i].Directory)
			chooseworld->AddElement(elements[i].Name);

	elements.Clear();

	MenuContainer_.AddWidget (chooseworld);

	CContainer* container = new CContainer (true, false, false, SFVector2D (0, 10), 
											SFVector2D (CApplication::GUI_SIZE_X, 4));
	container->AddWidget (new CButton (SFVector2D (13, 10), false, SFVector2D (18.0f, 3.5f), 1,
						  str_new_world, NEW_WORLD_BUTTON, this, SColor (255, 255, 0), false, true));
	container->AddWidget (new CButton (SFVector2D (35, 10), false, SFVector2D (18.0f, 3.5f), 1,
						  str_rename, RENAME_WORLD_BUTTON, this, SColor (255, 255, 0), false, false));
	container->AddWidget (new CButton (SFVector2D (57, 10), false, SFVector2D (18.0f, 3.5f), 1,
						  str_remove, DELETE_WORLD_BUTTON, this, SColor (255, 255, 0), false, false));
	container->AddWidget (new CButton (SFVector2D (83, 10), false, SFVector2D (26, 3.5f), 1,
						  str_play, START_GAME_BUTTON, this, SColor (0, 255, 0), false, false));

	MenuContainer_.AddWidget (container);
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_main_menu, MAIN_MENU_BUTTON, this, SColor (255, 0, 0)));
}

void CMenu::NewWorldMenu_ (bool fromworldslist)
{
	if (!fromworldslist)
		WorldParamsMenuGetParams_();

	MenuContainer_.RemoveAllWidgets();

	MenuNum_ = NEW_WORLD;

	if (fromworldslist)
		SetStandartWorldParams_();

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_new_world, 5));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str_enter_world_name, 2.5f));

	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -25), 
							   true, SFVector2D (40, 3.0f), WORLD_NAME_SIZE, false));
	if (!fromworldslist)
	{
		((CEditText*) MenuContainer_.GetWidget (2))->SetStr (world_name_);
	}

	/*MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -30), true, 
		SFVector2D (30, 3), 1, str_world_parameters, WORLD_PARAMS_BUTTON, 
		this, 255, 255, 0, 255));*/
		
	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -44), true, SFVector2D (40, 5), 1,
							   str_play, START_GAME_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, WORLDS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::WorldParamsMenu_()
{
	/*MenuNum_ = WORLD_PARAMS;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_world_parameters, 5));
	
	MenuContainer_.AddWidget (new CCheckBox (SFVector2D ((CApplication::GUI_SIZE_X - 15) / 2, -26), 
							   true, str_day_night, 1.5f, 
							   worldparams_[2] == WORLDPARAMSBYTE2_DAY_NIGHT));
	MenuContainer_.AddWidget (new CCheckBox (SFVector2D ((CApplication::GUI_SIZE_X - 15) / 2, -28), 
							   true, str_plain, 1.5f, 
							   worldparams_[0] == WORLDPARAMSBYTE0_GENERATION_FLAT));
	MenuContainer_.AddWidget (new CCheckBox (SFVector2D ((CApplication::GUI_SIZE_X - 15) / 2, -30), 
							   true, str_plants, 1.5f, 
							   (worldparams_[1] & WORLDPARAMSBYTE1_GENERATE_TREES_CACTUSES) != 0));
	MenuContainer_.AddWidget (new CCheckBox (SFVector2D ((CApplication::GUI_SIZE_X - 15) / 2, -32), 
							   true, str_resources, 1.5f, 
							   (worldparams_[1] & WORLDPARAMSBYTE1_GENERATE_ORES) != 0));

	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -44), true, SFVector2D (40, 5), 1,
							   str_ok, NEW_WORLD_BUTTON, this, SColor (0, 255, 0)));*/
}

void CMenu::SetStandartWorldParams_()
{
	/*worldparams_[0] = WORLDPARAMSBYTE0_GENERATION_NORMAL;
	worldparams_[1] = WORLDPARAMSBYTE1_GENERATE_TREES_CACTUSES | WORLDPARAMSBYTE1_GENERATE_ORES;
	worldparams_[2] = WORLDPARAMSBYTE2_DAY_NIGHT;*/
}

void CMenu::WorldParamsMenuGetParams_()
{
	/*worldparams_[2] = ((CCheckBox*) MenuContainer_.GetWidget (1))->IsChecked() ? 
		WORLDPARAMSBYTE2_DAY_NIGHT : WORLDPARAMSBYTE2_ONLY_DAY;
	worldparams_[0] = ((CCheckBox*) MenuContainer_.GetWidget (2))->IsChecked() ? 
		WORLDPARAMSBYTE0_GENERATION_FLAT : WORLDPARAMSBYTE0_GENERATION_NORMAL;
	worldparams_[1] = (((CCheckBox*) MenuContainer_.GetWidget (3))->IsChecked() ? 
			WORLDPARAMSBYTE1_GENERATE_TREES_CACTUSES : 0) | 
		(((CCheckBox*) MenuContainer_.GetWidget (4))->IsChecked() ? 
			WORLDPARAMSBYTE1_GENERATE_ORES : 0);*/
}

void CMenu::StartNewWorld_()
{	
	CString name = ((CEditText*) MenuContainer_.GetWidget (2))->GetStr();

	if (name.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_world_name);
		return;
	}

	char addr[WORLD_NAME_SIZE + 50] = "";
	sprintf (addr, "Worlds/%s", name.Data());
	if (!CFileSystem::CreateDir (addr))
	{
		MenuContainer_.OpenMessageBox (str_mb_world_exists);
		return;
	}

	MenuContainer_.RemoveAllWidgets();
	LoadingMenu();
	APPLICATION->StartGame (true, name);
}

void CMenu::LoadWorld_()
{
	world_name_ = ((CChooseElementBar*) MenuContainer_.
				GetWidget (1))->GetSelectedElementName();
	MenuContainer_.RemoveAllWidgets();
	LoadingMenu();
	APPLICATION->StartGame (false, world_name_);
}

void CMenu::RenameWorldMenu_()
{
	MenuNum_ = RENAME_WORLD;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_rename_world, 5));
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str_enter_world_name, 2.5f));

	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -25), 
							   true, SFVector2D (40, 3.0f), WORLD_NAME_SIZE, false, world_name_));

	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -40), true, SFVector2D (30, 5), 1,
							   str_rename, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, WORLDS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::RenameWorld_()
{
	CString& new_world_name = ((CEditText*) MenuContainer_.GetWidget (2))->GetStr();
	char new_world_addr[WORLD_NAME_SIZE + 50] = "";
	sprintf (new_world_addr, "Worlds/%s", new_world_name.Data());
	char world_addr[MAX_PATH + 50] = "";
	sprintf (world_addr, "Worlds/%s", world_name_.Data());

	if (new_world_name.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_world_name);
		return;
	}

	char Code = CFileSystem::RenameDir (world_addr, new_world_addr);
	if (Code == FILESYSTEM_RENAMEDIR_EXISTS)
	{
		MenuContainer_.OpenMessageBox (str_mb_world_exists);
		return;
	}
	else if (Code == FILESYSTEM_RENAMEDIR_NOTFOUND)
		MenuContainer_.OpenMessageBox (str_mb_rename_failed);

	MenuContainer_.RemoveAllWidgets();
	WorldsList_();
}

void CMenu::DeleteWorldMenu_()
{
	MenuNum_ = DELETE_WORLD;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_remove_world, 5));

	CString str;
	str.Print ("%s %s", str_world_name.Data(), world_name_.Data());
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str, 2.5f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -25),
							   true, 1, str_remove_confirmation, 2.5f));
	
	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -40), true, SFVector2D (30, 5), 1,
							   str_remove, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							  str_back, WORLDS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::DeleteWorld_()
{
	char world_addr[MAX_PATH + 50] = "";
	sprintf (world_addr, "Worlds/%s", world_name_.Data());

	if (!CFileSystem::DeleteDir (world_addr))
		MenuContainer_.OpenMessageBox (str_mb_remove_failed);

	MenuContainer_.RemoveAllWidgets();
	WorldsList_();
}

void CMenu::ServersList_()
{
	MenuNum_ = SERVERS_LIST;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_multiplayer, 5));

	CChooseElementBar* chooseserver = new CChooseElementBar (SFVector2D (3, -10), true, true, 
						SFVector2D (CApplication::GUI_SIZE_X - 6, MenuContainer_.GetSize().y - 27), 
						WORLD_NAME_SIZE - 1, IP_ADDRESS_SIZE - 1, 4.5f, this, SColor (100, 100, 255), SColor (160, 160, 255));
	CFile File ("ServersList", CFILE_READ | CFILE_BINARY, false);

	while (File && !File.EndOfFile())
	{
		char servername[256] = {};
		char serverip[256] = {};
		File.GetString (servername, 255);
		File.GetString (serverip, 255);
		if (servername[0] == 0)
			break;

		if (servername[strlen (servername) - 1] == '\n')
			servername[strlen (servername) - 1] = 0;
		if (serverip[strlen (serverip) - 1] == '\n')
			serverip[strlen (serverip) - 1] = 0;
		

		CString _servername = servername;
		CString _serverip = serverip;

		chooseserver->AddElement (_servername, _serverip);
	}
	if (File)
		File.Destroy();

	MenuContainer_.AddWidget (chooseserver);

	CContainer* container = new CContainer (true, false, false, SFVector2D (0, 15), 
											SFVector2D (CApplication::GUI_SIZE_X, 4));
	container->AddWidget (new CButton (SFVector2D (13, 10), false, SFVector2D (18, 3.5f), 1,
						  str_add, ADD_SERVER_BUTTON, this, SColor (255, 255, 0), false, true));
	container->AddWidget (new CButton (SFVector2D (35, 10), false, SFVector2D (18, 3.5f), 1,
						  str_edit, CHANGE_SERVER_BUTTON, this, SColor (255, 255, 0), false, false));
	container->AddWidget (new CButton (SFVector2D (57, 10), false, SFVector2D (18, 3.5f), 1,
						  str_remove, REMOVE_SERVER_BUTTON, this, SColor (255, 255, 0), false, false));
	container->AddWidget (new CButton (SFVector2D (83, 10), false, SFVector2D (26, 3.5f), 1,
						  str_play, CONNECT_TO_SERVER_BUTTON, this, SColor (0, 255, 0), false, false));
	MenuContainer_.AddWidget (container);
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_main_menu, MAIN_MENU_BUTTON, this, SColor (100, 100, 255)));
}

bool CMenu::CheckAddress_ (char* Address)
{
	int Ip[4] = {};
	int Port = 0;
	int Pos = 0;

	for (int i = 0; i < 4; i++)
	{
		int AddPos = 0;
		if (sscanf (&Address[Pos], "%d%n", &Ip[i], &AddPos) != 1)
			return false;
		Pos += AddPos;

		if (Ip[i] < 0 || Ip[i] > 255)
			return false;
		
		if (i != 3 && Address[Pos] != '.')
			return false;

		Pos++;
	}
	
	Pos--;
	if (Address[Pos] == 0)
		return true;
	
	if (Address[Pos] != ':')
		return false;

	Pos++;
	int AddPos = 0;
	if (sscanf (&Address[Pos], "%d%n", &Port, &AddPos) != 1)
		return false;
	Pos += AddPos;

	if (Port <= 0 || Port > (1 << 16) - 1)
		return false;

	if (Address[Pos] != 0)
		return false;

	return true;
}

void CMenu::AddServerMenu_()
{
	MenuNum_ = ADD_SERVER;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_add_server, 5, SColor (0, 0, 0)));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str_server_name, 2.5f, SColor (0, 0, 0, 255)));
	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -25), 
							   true, SFVector2D (40, 3.0f), WORLD_NAME_SIZE, false));
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -30),
							   true, 1, str_server_ip, 2.5f));
	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -35), 
							   true, SFVector2D (40, 3.0f), IP_ADDRESS_SIZE, true));

	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -44), true, SFVector2D (40, 5), 1,
							   str_ok, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, SERVERS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::AddServer_()
{
	CString& servername = ((CEditText*) MenuContainer_.GetWidget (2))->GetStr();
	CString& serverip = ((CEditText*) MenuContainer_.GetWidget (4))->GetStr();

	if (servername.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_server_name);
		return;
	}
	else if (serverip.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_server_ip);
		return;
	}
	else if (!CheckAddress_ (serverip.Data()))
	{
		MenuContainer_.OpenMessageBox (str_mb_incorrect_ip, true);
		return;
	}
	else
	{
		CFile File ("ServersList", CFILE_APPEND | CFILE_BINARY);
		File.Print ("%s\n%s\n", servername.Data(), serverip.Data());
		File.Destroy();
	}

	MenuContainer_.RemoveAllWidgets();
	ServersList_();
}

void CMenu::ChangeServerMenu_()
{
	MenuNum_ = CHANGE_SERVER;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_edit_server, 5));

	CFile File ("ServersList", CFILE_READ | CFILE_BINARY);
	for (int i = 0; i < 2 * servernum_; i++)
	{
		assert (!File.EndOfFile());
		char str[256] = {};
		File.GetString (str, 255);
	}
	char servername[256] = {};
	char serverip[256] = {};
	File.GetString (servername, 255);
	File.GetString (serverip, 255);
	File.Destroy();
	if (servername[strlen (servername) - 1] == '\n')
		servername[strlen (servername) - 1] = 0;
	if (serverip[strlen (serverip) - 1] == '\n')
		serverip[strlen (serverip) - 1] = 0;
	servername[WORLD_NAME_SIZE - 1] = 0;
	serverip[IP_ADDRESS_SIZE - 1] = 0;
	CString cservername = servername;
	CString cserverip = serverip;


	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str_server_name, 2.5f));
	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -25), 
							   true, SFVector2D (40, 3.0f), WORLD_NAME_SIZE, false, cservername));
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -30),
							   true, 1, str_server_ip, 2.5f));
	MenuContainer_.AddWidget (new CEditText (SFVector2D ((CApplication::GUI_SIZE_X - 40) / 2, -35), 
							   true, SFVector2D (40, 3.0f), IP_ADDRESS_SIZE, true, cserverip));

	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -44), true, SFVector2D (30, 5), 1,
							   str_ok, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, SERVERS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::ChangeServer_()
{
	CString& servername = ((CEditText*) MenuContainer_.GetWidget (2))->GetStr();
	CString& serverip = ((CEditText*) MenuContainer_.GetWidget (4))->GetStr();

	if (servername.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_server_name);
		return;
	}
	else if (serverip.Empty())
	{
		MenuContainer_.OpenMessageBox (str_mb_enter_server_ip);
		return;
	}
	else if (!CheckAddress_ (serverip.Data()))
	{
		MenuContainer_.OpenMessageBox (str_mb_incorrect_ip, true);
		return;
	}
	else
	{
		CFile File ("ServersList", CFILE_READ | CFILE_BINARY);
		assert (File);
		char str[256] = {};
		for (int i = 0; i < 2 * servernum_; i++)
			File.GetString (str, 255);
		unsigned pos = File.Tell();
		for (int i = 0; i < 2; i++)
			File.GetString (str, 255);
		unsigned pos2 = File.Tell();
		File.Seek (0, SEEK_END);
		unsigned size = File.Tell();
		File.Seek (0, SEEK_SET);
		char* filestr = new char[size + 1];
		File.Read (filestr, size);
		File.Destroy();

		File.Init ("ServersList", CFILE_WRITE | CFILE_BINARY);
		if (pos != 0)
			File.Write (filestr, pos);
		File.Print ("%s\n%s\n", servername.Data(), serverip.Data());
		if (size - pos2 != 0)
			File.Write (&filestr[pos2], size - pos2);
		File.Destroy();
		delete[] filestr;
	}

	MenuContainer_.RemoveAllWidgets();
	ServersList_();
}

void CMenu::RemoveServerMenu_()
{
	MenuNum_ = REMOVE_SERVER;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_remove_server, 5));

	CFile File ("ServersList", CFILE_READ | CFILE_BINARY);
	for (int i = 0; i < 2 * servernum_; i++)
	{
		assert (!File.EndOfFile());
		char str[256] = {};
		File.GetString (str, 255);
	}
	char servername[256] = {};
	char serverip[256] = {};
	File.GetString (servername, 255);
	File.GetString (serverip, 255);
	File.Destroy();
	if (servername[strlen (servername) - 1] == '\n')
		servername[strlen (servername) - 1] = 0;
	if (serverip[strlen (serverip) - 1] == '\n')
		serverip[strlen (serverip) - 1] = 0;
	servername[WORLD_NAME_SIZE - 1] = 0;
	serverip[IP_ADDRESS_SIZE - 1] = 0;

	CString str;
	str.Print("%s %s", str_server_name.Data(), servername);
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str, 2.5f));
	str.Print("%s %s", str_server_ip.Data(), serverip);
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -25),
							   true, 1, str, 2.5f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -30),
							   true, 1, str_remove_confirmation, 2.5f));

	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -44), true, SFVector2D (30, 5), 1,
							   str_remove, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, SERVERS_LIST_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::RemoveServer_()
{
	CFile File ("ServersList", CFILE_READ | CFILE_BINARY);
	char str[256] = {};
	for (int i = 0; i < 2 * servernum_; i++)
		File.GetString (str, 255);
	unsigned pos = File.Tell();
	for (int i = 0; i < 2; i++)
		File.GetString (str, 255);
	unsigned pos2 = File.Tell();
	File.Seek (0, SEEK_END);
	unsigned size = File.Tell();
	File.Seek (0, SEEK_SET);
	char* filestr = new char[size + 1];
	File.Read (filestr, size);
	File.Destroy();
	
	File.Init ("ServersList", CFILE_WRITE | CFILE_BINARY);
	if (pos != 0)
		File.Write (filestr, pos);
	if (size - pos2 != 0)
		File.Write (&filestr[pos2], size - pos2);
	File.Destroy();
	delete[] filestr;

	MenuContainer_.RemoveAllWidgets();
	ServersList_();
}

void CMenu::ConnectToServerMenu_()
{
	MenuNum_ = CONNECT_TO_SERVER_MENU;

	/*MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, CString::EmptyString, 2.5f));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, 15), false, SFVector2D (30, 5), 1,
							  CString::EmptyString, CANCEL_BUTTON, this, SColor (100, 100, 255)));*/
}

void CMenu::ConnectToServer_()
{
	char IP[256] = "";
	char* Source = ((CChooseElementBar*) MenuContainer_.GetWidget (1))->
						GetSelectedElementAdditionalText().Data();
	strncpy (IP, Source, strlen (Source));
	int IntPort = -1;

	assert (CheckAddress_ (IP));

	for (int i = 0; IP[i] != 0; i++)
	{
		if (IP[i] == ':')
		{
			assert (sscanf (&IP[i + 1], "%d", &IntPort) == 1);
			IP[i] = 0;
		}
	}
	unsigned short Port = 0;
	if (IntPort == -1)
		Port = CApplicationModule::STANDART_PORT;
	else
		Port = (unsigned short) IntPort;

	MenuContainer_.RemoveAllWidgets();
	LoadingMenu();
	
	APPLICATION->ConnectToServer (IP, Port);
}

void CMenu::Settings_ (bool main_menu)
{
	MenuNum_ = SETTINGS_MENU;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_settings, 5));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (5, -11),
							   true, 0, str_view_distance, 1.8f));

	MenuContainer_.AddWidget (new CSliderBar (SFVector2D (35, -11.7f), true,
							   56, MIN_SIGHT_RANGE, MAX_SIGHT_RANGE, 1, (float) SETTINGS.SightRange, SUBWORLD_SIZE_XZ));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (35, -8.7f),
							   true, 1, str_low_distance, 1.8f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (35 + 56.0f * 4.0f / 14.0f, -8.7f),
							   true, 1, str_average_distance, 1.8f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (35 + 56.0f * 9.0f / 14.0f, -8.7f),
							   true, 1, str_long_distance, 1.8f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (35 + 56, -8.7f),
							   true, 1, str_great_distance, 1.8f));
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (5, -15),
							   true, 0, str_mouse_sensitivity, 1.8f));
	MenuContainer_.AddWidget (new CSliderBar (SFVector2D (35, -15.7f), true,
							   56, 0, 100, 1, (float) SETTINGS.MouseSensitivity, 1.0f, 5));
	
	CContainer* container = new CContainer (true, true, false, SFVector2D (0, -19.0f), SFVector2D (CApplication::GUI_SIZE_X, 2.5f));
	container->AddWidget (new CCheckBox (SFVector2D (5, -19.0f), true, str_shadows, 1.8f, SETTINGS.Shadows));
	container->AddWidget (new CCheckBox (SFVector2D (50, -19.0f), true, 
						   str_vsync, 1.8f, SETTINGS.VSync));
	MenuContainer_.AddWidget (container);
	
	container = new CContainer (true, true, false, SFVector2D (0, -21.5f), SFVector2D (CApplication::GUI_SIZE_X, 2.5f));
	container->AddWidget (new CCheckBox (SFVector2D (5, -21.5f), true, str_smooth_lighting, 1.8f, SETTINGS.SmoothLighting));
	container->AddWidget (new CCheckBox (SFVector2D (50, -21.5f), true, str_show_fps, 1.8f, SETTINGS.ShowFPS));
	MenuContainer_.AddWidget (container);
	
	container = new CContainer (true, true, false, SFVector2D (0, -24.0f), SFVector2D (CApplication::GUI_SIZE_X, 2.5f));
	container->AddWidget (new CCheckBox (SFVector2D (5, -24.0f), true, str_run_fullscreen, 1.8f, SETTINGS.RunFullscreen));
	container->AddWidget (new CCheckBox (SFVector2D (50, -24.0f), true, str_third_person, 1.8f, SETTINGS.ThirdPerson));
	MenuContainer_.AddWidget (container);
	
	
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -29),
							   true, 1, str_controls, 3.5f));

	CString str;

	container = new CContainer (true, true, false, SFVector2D (0, -34), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyForward, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -34), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 0, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyPrevBlock, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -34), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 1, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -33.2f),
							true, 0, str_forward, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -33.2f),
							true, 2, str_previous_block, 1.95f));
	MenuContainer_.AddWidget (container);

	container = new CContainer (true, true, false, SFVector2D (0, -38), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyBackward, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -38), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 2, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyNextBlock, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -38), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 3, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -37.2f),
							true, 0, str_backward, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -37.2f), 
							true, 2, str_next_block, 1.95f));
	MenuContainer_.AddWidget (container);

	container = new CContainer (true, true, false, SFVector2D (0, -42), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyLeftward, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -42), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 4, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyAddBlock, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -42), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 5, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -41.2f),
							true, 0, str_leftward, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -41.2f),
							true, 2, str_add_block, 1.95f));
	MenuContainer_.AddWidget (container);

	container = new CContainer (true, true, false, SFVector2D (0, -46), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyRightward, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -46), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 6, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyRemoveBlock, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -46), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 7, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -45.2f),
							true, 0, str_rightward, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -45.2f),
							true, 2, str_remove_block, 1.95f));
	MenuContainer_.AddWidget (container);

	container = new CContainer (true, true, false, SFVector2D (0, -50), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyJump, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -50), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 8, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyInventory, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -50), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 9, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -49.2f),
							true, 0, str_jump, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -49.2f),
							true, 2, str_open_inventory, 1.95f));
	MenuContainer_.AddWidget (container);

	container = new CContainer (true, true, false, SFVector2D (0, -54), SFVector2D (CApplication::GUI_SIZE_X, 3));
	assert (KeyToStr (SETTINGS.KeyScreenshot, str));
	container->AddWidget (new CButton (SFVector2D (2.5f, -54), true, SFVector2D (13, 3), 0,
							str, SETTINGS_BUTTON_START + 10, this, SColor (255, 0, 0), true));
	assert (KeyToStr (SETTINGS.KeyChat, str));
	container->AddWidget (new CButton (SFVector2D (97.5f, -54), true, SFVector2D (13, 3), 2,
							str, SETTINGS_BUTTON_START + 11, this, SColor (255, 0, 0), true));
	container->AddWidget (new CLabel (SFVector2D (18, -53.2f),
							true, 0, str_take_screenshot, 1.95f));
	container->AddWidget (new CLabel (SFVector2D (82, -53.2f),
							true, 2, str_open_chat, 1.95f));
	MenuContainer_.AddWidget (container);

	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X - 17.5f, -61), true, SFVector2D (25, 3), 1,
						  str_reset_settings, RESET_SETTINGS_BUTTON, this, SColor (255, 0, 0)));

	if (main_menu)
		MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
								  str_main_menu, MAIN_MENU_BUTTON, this, SColor (255, 128, 64)));
	else
		MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
								  str_game_menu, GAME_MENU_BUTTON, this, SColor (255, 128, 64)));
}

void CMenu::SaveSettings_()
{
	SETTINGS.SightRange			= (byte) ((CSliderBar*) MenuContainer_.GetWidget (2))->GetCurrent();
	SETTINGS.MouseSensitivity	= (byte) ((CSliderBar*) MenuContainer_.GetWidget (8))->GetCurrent();

	CContainer* Container		= (CContainer*) MenuContainer_.GetWidget (9);

	SETTINGS.Shadows			= ((CCheckBox*) Container->GetWidget (0))->IsChecked();
	SETTINGS.VSync				= ((CCheckBox*) Container->GetWidget (1))->IsChecked();
	
	Container					= (CContainer*) MenuContainer_.GetWidget (10);

	SETTINGS.SmoothLighting		= ((CCheckBox*) Container->GetWidget (0))->IsChecked();
	SETTINGS.ShowFPS			= ((CCheckBox*) Container->GetWidget (1))->IsChecked();
	
	Container					= (CContainer*) MenuContainer_.GetWidget (11);

	SETTINGS.RunFullscreen		= ((CCheckBox*) Container->GetWidget (0))->IsChecked();
	SETTINGS.ThirdPerson		= ((CCheckBox*) Container->GetWidget (1))->IsChecked();

	SETTINGS.Save();
}

void CMenu::SettingsSetKey_ (unsigned key)
{
	APPLICATION->KeyToMenu (false);
	MenuContainer_.CloseMessageBox();
	
	unsigned* keys[SETTINGS_NUM_KEYS] = {};
	bool key_use = false;
#define TO_KEYS_ARRAY(a,b) keys[a] = &(SETTINGS.b);
	TO_KEYS_ARRAY (0, KeyForward);
	TO_KEYS_ARRAY (1, KeyPrevBlock);
	TO_KEYS_ARRAY (2, KeyBackward);
	TO_KEYS_ARRAY (3, KeyNextBlock);
	TO_KEYS_ARRAY (4, KeyLeftward);
	TO_KEYS_ARRAY (5, KeyAddBlock);
	TO_KEYS_ARRAY (6, KeyRightward);
	TO_KEYS_ARRAY (7, KeyRemoveBlock);
	TO_KEYS_ARRAY (8, KeyJump);
	TO_KEYS_ARRAY (9, KeyInventory);
	TO_KEYS_ARRAY (10, KeyScreenshot);
	TO_KEYS_ARRAY (11, KeyChat);
#undef TO_KEYS_ARRAY

	for (int i = 0; i < SETTINGS_NUM_KEYS; i++)
	{
		if (i == choose_button_id_)
			continue;
		if ((*keys[i]) == key)
		{
			key_use = true;
			break;
		}
	}

	if (key_use || key == KEY_ESCAPE)
	{
		CString str;
		assert (KeyToStr ((*keys[choose_button_id_]), str));

		CContainer* container = ((CContainer*) MenuContainer_.GetWidget (SETTINGS_BUTTONS_ID_START + (int) (choose_button_id_ / 2)));
		((CButton*) container->GetWidget (2 * (choose_button_id_ % 2)))->SetText (str);
		((CButton*) container->GetWidget (2 * (choose_button_id_ % 2)))->Unpress();

		if (key_use)
			MenuContainer_.OpenMessageBox (str_mb_key_used);
		
		choose_button_id_ = -1;
		return;
	}

	CString str;
	assert (KeyToStr (key, str));

	CContainer* container = ((CContainer*) MenuContainer_.GetWidget (SETTINGS_BUTTONS_ID_START + (int) (choose_button_id_ / 2)));
	((CButton*) container->GetWidget (2 * (choose_button_id_ % 2)))->SetText (str);
	((CButton*) container->GetWidget (2 * (choose_button_id_ % 2)))->Unpress();

	(*keys[choose_button_id_]) = key;
	
	choose_button_id_ = -1;
}

void CMenu::SettingsOnButton_ (int id)
{
	CContainer* container = ((CContainer*) MenuContainer_.GetWidget 
		(SETTINGS_BUTTONS_ID_START + (int) ((id - SETTINGS_BUTTON_START) / 2)));
	((CButton*) container->GetWidget (2 * ((id - SETTINGS_BUTTON_START) % 2)))->SetText (CString::EmptyString);
	choose_button_id_ = id - SETTINGS_BUTTON_START;
	APPLICATION->KeyToMenu (true);
	MenuContainer_.OpenMessageBox (str_mb_enter_key, false);
}

void CMenu::ResetSettingsMenu_()
{
	MenuNum_ = RESET_SETTINGS_MENU;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_reset_settings, 5));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -20),
							   true, 1, str_reset_confirmation, 2.5f));
	
	MenuContainer_.AddWidget (new CButton (SFVector2D (50, -40), true, SFVector2D (30, 5), 1,
							   str_reset, CONFIRMATION_BUTTON, this, SColor (0, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							  str_back, SETTINGS_BUTTON, this, SColor (255, 255, 0)));
}

void CMenu::ResetSettings_()
{
	CFileSystem::DeleteFile ("Settings");
	SETTINGS.Load();

	MenuContainer_.RemoveAllWidgets();
	Settings_ (!ingame_);
}

void CMenu::AboutGame_()
{
	MenuNum_ = ABOUT_GAME;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_about_game, 6));

	MenuContainer_.AddWidget (new CImage (SFVector2D (25, -7), 
		true, true, CApplication::GUI_SIZE_X - 50, texturecoef_));

	CString Version, StrVersion;
	APPMODULE.Version (StrVersion);
	Version.Print ("%s %s", str_version.Data(), StrVersion.Data());

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2.0f, -16.0f), 
							   true, 1, Version, 2.0f));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2.0f, -19.6f),
							   true, 1, str_game_website, 2.0f));
	CString Site ("blocksworld.com");
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2.0f, -22.1f),
							   true, 1, Site, 2.0f));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (10, -34),
							   true, 0, str_idea_author, 3.0f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 10, -34),
							   true, 2, str_jeka, 3.0f));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (10, -38),
							   true, 0, str_developers, 3.0f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 10, -38),
							   true, 2, str_jeka, 3.0f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 10, -42),
							   true, 2, str_eviom, 3.0f));

	MenuContainer_.AddWidget (new CLabel (SFVector2D (10, -46),
							   true, 0, str_creative_consultant, 3.0f));
	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 10, -46),
							   true, 2, str_nikiten, 3.0f));

	CContainer* container = new CContainer (true, false, false, SFVector2D (2, 0), SFVector2D (CApplication::GUI_SIZE_X, 5));
	container->AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
						  str_main_menu, MAIN_MENU_BUTTON, this, SColor (255, 255, 0)));
	container->AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X - 5, 7), false, 2,
						  str_license_agreement_pt_sans, 2.5f));
	container->AddWidget (new CButton (SFVector2D (30 + (CApplication::GUI_SIZE_X - 30 - 26) / 2, 2), false, 
						  SFVector2D (26, 3.5f), 1, str_version_history, VERSION_HISTORY_BUTTON, this, SColor (0, 255, 0)));
	container->AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X - 5, 2), false, SFVector2D (26, 3.5f), 2,
						  str_license_agreement, PT_SANS_LICENSE_BUTTON, this, SColor (150, 150, 150)));
	MenuContainer_.AddWidget (container);
}

void CMenu::PTSansLicense_()
{
	MenuNum_ = PT_SANS_LICENSE_MENU;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_license_agreement_pt_sans, 3));

	CMultiLineText* version_history = new CMultiLineText (SFVector2D (3, -10), true, true, 
							   SFVector2D (CApplication::GUI_SIZE_X - 6,
							   MenuContainer_.GetSize().y - 20), SColor (150, 150, 150), SColor (200, 200, 200));

	CFile File ("PTSansLicense.txt", CFILE_READ);
	for (;;)
	{
		int i = 0;
		char str[MAX_STR_SIZE] = {};
		bool GetStringOK = File.GetString (str, MAX_STR_SIZE - 1, false);
		if (File.EndOfFile())
			break;
		assert (GetStringOK);
		if (str[strlen (str) - 1] == L'\n')
			str[strlen (str) - 1] = 0;
		version_history->AddStr (str);
		i++;
	}
	File.Destroy();

	MenuContainer_.AddWidget (version_history);

	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							   str_back, ABOUT_GAME_BUTTON, this, SColor (150, 150, 150)));
}

void CMenu::VersionHistory_()
{
	MenuNum_ = MENU_VERSION_HISTORY;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5),
							   true, 1, str_version_history, 5));

	CMultiLineText* version_history = new CMultiLineText (SFVector2D (3, -10), true, true, 
							   SFVector2D (CApplication::GUI_SIZE_X - 6,
							   MenuContainer_.GetSize().y - 20), SColor (0, 255, 0), SColor (150, 255, 150));

	CFile File ("Localisation/Russian/VersionHistory.txt", CFILE_READ);
	for (;;)
	{
		int i = 0;
		char str[MAX_STR_SIZE] = {};
		bool GetStringOK = File.GetString (str, MAX_STR_SIZE);
		if (File.EndOfFile())
			break;
		assert (GetStringOK);
		if (str[strlen (str) - 1] == L'\n')
			str[strlen (str) - 1] = 0;
		int versionpart[3] = {};
		version_history->AddStr (str, sscanf (str, "%d.%d.%d", &versionpart[0], &versionpart[1], &versionpart[2]) == 3);
		i++;
	}
	File.Destroy();

	MenuContainer_.AddWidget (version_history);

	MenuContainer_.AddWidget (new CButton (SFVector2D (20, 2), false, SFVector2D (30, 5), 1,
							  str_back, ABOUT_GAME_BUTTON, this, SColor (0, 255, 0)));
}

void CMenu::LoadingMenu()
{
	MenuContainer_.RemoveAllWidgets();

	MenuNum_ = LOADING_MENU;

	CProgressBar* progressbar = new CProgressBar (SFVector2D ((CApplication::GUI_SIZE_X - 50) / 2, 
		(CApplication::GUI_SIZE_Y - 5) / 2), false, SFVector2D (50, 5));
	progressbar->SetInfo (str_loading);
	MenuContainer_.AddWidget (progressbar);
}

void CMenu::GameMenu()
{
	MenuContainer_.RemoveAllWidgets();

	MenuNum_ = GAME_MENU;

	MenuContainer_.AddWidget (new CLabel (SFVector2D (CApplication::GUI_SIZE_X / 2, -5), true, 1,
											str_game_menu, 5));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -35), true, SFVector2D (36, 6), 1,
											str_resume_game, RESUME_GAME_BUTTON, this, SColor (255, 0, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -42), true, SFVector2D (36, 6), 1,
											str_settings, SETTINGS_BUTTON, this, SColor (255, 128, 64)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -49), true, SFVector2D (36, 6), 1,
											str_main_menu, MAIN_MENU_BUTTON, this, SColor (255, 255, 0)));
	MenuContainer_.AddWidget (new CButton (SFVector2D (CApplication::GUI_SIZE_X / 2, -56), true, SFVector2D (36, 6), 1,
											str_quit, EXIT_BUTTON, this, SColor (255, 0, 255)));
}

void CMenu::CButtonOnClick (int id)
{
	assert (buttonid_ == -1);
	buttonid_ = id;
}

void CMenu::CButtonOnClick_()
{
	int copyid = buttonid_;
	buttonid_ = -1;
	switch (copyid)
	{
		case WORLDS_LIST_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			WorldsList_();
		break;

		case NEW_WORLD_BUTTON:
			NewWorldMenu_ (MenuNum_ == WORLDS_LIST);
		break;

		case WORLD_PARAMS_BUTTON:
		{
			world_name_ = ((CEditText*) MenuContainer_.GetWidget (2))->GetStr();
			MenuContainer_.RemoveAllWidgets();
			WorldParamsMenu_();
		}
		break;

		case RENAME_WORLD_BUTTON:
			world_name_ = ((CChooseElementBar*) MenuContainer_.
				GetWidget (1))->GetSelectedElementName();
			MenuContainer_.RemoveAllWidgets();
			RenameWorldMenu_();
		break;

		case DELETE_WORLD_BUTTON:
			world_name_ = ((CChooseElementBar*) MenuContainer_.
				GetWidget (1))->GetSelectedElementName();
			MenuContainer_.RemoveAllWidgets();
			DeleteWorldMenu_();
		break;

		case START_GAME_BUTTON:
			ingame_ = true;
			if (MenuNum_== NEW_WORLD)
				StartNewWorld_();
			else if (MenuNum_ == WORLDS_LIST)
				LoadWorld_();
		break;

		case CONFIRMATION_BUTTON:
			if (MenuNum_== RENAME_WORLD)
				RenameWorld_();
			else if (MenuNum_ == DELETE_WORLD)
				DeleteWorld_();
			else if (MenuNum_ == ADD_SERVER)
				AddServer_();
			else if (MenuNum_ == CHANGE_SERVER)
				ChangeServer_();
			else if (MenuNum_ == REMOVE_SERVER)
				RemoveServer_();
			else if (MenuNum_ == RESET_SETTINGS_MENU)
				ResetSettings_();
		break;

		case SERVERS_LIST_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			ServersList_();
		break;

		case ADD_SERVER_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			AddServerMenu_();
		break;

		case CHANGE_SERVER_BUTTON:
			servernum_ = ((CChooseElementBar*) MenuContainer_.GetWidget (1))->GetSelectedElementID();
			MenuContainer_.RemoveAllWidgets();
			ChangeServerMenu_();
		break;

		case REMOVE_SERVER_BUTTON:
			servernum_ = ((CChooseElementBar*) MenuContainer_.GetWidget (1))->GetSelectedElementID();
			MenuContainer_.RemoveAllWidgets();
			RemoveServerMenu_();
		break;

		case CONNECT_TO_SERVER_BUTTON:
			ingame_ = true;
			ConnectToServer_();
		break;

		case VERSION_HISTORY_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			VersionHistory_();
		break;

		case SETTINGS_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			Settings_ (!ingame_);
		break;

		case RESET_SETTINGS_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			ResetSettingsMenu_();
		break;

		case ABOUT_GAME_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			AboutGame_();
		break;

		case EXIT_BUTTON:
			APPLICATION->DestroyWindow();
		break;

		case PT_SANS_LICENSE_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			PTSansLicense_();
		break;

		case MAIN_MENU_BUTTON:
			ingame_ = false;
			if (MenuNum_ == SETTINGS_MENU)
				SaveSettings_();
			else if (MenuNum_ == GAME_MENU)
				APPMODULE.DestroyGameAndServer();

			MenuContainer_.RemoveAllWidgets();
			MainMenu_();
		break;

		case GAME_MENU_BUTTON:
			if (MenuNum_ == SETTINGS_MENU)
				SaveSettings_();

			GameMenu();
		break;

		case RESUME_GAME_BUTTON:
			MenuContainer_.RemoveAllWidgets();
			APPLICATION->ResumeGame();
		break;

		case RUN_GAME_BUTTON:
		{
			ingame_ = true;

			CFileSystem::DeleteDir("Server/Worlds");
			CFileSystem::DeleteDir("WorldsCache");
			CFileSystem::DeleteDir("Worlds");
			CFileSystem::CreateDir("Server/Worlds");
			CFileSystem::CreateDir("WorldsCache");
			CFileSystem::CreateDir("Worlds");

			MenuContainer_.RemoveAllWidgets();
			LoadingMenu();
			CString String ("World");
			APPLICATION->StartGame (true, String);
		}
		break;

		default:
			if (copyid >= SETTINGS_BUTTON_START + 0 && copyid < SETTINGS_BUTTON_START + SETTINGS_NUM_KEYS)
				SettingsOnButton_ (copyid);
			else
				error ("Unknown button id");
		break;
	}
}
