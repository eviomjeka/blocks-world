﻿#include "CFPSCounter.h"
#include "../CApplication.h"

void CFPSCounter::Render (int FPS)
{
	if (!SETTINGS.ShowFPS)
		return;
	
	CString Str;
	Str.Print ("%d", FPS);

	GUIRENDERER.PushMatrix();
	GUIRENDERER.SetColor (0, 0, 0, 64);
	GUIRENDERER.AddRect (SFVector (-APPLICATION->GetGuiShift(), CApplication::GUI_SIZE_Y - 2.8f, 0.0f), 
						 SFVector (FONT.GetLength (Str, 2.5f) + 1.1f - APPLICATION->GetGuiShift(), 
						 CApplication::GUI_SIZE_Y, 0.0f));

	GUIRENDERER.SetColor (255, 255, 255, 255);
	GUIRENDERER.Translate (0.5f - APPLICATION->GetGuiShift(), CApplication::GUI_SIZE_Y - 2.1f, 0.1f);
	FONT.Render(Str, 2.5f);
	GUIRENDERER.PopMatrix();
}