﻿#include "CText.h"
#include "../CApplication.h"

CText::CText() :
Size_(0)
{
}

CText::CText(CString& String)
{
	CText::operator=(String);
}

void CText::Print(const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VPrint (Format, Args);
	va_end(Args);
}

void CText::VPrint (const char* Format, va_list Args)
{
	vsnprintf (Data_, MAX_STR_SIZE, Format, Args);
	Size_ = FONT.GetLength(*this, 1.0f);
}

void CText::Render(int Alignment, float Size, int Start, int End)
{
	GUIRENDERER.PushMatrix();
	if (Alignment == CTEXT_POS_CENTER)
		GUIRENDERER.Translate(-Size_ * Size / 2.0f, 0.0f, 0.0f);
	else if (Alignment == CTEXT_POS_RIGHT)
		GUIRENDERER.Translate(-Size_ * Size, 0.0f, 0.0f);
	
	FONT.Render(*this, Size, Start, End);
	
	GUIRENDERER.PopMatrix();
}

float CText::Size()
{
	return Size_;
}

void CText::RecalculateSize()
{
	Size_ = FONT.GetLength (*this, 1.0f);
}

void CText::operator = (CString& String)
{
	CString::operator = (String);
	RecalculateSize();
}
