﻿#ifndef CITEMCELL
#define CITEMCELL

#include <Engine.h>
#include "CGUIRenderer.h"
#include "../BlockItemBase/BlockItemInclude.h"

#define ITEMCELL_BACK_SIZE		4.0f
#define ITEMCELL_MAX_STATE		20
#define ITEMCELL_ONFOCUS_COEF	0.5f

class CItemCell
{
public:

	void Init (byte Alpha);
	void Destroy();

	void SetItem (CItemData Item);
	CItemData Item();
	void SetSelected (bool Selected, bool Animation = true);
	float GetState();
	void Render (int Time);

private:

	byte Alpha_;
	CItemData Item_;
	CMesh Mesh_;
	SFVector Translate_;
	float MaxSize_;
	bool Selected_;
	float State_;

};

#endif
