﻿#include "CItemCell.h"
#include "../CApplication.h"

void CItemCell::Init (byte Alpha)
{
	Alpha_ = Alpha;
	Selected_ = false;
	State_ = 0;

	Translate_ = SFVector (0.0f);
	MaxSize_ = 1.0f;
}

void CItemCell::Destroy()
{
	Mesh_.Destroy();
}

void CItemCell::SetItem (CItemData Item)
{
	Item_ = Item;

	if (!Item_.Void())
	{
		INVENTORY.MeshQueue.Begin (SIVector (0));
		Translate_ = CItemRenderer::Instance.Render (SIVector (0), Item_, &INVENTORY.MeshQueue, MaxSize_);
		INVENTORY.MeshQueue.End();
		INVENTORY.MeshQueue.Load (Mesh_);
	}
}

CItemData CItemCell::Item()
{
	return Item_;
}

void CItemCell::SetSelected (bool Selected, bool Animation)
{
	Selected_ = Selected;
	if (!Animation)
	{
		if (!Selected_)
			State_ = 0;
		else
			State_ = ITEMCELL_MAX_STATE;
	}
}

float CItemCell::GetState()
{
	return State_;
}

void CItemCell::Render (int Time)
{
	if (Selected_)
	{
		if (State_ < ITEMCELL_MAX_STATE)
			State_ += 1 * Time / 10.0f;
		if (State_ > ITEMCELL_MAX_STATE)
			State_ = ITEMCELL_MAX_STATE;
	}
	else
	{
		if (State_ > 0)
			State_ -= 1 * Time / 10.0f;
		if (State_ < 0)
			State_ = 0.0f;
	}

	float StateZoom = (1.0f + State_ * ITEMCELL_ONFOCUS_COEF / (float) ITEMCELL_MAX_STATE);
	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (-(StateZoom - 1.0f) * (ITEMCELL_BACK_SIZE) / 2.0f,
						   -(StateZoom - 1.0f) * ITEMCELL_BACK_SIZE / 2.0f, 0.0f);

	byte BackColor = 155 - (byte) (155.0f * State_ / (float) ITEMCELL_MAX_STATE);
	GUIRENDERER.SetColor (BackColor, BackColor, BackColor, Alpha_);

	GUIRENDERER.AddRect (SFVector (0, 0, 0), 
		SFVector (ITEMCELL_BACK_SIZE * StateZoom, ITEMCELL_BACK_SIZE * StateZoom, 0));
	
	GUIRENDERER.PopMatrix();

	if (Item_.Void() || !Mesh_)
		return;

	GUIRENDERER.PushMatrix();
	GUIRENDERER.Translate (-(StateZoom / MaxSize_ - 1.0f) * (ITEMCELL_BACK_SIZE) / 2.0f,
						   -(StateZoom / MaxSize_ - 1.0f) * ITEMCELL_BACK_SIZE / 2.0f, 0.0f);
	GUIRENDERER.Translate (0.5f * StateZoom / MaxSize_, 0.5f * StateZoom / MaxSize_, 0.1f);
	float Scale = (ITEMCELL_BACK_SIZE - 1.0f);

	UseShaderProgram (ComplexBlocksShader);
	CMatrixEngine* ME = GUIRENDERER.GetMatrixEngine();
	ME->PushModelViewMatrix();
	ME->Translate (GUIRENDERER.GetTranslateVector());
	ME->Scale (SFVector (Scale * StateZoom / MaxSize_));
	ME->Translate (Translate_ + SFVector (0.0f, 0.0f, 10.0f - 1.0f / MaxSize_));
	ME->Apply (&CShaders::ComplexBlocksShader);
	PLAYERCONTROLLER.WorldTexture().Bind();
	
	Mesh_.Render();

	ME->PopModelViewMatrix();

	GUIRENDERER.PopMatrix();
}
