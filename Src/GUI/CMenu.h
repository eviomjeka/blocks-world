﻿#ifndef CMENU
#define CMENU

#include <Engine.h>
#include "../WorldGenerator/CWorldGenerator.h"
#include "../Localisation/Localisation.h"
#include "GUIInclude.h"

enum Menu
{
	MAIN_MENU, 
	WORLDS_LIST, 
	NEW_WORLD, 
	WORLD_PARAMS, 
	RENAME_WORLD, 
	DELETE_WORLD, 
	SERVERS_LIST, 
	ADD_SERVER, 
	CHANGE_SERVER, 
	REMOVE_SERVER, 
	CONNECT_TO_SERVER_MENU, 
	MENU_VERSION_HISTORY, 
	SETTINGS_MENU, 
	RESET_SETTINGS_MENU, 
	ABOUT_GAME, 
	PT_SANS_LICENSE_MENU, 
	LOADING_MENU,
	GAME_MENU, 
	SERVER_MENU
};

enum Button
{
	WORLDS_LIST_BUTTON, 
	VERSION_HISTORY_BUTTON, 
	SETTINGS_BUTTON, 
	EXIT_BUTTON, 
	RUN_GAME_BUTTON, 
	ABOUT_GAME_BUTTON, 
	PT_SANS_LICENSE_BUTTON, 
	MAIN_MENU_BUTTON, 
	GAME_MENU_BUTTON, 
	RESUME_GAME_BUTTON, 

	NEW_WORLD_BUTTON, 
	WORLD_PARAMS_BUTTON, 
	RENAME_WORLD_BUTTON, 
	DELETE_WORLD_BUTTON, 
	START_GAME_BUTTON, 

	SERVERS_LIST_BUTTON, 
	ADD_SERVER_BUTTON, 
	CHANGE_SERVER_BUTTON, 
	REMOVE_SERVER_BUTTON, 
	CONNECT_TO_SERVER_BUTTON, 
	CANCEL_BUTTON, 

	CONFIRMATION_BUTTON, 
	RESET_SETTINGS_BUTTON, 
	SETTINGS_BUTTON_START
};

#define SETTINGS_NUM_KEYS 12
#define SETTINGS_BUTTONS_ID_START 12

#define IP_ADDRESS_SIZE 22

class CMenu : public CButton::CButtonOnClickListeter, CChooseElementBar::CChooseElementListener
{
private:

	int MenuNum_;
	int choose_button_id_;
	CString world_name_;
	int servernum_;
	float texturecoef_;
	int buttonid_;
	bool ingame_;
	CContainer MenuContainer_;
	
	void CButtonOnClick_();
	void MainMenu_();
	void ServerMenu_();

	void WorldsList_();
	void NewWorldMenu_ (bool fromworldslist);
	void WorldParamsMenu_();
	void RenameWorldMenu_();
	void RenameWorld_();
	void DeleteWorldMenu_();
	void DeleteWorld_();

	void ServersList_();
	bool CheckAddress_ (char* Address);
	void AddServerMenu_();
	void AddServer_();
	void ChangeServerMenu_();
	void ChangeServer_();
	void RemoveServerMenu_();
	void RemoveServer_();
	void ConnectToServerMenu_();
	void ConnectToServer_();

	void AboutGame_();
	void Settings_ (bool main_menu);
	void ResetSettingsMenu_();
	void ResetSettings_();
	void SaveSettings_();
	void VersionHistory_();
	void PTSansLicense_();

	void SettingsSetKey_ (unsigned key);
	void SettingsOnButton_ (int id);

	void StartNewWorld_();
	void LoadWorld_();
	void SetStandartWorldParams_  ();
	void WorldParamsMenuGetParams_();

public:

	CMenu();
	virtual ~CMenu();

	void Init (bool IsGame);
	void GameMenu();
	void LoadingMenu();
	void FocusLost();

	void Render (int time);
	void KeyDown (unsigned key);
	void KeyUp (unsigned key);
	void LeftButtonDown();
	void LeftButtonUp();
	void RightButtonDown();
	void RightButtonUp();
	void MouseWhell (int delta);
	void MouseMotion (SFVector2D mouse);
	void Resize (float gui_shift);

	virtual void CButtonOnClick (int id);
	virtual void CChooseElementBarOnSelectFirstTime();

};

#endif