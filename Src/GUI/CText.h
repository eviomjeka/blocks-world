﻿#ifndef CTEXT
#define CTEXT

#include <Engine.h>

#define CTEXT_POS_LEFT		0
#define CTEXT_POS_CENTER	1
#define CTEXT_POS_RIGHT		2

class CText : public CString
{
private:
	float Size_;

public:
	CText();
	CText(CString& String);

	void Print (const char* Format, ...);
	void VPrint (const char* Format, va_list Args);
	void Render (int Alignment, float Size, int Start = 0, int End = -1);
	float Size();
	void RecalculateSize();

	void operator = (CString& Strng);

};

#endif