#include "CWorldObject.h"
#include "../World/CSubWorld.h"

CWorldObject::CWorldObject()
{
	Initialized_ = false;
}

CWorldObject::CWorldObject (int ID, int Attributes, int Seed, SIVector2D Position, SIVector2D Size, bool CanIntersect)
{
	ID_ = ID;
	Attributes_ = Attributes;

	CRandom Random (Seed);
	Random_ = Random.RandomFromCoordinates (Position);

	Position_ = Position;
	Size_ = Size;
	CanIntersect_ = CanIntersect;

	Initialized_ = true;
}

int CWorldObject::ID()
{
	return ID_;
}

bool CWorldObject::Valid()
{
	return Initialized_;
}

CRandom& CWorldObject::Random()
{
	return Random_;
}

SIVector2D CWorldObject::Position()
{
	return Position_;
}

SIVector2D CWorldObject::Size()
{
	return Size_;
}

SIVector2D CWorldObject::FirstSubWorld()
{
	return Div2D (Position_, SUBWORLD_SIZE_XZ);
}

SIVector2D CWorldObject::LastSubWorld()
{
	return Div2D (Position_ + Size_, SUBWORLD_SIZE_XZ);
}

bool CWorldObject::Intersects (CWorldObject& Object)
{
	if (!Initialized_ || !Object.Initialized_)
		return false;

	return Intersects_ (*this, Object) || Intersects_ (Object, *this);
}

bool CWorldObject::Intersects_ (CWorldObject& Object1, CWorldObject& Object2)
{
	SIVector2D Points[4];
	Points[0] = SIVector2D (Object1.Position_.x, Object1.Position_.y);
	Points[1] = SIVector2D (Object1.Position_.x + Object1.Size_.x, Object1.Position_.y);
	Points[2] = SIVector2D (Object1.Position_.x + Object1.Size_.x, Object1.Position_.y + Object1.Size_.y);
	Points[3] = SIVector2D (Object1.Position_.x, Object1.Position_.y + Object1.Size_.y);

	for (int i = 0; i < 4; i++)
	{
		if ((Points[i].x >= Object2.Position_.x && Points[i].y >= Object2.Position_.y) && 
			(Points[i].x <= Object2.Position_.x + Object2.Size_.x && Points[i].y >= Object2.Position_.y + Object2.Size_.y))
			return true;
	}

	return false;
}

bool CWorldObject::CanIntersect (CWorldObject& Object)
{
	return ID_ == Object.ID() && CanIntersect_;
}

bool CWorldObject::Replaces (CWorldObject&)
{
	return true; // TODO: fix
}

bool operator == (_SWorldObject& A, _SWorldObject& B)
{
	return (
		A.ID_			== B.ID_		&&
		A.Position_		== B.Position_	&&
		A.Size_			== B.Size_		&&
		A.Attributes_	== B.Attributes_);
}

void operator << (CStream& Stream, _SWorldObject& Object)
{
	Stream << Object.ID_ << Object.Position_ << Object.Size_ << Object.Attributes_;
}

void operator >> (CStream& Stream, _SWorldObject& Object)
{
	Stream >> Object.ID_ >> Object.Position_ >> Object.Size_ >> Object.Attributes_;
}
