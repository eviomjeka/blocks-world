﻿#ifndef CNOISE
#define CNOISE

#include <Engine.h>

class CNoise
{
public:

	void Init (CRandom& Random);
	
	float Perlin2D (SFVector2D Position);
	float Perlin3D (SFVector   Position);

	float Simplex2D (SFVector2D Position);
	float Simplex3D (SFVector   Position);

private:

	static const SIVector2D	Grad2D_[12];
	static const SIVector	Grad3D_[12];
	
	int Perm_[512];

	float Noise_	(float x, float y);
	int FastFloor_	(float x);
	float Dot2D_	(SIVector2D v, float x, float y);
	float Dot3D_	(SIVector v, float x, float y, float z);
	float Mix_		(float a, float b, float t);
	float Fade_		(float t);

};

#endif