﻿#ifndef CTREES
#define CTREES

#include "CWorldObjectGenerator.h"

class CTrees : public CWorldObjectGenerator
{
public:

	virtual int MaxSize();
	virtual void GetObjects (SIVector2D Position, CArray<CWorldObject>& WorldObjects);
	virtual void Generate (CBlockManager* BlockManager, CWorldObject& Object);

private:

	int GetTreeStartY_ (SIVector2D Position, CBlockManager* BlockManager);

	void GenerateTree1_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random);
	void GenerateTree2_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random);
	void GenerateTree3_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random);
	void GenerateTree4_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random);

};

#endif