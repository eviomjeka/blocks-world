﻿#ifndef CWORLDGENERATOR
#define CWORLDGENERATOR

#include <Engine.h>
#include "../World/CSubWorld.h"
#include "CNoise.h"

struct SWorldObject
{
	SIVector2D Start;
	SIVector2D End;
	int Priority;
	int ID;
	int Extra;
};

struct SWorldParams
{
	int Seed;	
	byte Unused[256];
};

CStream& operator << (CStream& Stream, SWorldParams& WorldParams);
CStream& operator >> (CStream& Stream, SWorldParams& WorldParams);

class CWorldGenerator
{
public:

	void Init (SWorldParams& WorldParams);

	void Generate (CSubWorld& SubWorld);
	void PostGenerate (CSubWorld& SubWorld);
	
	/*void GetDependencies (SIVector2D SubWorld, CArray<SIVector2D>& Dependencies);
	void GetWorldObjects (SIVector2D Position, CArray<SWorldObject>& Objects);
	void GenerateWorldObject (CVectorArray2D<CSubWorld*>& SubWorld, SWorldObject& Object);*/

private:
	
	CRandom Random_;

	CNoise Height_[4];
	CNoise Humidity_[4];
	CNoise Perlin_;
	CNoise Simplex_;

};

#endif
