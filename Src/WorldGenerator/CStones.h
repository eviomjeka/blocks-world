﻿#ifndef CSTONES
#define CSTONES

#include "CWorldObjectGenerator.h"

class CStones : public CWorldObjectGenerator
{
public:

	virtual int MaxSize();
	virtual void GetObjects (SIVector2D Position, CArray<CWorldObject>& WorldObjects);
	virtual void Generate (CBlockManager* BlockManager, CWorldObject& Object);

private:

	int GetStoneStartY_ (SIVector2D Position, CBlockManager* BlockManager);

};

#endif
