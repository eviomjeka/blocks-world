﻿#include "CNoise.h"

const SIVector2D CNoise::Grad2D_[12] = 
{
	SIVector2D (1,0), SIVector2D (-1,0), SIVector2D (1,0),  SIVector2D (-1,0), 
	SIVector2D (1,1), SIVector2D (-1,1), SIVector2D (1,-1), SIVector2D (-1,-1), 
	SIVector2D (0,1), SIVector2D (0,1),  SIVector2D (0,-1), SIVector2D (0,-1)
};

const SIVector CNoise::Grad3D_[12] = 
{
	SIVector (1,1,0), SIVector (-1,1,0), SIVector (1,-1,0), SIVector (-1,-1,0), 
	SIVector (1,0,1), SIVector (-1,0,1), SIVector (1,0,-1), SIVector (-1,0,-1), 
	SIVector (0,1,1), SIVector (0,-1,1), SIVector (0,1,-1), SIVector (0,-1,-1)
};

void CNoise::Init (CRandom& Random)
{
	bool Values[256] = {};
	for (int i = 0; i < 256; i++)
	{
		int x = Mod (Random(), (256 - i));
		int j = 0;
		
		for (int k = 0;; j++, k++)
		{
			assert (j < 256);
			if (Values[j] == true)
				k--;

			if (k >= x)
				break;
		}
		assert (Values[j] == false);
		Values[j] = true;
		Perm_[j] = i;
	}

	for (int i = 0; i < 256; i++)
		Perm_[i + 256] = Perm_[i];
}

float CNoise::Perlin2D (SFVector2D Position)
{
	float x = Position.x;
	float y = Position.y;

	int X = FastFloor_ (x);
	int Y = FastFloor_ (y);

	x = x - X;
	y = y - Y;

	X = X & 255;
	Y = Y & 255;

	int gi00 = Perm_[X + 0 + Perm_[Perm_[Y + 0]]] % 12;
	int gi01 = Perm_[X + 0 + Perm_[Perm_[Y + 1]]] % 12;
	int gi10 = Perm_[X + 1 + Perm_[Perm_[Y + 0]]] % 12;
	int gi11 = Perm_[X + 1 + Perm_[Perm_[Y + 1]]] % 12;

	float n00 = Dot2D_ (Grad2D_[gi00], x - 0, y - 0);
	float n01 = Dot2D_ (Grad2D_[gi01], x - 0, y - 1);
	float n10 = Dot2D_ (Grad2D_[gi10], x - 1, y - 0);
	float n11 = Dot2D_ (Grad2D_[gi11], x - 1, y - 1);

	float u = Fade_ (x);
	float w = Fade_ (y);

	float nx0 = Mix_ (n00, n10, u);
	float nx1 = Mix_ (n01, n11, u);

	float nxy = Mix_ (nx0, nx1, w);

	return nxy;
}

float CNoise::Perlin3D (SFVector Position)
{
	float x = Position.x;
	float y = Position.y;
	float z = Position.z;

	int X = FastFloor_ (x);
	int Y = FastFloor_ (y);
	int Z = FastFloor_ (z);

	x = x - X;
	y = y - Y;
	z = z - Z;

	X = X & 255;
	Y = Y & 255;
	Z = Z & 255;

	int gi000 = Perm_[X + 0 + Perm_[Y + 0 + Perm_[Z + 0]]] % 12;
	int gi001 = Perm_[X + 0 + Perm_[Y + 0 + Perm_[Z + 1]]] % 12;
	int gi010 = Perm_[X + 0 + Perm_[Y + 1 + Perm_[Z + 0]]] % 12; 
	int gi011 = Perm_[X + 0 + Perm_[Y + 1 + Perm_[Z + 1]]] % 12;
	int gi100 = Perm_[X + 1 + Perm_[Y + 0 + Perm_[Z + 0]]] % 12;
	int gi101 = Perm_[X + 1 + Perm_[Y + 0 + Perm_[Z + 1]]] % 12;
	int gi110 = Perm_[X + 1 + Perm_[Y + 1 + Perm_[Z + 0]]] % 12;
	int gi111 = Perm_[X + 1 + Perm_[Y + 1 + Perm_[Z + 1]]] % 12;

	float n000 = Dot3D_ (Grad3D_[gi000], x - 0, y - 0, z - 0);
	float n001 = Dot3D_ (Grad3D_[gi001], x - 0, y - 0, z - 1);
	float n010 = Dot3D_ (Grad3D_[gi010], x - 0, y - 1, z - 0);
	float n011 = Dot3D_ (Grad3D_[gi011], x - 0, y - 1, z - 1);
	float n100 = Dot3D_ (Grad3D_[gi100], x - 1, y - 0, z - 0);
	float n101 = Dot3D_ (Grad3D_[gi101], x - 1, y - 0, z - 1);
	float n110 = Dot3D_ (Grad3D_[gi110], x - 1, y - 1, z - 0);
	float n111 = Dot3D_ (Grad3D_[gi111], x - 1, y - 1, z - 1);

	float u = Fade_ (x);
	float v = Fade_ (y);
	float w = Fade_ (z);

	float nx00 = Mix_ (n000, n100, u);
	float nx01 = Mix_ (n001, n101, u);
	float nx10 = Mix_ (n010, n110, u);
	float nx11 = Mix_ (n011, n111, u);

	float nxy0 = Mix_ (nx00, nx10, v);
	float nxy1 = Mix_ (nx01, nx11, v);

	float nxyz = Mix_ (nxy0, nxy1, w);

	return nxyz;
}

float CNoise::Simplex2D (SFVector2D Position)
{
	float x = Position.x;
	float y = Position.y;

	float n0 = 0, n1 = 0, n2 = 0;

	const float F2 = 0.5f * (sqrt (3.0f) - 1.0f);
	float s = (x + y) * F2;
	int i = FastFloor_ (x + s);
	int j = FastFloor_ (y + s);
	const float G2 = (3.0f - sqrt (3.0f)) / 6.0f;
	float t = (i + j) * G2;
	float X0 = i - t;
	float Y0 = j - t;
	float x0 = x - X0;
	float y0 = y - Y0;

	int i1 = 0, j1 = 0;
	if (x0 > y0)
	{
		i1 = 1;
		j1 = 0;
	}
	else
	{
		i1 = 0;
		j1 = 1;
	}

	float x1 = x0 - i1 + G2;
	float y1 = y0 - j1 + G2;
	float x2 = x0 - 1.0f + 2.0f * G2;
	float y2 = y0 - 1.0f + 2.0f * G2;

	int ii = i & 255;
	int jj = j & 255;
	int gi0 = Perm_[ii + Perm_[jj]] % 12;
	int gi1 = Perm_[ii + i1 + Perm_[jj + j1]] % 12;
	int gi2 = Perm_[ii + 1 + Perm_[jj + 1]] % 12;

	float t0 = 0.5f - x0 * x0 - y0 * y0;
	if (t0 < 0.0f)
		n0 = 0.0f;
	else
	{
		t0 *= t0;
		n0 = t0 * t0 * Dot2D_ (Grad2D_[gi0], x0, y0);
	}
	float t1 = 0.5f - x1 * x1 - y1 * y1;
	if (t1 < 0.0f)
		n1 = 0.0f;
	else
	{
		t1 *= t1;
		n1 = t1 * t1 * Dot2D_ (Grad2D_[gi1], x1, y1);
	}
	float t2 = 0.5f - x2 * x2 - y2 * y2;
	if (t2 < 0.0f)
		n2 = 0.0;
	else
	{
		t2 *= t2;
		n2 = t2 * t2 * Dot2D_ (Grad2D_[gi2], x2, y2);
	}

	return 70.0f * (n0 + n1 + n2);
}

float CNoise::Simplex3D (SFVector Position)
{
	float x = Position.x;
	float y = Position.y;
	float z = Position.z;

	float n0 = 0, n1 = 0, n2 = 0, n3 = 0;

	const float F3 = 1.0f / 3.0f;
	float s = (x + y + z) * F3;
	int i = FastFloor_ (x + s);
	int j = FastFloor_ (y + s);
	int k = FastFloor_ (z + s);
	const float G3 = 1.0f / 6.0f;
	float t = (i + j + k) * G3;
	float X0 = i - t;
	float Y0 = j - t;
	float Z0 = k - t;
	float x0 = Position.x - X0;
	float y0 = Position.y - Y0;
	float z0 = Position.z - Z0;

	int i1 = 0, j1 = 0, k1 = 0;
	int i2 = 0, j2 = 0, k2 = 0;
	if (x0 >= y0)
	{
		if (y0 >= z0)
		{
			i1 = 1;
			j1 = 0;
			k1 = 0;
			i2 = 1;
			j2 = 1;
			k2 = 0;
		}
		else if (x0 >= z0)
		{
			i1 = 1;
			j1 = 0;
			k1 = 0;
			i2 = 1;
			j2 = 0;
			k2 = 1;
		}
		else
		{
			i1 = 0;
			j1 = 0;
			k1 = 1;
			i2 = 1;
			j2 = 0;
			k2 = 1;
		}
	}
	else
	{
		if (y0 < z0)
		{
			i1 = 0;
			j1 = 0;
			k1 = 1;
			i2 = 0;
			j2 = 1;
			k2 = 1;
		}
		else if (x0 < z0)
		{
			i1 = 0;
			j1 = 1;
			k1 = 0;
			i2 = 0;
			j2 = 1;
			k2 = 1;
		}
		else
		{
			i1 = 0;
			j1 = 1;
			k1 = 0;
			i2 = 1;
			j2 = 1;
			k2 = 0;
		}
	}

	float x1 = x0 - i1 + G3;
	float y1 = y0 - j1 + G3;
	float z1 = z0 - k1 + G3;
	float x2 = x0 - i2 + 2.0f * G3;
	float y2 = y0 - j2 + 2.0f * G3;
	float z2 = z0 - k2 + 2.0f * G3;
	float x3 = x0 - 1.0f + 3.0f * G3;
	float y3 = y0 - 1.0f + 3.0f * G3;
	float z3 = z0 - 1.0f + 3.0f * G3;

	int ii = i & 255;
	int jj = j & 255;
	int kk = k & 255;
	int gi0 = Perm_[ii + Perm_[jj + Perm_[kk]]] % 12;
	int gi1 = Perm_[ii + i1 + Perm_[jj + j1 + Perm_[kk + k1]]] % 12;
	int gi2 = Perm_[ii + i2 + Perm_[jj + j2 + Perm_[kk + k2]]] % 12;
	int gi3 = Perm_[ii + 1 + Perm_[jj + 1 + Perm_[kk + 1]]] % 12;

	float t0 = 0.6f - x0 * x0 - y0 * y0 - z0 * z0;
	if (t0 < 0.0f)
		n0 = 0.0f;
	else
	{
		t0 *= t0;
		n0 = t0 * t0 * Dot3D_ (Grad3D_[gi0], x0, y0, z0);
	}
	float t1 = 0.6f - x1 * x1 - y1 * y1 - z1 * z1;
	if (t1 < 0.0f)
		n1 = 0.0f;
	else
	{
		t1 *= t1;
		n1 = t1 * t1 * Dot3D_ (Grad3D_[gi1], x1, y1, z1);
	}
	float t2 = 0.6f - x2 * x2 - y2 * y2 - z2 * z2;
	if (t2 < 0.0f)
		n2 = 0.0f;
	else
	{
		t2 *= t2;
		n2 = t2 * t2 * Dot3D_ (Grad3D_[gi2], x2, y2, z2 );
	}
	float t3 = 0.6f - x3 * x3 - y3 * y3 - z3 * z3;
	if (t3 < 0.0f)
		n3 = 0.0f;
	else
	{
		t3 *= t3;
		n3 = t3 * t3 * Dot3D_ (Grad3D_[gi3], x3, y3, z3);
	}

	return 32.0f * (n0 + n1 + n2 + n3);
}

int CNoise::FastFloor_ (float x)
{
	return x < 0 ? (int) x - 1 : (int) x;
}

float CNoise::Dot2D_ (SIVector2D v, float x, float y)
{
	return v.x * x + v.y * y;
}

float CNoise::Dot3D_ (SIVector v, float x, float y, float z)
{
	return v.x * x + v.y * y + v.z * z;
}

float CNoise::Mix_ (float a, float b, float t)
{
/*	// cos interpolation
	float ft = t * 3.1415927f;
    float f = (1.0f - cos(ft)) * 0.5f
    return a * (1.0f - f) + b * f;
*/
	return (1 - t) * a + t * b;
}

float CNoise::Fade_ (float t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}
