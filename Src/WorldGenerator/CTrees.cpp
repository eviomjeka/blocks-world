﻿#include "CTrees.h"

int CTrees::MaxSize()
{
	return 1;
}

void CTrees::GetObjects (SIVector2D Position, CArray<CWorldObject>& WorldObjects)
{
	CRandom Random (Position.x * 131071 + Position.y);
	for (int x = 0; x < 2; x++)
		for (int y = 0; y < 2; y++)
		{
			if (Random() % 5 == 0)
			{
				SIVector2D ObjectPosition = Position * SUBWORLD_SIZE_XZ + 
					SIVector2D (x * SUBWORLD_SIZE_XZ / 2 + Random() % (SUBWORLD_SIZE_XZ / 2), 
								y * SUBWORLD_SIZE_XZ / 2 + Random() % (SUBWORLD_SIZE_XZ / 2));
				WorldObjects.Insert (CWorldObject (ID_, 0, Seed_, ObjectPosition, SIVector2D (SUBWORLD_SIZE_XZ), true));
			}
		}
}

void CTrees::Generate (CBlockManager* BlockManager, CWorldObject& Object)
{
	CRandom& Random = Object.Random();

	int x = Object.Position().x + Object.Size().x / 2;
	int z = Object.Position().y + Object.Size().y / 2;
	int y = GetTreeStartY_ (SIVector2D (x, z), BlockManager);
	assert (y != -1);
	SIVector TreePosition (x, y, z);
	
	CBlockData Block = BlockManager->GetBlock (TreePosition);
	if (Block != BlockDataEx (Usual, BLOCK_GRASS))
		return;
		
	TreePosition.y++;
	
	int TreeType = Random() % 4;
	switch (TreeType)
	{
		case 0: GenerateTree1_ (TreePosition, BlockManager, Random); break;
		case 1: GenerateTree2_ (TreePosition, BlockManager, Random); break;
		case 2: GenerateTree3_ (TreePosition, BlockManager, Random); break;
		case 3: GenerateTree4_ (TreePosition, BlockManager, Random); break;
	}
}

int CTrees::GetTreeStartY_ (SIVector2D Position, CBlockManager* BlockManager)
{
	for (int y = SUBWORLD_SIZE_Y; y >= 0; y--)
	{
		CBlockData Block = BlockManager->GetBlock (SIVector (Position.x, y, Position.y));
		if (Block.ID() && Block != BlockDataEx (Usual, BLOCK_LEAVES))
			return y;
	}

	return -1;
}

void CTrees::GenerateTree1_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random)
{
	int TrunkSize = 7 + Random() % 6;
	int Radius = 3 + Random() % 4;

	for (int y = Position.y; y <= Position.y + TrunkSize + Radius * 1.5f - 1.0f; y++)
		BlockManager->SetBlock (SIVector (Position.x, y, Position.z), BlockDataEx (Usual, BLOCK_WOOD));
	Position.y += TrunkSize;

	for (int x = Position.x - Radius; x < Position.x + Radius; x++)
		for (int y = Position.y - Radius; y < Position.y + Radius * 1.5f; y++)
			for (int z = Position.z - Radius; z < Position.z + Radius; z++)
			{
				if ((Position.x - x) * (Position.x - x) + (Position.y - y) * (Position.y - y) / 2.25 + 
					(Position.z - z) * (Position.z - z) <= Radius * Radius)
					BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_LEAVES));
			}
}

void CTrees::GenerateTree2_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random)
{
	int NumParts = 2 + Random() % 4;
	int Radius = 5 + Random() % NumParts + Random() % 2;
	int TrunkSize = Random() % 2;

	for (int y = 0; y <= TrunkSize; y++)
		BlockManager->SetBlock (Position + SIVector(0, y, 0), BlockDataEx (Usual, BLOCK_WOOD));

	Position.y += TrunkSize;

	for (int i = 0; i < NumParts; i++)
	{
		int RadiusMinus = (i == NumParts - 1) ? 0 : 2;
		int Height = (i == NumParts - 1) ? (Radius - RadiusMinus) : min (Radius - RadiusMinus, 5);
		for (int x = Position.x - Radius; x < Position.x + Radius; x++)
			for (int z = Position.z - Radius; z < Position.z + Radius; z++)
				for (int y = Position.y; y < Position.y + Height; y++)
				{
					if (x == Position.x && z == Position.z && 
						!(i == NumParts - 1 && Radius - 1 + Position.y - y == 0))
					{
						BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_WOOD));
						continue;
					}
						
					if ((x - Position.x) * (x - Position.x) + (z - Position.z) * (z - Position.z) <= 
						(Radius - 1 + Position.y - y) * (Radius - 1 + Position.y - y))
						BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_LEAVES));
				}
		
		Position.y += Height;
		Radius--;
	}
}

void CTrees::GenerateTree3_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random)
{
	int Size = 14 + Random() % 7;
	int NumParts = 4 + Random() % 3;
	int StartY = Position.y;
	
	for (; Position.y <= StartY + Size; Position.y++)
		BlockManager->SetBlock (Position, BlockDataEx (Usual, BLOCK_WOOD));

	for (int i = 0; i < NumParts + 1; i++)
	{
		SIVector BlockPosition;
		int Radius = 2;

		if (i == NumParts)
		{
			BlockPosition = Position;
			BlockPosition.y = StartY + Size;
		}
		else
		{
			BlockPosition.y = StartY + Size / 2 + Random() % ((int) (Size / 2));
			Position.y = BlockPosition.y;

			int Side = Random() % 4;
			if (Side == 0)
			{
				BlockPosition.x = Position.x + Radius + 1;
				BlockPosition.z = Position.z;
				
				BlockManager->SetBlock (Position + SIVector (1, 0, 0), 
										BlockDataEx (Usual, BLOCK_WOOD));
			}
			else if (Side == 1)
			{
				BlockPosition.x = Position.x - Radius - 1;
				BlockPosition.z = Position.z;

				BlockManager->SetBlock (Position - SIVector (1, 0, 0), 
										BlockDataEx (Usual, BLOCK_WOOD));
			}
			else if (Side == 2)
			{
				BlockPosition.x = Position.x;
				BlockPosition.z = Position.z + Radius + 1;

				BlockManager->SetBlock (Position + SIVector (0, 0, 1), 
										BlockDataEx (Usual, BLOCK_WOOD));
			}
			else
			{
				BlockPosition.x = Position.x;
				BlockPosition.z = Position.z - Radius - 1;
				
				BlockManager->SetBlock (Position - SIVector (0, 0, 1), 
										BlockDataEx (Usual, BLOCK_WOOD));
			}
					
			BlockManager->SetBlock (BlockPosition, BlockDataEx (Usual, BLOCK_WOOD));
		}
				
		for (int x = BlockPosition.x - Radius; x < BlockPosition.x + Radius; x++)
			for (int y = BlockPosition.y - Radius; y < BlockPosition.y + Radius; y++)
				for (int z = BlockPosition.z - Radius; z < BlockPosition.z + Radius; z++)
				{
					if ((x - BlockPosition.x) * (x - BlockPosition.x) + 
						(y - BlockPosition.y) * (y - BlockPosition.y) + 
						(z - BlockPosition.z) * (z - BlockPosition.z) < 
						Radius * Radius - 1)
						BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_LEAVES));
				}
	}
}

void CTrees::GenerateTree4_ (SIVector Position, CBlockManager* BlockManager, CRandom& Random)
{
	int Radius = 4 + Random() % 5;
	int Size = 1 + Random() % 2;

	for (int y = Position.y; y <= Position.y + Size + 2 * Radius - 3; y++)
		BlockManager->SetBlock (SIVector (Position.x, y, Position.z), BlockDataEx (Usual, BLOCK_WOOD));
	Position.y += Size;

	for (int x = Position.x - Radius; x < Position.x + Radius; x++)
		for (int y = Position.y; y < Position.y + 2 * Radius; y++)
			for (int z = Position.z - Radius; z < Position.z + Radius; z++)
			{
				if ((x - Position.x) * (x - Position.x) + (z - Position.z) * (z - Position.z) <= 
					(Radius - 1 - ((y - Position.y) / 2)) * (Radius - 1 - (int) ((y - Position.y) / 2)))
				{
					if (x == Position.x && z == Position.z && y <= Position.y - 2)
						BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_WOOD));
					else
						BlockManager->SetBlock (SIVector (x, y, z), BlockDataEx (Usual, BLOCK_LEAVES));
				}
			}
}
