﻿#include "CStones.h"

int CStones::MaxSize()
{
	return 1;
}

void CStones::GetObjects (SIVector2D Position, CArray<CWorldObject>& WorldObjects)
{
	CRandom Random (Position.x * 131071 + Position.y);

	for (int x = 0; x < 2; x++)
		for (int y = 0; y < 2; y++)
		{
			if (Random() % 50 == 0)
			{
				SIVector2D ObjectPosition = Position * SUBWORLD_SIZE_XZ + 
					SIVector2D (x * SUBWORLD_SIZE_XZ / 2 + Random() % (SUBWORLD_SIZE_XZ / 2), 
								y * SUBWORLD_SIZE_XZ / 2 + Random() % (SUBWORLD_SIZE_XZ / 2));
				WorldObjects.Insert (CWorldObject (ID_, 0, Seed_, ObjectPosition, SIVector2D (SUBWORLD_SIZE_XZ), true));
			}
		}
}

void CStones::Generate (CBlockManager* BlockManager, CWorldObject& Object)
{
	CRandom& Random = Object.Random();

	int xx = Object.Position().x + Object.Size().x / 2;
	int zz = Object.Position().y + Object.Size().y / 2;
	int yy = GetStoneStartY_ (SIVector2D (xx, zz), BlockManager);
	assert (yy != -1);
	SIVector StonePosition (xx, yy, zz);
	
	CBlockData Block = BlockManager->GetBlock (StonePosition);
	if (Block != BlockDataEx (Usual, BLOCK_GRASS))
		return;
		
	StonePosition.y++;
	
	int Size = 1 + Random() % 3;
	if (Random() % 5 == 0)
		Size += Random() % 5;	

	for (int x = xx - Size; x < xx + Size; x++)
		for (int y = yy - Size; y < yy + Size; y++)
			for (int z = zz - Size; z < zz + Size; z++)
			{
				if ((x - xx) * (x - xx) + (y - yy) * (y - yy) + (z - zz) * (z - zz) <= Size * Size)
				{
					if (Random() % 20 != 0)
						BlockManager->SetBlock(SIVector(x, y, z), BlockDataEx (BlocksList.Usual, BLOCK_STONE));
				}
			}
}

int CStones::GetStoneStartY_ (SIVector2D Position, CBlockManager* BlockManager)
{
	for (int y = SUBWORLD_SIZE_Y; y >= 0; y--)
	{
		CBlockData Block = BlockManager->GetBlock (SIVector (Position.x, y, Position.y));
		if (Block.ID() && Block != BlockDataEx (Usual, BLOCK_LEAVES))
			return y;
	}

	return -1;
}
