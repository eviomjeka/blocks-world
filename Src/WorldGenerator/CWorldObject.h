#ifndef CWORLDOBJECT
#define CWORLDOBJECT

#include <Engine.h>


/*To store it in CSubWorld*/
struct _SWorldObject
{
protected:

	bool		Initialized_;
	int			ID_;
	int			Attributes_;
	CRandom		Random_;
	SIVector2D	Position_;
	SIVector2D	Size_;
	bool		CanIntersect_;

public:

	friend bool operator == (_SWorldObject& A, _SWorldObject& B);
	friend void operator << (CStream& Stream, _SWorldObject& Object);
	friend void operator >> (CStream& Stream, _SWorldObject& Object);

};

class CWorldObject :
	public _SWorldObject
{

public:

	CWorldObject();
	CWorldObject (int ID, int Attributes, int Seed, SIVector2D Position, SIVector2D Size, bool CanIntersect);
	
	int			ID();
	bool		Valid();
	CRandom&	Random();
	SIVector2D	Position();
	SIVector2D	Size();
	SIVector2D	FirstSubWorld();
	SIVector2D	LastSubWorld();
	bool		Intersects (CWorldObject& Object);
	bool		CanIntersect (CWorldObject& Object);
	bool		Replaces (CWorldObject& Object);
	

private:

	bool Intersects_ (CWorldObject& Object1, CWorldObject& Object2);

};

#endif