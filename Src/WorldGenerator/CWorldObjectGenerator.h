#ifndef CWORLDOBJECTGENERATOR
#define CWORLDOBJECTGENERATOR

#include <Engine.h>
#include "../World/CSubWorld.h"
#include "../BlockItemBase/BlockItemInclude.h"
#include "CWorldObject.h"

class CWorldObjectGenerator
{
public:

	void Init (int ID, int Seed);

	virtual int MaxSize() = 0;
	virtual void GetObjects (SIVector2D Position, CArray<CWorldObject>& WorldObjects) = 0;
	virtual void Generate (CBlockManager* BlockManager, CWorldObject& Object) = 0;

protected:

	int ID_;
	int Seed_;

};

#endif