﻿#include "CWorldGenerator.h"
#include "../CApplication.h"

void CWorldGenerator::Init (SWorldParams& WorldParams)
{	
	Random_.SetSeed (WorldParams.Seed);

	for (int i = 0; i < 4; i++)
		Height_[i].Init (Random_);

	for (int i = 0; i < 4; i++)
		Humidity_[i].Init (Random_);

	Perlin_.Init (Random_);
	Simplex_.Init (Random_); // TODO: wtf?? one CNoise?
}

void CWorldGenerator::Generate (CSubWorld& SubWorld)
{
	SIVector BlockPosition = SIVector (0, 0, 0);
	for (BlockPosition.x = 0; BlockPosition.x < SUBWORLD_SIZE_XZ; BlockPosition.x++)
		for (BlockPosition.z = 0; BlockPosition.z < SUBWORLD_SIZE_XZ; BlockPosition.z++)
		{
			BlockPosition.y = 0;
			SFVector2D AbsolutePosition = SFVector2D ((float) SubWorld.Position().x * SUBWORLD_SIZE_XZ + BlockPosition.x, 
													  (float) SubWorld.Position().y * SUBWORLD_SIZE_XZ + BlockPosition.z);
			
			float Height = (Height_[0].Perlin2D (AbsolutePosition / 30.0f)  * 4.0f + 
							Height_[1].Perlin2D (AbsolutePosition / 90.0f)  * 10.0f + 
							Height_[2].Perlin2D (AbsolutePosition / 180.0f) * 20.0f + 
							Height_[3].Perlin2D (AbsolutePosition / 800.0f) * 30.0f + 
							10.0f) * 2.0f + 58.0f;
			float Humidity = Humidity_[0].Perlin2D (AbsolutePosition / 100.0f)  * 4.0f + 
							 Humidity_[1].Perlin2D (AbsolutePosition / 360.0f)  * 20.0f + 
							 Humidity_[2].Perlin2D (AbsolutePosition / 1600.0f) * 50.0f + 
							 Humidity_[3].Perlin2D (AbsolutePosition / 2600.0f) * 80.0f;
			
			if (Height > 64 + 55)
				Height = 64 + 55 + (Height - 64 - 55) * 4.0f;
			
			bool IsTop = true;
			BlockPosition.y = Height <= 64 ? 64 : min (SUBWORLD_SIZE_Y - 1, (int) Height); // TODO: use min/max
			for (; BlockPosition.y >= 0; BlockPosition.y--)
			{
				CBlockData Block;

				if (Humidity > 28 && BlockPosition.y >= Height && BlockPosition.y <= 64)
				{
					if (BlockPosition.y == 64)
						Block = BlockData (BlocksList.Ice);
					else
						Block = BlockData (BlocksList.Water);
				}
				else if (BlockPosition.y >= Height && BlockPosition.y <= 64)
					Block = BlockData (BlocksList.Water);
				else if (BlockPosition.y <= Height - 6 && !IsTop)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_STONE);
				else if (Height > 64 + 54)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_STONE);
				else if (BlockPosition.y < 64 + 6 && Humidity <= 25)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_SAND);
				else if (Humidity < -28)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_SAND);
				else
				{
					if (IsTop)
						Block = BlockDataEx (BlocksList.Usual, BLOCK_GRASS);
					else
						Block = BlockDataEx (BlocksList.Usual, BLOCK_DIRT);
				}

				IsTop = false;
				if (!Block.ID())
					continue;

				SubWorld.SetBlock (BlockPosition, Block);
			}
	}

	// not optimized code for caves
	/*SIVector2D CavesPosition = Div2D (SubWorld.Position(), 8);
	CRandom Random = Random_.RandomFromCoordinates (CavesPosition);
	
	SIVector2D Start = (SubWorld.Position()) * SUBWORLD_SIZE_XZ;
	SIVector2D End   = (SubWorld.Position() + SIVector2D (1)) * SUBWORLD_SIZE_XZ;

	for (int i = 0; i < 100; i++)
	{
		SIVector StartPoint (Random (0, SUBWORLD_SIZE_XZ * 8) + CavesPosition.x * 8 * SUBWORLD_SIZE_XZ, 
						Random (0, 64), 
						Random (0, SUBWORLD_SIZE_XZ * 8) + CavesPosition.y * 8 * SUBWORLD_SIZE_XZ);
		SIVector PrevPoint = StartPoint;

		SIVector Delta (Random (-SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ), 
						Random (-5, 5), 
						Random (-SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ));
		SIVector Point = PrevPoint + Delta;

		for (int j = 0; j < 10; j++)
		{
			Delta += SIVector (Random (-SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ), 
							   Random (-10, 10), 
							   Random (-SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ)) / 2;

			PrevPoint = Point;
			Point = Point + Delta;

			SIVector MinPoint = SIVector (
				min (PrevPoint.x, Point.x), min (PrevPoint.y, Point.y), min (PrevPoint.z, Point.z));
			SIVector MaxPoint = SIVector (
				max (PrevPoint.x, Point.x), max (PrevPoint.y, Point.y), max (PrevPoint.z, Point.z));

			/*if ((MaxPoint.x + 10 > End.x || MinPoint.x - 10 < Start.x) || 
				(MaxPoint.z + 10 > End.y || MinPoint.z - 10 < Start.y))
				continue;//

			for (int x = MinPoint.x - 10; x < MaxPoint.x + 10; x++)
				for (int y = MinPoint.y - 10; y < MaxPoint.y + 10; y++)
					for (int z = MinPoint.z - 10; z < MaxPoint.z + 10; z++)
					{
						SIVector Position (x, y, z);
						if (Position.x >= Start.x && Position.x < End.x && 
							Position.y > 0 && Position.y < SUBWORLD_SIZE_Y && 
							Position.z >= Start.y && Position.z < End.y)
						{
							if (((Point - Position) * (Point - PrevPoint)).SqLen() / (Point - PrevPoint).SqLen() < 
								25 + pow (Perlin_.Perlin3D (Position) * 50.0f, 10.0f))
							{
								if (rand() % 1000 == 0)
									SubWorld.SetBlock (Position - SIVector (Start.x, 0, Start.y), BlockData (Lamp));
								else
									SubWorld.SetBlock (Position - SIVector (Start.x, 0, Start.y), CBlockData());
							}
						}
					}
		}
	}*/
}

/*void CWorldGenerator::Generate (CSubWorld& SubWorld)
{
	SIVector BlockPosition = SIVector (0, 0, 0);
	for (BlockPosition.x = 0; BlockPosition.x < SUBWORLD_SIZE_XZ; BlockPosition.x++)
		for (BlockPosition.z = 0; BlockPosition.z < SUBWORLD_SIZE_XZ; BlockPosition.z++)
		{
			BlockPosition.y = 0;
			SFVector2D AbsolutePosition = SFVector2D ((float) SubWorld.Position().x * SUBWORLD_SIZE_XZ + BlockPosition.x, 
													  (float) SubWorld.Position().y * SUBWORLD_SIZE_XZ + BlockPosition.z);
			
			float Height = (Height_[0].Perlin2D (AbsolutePosition / 30.0f)  * 4.0f + 
							Height_[1].Perlin2D (AbsolutePosition / 90.0f)  * 10.0f + 
							Height_[2].Perlin2D (AbsolutePosition / 180.0f) * 20.0f + 
							Height_[3].Perlin2D (AbsolutePosition / 800.0f) * 30.0f + 
							10.0f) * 2.0f + 58.0f;
			float Humidity = Humidity_[0].Perlin2D (AbsolutePosition / 100.0f)  * 4.0f + 
							 Humidity_[1].Perlin2D (AbsolutePosition / 360.0f)  * 20.0f + 
							 Humidity_[2].Perlin2D (AbsolutePosition / 1600.0f) * 50.0f + 
							 Humidity_[3].Perlin2D (AbsolutePosition / 2600.0f) * 80.0f;
			
			if (Height > 64 + 55)
				Height = 64 + 55 + (Height - 64 - 55) * 4.0f;
			
			bool IsTop = true;
			BlockPosition.y = Height <= 64 ? 64 : min (SUBWORLD_SIZE_Y - 1, (int) Height);
			for (; BlockPosition.y >= 0; BlockPosition.y--)
			{
				CBlockData Block;

				if (Humidity > 28 && BlockPosition.y >= Height && BlockPosition.y <= 64)
				{
					if (BlockPosition.y == 64)
						Block = BlockData (BlocksList.Ice);
					else
						Block = BlockData (BlocksList.Water);
				}
				else if (BlockPosition.y >= Height && BlockPosition.y <= 64)
					Block = BlockData (BlocksList.Water);
				else if (BlockPosition.y <= Height - 6 && !IsTop)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_STONE);
				else if (Height > 64 + 54)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_STONE);
				else if (BlockPosition.y < 64 + 6 && Humidity <= 25)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_SAND);
				else if (Humidity < -28)
					Block = BlockDataEx (BlocksList.Usual, BLOCK_SAND);
				else
				{
					if (IsTop)
						Block = BlockDataEx (BlocksList.Usual, BLOCK_GRASS);
					else
						Block = BlockDataEx (BlocksList.Usual, BLOCK_DIRT);
				}

				IsTop = false;
				if (!Block.ID())
					continue;

				SubWorld.SetBlock (BlockPosition, Block);
			}
	}
}*/

void CWorldGenerator::PostGenerate (CSubWorld&)
{
	/*SIVector BlockPosition = SIVector (0, 0, 0);
	for (BlockPosition.x = 0; BlockPosition.x < SUBWORLD_SIZE_XZ; BlockPosition.x++)
		for (BlockPosition.z = 0; BlockPosition.z < SUBWORLD_SIZE_XZ; BlockPosition.z++)
		{
			BlockPosition.y = 0;
			for (int y = SUBWORLD_SIZE_Y - 1; y >= 0; y--)
			{
				CBlockData Block = SubWorld.GetBlock (SIVector (BlockPosition.x, y, BlockPosition.z));
				if (Block.ID() && Block != BlockData (Water) && Block != BlockData (Ice))
				{
					BlockPosition.y = y + 1;
					break;
				}
			}
			SubWorld.SetBlock (SIVector (BlockPosition), BlockDataEx (Usual, BLOCK_SNOW));
		}*/
}

CStream& operator <<(CStream& Stream, SWorldParams& WorldParams)
{	
	Stream << WorldParams.Seed;
	Stream.Write((byte*) WorldParams.Unused, sizeof WorldParams.Unused);

	return Stream;
}

CStream& operator >>(CStream& Stream, SWorldParams& WorldParams)
{
	Stream >> WorldParams.Seed;	
	Stream.Read((byte*) WorldParams.Unused, sizeof WorldParams.Unused);

	return Stream;
}