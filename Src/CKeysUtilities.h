﻿#ifndef CKEYSUTILITIES
#define CKEYSUTILITIES

#include <Engine.h>
#include "Localisation/Localisation.h"

class CKeyToCharacter
{
public:

	class CCharacterListener
	{
	public:
		virtual void Character (char Character) = 0;
		virtual void SpecialKey (unsigned Key) = 0;
	};

	void Init (CCharacterListener* Listener, bool AdditionalSymbols);

	void KeyDown (unsigned Key);
	void KeyUp   (unsigned Key);
	void Update  (int Time);
	void StopKeyRepetition();

private:

	CCharacterListener* Listener_;
	bool Shift_;
	bool RepeatKey_;
	unsigned KeyCode_;
	float KeyTime_;
	bool AdditionalSymbols_;

	bool IsKeyNeeded_ (unsigned Key);
	void KeyEvent_ (unsigned Key);

};

bool KeyToStr (unsigned Key, CString& Str);

#endif
