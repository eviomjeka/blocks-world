﻿#ifndef CAPPLICATION
#define CAPPLICATION

#include <Engine.h>
#include "CApplicationModule.h"
#include "Network/CServer.h"
#include "Network/CClient.h"
#include "GUI/GUIInclude.h"
#include "CPlayerController.h"
#include "CSettings.h"
#include "World/CWorld.h"

#define GUI_SIZE_Y	 Instance->GetGuiSizeY()
#define APPLICATION	 CApplication::Instance

class CApplication : public CWindow
{
public:

	static const float GUI_SIZE_X;
	static const float MIN_GUI_SIZE_Y;
	static const int WINDOW_WIDTH;
	static const int WINDOW_HEIGHT;
	static const int SERVER_WINDOW_WIDTH;
	static const int SERVER_WINDOW_HEIGHT;
	static const int MIN_WINDOW_WIDTH;
	static const int MIN_WINDOW_HEIGHT;

	CApplication();
	virtual ~CApplication();
	static CApplication* Instance;
	static CApplicationModule ApplicationModule;
	
	virtual void Init (bool IsGameMode, int Port);
	virtual void Proc (int Time);
	virtual void KeyDown (unsigned Key);
	virtual void KeyUp (unsigned Key);
	virtual void LeftButtonDown();
	virtual void LeftButtonUp();
	virtual void RightButtonDown();
	virtual void RightButtonUp();
	virtual void MouseWhell (int Delta);
	virtual void MouseMotion (SIVector2D Position);
	virtual void Resize();
	virtual void Unfocus();
	virtual void Destroy();

	void StartGame (bool NewWorld, CString& WorldName);
	void ConnectToServer (const char* IP, unsigned short Port);
	void ResumeGame();

	bool IsMenu();
	void KeyToMenu (bool ToMenu);
	float GetGuiShift();
	float GetGuiSizeY();
	float GetFullSizeX();
	float GetWindowSizeCoef();

private:

	void InitOther_();

	void ToMenuMode_ (bool ChangeIsMenu = true);
	void ToGameMode_ (bool ChangeIsMenu = true);

	void EventDown_ (unsigned Key);
	void EventUp_ (unsigned Key);

	void SetOpenGLParameters_();
	void PrintOpenGLInformation_();

	SFVector2D CursorPosToGUIPos_ (SIVector2D Position);

	bool IsGameMode_;
	bool IsMenu_;
	bool LoadingGame_;
	bool TakeScreenshot_;
	float GuiSizeY_;
	float GuiShift_;
	float OrthoGuiSizeX_;
	float WindowSizeCoef_;
	CMenu Menu_;
	CFPSCounter FPSCounter_;
	SIVector2D PrevousCursorPos_;
	bool KeyToMenu_;
	
};

#endif
