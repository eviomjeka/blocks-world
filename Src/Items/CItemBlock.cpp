﻿#include "CItemBlock.h"
#include "../CApplication.h"

void CItemBlock::GetName (byte Attributes, CString& Name) const
{
	BlocksList ((byte) ID())->GetName (Attributes, Name);
}

void CItemBlock::GetInventoryItems (CArray<byte>& InventoryItems) const
{
	BlocksList ((byte) ID())->GetInventoryItems (InventoryItems);
}

bool CItemBlock::Render (byte Attributes, SVector2D<byte>& TextureOffset) const
{
	return BlocksList ((byte) ID())->RenderPreview (Attributes, TextureOffset);
}

void CItemBlock::OnAdd (byte Attributes, CBlockManager* BlockManager, SIVector Position, byte Face, SFVector2D Angle) const
{
	BlocksList ((byte) ID())->OnAdd (Attributes, BlockManager, Position, Face, Angle);
}