﻿#include "CItemVoid.h"
#include "../CApplication.h"

void CItemVoid::GetName (byte, CString& Name) const
{
	Name = "";
}

void CItemVoid::GetInventoryItems (CArray<byte>&) const
{
}

bool CItemVoid::Render (byte, SVector2D<byte>&) const
{
	return false;
}

void CItemVoid::OnAdd (byte, CBlockManager*, SIVector, byte, SFVector2D) const
{
}