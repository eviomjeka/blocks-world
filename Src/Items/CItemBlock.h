﻿#ifndef CITEMBLOCK
#define CITEMBLOCK

#include "../BlockItemBase/CItem.h"

class CItemBlock : public CItem
{
public:

	virtual void GetName			(byte Attributes, CString& Name) const;
	virtual void GetInventoryItems	(CArray<byte>& InventoryItems) const;
	virtual bool Render				(byte Attributes, SVector2D<byte>& TextureOffset) const;
	virtual void OnAdd				(byte Attributes, CBlockManager* BlockManager, 
									 SIVector Position, byte Face, SFVector2D Angle) const;

};

#endif