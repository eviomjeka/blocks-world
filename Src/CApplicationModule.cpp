﻿#include "CApplicationModule.h"
#include "CApplication.h"

const char* CApplicationModule::VERSION = "0.3.8";

const unsigned short CApplicationModule::STANDART_PORT = 28888;

CApplicationModule::CApplicationModule() :
	ServerThreadReady_ (false), 
	ServerThreadLaunched_ (false), 
	IsGameMode_ (true), 
	FPS_ (0), 
	Frames_ (0), 
	Time_ (0)
{}

void CApplicationModule::SetIsGameMode (bool IsGameMode, int Port)
{
	IsGameMode_ = IsGameMode;
	Port_ = Port;
}

int CApplicationModule::Port()
{
	return Port_;
}

bool CApplicationModule::IsGameMode()
{
	return IsGameMode_;
}

void CApplicationModule::Init()
{
	glClearColor (0.92f, 0.95f, 0.97f, 1.0f);

	glEnable (GL_DEPTH_TEST);
	glEnable (GL_CULL_FACE);

	glDepthFunc (GL_LESS);
	glFrontFace (GL_CW);
	glCullFace (GL_FRONT);
}

void CApplicationModule::InitLog()
{
	CLog::Instance.Init (IsGameMode_);
	CString Version;
	APPMODULE.Version (Version);

	__LOG ("BLOCKS WORLD [ver. %s]\n", Version.Data());
	
	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	CString Info;
	strftime (Info.Data(), 255, "Launch time: [%d-%m-%y %H.%M.%S]\n", timeinfo);
	__LOG (Info.Data());

	CString Device (DEVICE_NAME);
	CString Os (OS_NAME);

	__LOG ("Device: %s\nOS: %s\n\n", Device.Data(), Os.Data());

	__LOG ("Last line of log should be: \"APPLICATION DESTROYED\". "
		   "If the last line of the other, the application not completed successfully.\n\n");

	LOG ("APPLICATION IS INITIALIZED");
}

void CApplicationModule::InitOther()
{
	ItemsList.Init();
	BlocksList.Init();
	CSubWorld::SubWorldMemoryManager.Init();
}

void CApplicationModule::PrepareDirectories()
{
#ifdef FOR_PACKAGE
	system("mkdir -p ~/BlocksWorld/Files");
	system("cp -r /usr/share/blocks-world/* ~/BlocksWorld/Files");
#endif

	CFileSystem::CreateDir ("Logs");
	CFileSystem::CreateDir ("Screenshots");
	CFileSystem::CreateDir ("Server");
	CFileSystem::CreateDir ("Server/Worlds");
	CFileSystem::CreateDir ("WorldsCache");
	CFileSystem::CreateDir ("Worlds");
}

void CApplicationModule::DestroyGameAndServer()
{
	if (GameData.Exist())
	{
		CLIENT.Destroy();
		INVENTORY.Destroy();
		PLAYERCONTROLLER.Destroy();
		COpenAL::Destroy();
		CClientSubWorld::ClientSubWorldMemoryManager.Destroy();
		GameData.Release();
	}

	StopServer();
	CSubWorld::SubWorldMemoryManager.Destroy();
}

void CApplicationModule::Version (CString& Version)
{
#if RELEASE == TRUE
	Version.Print ("%s", VERSION);
#else

	char Device = DEVICE_CHARACTER;
	char Os = OS_CHARACTER;

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	char Date[256] = "";
	strftime (Date, 255, "%d.%m.%y", timeinfo);
	Version.Print ("%sdev_%c%c_%s", VERSION, Device, Os, Date);

#endif
}

void CApplicationModule::LaunchServerIfNeeded()
{
	if (IsGameMode_)
		return;
	
	int Time = TimeMS();

	CString WorldName ("World");
	APPMODULE.StartServer (WorldName);

	LOG ("%dms - Server initialized", TimeMS() - Time);
}

void CApplicationModule::StartServer (CString& WorldName)
{
	ServerThreadLaunched_ = true;

	LaunchThread(_ServerThread_, (void*) WorldName.Data());
	while (!ServerThreadReady_)
		Sleep(1);
}

void CApplicationModule::StopServer()
{
	ServerThreadReady_ = false;

	if (ServerThreadLaunched_)
		while (!ServerThreadReady_)
			Sleep(1);

	ServerThreadLaunched_ = false;
}

void CApplicationModule::_ServerThread_(void* WorldName)
{
	APPMODULE.ServerThread_((const char*) WorldName);
}

void CApplicationModule::ServerThread_(const char* WorldName)
{
	char MapDirectory[MAX_PATH] = "";
	sprintf(MapDirectory, "Server/Worlds/%s", WorldName);	
	CFileSystem::CreateDir(MapDirectory);
	SERVER.Init(WorldName, MapDirectory, Port_, Port_ + 1, Port_);

	ServerThreadReady_ = true;

	int Time = TimeMS();
	while (ServerThreadReady_)
	{
		int NewTime = TimeMS();
		SERVER.Step ((!IsGameMode_ || !APPLICATION->IsMenu()) ? (NewTime - Time) : 0);
		Time = NewTime;
		Sleep(10);
	}
	SERVER.Destroy();
	ServerThreadReady_ = true;
}

void CApplicationModule::UpdateFPS (int Time)
{
	Frames_++;
	Time_ += Time;
	if (Time_ > 1000.0f)
	{
		Time_ = 0;
		FPS_ = Frames_;
		Frames_ = 0;
	}
}

int CApplicationModule::FPS()
{
	return FPS_;
}

void CApplicationModule::TakeScreenshot()
{
	time_t Seconds = time (NULL);
	tm* TimeInfo = localtime (&Seconds);
	char FileName[256] = "";
	strftime (FileName, 255, "Screenshots/Screenshot [%d-%m-%y %H.%M.%S].png", TimeInfo);

	SIVector2D ImageSize = APPLICATION->GetWindowSize();
	byte* Data = new byte[4 * ImageSize.x * ImageSize.y];
	
	glPixelStorei (GL_PACK_ALIGNMENT, 1);
	glReadPixels (0, 0, ImageSize.x, ImageSize.y, GL_RGB, GL_UNSIGNED_BYTE, Data);

	CPngImage::Instance.SaveToFile (FileName, Data, ImageSize, CPNGIMAGE_RGB);
	
	strftime (FileName, 255, "Screenshot [%d-%m-%y %H.%M.%S].png", TimeInfo);
	LOG ("Screenshot saved with name \"%s\"", FileName);
	
	delete[] Data;
}

