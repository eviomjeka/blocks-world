﻿#include "CApplication.h"

CApplicationModule CApplication::ApplicationModule;
CApplication* CApplication::Instance = NULL;

const float CApplication::GUI_SIZE_X		 = 100.0f;
const float CApplication::MIN_GUI_SIZE_Y	 = 65.0f;
const int CApplication::WINDOW_WIDTH		 = 800;
const int CApplication::WINDOW_HEIGHT		 = 600;
const int CApplication::SERVER_WINDOW_WIDTH	 = 700;
const int CApplication::SERVER_WINDOW_HEIGHT = 500;
const int CApplication::MIN_WINDOW_WIDTH	 = 700;
const int CApplication::MIN_WINDOW_HEIGHT	 = 500;

CApplication::CApplication() :
	IsGameMode_(true),
	IsMenu_(true),
	LoadingGame_(false),
	TakeScreenshot_(false),
	GuiSizeY_(-1),
	WindowSizeCoef_(-1),
	PrevousCursorPos_(-1),
	GuiShift_(-1),
	OrthoGuiSizeX_(-1),
	KeyToMenu_(false)
{}

CApplication::~CApplication() {}

void CApplication::Init (bool IsGameMode, int Port)
{
	Instance = this;
	APPMODULE.SetIsGameMode (IsGameMode, Port);
	IsGameMode_ = IsGameMode;
	APPMODULE.PrepareDirectories();
	APPMODULE.InitLog();

	int Time = TimeMS();
	if (IsGameMode_)
		SETTINGS.Load();

	SetOpenGLParameters_();

	CreateWindow (SETTINGS.RunFullscreen, IsGameMode_ ? SIVector2D (WINDOW_WIDTH, WINDOW_HEIGHT) : 
				  SIVector2D (SERVER_WINDOW_WIDTH, SERVER_WINDOW_HEIGHT), SIVector2D (MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));

	PrintOpenGLInformation_();

	APPMODULE.Init();
	LOG ("%dms - Window created", TimeMS() - Time); Time = TimeMS();

	CShaders::Init();
	LOG ("%dms - Shader programs initialized", TimeMS() - Time); Time = TimeMS();

	FONT.Init();
	CLocalisation::Init ("Russian");
	LOG ("%dms - Font and localisation initialized", TimeMS() - Time); Time = TimeMS();

	InitOther_();
	LOG ("%dms - Other initialized", TimeMS() - Time);

	APPMODULE.LaunchServerIfNeeded();
	LOG ("APPLICATION INITIALIZED");
}

void CApplication::InitOther_()
{
	GUIRENDERER.Init();
	Menu_.Init (IsGameMode_);
	APPMODULE.InitOther();
}

void CApplication::Destroy()
{
	LOG ("DESTROYING APPLICATION");

	APPMODULE.DestroyGameAndServer();

	GUIRENDERER.Destroy();
	FONT.Destroy();
	CShaders::Destroy();

	LOG ("APPLICATION DESTROYED");
	CLog::Instance.Destroy();
}

//==============================================================================

void CApplication::Proc (int Time)
{
	APPMODULE.UpdateFPS (Time);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (IsGameMode_ && APPMODULE.GameData.Data() && CLIENT.Initialized())
	{
		CLIENT.Step();
		PLAYERCONTROLLER.Proc (IsMenu_ ? 0 : Time, (!IsMenu_ || (IsMenu_ && LoadingGame_)));
	}
	if (LoadingGame_ && CLIENT.MapLoader().GameStarted())
	{
		LoadingGame_ = false;
		ResumeGame();
	}

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	GUIRENDERER.Translate (GuiShift_, 0.0f, 0.0f);
	GUIRENDERER.Begin();
	if (IsMenu_)
		Menu_.Render (Time);
	else
	{
		CHATWINDOW.Render (Time);
		INVENTORY.Render (Time);
	}
	FPSCounter_.Render (APPMODULE.FPS());
	GUIRENDERER.End();
	GUIRENDERER.Render();
	glDisable (GL_BLEND);

	if (TakeScreenshot_)
	{
		APPMODULE.TakeScreenshot();
		TakeScreenshot_ = false;
	}
}

void CApplication::KeyDown (unsigned Key)
{
	EventDown_ (Key);
}

void CApplication::KeyUp (unsigned Key)
{
	EventUp_ (Key);
}

void CApplication::LeftButtonDown()
{
	if (!IsMenu_)
		EventDown_ (MOUSE_LEFT_BUTTON);
	else
		Menu_.LeftButtonDown();
}

void CApplication::LeftButtonUp()
{
	if (IsMenu_)
		Menu_.LeftButtonUp();
	else
		EventUp_ (MOUSE_LEFT_BUTTON);
}

void CApplication::RightButtonDown()
{
	if (!IsMenu_)
		EventDown_ (MOUSE_RIGHT_BUTTON);
	else
		Menu_.RightButtonDown();
}

void CApplication::RightButtonUp()
{
	if (IsMenu_)
		Menu_.RightButtonUp();
	else
		EventUp_ (MOUSE_RIGHT_BUTTON);
}

void CApplication::MouseWhell (int Delta)
{
	if (IsMenu_)
		Menu_.MouseWhell (Delta);
	else
	{
		if (Delta > 0)
		{
			for (int i = 0; i < Delta; i++)
				EventDown_ (MOUSE_WHELL_UP);
		}
		else
		{
			for (int i = 0; i < -Delta; i++)
				EventDown_ (MOUSE_WHELL_DOWN);
		}
	}
}

void CApplication::EventDown_ (unsigned Key)
{
	if (!IsGameMode_)
	{
		Menu_.KeyDown (Key);
		return;
	}

	if (Key == SETTINGS.KeyScreenshot)
		TakeScreenshot_ = true;
	else if (KeyToMenu_)
		Menu_.KeyDown (Key);
	else if (!IsMenu_ && Key == KEY_ESCAPE && !CHATWINDOW.IsOpened() && !INVENTORY.IsOpened())
	{
		INVENTORY.OpenClose (false);
		CHATWINDOW.Close (false);
		PLAYERCONTROLLER.Stop();
		ToMenuMode_();
		Menu_.GameMenu();
		Menu_.MouseMotion (CursorPosToGUIPos_ (PrevousCursorPos_));
	}
	else if (Key == KEY_F11)
	{
		FullscreenMode (!IsFullScreen());
	}
	else if (!IsMenu_ && 
			 ((CHATWINDOW.IsOpened() && Key != MOUSE_LEFT_BUTTON && Key != MOUSE_RIGHT_BUTTON && 
			  Key != MOUSE_MIDDLE_BUTTON && Key != KEY_ESCAPE) || 
			  (!CHATWINDOW.IsOpened() && !INVENTORY.IsOpened() && Key == KEY_UP)))
	{
		if (Key != MOUSE_WHELL_UP && Key != MOUSE_WHELL_DOWN)
			CHATWINDOW.KeyDown (Key);
		else
			CHATWINDOW.MouseWhell (Key == MOUSE_WHELL_UP ? -1 : 1);
	}
	else if (!IsMenu_ && Key == SETTINGS.KeyInventory)
	{
		INVENTORY.OpenClose (!INVENTORY.IsOpened());
		if (INVENTORY.IsOpened())
			PLAYERCONTROLLER.Stop();
		PLAYERCONTROLLER.Stop();
		if (INVENTORY.IsOpened())
			ToMenuMode_ (false);
		else
			ToGameMode_ (false);
	}
	else if (!IsMenu_ && INVENTORY.IsOpened())
	{
		if (Key == MOUSE_LEFT_BUTTON)
			INVENTORY.LeftButtonDown();
	}
	else if (!IsMenu_ && !INVENTORY.IsOpened() && Key == SETTINGS.KeyChat)
	{
		CHATWINDOW.Open();
		PLAYERCONTROLLER.Stop();
	}
	else if (IsMenu_)							Menu_.KeyDown (Key);
	else if (Key == SETTINGS.KeyAddBlock)		PLAYERCONTROLLER.InteractWithBlock();
	else if (Key == SETTINGS.KeyRemoveBlock)	PLAYERCONTROLLER.RemoveBlock();
	else if (Key == SETTINGS.KeyNextBlock)		INVENTORY.Right();
	else if (Key == SETTINGS.KeyPrevBlock)		INVENTORY.Left();
	else if (Key == SETTINGS.KeyForward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_Z, true);
	else if (Key == SETTINGS.KeyBackward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_Z, true);
	else if (Key == SETTINGS.KeyRightward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_X, true);
	else if (Key == SETTINGS.KeyLeftward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_X, true);
	else if (Key == KEY_E)						PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_Y, true);
	else if (Key == KEY_Q)						PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_Y, true);
	else if (Key == SETTINGS.KeyJump)			PLAYERCONTROLLER.Jump();
#define INV_NUM(n) else if (Key == KEY_ ##n)	INVENTORY.SetCurrentCell ((n == 0) ? 9 : (n - 1));
	INV_NUM (0) INV_NUM (1) INV_NUM (2) INV_NUM (3) INV_NUM (4)
	INV_NUM (5) INV_NUM (6) INV_NUM (7) INV_NUM (8) INV_NUM (9)
#undef INV_NUM
}

void CApplication::EventUp_ (unsigned Key)
{
	if (!IsGameMode_)
	{
		Menu_.KeyUp (Key);
		return;
	}
	
	if (!IsMenu_ && CHATWINDOW.IsOpened() && Key != MOUSE_LEFT_BUTTON && Key != MOUSE_RIGHT_BUTTON && 
		Key != MOUSE_MIDDLE_BUTTON && Key != MOUSE_WHELL_UP && Key != MOUSE_WHELL_DOWN && 
		Key != KEY_ESCAPE)
	{
		CHATWINDOW.KeyUp (Key);
	}
	else if (APPMODULE.GameData.Data() && INVENTORY.IsOpened() && Key == MOUSE_LEFT_BUTTON)
	{
		INVENTORY.LeftButtonUp();
	}
	else if (IsMenu_)
		Menu_.KeyUp (Key);
	else if (Key == KEY_ESCAPE)
	{
		if (CHATWINDOW.IsOpened())
			CHATWINDOW.Close();
		else
		{
			INVENTORY.OpenClose (false);
			ToGameMode_ (false);
		}
	}
	else if (Key == SETTINGS.KeyForward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_Z, false);
	else if (Key == SETTINGS.KeyBackward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_Z, false);
	else if (Key == SETTINGS.KeyRightward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_X, false);
	else if (Key == SETTINGS.KeyLeftward)		PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_X, false);
	else if (Key == KEY_E)						PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_HIGH_Y, false);
	else if (Key == KEY_Q)						PLAYERCONTROLLER.MoveDirection (MOVE_DIRECTION_LOW_Y, false);
}

void CApplication::MouseMotion (SIVector2D Position)
{
	if (IsMenu_)
	{
		Menu_.MouseMotion (CursorPosToGUIPos_ (Position));
		return;
	}
	else if (INVENTORY.IsOpened())
	{
		INVENTORY.MouseMotion (CursorPosToGUIPos_ (Position));
		return;
	}

	PLAYERCONTROLLER.Rotate(SFVector2D(
		(float) (Position.y - PrevousCursorPos_.y) * SETTINGS.MouseCoef,
		(float) (Position.x - PrevousCursorPos_.x) * SETTINGS.MouseCoef));

	PrevousCursorPos_.x = Position.x;
	PrevousCursorPos_.y = Position.y;

	if (PrevousCursorPos_.x < 80)
	{
		PrevousCursorPos_.x = GetWindowSize().x / 2;
		SetCursorPosition (PrevousCursorPos_);
	}
	if (PrevousCursorPos_.x > GetWindowSize().x - 80)
	{
		PrevousCursorPos_.x = GetWindowSize().x / 2;
		SetCursorPosition (PrevousCursorPos_);
	}
	if (PrevousCursorPos_.y < 80)
	{
		PrevousCursorPos_.y = GetWindowSize().y / 2;
		SetCursorPosition (PrevousCursorPos_);
	}
	if (PrevousCursorPos_.y > GetWindowSize().y - 80)
	{
		PrevousCursorPos_.y = GetWindowSize().y / 2;
		SetCursorPosition (PrevousCursorPos_);
	}
}

void CApplication::Resize()
{
	GuiSizeY_ = GUI_SIZE_X * (GetWindowSize().y / (float) GetWindowSize().x);
	PrevousCursorPos_ = GetWindowSize() / 2;

	WindowSizeCoef_ = GetWindowSize().x / (float) GetWindowSize().y;
	
	glViewport (0, 0, GetWindowSize().x, GetWindowSize().y);

	GuiShift_ = (GuiSizeY_ < MIN_GUI_SIZE_Y) ?
				 (-(GUI_SIZE_X - GUI_SIZE_X * MIN_GUI_SIZE_Y / GuiSizeY_) / 2.0f) : 0;
	OrthoGuiSizeX_ = (GuiSizeY_ < MIN_GUI_SIZE_Y) ? (GUI_SIZE_X * MIN_GUI_SIZE_Y / GuiSizeY_) : GUI_SIZE_X;

	if (GuiSizeY_ < MIN_GUI_SIZE_Y)
		GuiSizeY_ = MIN_GUI_SIZE_Y;

	Menu_.Resize (GuiShift_);

	GUIRENDERER.Resize (OrthoGuiSizeX_);
	MATRIXENGINE.ResetProjectionMatrix();
	MATRIXENGINE.Perspective (70.0f, WindowSizeCoef_, 0.05f, 2500.0f);
}

void CApplication::Unfocus()
{
	if (IsMenu_)
	{
		Menu_.FocusLost();
		return;
	}

	if (IsGameMode_)
	{
		INVENTORY.OpenClose (false);
		CHATWINDOW.Close (false);
		PLAYERCONTROLLER.Stop();
		ToMenuMode_();
		Menu_.GameMenu();
	}
}

//==============================================================================

void CApplication::StartGame (bool/*TODO: fix NewWorld*/, CString& WorldName)
{
	assert (!APPMODULE.GameData.Data());

	COpenAL::Init();

	CClientSubWorld::ClientSubWorldMemoryManager.Init();

	CSingle* Single = new CSingle();

	APPMODULE.GameData(new CGameData);
	APPMODULE.GameData->Game = Single;
	PLAYERCONTROLLER.Init (APPMODULE.GameData->Game);
	Single->Init(WorldName.Data());

	LoadingGame_ = true;
}

void CApplication::ConnectToServer (const char* IP, unsigned short Port)
{
	assert (!APPMODULE.GameData.Data());

	COpenAL::Init();

	CClientSubWorld::ClientSubWorldMemoryManager.Init();

	CClient* Client = new CClient();

	APPMODULE.GameData(new CGameData);
	APPMODULE.GameData->Game = Client;
	PLAYERCONTROLLER.Init (APPMODULE.GameData->Game);
	Client->Init(IP, Port);

	LoadingGame_ = true;
}

void CApplication::ResumeGame()
{
	ToGameMode_();
}

bool CApplication::IsMenu()
{
	return IsMenu_;
}

void CApplication::KeyToMenu (bool ToMenu)
{
	KeyToMenu_ = ToMenu;
}

float CApplication::GetGuiShift()
{
	return GuiShift_;
}

float CApplication::GetGuiSizeY()
{
	return GuiSizeY_;
}

float CApplication::GetFullSizeX()
{
	return OrthoGuiSizeX_;
}

float CApplication::GetWindowSizeCoef()
{
	return WindowSizeCoef_;
}

void CApplication::ToMenuMode_ (bool ChangeIsMenu)
{
	ShowCursor (true);

	SetCursorPosition (SIVector2D (GetWindowSize().x / 2, GetWindowSize().y / 2));

	if (ChangeIsMenu)
		IsMenu_ = true;
}

void CApplication::ToGameMode_ (bool ChangeIsMenu)
{
	ShowCursor (false);

	PrevousCursorPos_.x = GetWindowSize().x / 2;
	PrevousCursorPos_.y = GetWindowSize().y / 2;
	SetCursorPosition (PrevousCursorPos_);

	if (ChangeIsMenu)
		IsMenu_ = false;
}

void CApplication::SetOpenGLParameters_()
{
	SetRGBA();
	SetMinimumColorSize(8, 8, 8, 8);
	SetDoubleBuffer();
	SetMinimumDepthSize(16);

	RequestMSAA(4);

}

void CApplication::PrintOpenGLInformation_()
{
	LOG ("OpenGL information:");
	LOG ("Vendor: %s", glGetString (GL_VENDOR));
	LOG ("Renderer: %s", glGetString (GL_RENDERER));
	LOG ("OpenGL version: %s", glGetString (GL_VERSION));
	LOG ("GLSL version: %s", glGetString (GL_SHADING_LANGUAGE_VERSION));
	LOG ("Extensions: %s", glGetString (GL_EXTENSIONS));

	glassert();
}

SFVector2D CApplication::CursorPosToGUIPos_ (SIVector2D Position)
{
	if (GuiSizeY_ == MIN_GUI_SIZE_Y)
		return SFVector2D (Position.x * GUI_SIZE_Y / (float) GetWindowSize().y - GuiShift_,
						   Position.y * GUI_SIZE_Y / (float) GetWindowSize().y);
	else
		return SFVector2D (Position.x * GUI_SIZE_X / (float) GetWindowSize().x,
						   Position.y * GUI_SIZE_X / (float) GetWindowSize().x);
}
