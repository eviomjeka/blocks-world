﻿#ifndef CCIRCULARARRAY2D
#define CCIRCULARARRAY2D

#include <Error.h>
#include "../Utilities/SVector2D.h"
#include "../Utilities/Misc.h"

template <typename T>
class CCircularArray2D
{
public:

	CCircularArray2D(const CCircularArray2D<T>& Other);
	CCircularArray2D<T>& operator = (const CCircularArray2D<T>& Other);

	CCircularArray2D();
	CCircularArray2D (SIVector2D Dimentions);
	~CCircularArray2D();
	
	T& operator () (int x, int y);
	T& operator () (SIVector2D Position);
	T& operator [] (int i);
	
	void Resize (SIVector2D Dimentions);
	int Size();
	
private:

	T* Data_;
	SIVector2D Dimentions_;

};

template<typename T>
CCircularArray2D<T>::CCircularArray2D() :
	Data_ (NULL),
	Dimentions_ (0)
{
}

template<typename T>
CCircularArray2D<T>::CCircularArray2D (SIVector2D Dimentions) :
	Data_ (NULL),
	Dimentions_ (Dimentions)
{
	assert (Dimentions_.x > 0 && Dimentions_.y > 0);
	Data_ = new T[Dimentions_.x * Dimentions_.y];
	assert (Data_);
}

template<typename T>
CCircularArray2D<T>::~CCircularArray2D()
{
	delete[] Data_;
	Data_ = NULL;
}

template<typename T>
T& CCircularArray2D<T>::operator () (int x, int y)
{
	__assert(Data_);
	return Data_[Mod(y, Dimentions_.y) * Dimentions_.x +
				 Mod(x, Dimentions_.x)];
}

template<typename T>
T& CCircularArray2D<T>::operator () (SIVector2D Position)
{
	return (*this) (Position.x, Position.y);
}

template<typename T>
T& CCircularArray2D<T>::operator [] (int i)
{
	__assert(i >= 0 && i < Dimentions_.x * Dimentions_.y);
	return Data_[i];
}

template<typename T>
void CCircularArray2D<T>::Resize (SIVector2D Dimentions)
{
	assert (Dimentions.x > 0 && Dimentions.y > 0);
	if (Data_)
		delete[] Data_;

	Dimentions_ = Dimentions;
	Data_ = new T[Dimentions_.x * Dimentions_.y];
	assert (Data_);
}

template<typename T>
int CCircularArray2D<T>::Size()
{
	return Dimentions_.x * Dimentions_.y;
}

#endif
