﻿#ifndef CLIST
#define CLIST

#include <Error.h>

template<typename T>
class CList
{
private:

	struct tElement
	{
		tElement* volatile	Prev;
		tElement* volatile	Next;
		T			Data;
		
		tElement();
		tElement(tElement* aPrev, tElement* aNext);
		~tElement();
		
	};
	
	tElement End_;
	
public:
	
	class tIterator
	{
	private:
		tElement* volatile Element_;
	
	public:
		
		tIterator();
		tIterator(tElement* Element);
		tElement* Element();

		tIterator operator ++();
		tIterator operator --();
		tIterator operator ++(int);
		tIterator operator --(int);

		T& operator ()();
		bool operator !=(tIterator aOther);
		bool operator ==(tIterator aOther);
	};

	CList(const CList<T>& Other);
	CList<T>& operator = (const CList<T>& Other);
	
	CList();
	~CList();

	tIterator Find(int Index);

	tIterator InsertBefore(tIterator Iter);
	tIterator InsertAfter(tIterator Iter);
	tIterator Insert();

	void Remove(tIterator Iter);
	void Clear();
	
	tIterator End();
	tIterator First();
	tIterator Last();

};

template<typename T>
CList<T>::tElement::tElement() :
Prev(this),
Next(this)
{
}

template<typename T>
CList<T>::tElement::tElement(tElement* aPrev, tElement* aNext) :
	Prev(aPrev),
	Next(aNext)
{
	__assert(Prev);
	__assert(Next);

	Prev->Next = this;
	Next->Prev = this;
}

template<typename T>
CList<T>::tElement::~tElement()
{
	__assert(this);
	__assert(Prev);
	__assert(Next);

	Prev->Next = Next;
	Next->Prev = Prev;
	Prev = NULL;
	Next = NULL;
}

template<typename T>
CList<T>::tIterator::tIterator() : 
	Element_ (NULL)
{}

template<typename T>
CList<T>::tIterator::tIterator(tElement* Element) : 
	Element_(Element)
{
}

template<typename T>
typename CList<T>::tElement* CList<T>::tIterator::Element()
{
	return Element_;
}

template<typename T>
typename CList<T>::tIterator CList<T>::tIterator::operator ++()
{
	assert(Element_->Next);
	Element_ = Element_->Next;
	return *this;
} 

template<typename T>
typename CList<T>::tIterator CList<T>::tIterator::operator --()
{
	assert(Element_->Prev);
	Element_ = Element_->Prev;
	return *this;
}

template<typename T>
typename CList<T>::tIterator CList<T>::tIterator::operator ++(int)
{
	tIterator Old = *this;
	++(*this);
	return Old;
}

template<typename T>
typename CList<T>::tIterator CList<T>::tIterator::operator --(int)
{
	CList<T>::tIterator Old = *this;
	--(*this);
	return Old;
}

template<typename T>
T& CList<T>::tIterator::operator ()()
{
	return Element_->Data;
}

template<typename T>
bool CList<T>::tIterator::operator !=(tIterator aOther)
{
	return Element_ != aOther.Element_;
}

template<typename T>
bool CList<T>::tIterator::operator ==(tIterator aOther)
{
	return Element_ == aOther.Element_;
}

template<typename T>
CList<T>::CList()
{
}

template<typename T>
CList<T>::~CList() 
{
	Clear();
}

template<typename T>
typename CList<T>::tIterator CList<T>::Find(int Index)
{
	tIterator i = First();
	for (int c = 0; c < Index && i != End(); c++)
		++i;
	return i;
}

template<typename T>
typename CList<T>::tIterator CList<T>::InsertBefore(tIterator Iter)
{
	tElement* Next = Iter.Element();
	tElement* Prev = Next->Prev;
	tElement* New  = new tElement(Prev, Next);
	return tIterator(New);
}

template<typename T>
typename CList<T>::tIterator CList<T>::InsertAfter(tIterator Iter)
{
	tElement* Prev = Iter.Element();
	tElement* Next = Prev->Next;
	tElement* New  = new tElement(Prev, Next);
	return tIterator(New);
}

template<typename T>
typename CList<T>::tIterator CList<T>::Insert()
{
	return InsertBefore(End());
}

template<typename T>
void CList<T>::Remove(tIterator Iter)
{
	assert(Iter != End());
	delete (Iter.Element());
}

template<typename T>
void CList<T>::Clear()
{
	while (First() != End())
		Remove(First());
}

template<typename T>
typename CList<T>::tIterator CList<T>::End()
{
	return tIterator(&End_);
}

template<typename T>
typename CList<T>::tIterator CList<T>::First()
{
	return tIterator(End_.Next);
}

template<typename T>
typename CList<T>::tIterator CList<T>::Last()
{
	return tIterator(End_.Prev);
}

#endif
