﻿#ifndef CSARRAY
#define CSARRAY

#include <Error.h>
#include "../Utilities/CStream.h"

template<typename T, int N>
class CSArray
{
public:
	
	//CSArray(const CSArray<T, N>& Other);
	//CSArray<T, N>& operator = (const CSArray<T, N>& Other);

	CSArray();
	~CSArray();

	T& operator [] (unsigned i);
	int Size();
	
private:

	T Data_[N];

};

template<typename T, int N>
CSArray<T,N>::CSArray()
{
}

template<typename T, int N>
CSArray<T,N>::~CSArray()
{
}

template<typename T, int N>
T& CSArray<T,N>::operator[](unsigned i)
{
	__assert(i >= 0 && i < N);
	return Data_[i];
}

template<typename T, int N>
int CSArray<T,N>::Size()
{
	return N;
}

template<typename T, int N>
CStream& operator <<(CStream& Stream, CSArray<T, N>& Array)
{
	int Size = N;
	Stream << Size;
	for (int i = 0; i < N; i++)
		Stream << Array[i];

	return Stream;
}

template<typename T, int N>
CStream& operator >>(CStream& Stream, CSArray<T, N>& Array)
{
	int Size = 0;
	Stream >> Size;
	assert (N == Size);
	for (int i = 0; i < N; i++)
		Stream >> Array[i];

	return Stream;
}

#endif