﻿#ifndef CSVECTORARRAY3D
#define CSVECTORARRAY3D

#include <Error.h>
#include "../Utilities/SVector.h"

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX = 0, int PositionY = 0, int PositionZ = 0>
class CSVectorArray3D
{
public:

	CSVectorArray3D(const CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>& Other);
	CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>& operator = 
		(const CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>& Other);

	CSVectorArray3D();
	~CSVectorArray3D();
	
	T& operator () (int x, int y, int z);
	T& operator () (SIVector Position);
	T& operator [] (int i);

	void Clear();
	int Size();

	void Read (CStream& Stream);
	void Write (CStream& Stream);
	
private:

	T Data_[SizeX * SizeY * SizeZ];

};

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::CSVectorArray3D()
{}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::~CSVectorArray3D() {}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
T& CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::operator () (int x, int y, int z)
{
	__assert (x >= PositionX && x - PositionX < SizeX);
	__assert (y >= PositionY && y - PositionY < SizeY);
	__assert (z >= PositionZ && z - PositionZ < SizeZ);

	return Data_[(z - PositionZ) * SizeX * SizeY +
					(y - PositionY) * SizeX +
					(x - PositionX)];
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
T& CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::operator () (SIVector Position)
{
	return (*this) (Position.x, Position.y, Position.z);
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
T& CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::operator [] (int i)
{
	__assert (i >= 0 && i < SizeX * SizeY * SizeZ);
	return Data_[i];
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
void CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::Clear()
{
	for (int i = 0; i < SizeX * SizeY * SizeZ; i++)
		Data_[i] = T();
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
int CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::Size()
{
	return SizeX * SizeY * SizeZ;
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
void CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::Read (CStream& Stream)
{
	Stream.Read (Data_, sizeof (T) * Size());
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
void CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>::Write (CStream& Stream)
{
	Stream.Write (Data_, sizeof (T) * Size());
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
CStream& operator >>(CStream& Stream, 
	CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>& Array)
{
	for (int i = 0; i < SizeX * SizeY * SizeZ; i++)
		Stream >> Array[i];
	return Stream;
}

template <typename T, int SizeX, int SizeY, int SizeZ, 
	int PositionX, int PositionY, int PositionZ>
CStream& operator <<(CStream& Stream, 
	CSVectorArray3D<T, SizeX, SizeY, SizeZ, PositionX, PositionY, PositionZ>& Array)
{
	for (int i = 0; i < SizeX * SizeY * SizeZ; i++)
		Stream << Array[i];
	return Stream;
}

#endif