﻿#ifndef CQUEUE
#define CQUEUE

#include <Error.h>

template<typename T>
class CQueue
{
private:

	int Head_;
	int Tail_;
	int Size_;
	T*  Data_;

public:
	
	CQueue operator = (CQueue&);
	CQueue (CQueue&);

	CQueue (int Size = 0);
	~CQueue();

	void Resize (int Size);
	void Clear();
	void Push (T Element);
	T	 Pop();
	bool Full();
	bool Empty();
	T& Tail();
	T& Head();
	int TailElement();
	int HeadElement();

};

template<typename T>
CQueue<T>::CQueue (int Size)
	: Head_ (0), Tail_ (0), Size_ (Size + 1), Data_ (NULL)
{
	assert (Size >= 0);
	if (Size > 0)
	{
		Data_ = new T[Size + 1];
		assert (Data_);
	}
}

template<typename T>
CQueue<T>::~CQueue()
{
	delete[] Data_;
	Data_ = NULL;
}

template<typename T>
void CQueue<T>::Resize (int Size)
{
	if (Size_ == Size + 1)
		return;

	delete[] Data_;
	Data_ = Size ? new T[Size + 1] : 0;
	assert (Size == 0 || Data_);

	Size_ = Size + 1;
	Clear();
}

template<typename T>
void CQueue<T>::Clear()
{
	Tail_ = 0;
	Head_ = 0;
}

template<typename T>
void CQueue<T>::Push (T Element)
{
	__assert (!Full());

	Data_[Tail_] = Element;
	Tail_ = (Tail_ + 1) % Size_;
}

template<typename T>
T CQueue<T>::Pop()
{
	__assert (!Empty());

	T Element = Data_[Head_];
	Head_ = (Head_ + 1) % Size_;
	return Element;
}

template<typename T>
bool CQueue<T>::Full()
{
	return (Tail_ + 1) % Size_ == Head_;
}

template<typename T>
bool CQueue<T>::Empty()
{
	return Tail_ == Head_;
}

template<typename T>
T& CQueue<T>::Tail()
{
	__assert(!Empty());
	return Data_[Mod (Tail_ - 1, Size_)];
}

template<typename T>
T& CQueue<T>::Head()
{
	__assert(!Empty());
	return Data_[Head_];
}

template<typename T>
int CQueue<T>::TailElement()
{
	return Tail_;
}

template<typename T>
int CQueue<T>::HeadElement()
{
	return Head_;
}

#endif
