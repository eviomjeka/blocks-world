﻿#ifndef CCIRCULARARRAY3D
#define CCIRCULARARRAY3D

#include <Error.h>
#include "../Utilities/SVector.h"
#include "../Utilities/Misc.h"

template <typename T>
class CCircularArray3D
{
public:
	
	CCircularArray3D(const CCircularArray3D<T>& Other);
	CCircularArray3D<T>& operator = (const CCircularArray3D<T>& Other);
	
	CCircularArray3D();
	CCircularArray3D (SIVector Dimentions);
	~CCircularArray3D();
	
	T& operator () (int x, int y, int z);
	T& operator () (SIVector Position);
	T& operator [] (int i);

	void Resize (SIVector Dimentions);
	int Size();
	
private:

	T* Data_;
	SIVector Dimentions_;

};

template<typename T>
CCircularArray3D<T>::CCircularArray3D() :
	Data_ (NULL),
	Dimentions_ (0)
{
}

template<typename T>
CCircularArray3D<T>::CCircularArray3D (SIVector Dimentions) :
	Data_ (NULL),
	Dimentions_ (Dimentions)
{
	assert (Dimentions_.x > 0 && Dimentions_.y > 0 && Dimentions_.z > 0);
	Data_ = new T[Dimentions_.x * Dimentions_.y * Dimentions_.z];
	assert (Data_);
}

template<typename T>
CCircularArray3D<T>::~CCircularArray3D()
{
	delete[] Data_;
	Data_ = NULL;
}

template<typename T>
T& CCircularArray3D<T>::operator () (int x, int y, int z)
{
	return Data_[(Mod(z, Dimentions_.z)) * Dimentions_.x * Dimentions_.y +
				  Mod(y, Dimentions_.y)  * Dimentions_.x +
				  Mod(x, Dimentions_.x)];
}

template<typename T>
T& CCircularArray3D<T>::operator () (SIVector Position)
{
	__assert (Data_);
	return (*this) (Position.x, Position.y, Position.z);
}

template<typename T>
T& CCircularArray3D<T>::operator [] (int i)
{
	__assert(i >= 0 && i < Dimentions_.x * Dimentions_.y * Dimentions_.z);
	return Data_[i];
}

template<typename T>
void CCircularArray3D<T>::Resize (SIVector Dimentions)
{
	assert (Dimentions.x > 0 && Dimentions.y > 0 && Dimentions.z > 0);
	if (Data_)
		delete[] Data_;

	Dimentions_ = Dimentions;
	Data_ = new T[Dimentions_.x * Dimentions_.y * Dimentions_.z];
	assert (Data_);
}

template<typename T>
int CCircularArray3D<T>::Size()
{
	return Dimentions_.x * Dimentions_.y * Dimentions_.z;
}

#endif // CCIRCULARARRAY3D
