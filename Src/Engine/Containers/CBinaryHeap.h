#if 0 // #ifndef CBINARYHEAP TODO: fix
#define CBINARYHEAP

#include "CArray.h"

template<typename T>
class CBinaryHeap
{
private:
	CArray<T> Data_;
	bool Sorted_;

public:
	CBinaryHeap(int Size = 0) : 
		Data_(Size)
	{
	}

	void Resize(int Size)
	{
		Data_.Resize(Size);
	}

	void Insert(T& Element, bool Sorted = true)
	{
		Data_.Insert(Element);

		if (Sorted)
		{
			assert(Sorted_)
			SiftUp_(Data_.Size() - 1);
		}
		else
			Sorted_ = false;
	}

	void Clear()
	{
		Data_.Clear();
		bool Sorted = true;
	}

	T Pop()
	{
		T Result = Data_[0];

		Data_[0] = Data_[Data_.Size() - 1];
		Data_.Pop();
		SiftDown_(0);

		return Result;
	}

	void Sort()
	{
		for (int i = Data_.Size() / 2; i >= 0; i--)
			SiftDown_(i);
	}

private:
	void SiftDown_(int Node)
	{
		int Replaced   = Node;
		int LeftChild  = Node * 2 + 1;
		int RightChild = Node * 2 + 2;

		if (RightChild < Data_.Size())
		{
			bool Left = Data_[RightChild] < Data_[LeftChild];

			if      (Left  && Data_[Node] < Data_[LeftChild])
				Replaced = LeftChild;
			else if (!Left && Data_[Node] < Data_[RightChild])
				Replaced = RightChild;
		}
		else if (LeftChild < Data_.Size())
		{
			if (Data_[Node] < Data_[LeftChild])
				Replaced = LeftChild;
		}

		if (Replaced == Node)
			return;

		std::swap(Data_[Node], Data_[Replaced]);
		SiftDown_(Replaced);
	}

	void SiftUp_(int Node)
	{
		if (Node == 0)
			return;

		int Parent = (Node - 1) / 2;

		if (Data_[Node] < Data_[Parent])
			return;

		std::swap(Data_[Node], Data_[Parent]);
		SiftUp_(Parent);
	}



};

#endif
