﻿#ifndef CSVECTORARRAY2D
#define CSVECTORARRAY2D

#include <Error.h>
#include "../Utilities/SVector2D.h"

template <typename T, int SizeX, int SizeY, int PositionX = 0, int PositionY = 0>
class CSVectorArray2D
{
public:
	
	//CSVectorArray2D(const CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>& Other);
	//CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>& operator = 
	//	(const CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>& Other);

	CSVectorArray2D();
	~CSVectorArray2D();
	
	T& operator () (int x, int y);
	T& operator () (SIVector2D Position);
	T& operator [] (int i);

	void Clear();
	int Size();

	void Read (CStream& Stream);
	void Write (CStream& Stream);
	
private:

	T Data_[SizeX * SizeY];

};

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::CSVectorArray2D()
{}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::~CSVectorArray2D()
{}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
T& CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::operator () (int x, int y)
{
	__assert (x >= PositionX && x - PositionX < SizeX);
	__assert (y >= PositionY && y - PositionY < SizeY);

	return Data_[(y - PositionY) * SizeX +  (x - PositionX)];
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
T& CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::operator () (SIVector2D Position)
{
	return (*this) (Position.x, Position.y);
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
T& CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::operator [] (int i)
{
	__assert (i >= 0 && i < SizeX * SizeY);
	return Data_[i];
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
void CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::Clear()
{
	for (int i = 0; i < SizeX * SizeY; i++)
		Data_[i] = T();
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
int CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::Size()
{
	return SizeX * SizeY;
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
void CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::Read (CStream& Stream)
{
	Stream.Read (Data_, sizeof (T) * Size());
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
void CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>::Write (CStream& Stream)
{
	Stream.Write (Data_, sizeof (T) * Size());
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
CStream& operator << (CStream& Stream, CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>& Array)
{
	for (int i = 0; i < Array.Size(); i++)
		Stream << Array[i];

	return Stream;
}

template <typename T, int SizeX, int SizeY, int PositionX, int PositionY>
CStream& operator >> (CStream& Stream, CSVectorArray2D<T, SizeX, SizeY, PositionX, PositionY>& Array)
{
	for (int i = 0; i < Array.Size(); i++)
		Stream >> Array[i];

	return Stream;
}

#endif