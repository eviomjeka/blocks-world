#ifndef CCACHE
#define CCACHE

#include "CHashTable.h"
#include "CList.h"
#include <CMutex.h>
#include "../Utilities/Misc.h"

#define WARNING_N_ROWS 1 // TODO: fix
#define CCACHE_PRIORITY_MAX 0x0fffffff

/*More precious (see allocate with Addref=0) algo for priority counting? Lists?!!!!! (ByPriority_)*/



struct __tPriority //for sorting
{
	int Index;
	int Priority;
};


/*
 * Thrad-safe implementation:
 * Master thread is allowed to call all functions.
 * Other threads are only allowed adding/removing references and getting data with false NoLock.
 */


template<typename tKey, typename tData, typename tPriorityDecider, bool ThreadSafe>
class CCache
{
public:	
	typedef __tPriority tPriority;

	struct tEntry
	{
		tData						Data;
		tKey						Key;
		int							References;
		CList<tPriority>::tIterator ListItem;
	};


protected:
	CArray<tEntry>								Data_;
	CArray<tPriority>							ByPriority_;
	CList<tPriority>							PriorityList_;
	CHashTable<tKey, int>						Map_;
	tPriorityDecider							PriorityDecider_;
	CMutex										Mutex_;


	void RecalculatePriorities_()
	{		
		if (IsThreadSafe())
			Mutex_.Lock();

		ByPriority_.Clear();
		PriorityList_.Clear();

		for (int i = 0; i < Data_.Size(); i++)
			if (Data_[i].References <= 0)
			{
				int Priority = GetPriority_(i);
				tPriority Curr = {i, Priority};
				ByPriority_.Insert(Curr);
			}

		ByPriority_.Sort();

		for (int i = 0; i < ByPriority_.Size(); i++)
		{
			Data_[ByPriority_[i].Index].ListItem   = PriorityList_.Insert();
			Data_[ByPriority_[i].Index].ListItem() = ByPriority_[i];
		}

		if (IsThreadSafe())
			Mutex_.Unlock();
	}

public:
	virtual int CalculatePriority(tKey& Key) = 0;

	CCache() :
		Map_(HashSIVector2D /* TODO: fix */, WARNING_N_ROWS)
	{
	}

	void Init(int NEntries)
	{
		Map_.Clear();
		ByPriority_.Clear();
		PriorityList_.Clear();
		Data_.Resize(NEntries);
		Data_.SetStatic();
		ByPriority_.Resize(NEntries);

		for (int i = 0; i < Data_.Size(); i++)
		{
			Data_[i].References = -1;
			Data_[i].ListItem   = PriorityList_.End();
		}
		RecalculatePriorities_();
	}

	void SetPriorityDecider(tPriorityDecider& PriorityDecider)
	{
		if (PriorityDecider != PriorityDecider_)
		{
			PriorityDecider_ = PriorityDecider;
			RecalculatePriorities_();
		}
	}

	bool IsSufficientPriority(tKey Key)
	{
		if (IsThreadSafe())
			Mutex_.Lock();

		int Priority    = CalculatePriority(Key);
		int MaxPriority = PriorityList_.Last()().Priority;
		bool Sufficient = Priority < MaxPriority;

		if (IsThreadSafe())
			Mutex_.Unlock();

		return Sufficient;
	}

	tData& Allocate(tKey Key)
	{
		if (IsThreadSafe())
			Mutex_.Lock();

		assert(!Map_.Data(Key));

		volatile tPriority Replaced = PriorityList_.Last()();
		PriorityList_.Remove(PriorityList_.Last());

		if (Data_[Replaced.Index].References != -1)
			Map_.Remove(Data_[Replaced.Index].Key);

		Data_[Replaced.Index].Key        = Key;
		Data_[Replaced.Index].References = 1;
		Data_[Replaced.Index].ListItem	 = PriorityList_.End();

		if (IsThreadSafe())
			Mutex_.Unlock();
		else
			ReportReady(&Data_[Replaced.Index].Data);

		return Data_[Replaced.Index].Data;
	}

	tData* AddReference(tKey Key, bool Lock = true)
	{
		if (IsThreadSafe() && Lock)
			Mutex_.Lock();

		int* _Index = Map_.Data(Key);

		if (!_Index)
		{
			if (IsThreadSafe() && Lock)
				Mutex_.Unlock();

			return NULL;
		}

		volatile int Index = *_Index;

		if (!Data_[Index].References)
			PriorityList_.Remove(Data_[Index].ListItem);
		Data_[Index].ListItem = PriorityList_.End();

		Data_[Index].References++;
		
		if (IsThreadSafe() && Lock)
			Mutex_.Unlock();

		return &Data_[Index].Data;
	}

	void RemoveReference(tKey Key, bool Lock = true)
	{
		if (IsThreadSafe() && Lock)
			Mutex_.Lock();

		int Index = Map_(Key, true);

		assert(Data_[Index].References-- > 0);

		if (!Data_[Index].References)
			ToList_(Index);
		
		if (IsThreadSafe() && Lock)
			Mutex_.Unlock();
	}

	tData* operator() (tKey Key, bool NoLock = true)
	{
		if (IsThreadSafe() && !NoLock)
			Mutex_.Lock();

		int* _Index = Map_.Data(Key);
		
		if (!_Index)
		{
			if (IsThreadSafe() && !NoLock)
				Mutex_.Unlock();

			return NULL;
		}

		volatile int Index = *_Index;
		
		if (IsThreadSafe() && !NoLock)
			Mutex_.Unlock();

		return &Data_[Index].Data;
	}

	CArray<tEntry>& Data()
	{
		return Data_;
	}

	void Destroy()
	{
		for (int i = 0; i < Data_.Size(); i++)
			assert(!Data_[i].References || Data_[i].References == -1);
	}

	void ReportReady(tData* Data)
	{
		if (IsThreadSafe())
			Mutex_.Lock();

		/*fuck it....*/
		int Shift = offsetof(tEntry, Data);
		byte* _Data = ((byte*) Data) - Shift;

		int i = ((tEntry*) _Data) - Data_.Data();

		Map_(Data_[i].Key) = i;	
		Data_[i].References = 0;

		ToList_(i);

		if (IsThreadSafe())
			Mutex_.Unlock();
	}

	void Unload(tData& Data)
	{
		if (IsThreadSafe())
			Mutex_.Lock();

		/*fuck it twice....*/
		int Shift = offsetof(tEntry, Data);
		byte* _Data = ((byte*) &Data) - Shift;

		int i = ((tEntry*) _Data) - Data_.Data();

		tEntry& Entry = Data_[i];

		assert(!Entry.References);

		if (Entry.ListItem != PriorityList_.End())
		{
			PriorityList_.Remove(Entry.ListItem);
			Entry.ListItem = PriorityList_.End();
			Map_.Remove(Entry.Key);
		}

		Entry.References = -1;

		ToList_(i);

		if (IsThreadSafe())
			Mutex_.Unlock();
	}

	void ToList_(int i)
	{
		int Priority = GetPriority_(i);
		CList<tPriority>::tIterator j;

		for (j = PriorityList_.Last(); j != PriorityList_.End(); j--)
			if (Priority >= j().Priority)
				break;

		CList<tPriority>::tIterator New = PriorityList_.InsertAfter(j);

		New().Index    		= i;
		New().Priority 		= Priority;
		Data_[i].ListItem 	= New;
	}

	int GetPriority_(int i)
	{
		return (Data_[i].References == -1) ? CCACHE_PRIORITY_MAX : CalculatePriority(Data_[i].Key);
	}

	bool IsThreadSafe() // VS C++ compiler C4127 and C4100 fix
	{
		return ThreadSafe;
	}


};

bool operator < (__tPriority a, __tPriority b);


#endif

