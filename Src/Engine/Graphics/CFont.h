﻿#ifndef CFONT
#define CFONT

#include "../Utilities/SVector.h"
#include "../Utilities/CString.h"
#include "../Containers/CSArray.h"
#include "../Graphics/CPngImage.h"
#include  "../MeshConstructors/CTextureMeshConstructor.h"

class CFont
{
private:

	CSArray<float, 256>			SymbolSize_;
	bool						Initialized_;
	CTextureMeshConstructor*	Current_;
	SFVector					Translate_;
	CTexture					Texture_;

public:

	CFont();
	void Init();
	void Destroy();
	~CFont();

	void SetMeshConstructor (CTextureMeshConstructor* MC);
	void Translate (SFVector TranslateVector);
	CTexture& Texture();
	void Render(const char* String, float Size, int Start = 0, int End = -1);
	void Render(CString& String, float Size, int Start = 0, int End = -1);
	float GetLength(const char* String, float Size, int Start = 0, int End = -1);
	float GetLength(CString& String, float Size, int Start = 0, int End = -1);

};

#endif