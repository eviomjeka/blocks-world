﻿#include "CTextureAtlas.h"
#include <Engine.h>

void CTextureAtlas::GetTextureAtlas (const char* Address, CTexture& Texture, SIVector2D NumTextures)
{
	byte* Data = NULL;
	SIVector2D Size = CPngImage::Instance.LoadFromFile (Address, NULL, &Data);

	assert (Size.x >= NumTextures.x && Size.y >= NumTextures.y);
	assert (Size.x % NumTextures.x == 0 && Size.y % NumTextures.y == 0);
	for (int w = Size.x; w > 1; w /= 2)
		assert(!(w % 2));
	for (int h = Size.y; h > 1; h /= 2)
		assert(!(h % 2));

	SIVector2D TexturesSize (Size.x / NumTextures.x, Size.y / NumTextures.y);
	byte* NewData = new byte[4 * Size.x * Size.y * 4];
	for (int x = 0; x < NumTextures.x; x++)
		for (int y = 0; y < NumTextures.y; y++)
		{
			for (int xx = 0; xx < TexturesSize.x; xx++)
				for (int yy = 0; yy < TexturesSize.y; yy++)
				{
					int Pos1 = ((Size.y - y*TexturesSize.y - yy - 1) * Size.x + x * TexturesSize.x + xx) * 4;
					int Pos2 = ((2*y*TexturesSize.y + yy + (int) (TexturesSize.y/2)) * 2*Size.x + 
								 2*x*TexturesSize.x + xx + (int) (TexturesSize.x/2)) * 4;

					NewData[Pos2+0] = Data[Pos1+0];
					NewData[Pos2+1] = Data[Pos1+1];
					NewData[Pos2+2] = Data[Pos1+2];
					NewData[Pos2+3] = Data[Pos1+3];
				}
				
			for (int xx = 0; xx < TexturesSize.x; xx++)
			{
				for (int i = 0; i < 2; i++)
				{
					int yy = (i == 0) ? 0 : TexturesSize.y - 1;

					int Pos1 = ((Size.y - y*TexturesSize.y - yy - 1) * Size.x + x * TexturesSize.x + xx) * 4;

					for (int j = 0; j < TexturesSize.y / 2; j++)
					{
						if (i == 0)
							yy = -j - 1;
						else
							yy = j + TexturesSize.y;
						int Pos2 = ((2*y*TexturesSize.y + yy + (int) (TexturesSize.y/2)) * 2*Size.x + 
									 2*x*TexturesSize.x + xx + (int) (TexturesSize.x/2)) * 4;

						NewData[Pos2+0] = Data[Pos1+0];
						NewData[Pos2+1] = Data[Pos1+1];
						NewData[Pos2+2] = Data[Pos1+2];
						NewData[Pos2+3] = Data[Pos1+3];
					}
				}
			}
			for (int yy = 0; yy < TexturesSize.y; yy++)
			{
				for (int i = 0; i < 2; i++)
				{
					int xx = (i == 0) ? 0 : TexturesSize.x - 1;

					int Pos1 = ((Size.y - y*TexturesSize.y - yy - 1) * Size.x + x * TexturesSize.x + xx) * 4;

					for (int j = 0; j < TexturesSize.x / 2; j++)
					{
						if (i == 0)
							xx = -j - 1;
						else
							xx = j + TexturesSize.y;
						int Pos2 = ((2*y*TexturesSize.y + yy + (int) (TexturesSize.y/2)) * 2*Size.x + 
									 2*x*TexturesSize.x + xx + (int) (TexturesSize.x/2)) * 4;

						NewData[Pos2+0] = Data[Pos1+0];
						NewData[Pos2+1] = Data[Pos1+1];
						NewData[Pos2+2] = Data[Pos1+2];
						NewData[Pos2+3] = Data[Pos1+3];
					}
				}
			}
			
			for (int i = 0; i < 4; i++)
			{
				int xx = (i % 2 == 0) ? 0 : TexturesSize.x - 1;
				int yy = (i / 2 == 0) ? 0 : TexturesSize.y - 1;
				int Pos1 = ((Size.y - y*TexturesSize.y - yy - 1) * Size.x + x * TexturesSize.x + xx) * 4;

				for (int x0 = 0; x0 < TexturesSize.x / 2; x0++)
					for (int y0 = 0; y0 < TexturesSize.y / 2; y0++)
					{
						xx = (i % 2 == 0) ? -x0 - 1 : TexturesSize.x + x0;
						yy = (i / 2 == 0) ? -y0 - 1 : TexturesSize.y + y0;

						int Pos2 = ((2*y*TexturesSize.y + yy + (int) (TexturesSize.y/2)) * 2*Size.x + 
									 2*x*TexturesSize.x + xx + (int) (TexturesSize.x/2)) * 4;

						NewData[Pos2+0] = Data[Pos1+0];
						NewData[Pos2+1] = Data[Pos1+1];
						NewData[Pos2+2] = Data[Pos1+2];
						NewData[Pos2+3] = Data[Pos1+3];
					}
			}
		}

	CPngImage::Instance.DeleteData();
	CPngImage::Instance.LoadFromData (NewData, &Texture, Size * 2, CPNGIMAGE_RGBA);
	Texture.Bind();
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	delete[] NewData;
}