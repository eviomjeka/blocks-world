﻿#include "CFont.h"

CFont::CFont() :
	Initialized_(false), 
	Current_ (NULL)
{}

void CFont::Init()
{
	assert (!Initialized_);

	Current_ = NULL;
	Translate_ = SFVector (0);
	
	byte* Data = NULL;
	SIVector2D Size = CPngImage::Instance.LoadFromFile ("Font_%d.png", &Texture_, &Data, 3);
	
	for (int i = 0; i < 256; i++)
	{
		SIVector2D TextureCoord (i % 16, i / 16);
		TextureCoord *= Size.x / 16;
		bool CharFounded = false;
		for (int j = Size.x / 16 * 15 / 16 - 1; j >= 0; j--)
		{
			for (int k = 0; k < (int) Size.x / 16; k++)
			{
				int x = TextureCoord.x + j;
				int y = TextureCoord.y + k;
				int Pos = x + Size.x * (Size.y - y - 1);
				if (Data[Pos] != 0)
				{
					CharFounded = true;
					break;
				}
			}
			if (CharFounded)
			{
				SymbolSize_[i] = (float) j + 3.0f;
				break;
			}
		}
		if (!CharFounded)
		{
			if (i == (unsigned char) ' ')
				SymbolSize_[i] = (Size.x / 16.0f - 1.0f) / 4.0f;
			else
				SymbolSize_[i] = 3.0f;
		}
		SymbolSize_[i] /= Size.x / 16.0f - 1.0f;
	}
	CPngImage::Instance.DeleteData();

	Initialized_ = true;
}

void CFont::Destroy()
{
	Texture_.Destroy();

	Initialized_ = false;
}

CFont::~CFont()
{
	assert (!Initialized_);
}

void CFont::SetMeshConstructor (CTextureMeshConstructor* MC)
{
	Current_ = MC;
}

void CFont::Translate (SFVector TranslateVector)
{
	Translate_ = TranslateVector;
}

CTexture& CFont::Texture()
{
	return Texture_;
}

void CFont::Render(const char* String, float Size, int Start, int End)
{
	assert (Initialized_);
	assert (Current_ != NULL);
	assert (Start >= 0 && (End >= 0 || End == -1));
	assert (End > Start || End == -1);

	const unsigned char* UString = (const unsigned char*) String;
	Size *= 1.3f;

	SFVector TranslateVector = Translate_;
	for (int i = Start; (End != -1 && i < End) || (End == -1 && UString[i]); i++)
	{
		SIVector2D TextureCoord (UString[i] % 16, 15 - UString[i] / 16);
		float TexShiftX = SymbolSize_[UString[i]];
		if (TexShiftX > 1.0f)
			TexShiftX = 1.0f;
		float TexShiftY = 15.0f / 16.0f;
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, 0)) / 16.0f);
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, 0)) / 16.0f);
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, TexShiftY)) / 16.0f);
		
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (SymbolSize_[UString[i]], 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (TexShiftX, TexShiftY)) / 16.0f);
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 1.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, TexShiftY)) / 16.0f);
		Current_->AddVertex (TranslateVector - SFVector (0.0f, Size * 0.3f, 0.0f) + SFVector (0.0f, 0.0f, 0.0f) * Size, 
			((SFVector2D) TextureCoord + SFVector2D (0, 0)) / 16.0f);
		
		TranslateVector.x += SymbolSize_[UString[i]] * Size;
	}
}

float CFont::GetLength(const char* String, float Size, int Start, int End)
{
	assert (Start >= 0 && (End >= 0 || End == -1));
	assert (End > Start || End == -1);
	
	const unsigned char* UString = (const unsigned char*) String;
	Size *= 1.3f;

	float Length = 0.0f;
	for (int i = Start; (End != -1 && i < End) || (End == -1 && UString[i]); i++)
	{
		Length += SymbolSize_[UString[i]] * Size;
	}

	return Length;
}

void CFont::Render(CString& String, float Size, int Start, int End)
{
	assert (End <= String.Length());
	Render(String.Data(), Size, Start, End);
}

float CFont::GetLength(CString& String, float Size, int Start, int End)
{
	assert (End <= String.Length());
	return GetLength(String.Data(), Size, Start, End);
}
