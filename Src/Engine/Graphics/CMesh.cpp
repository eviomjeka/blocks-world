﻿#include "CMesh.h"

CMesh::CMesh() :
	VBO_(0),
	Initialized_(false),
	ElementSize_(-1),
	Size_(-1)
{
}

CMesh::~CMesh()
{
	assert(!Initialized_);
}

void CMesh::Init(const void* Data, int Size, CArray<TAttribute>& Attributes, int ElementSize)
{
	assert (!Initialized_);

	if (Size == 0)
	{
		Size_ = 0;
		Initialized_ = true;
		return;
	}

	if (VBO_ == 0)
	{
		glGenBuffers(1, &VBO_);
		glassert();
	}

	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glassert();
	glBufferData(GL_ARRAY_BUFFER, Size * ElementSize, Data, GL_STATIC_DRAW);
	glassert();

	Attributes_		= Attributes;
	Size_			= Size;
	ElementSize_	= ElementSize;

	Initialized_ = true;
}

void CMesh::SetSubData(int Offset, const void* Data, int Size)
{
	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glassert();
	glBufferSubData(GL_ARRAY_BUFFER, Offset * ElementSize_, Size * ElementSize_, Data);
	glassert();
}

void CMesh::Render (CArray<TAttribute>* Attributes, bool EnableDisableVertexAttributeArray)
{
	if (!Size_)
		return;

	assert (Initialized_);

	if (!Attributes)
		Attributes = &Attributes_;

	glBindBuffer (GL_ARRAY_BUFFER, VBO_);
	glassert();

	if (EnableDisableVertexAttributeArray)
		EnableVertexAttributeArray (Attributes, ElementSize_);

	glDrawArrays(GL_TRIANGLES, 0, Size_);
	glassert();

	if (EnableDisableVertexAttributeArray)
	{
		DisableVertexAttributeArray (Attributes);
		glassert();
	}
}

void CMesh::EnableVertexAttributeArray (CArray<TAttribute>* Attributes, int ElementSize)
{
	for (int i = 0; i < Attributes->Size(); i++)
	{
		TAttribute& Attribute = (*Attributes)[i];
		glVertexAttribPointer(Attribute.Index, Attribute.Components, Attribute.Type,
				GL_FALSE, ElementSize, Attribute.Offset);
		glassert();
		glEnableVertexAttribArray(Attribute.Index);
		glassert();
	}
}

void CMesh::DisableVertexAttributeArray (CArray<TAttribute>* Attributes)
{
	for (int i = 0; i < (*Attributes).Size(); i++)
	{
		glDisableVertexAttribArray((*Attributes)[i].Index);
		glassert();
	}
}

void CMesh::Destroy()
{
	if (!Initialized_)
		return;

	if (VBO_ != 0)
	{
		glDeleteBuffers (1, &VBO_);
		glassert();
		VBO_ = 0;
	}

	Initialized_ = false;
}

int CMesh::Size()
{
	assert(Initialized_);
	return Size_;
}

CMesh::operator bool ()
{
	return Initialized_;
}

bool CMesh::operator ! ()
{
	return !Initialized_;
}
