﻿#ifndef CTEXTUREATLAS
#define CTEXTUREATLAS

#include "CPngImage.h"

class CTextureAtlas
{
private:

public:

	static void GetTextureAtlas (const char* Address, CTexture& Texture, SIVector2D NumTextures);

};

#endif