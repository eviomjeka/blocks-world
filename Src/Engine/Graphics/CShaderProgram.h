﻿#ifndef CSHADERPROGRAM
#define CSHADERPROGRAM

#include "../Utilities/SVector2D.h"
#include "../Utilities/SVector.h"
#include "../Utilities/CMatrix.h"

#define UseShaderProgram(a) CShaderProgram::Use (&CShaders::a)
#define UseShaderProgramAddr(a) CShaderProgram::Use (a)

class CShaders;

class CShaderProgram
{
private:

	friend class CShaders;

	static CShaderProgram* Current_;

	bool	 Initialized_;
	unsigned VertShader_;
	unsigned FragShader_;
	unsigned Program_;
	int		 MatrixLoc_;

	char* ShaderToString_ (const char* FileName);


public:
	
	CShaderProgram();
	void Init (const char* ShaderFileName);
	void Destroy();

	static void Use (CShaderProgram* ShaderProgram);

	int GetAttributeLocation (const char* Name);
	int GetUniformLocation (const char* Name);
	unsigned ProgramID();
	int MatrixLoc();

	static void UniformInt			(int Location, int I);
	static void UniformFloat		(int Location, float F);
	static void UniformMatrix4x4	(int Location, CMatrix<4,4>& M);
	static void UniformVector2D		(int Location, SFVector2D V);
	static void UniformVector3D		(int Location, SFVector V);
	static void UniformVector2D		(int Location, float x, float y);
	static void UniformVector3D		(int Location, float x, float y, float z);
	static void UniformVector4D		(int Location, float x, float y, float z, float w);

};

#endif
