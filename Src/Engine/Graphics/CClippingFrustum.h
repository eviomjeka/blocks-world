﻿#ifndef CCLIPPINGFRUSTUM
#define CCLIPPINGFRUSTUM

#include "CMatrixEngine.h"
#include "../Utilities/Misc.h"
#include "../Utilities/CPlane.h"
#include "../Containers/CSArray.h"

class CClippingFrustum
{

private:
	CMatrixEngine*		MatrixEngine_;
	CSArray<CPlane, 4>	ClippingPlanes_;
	bool				Initialized_;
	
	CPlane ExtractPlane_(CMatrix<4, 4>& M, int Row);

public:
	CClippingFrustum();

	CClippingFrustum(CClippingFrustum&);
	void operator =(CClippingFrustum&);

	void Init(CMatrixEngine* MatrixEngine);
	void Destroy();

	void Load();
	bool ContainsAABB(SFVector A, SFVector Dimentions);

};

#endif