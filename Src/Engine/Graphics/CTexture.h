﻿#ifndef CTEXTURE
#define CTEXTURE

#include <PlatformInclude.h>

class CTexture
{
public:
	
	CTexture (const CTexture& Other);
	CTexture& operator = (const CTexture& Other);

	CTexture();
	~CTexture();
	
	void Generate();
	void Bind();
	void Destroy();
	bool Initialized();

private:
	unsigned ID_;

};

#endif
