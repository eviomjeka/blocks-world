﻿#ifndef CMATRIXENGINE
#define CMATRIXENGINE

#include "../Utilities/SVector.h"
#include "../Utilities/SColor.h"
#include "../Utilities/Misc.h"
#include "../Utilities/CMatrix.h"
#include "../Containers/CArray.h"
#include "CShaderProgram.h"

class CMatrixEngine
{
public:
	CMatrix<4, 4>			ModelViewMatrix_;
	CMatrix<4, 4>			ProjectionMatrix_;
	CMatrix<4, 4>			ResultingMatrix_;
	bool					ResultingMatrixValid_;
	bool					Initialized_;
	CArray<CMatrix<4, 4> >	ModelViewMatrices_;	
	CArray<CMatrix<4, 4> >	ProjectionMatrices_;

public:
	CMatrixEngine();

	void Apply (CShaderProgram* ShaderProgram);
	void Ortho (float XMin, float XMax, float YMin, float YMax, float ZNear, float ZFar);
	void Frustum (float XMin, float XMax, float YMin, float YMax, float ZNear, float ZFar);
	void Perspective (float Fovy, float Aspect, float ZNear, float ZFar);
	void LookAt (SFVector EyePosition, SFVector Center, SFVector UpVector);
	void Translate (float x, float y, float z);
	void Translate (SFVector T);
	void Scale (float x, float y, float z);
	void Scale (SFVector S);
	void RotateX (float A);
	void RotateY (float B);
	void RotateZ (float C);
	void ResetModelViewMatrix();
	void ResetProjectionMatrix();

	CMatrix<4, 4> ModelViewMatrix();
	CMatrix<4, 4> ProjectionMatrix();
	CMatrix<4, 4> ResultingMatrix();

	void PushModelViewMatrix();
	void PopModelViewMatrix();
	void PushProjectionMatrix();
	void PopProjectionMatrix();

};

#endif