#include "CModel.h"
#include "../../CApplication.h"

void CModel::Init(const char* FileName)
{
	CFile File;
	File.Init(FileName, CFILE_READ | CFILE_BINARY);
	Init(File);
	File.Destroy();
}

void CModel::Init(CStream& Stream)
{
	CSArray<int, 2> Types;
	Types[0] = GL_FLOAT;
	Types[1] = GL_BYTE;

	SModel Model;
	Stream >> Model;

	Meshes_.Resize(Model.Meshes.Size());
	Meshes_.SetStatic();

	for (int i = 0; i < Model.Meshes.Size(); i++)
	{
		SMesh& MeshData = Model.Meshes[i];

		//TODO: custom shaders
		Meshes_[i].Shader.Init(MeshData.Texture ? "ModelShader_T" : "ModelShader_C");

		//COLOR!!!
		int ColorUniform_ = Meshes_[i].Shader.GetUniformLocation("Mesh_Color");

		CShaderProgram::Use(&Meshes_[i].Shader);
		Meshes_[i].Shader.UniformVector3D(ColorUniform_, MeshData.Color);

		CArray<TAttribute> Attributes;
		Attributes.Resize(MeshData.Attributes.Size());
		Attributes.SetStatic();
		for (int j = 0; j < MeshData.Attributes.Size(); j++)
		{
			Attributes[j].Index      = Meshes_[i].Shader.GetAttributeLocation(MeshData.Attributes[j].Name.Data());
			Attributes[j].Components = MeshData.Attributes[j].Components;
			Attributes[j].Type       = Types[MeshData.Attributes[j].Type];
			Attributes[j].Offset     = (void*) ((intptr_t) MeshData.Attributes[j].Offset);
		}

		Meshes_[i].Mesh.Init(MeshData.Data.Data(), MeshData.NVertices,
				Attributes, MeshData.Data.Size() / MeshData.NVertices);

		Meshes_[i].Joints.Resize(MeshData.Joints.Size());
		Meshes_[i].Joints.SetStatic();
		for (int j = 0; j < MeshData.Joints.Size(); j++)
		{
			CString UniformName;
			UniformName.Print("JointMatrix%d", j);

			Meshes_[i].Joints[j].Uniform = Meshes_[i].Shader.GetUniformLocation(UniformName.Data());
			Meshes_[i].Joints[j].Joint   = MeshData.Joints[j].Joint;
			Meshes_[i].Joints[j].InvBind = MeshData.Joints[j].InvBind;
		}

	}

	Joints_     = Model.Joints;
	Animations_ = Model.Animations;

	if (Model.Texture.Length())
		CPngImage::Instance.LoadFromFile(Model.Texture.Data(), &Texture_);
}

void CModel::Destroy()
{
	for (int i = 0; i < Meshes_.Size(); i++)
	{
		Meshes_[i].Shader.Destroy();
		Meshes_[i].Mesh.Destroy();
	}

	Texture_.Destroy();
}

void CModel::InitContext(SModelContext& Context)
{
	Context.JointsInvolved.Resize(Joints_.Size());
	Context.JointsInvolved.SetStatic();
	Context.Joints.Resize(Joints_.Size());
	Context.Joints.SetStatic();
	Context.Animations.Resize(Animations_.Size());
	Context.Animations.SetStatic();

	for (int i = 0; i < Joints_.Size(); i++)
		Context.JointsInvolved[i] = false;

	for (int i = 0; i < Animations_.Size(); i++)
		Context.Animations[i] = -1.0f;
}

void CModel::Draw(SModelContext& Context)
{
	if (Texture_.Initialized())
		Texture_.Bind();

	for (int i = 1; i < Joints_.Size(); i++)
		Context.Joints[i] = Context.Joints[Joints_[i].Parent] * Context.Joints[i];

	for (int i = 0; i < Meshes_.Size(); i++)
	{
		CShaderProgram::Use(&Meshes_[i].Shader);

		for (int j = 0; j < Meshes_[i].Joints.Size(); j++)
		{
			CMatrix<4, 4> M = Context.Joints[Meshes_[i].Joints[j].Joint] * Meshes_[i].Joints[j].InvBind;
			Meshes_[i].Shader.UniformMatrix4x4(Meshes_[i].Joints[j].Uniform, M);
		}

		MATRIXENGINE.Apply(&Meshes_[i].Shader);
		Meshes_[i].Mesh.Render();
	}
}

void CModel::UpdateAnimation(SModelContext& Context, int Time)
{
	for (int i = 0; i < Joints_.Size(); i++)
		Context.Joints[i] = Joints_[i].Transformation;

	for (int i = 0; i < Context.Animations.Size(); i++)
	{
		if (Context.Animations[i] < 0)
			continue;

		SAnimation& Animation = Animations_[i];

		Context.Animations[i] += Time;
		float Frame = Context.Animations[i] * Animation.FPS;
		int A       = (int) Frame;
		float t     = Frame - A;

		if (Animation.Looped)
			A %= Animation.Frames.Size() - 1;

		if (A >= Animation.Frames.Size() - 1)
		{
			StopAnimation(Context, i);
			continue;
		}

		for (int j = 0; j < Animation.ActiveJoints.Size(); j++)
		{
			int joint = Animation.ActiveJoints[j];

			Context.Joints[joint] =
					Animation.Frames[A + 0][j] * (1 - t) + Animation.Frames[A + 1][j] * t;
		}
	}
}

void CModel::PlayAnimation(SModelContext& Context, int _Animation)
{
	if (Context.Animations[_Animation] >= 0.0f)
		return;

	SAnimation& Animation = Animations_[_Animation];

	for (int i = 0; i < Animation.ActiveJoints.Size(); i++)
	{
		__assert(!Context.JointsInvolved[Animation.ActiveJoints[i]]);
		Context.JointsInvolved[Animation.ActiveJoints[i]] = true;
	}

	Context.Animations[_Animation] = 0.0f;
}

void CModel::StopAnimation(SModelContext& Context, int _Animation)
{
	if (Context.Animations[_Animation] < 0.0f)
		return;

	SAnimation& Animation = Animations_[_Animation];

	for (int i = 0; i < Animation.ActiveJoints.Size(); i++)
	{
		__assert(Context.JointsInvolved[Animation.ActiveJoints[i]]);
		Context.JointsInvolved[Animation.ActiveJoints[i]] = false;
	}

	Context.Animations[_Animation] = -1.0f;
}

CStream& operator << (CStream& Stream, SModel& Model)
{
	return Stream << Model.Texture << Model.Meshes << Model.Joints << Model.Animations;
}
CStream& operator >> (CStream& Stream, SModel& Model)
{
	return Stream >> Model.Texture >> Model.Meshes >> Model.Joints >> Model.Animations;
}
CStream& operator << (CStream& Stream, SAnimation& Animation)
{
	return Stream << Animation.FPS << Animation.ActiveJoints << Animation.Frames << Animation.Looped;
}
CStream& operator >> (CStream& Stream, SAnimation& Animation)
{
	return Stream >> Animation.FPS >> Animation.ActiveJoints >> Animation.Frames >> Animation.Looped;
}
CStream& operator << (CStream& Stream, SJoint& Joint)
{
	return Stream << Joint.Parent << Joint.Transformation;
}
CStream& operator >> (CStream& Stream, SJoint& Joint)
{
	return Stream >> Joint.Parent >> Joint.Transformation;
}
CStream& operator << (CStream& Stream, SMesh& Mesh)
{
	return Stream << Mesh.Texture << Mesh.Color << Mesh.Attributes << Mesh.Joints << Mesh.Data << Mesh.NVertices;
}
CStream& operator >> (CStream& Stream, SMesh& Mesh)
{
	return Stream >> Mesh.Texture >> Mesh.Color >> Mesh.Attributes >> Mesh.Joints >> Mesh.Data >> Mesh.NVertices;
}
CStream& operator << (CStream& Stream, SAttribute& Attribute)
{
	return Stream << Attribute.Name << Attribute.Components << Attribute.Type << Attribute.Offset;
}
CStream& operator >> (CStream& Stream, SAttribute& Attribute)
{
	return Stream >> Attribute.Name >> Attribute.Components >> Attribute.Type >> Attribute.Offset;
}
CStream& operator << (CStream& Stream, SInfluence& Influence)
{
	return Stream << Influence.Joint << Influence.InvBind;
}
CStream& operator >> (CStream& Stream, SInfluence& Influence)
{
	return Stream >> Influence.Joint >> Influence.InvBind;
}
