﻿#ifndef CSHADERS
#define CSHADERS

#include "CShaderProgram.h"

class CShaders
{
public:
	
	static void Init();
	static void Destroy();

#define START(a) static CShaderProgram a##Shader; class C##a { friend class CShaders;
#define UNIFORM(a) private: int a##_; public: int a() { return a##_; }
#define END(a) }; static class C##a a;

#include "../../ShadersInfo.h"

#undef START
#undef UNIFORM
#undef END

};

#endif