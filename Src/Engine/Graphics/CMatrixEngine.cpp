﻿#include "CMatrixEngine.h"

CMatrixEngine::CMatrixEngine() :
	Initialized_ (false), 
	ResultingMatrixValid_ (false)
{}

void CMatrixEngine::Apply (CShaderProgram* ShaderProgram)
{
	if (!ResultingMatrixValid_)
	{
		ResultingMatrix_ = ResultingMatrix();
		ResultingMatrixValid_ = true;
	}
	CShaderProgram::UniformMatrix4x4 (ShaderProgram->MatrixLoc(), ResultingMatrix_);
}

void CMatrixEngine::Translate (float dX, float dY, float dZ)
{
	CMatrix<4, 4> T;
	T.LoadIdentity();

	T(0, 3) = dX;
	T(1, 3) = dY;
	T(2, 3) = dZ;

	ModelViewMatrix_ = ModelViewMatrix_ * T;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::Translate (SFVector T)
{
	Translate(T.x, T.y, T.z);
}

void CMatrixEngine::Scale (float x, float y, float z)
{
	CMatrix<4, 4> S;
	S.LoadIdentity();

	S(0, 0) = x;
	S(1, 1) = y;
	S(2, 2) = z;
	S(3, 3) = 1;

	ModelViewMatrix_ = ModelViewMatrix_ * S;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::Scale (SFVector S)
{
	Scale (S.x, S.y, S.z);
}

void CMatrixEngine::RotateX(float A)
{
	A *= PI / 180.0f;
	CMatrix<4, 4> Rx;
	float CosA = cos(A);
	float SinA = sin(A);
	Rx.LoadZero();

	Rx(0, 0) = 1.0f;
	Rx(1, 1) = CosA;
	Rx(1, 2) = -SinA;
	Rx(2, 1) = SinA;
	Rx(2, 2) = CosA;
	
	Rx(3, 3) = 1.0f;

	ModelViewMatrix_ = ModelViewMatrix_ * Rx;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::RotateY(float B)
{
	B *= PI / 180.0f;
	CMatrix<4, 4> Ry;
	float CosB = cos(B);
	float SinB = sin(B);
	Ry.LoadZero();

	Ry(0, 0) = CosB;
	Ry(0, 2) = SinB;
	Ry(1, 1) = 1.0f;
	Ry(2, 0) = -SinB;
	Ry(2, 2) = CosB;
	
	Ry(3, 3) = 1.0f;
	
	ModelViewMatrix_ = ModelViewMatrix_ * Ry;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::RotateZ(float C)
{
	C *= PI / 180.0f;
	CMatrix<4, 4> Rz;
	float CosC = cos(C);
	float SinC = sin(C);
	Rz.LoadZero();

	Rz(0, 0) = CosC;
	Rz(0, 1) = -SinC;
	Rz(1, 0) = SinC;
	Rz(1, 1) = CosC;
	Rz(2, 2) = 1.0f;
	
	Rz(3, 3) = 1.0f;

	ModelViewMatrix_ = ModelViewMatrix_ * Rz;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::Perspective(float Fovy, float Aspect, float ZNear, float ZFar)
{
	float YMax = ZNear * tan (Fovy * PI / 360.0f);
	float YMin = -YMax;
	float XMin = YMin * Aspect;
	float XMax = YMax * Aspect;
	
	Frustum (XMin, XMax, YMin, YMax, ZNear, ZFar);
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::LookAt (SFVector EyePosition, SFVector Center, SFVector UpVector)
{
	SFVector Forward = (Center - EyePosition).Normalize();
	SFVector Side = (Forward * UpVector).Normalize();
	SFVector Up = Side * Forward;

	CMatrix<4, 4> L;
	L.LoadIdentity();
	L (0, 0) = Side.x;
	L (0, 1) = Side.y;
	L (0, 2) = Side.z;
	L (0, 3) = 0.0f;
	L (1, 0) = Up.x;
	L (1, 1) = Up.y;
	L (1, 2) = Up.z;
	L (1, 3) = 0.0f;
	L (2, 0) = -Forward.x;
	L (2, 1) = -Forward.y;
	L (2, 2) = -Forward.z;
	L (2, 3) = 0.0f;
	L (3, 0) = 0.0f;
	L (3, 1) = 0.0f;
	L (3, 2) = 0.0f;
	L (3, 3) = 1.0f;

	ModelViewMatrix_ = ModelViewMatrix_ * L;
	Translate (-EyePosition);
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::Frustum(float XMin, float XMax, float YMin, float YMax, float ZMin, float ZMax)
{
	CMatrix<4, 4> F;
	F.LoadZero();

	F(0, 0) = 2.0f * ZMin / (XMax - XMin);
	F(0, 2) = (XMin + XMax) / (XMax - XMin);
	F(1, 1) = 2.0f * ZMin / (YMax - YMin);
	F(1, 2) = (YMin + YMax) / (YMax - YMin);
	F(2, 2) = -(ZMin + ZMax) / (ZMax - ZMin);
	F(2, 3) = -2.0f * ZMin * ZMax / (ZMax - ZMin);
	F(3, 2) = -1.0f;

	ProjectionMatrix_ = ProjectionMatrix_ * F;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::Ortho(float XMin, float XMax, float YMin, float YMax, float ZMin, float ZMax)
{
	CMatrix<4, 4> O;
	O.LoadZero();

	O(0, 0) = +2.0f / (XMax - XMin);
	O(0, 3) = -(XMin + XMax) / (XMax - XMin);
	O(1, 1) = +2.0f / (YMax - YMin);
	O(1, 3) = -(YMin + YMax) / (YMax - YMin);
	O(2, 2) = -2.0f / (ZMax - ZMin);
	O(2, 3) = -(ZMin + ZMax) / (ZMax - ZMin);
	O(3, 3) = 1.0f;

	ProjectionMatrix_ = ProjectionMatrix_ * O;
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::ResetModelViewMatrix()
{
	ModelViewMatrix_.LoadIdentity();
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::ResetProjectionMatrix()
{
	ProjectionMatrix_.LoadIdentity();
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::PushModelViewMatrix()
{
	ModelViewMatrices_.Insert(ModelViewMatrix_, true);
}

void CMatrixEngine::PopModelViewMatrix()
{
	ModelViewMatrix_ = ModelViewMatrices_.Pop();
	ResultingMatrixValid_ = false;
}

void CMatrixEngine::PushProjectionMatrix()
{
	ProjectionMatrices_.Insert(ProjectionMatrix_, true);
}

void CMatrixEngine::PopProjectionMatrix()
{
	ProjectionMatrix_ = ProjectionMatrices_.Pop();
	ResultingMatrixValid_ = false;
}

CMatrix<4, 4> CMatrixEngine::ModelViewMatrix()
{
	return ModelViewMatrix_;
}

CMatrix<4, 4> CMatrixEngine::ProjectionMatrix()
{
	return ProjectionMatrix_;
}

CMatrix<4, 4> CMatrixEngine::ResultingMatrix()
{
	return ProjectionMatrix_ * ModelViewMatrix_;
}
