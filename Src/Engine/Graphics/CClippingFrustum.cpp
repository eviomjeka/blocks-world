﻿#include "CClippingFrustum.h"

CPlane CClippingFrustum::ExtractPlane_(CMatrix<4, 4>& M, int Row)
{
	int Scale = Sign(Row);
	Row = std::abs(Row) - 1;

	float a = M(3, 0) + Scale * M(Row, 0);
	float b = M(3, 1) + Scale * M(Row, 1);
	float c = M(3, 2) + Scale * M(Row, 2);
	float d = M(3, 3) + Scale * M(Row, 3);

	return CPlane(a, b, c, d);
}

CClippingFrustum::CClippingFrustum() :
	MatrixEngine_(NULL),
	Initialized_(false)
{
}

void CClippingFrustum::Init(CMatrixEngine* MatrixEngine)
{
	assert(MatrixEngine);
	MatrixEngine_ = MatrixEngine;
	Initialized_ = true;
}

void CClippingFrustum::Destroy()
{
	Initialized_ = false;
}

void CClippingFrustum::Load()
{
	CMatrix<4, 4> M = MatrixEngine_->ResultingMatrix();

	ClippingPlanes_[0] = ExtractPlane_(M, 1);
	ClippingPlanes_[1] = ExtractPlane_(M, -1);
	ClippingPlanes_[2] = ExtractPlane_(M, 2);
	ClippingPlanes_[3] = ExtractPlane_(M, -2);
}

bool CClippingFrustum::ContainsAABB(SFVector A, SFVector Dimentions)
{
	SFVector XDimention = SFVector(Dimentions.x, 0.0f, 0.0f);
	SFVector YDimention = SFVector(0.0f, Dimentions.y, 0.0f);
	SFVector ZDimention = SFVector(0.0f, 0.0f, Dimentions.z);

	SFVector B = A + XDimention;
	SFVector C = A + YDimention;
	SFVector D = A + ZDimention;
	SFVector E = A + XDimention + YDimention;
	SFVector F = A + YDimention + ZDimention;
	SFVector G = A + ZDimention + XDimention;
	SFVector H = A + Dimentions;

	bool Inside = true;

	for (int i = 0; i < ClippingPlanes_.Size(); i++)
		Inside = Inside && (
			ClippingPlanes_[i].GetDistance(A) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(B) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(C) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(D) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(E) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(F) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(G) >= 0.0f ||
			ClippingPlanes_[i].GetDistance(H) >= 0.0f);

	return Inside;
}
