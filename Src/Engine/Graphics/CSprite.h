﻿#ifndef CSPRITE
#define CSPRITE

#include "../Utilities/SVector.h"
#include "../Utilities/SColor.h"
#include "../Graphics/CMatrixEngine.h"
#include "../MeshConstructors/CTextureMeshConstructor.h"

class CSprite
{
public:

	static void ToMeshConstructor (CMatrixEngine* MatrixEngine, CTextureMeshConstructor* MeshConstructor, SFVector Position, 
		float Size, SColor Color, SFVector2D StartTextureCoord, SFVector2D EndTextureCoords);

};

#endif