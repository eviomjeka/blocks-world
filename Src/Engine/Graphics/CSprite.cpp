﻿#include "CSprite.h"
#include <Engine.h>

void CSprite::ToMeshConstructor (CMatrixEngine* MatrixEngine, CTextureMeshConstructor* MeshConstructor, SFVector Position, 
								 float Size, SColor Color, SFVector2D StartTextureCoord, SFVector2D EndTextureCoord)
{
	CMatrix<4,4> MV = MatrixEngine->ModelViewMatrix();

	SFVector DirX = SFVector (MV (0, 0), MV (0, 1), MV (0, 2));
	SFVector DirY = SFVector (MV (1, 0), MV (1, 1), MV (1, 2));

	SFVector v[4] = {};

	v[0] = Position + (-DirX - DirY) * Size;
	v[1] = Position + ( DirX - DirY) * Size;
	v[2] = Position + ( DirX + DirY) * Size;
	v[3] = Position + (-DirX + DirY) * Size;

	MeshConstructor->SetColor (Color);
	SFVector2D TextureCoords[4] = { 
		SFVector2D (StartTextureCoord.x, StartTextureCoord.y), 
		SFVector2D (StartTextureCoord.x, EndTextureCoord.y), 
		SFVector2D (EndTextureCoord.x  , EndTextureCoord.y), 
		SFVector2D (EndTextureCoord.x  , StartTextureCoord.y) };
		
	MeshConstructor->SetColor (Color);

	const int VertexIndexes[6] = { 0, 1, 2, 2, 3, 0 };
	for (int i = 0; i < 6; i++)
	{
		int VertexIndex = VertexIndexes[i];
		MeshConstructor->AddVertex (v[VertexIndex], TextureCoords[VertexIndex]);
	}
}