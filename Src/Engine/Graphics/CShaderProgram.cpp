﻿#include "CShaderProgram.h"
#include <Engine.h>

CShaderProgram* CShaderProgram::Current_ = NULL;

CShaderProgram::CShaderProgram() :
Initialized_(false)
{
}

void CShaderProgram::Init (const char* ShaderFileName)
{
	VertShader_ = glCreateShader (GL_VERTEX_SHADER);
	assert (VertShader_ != 0);
	glassert();
	char VertAddr[MAX_PATH] = "";
	sprintf (VertAddr, "Shaders/%s.vert.glsl", ShaderFileName);
	const char* String = (const char*) ShaderToString_ (VertAddr);
	glShaderSource (VertShader_, 1, &String, NULL);
	glassert();
	glCompileShader (VertShader_);
	glassert();
	delete[] String;
	int CompileSuccess = 0;
	int Size = 2047;
	char Log[2048] = "";
	int LogSize = 0;
	glGetShaderiv (VertShader_, GL_COMPILE_STATUS, &CompileSuccess);
	glGetShaderiv (VertShader_, GL_INFO_LOG_LENGTH, &LogSize);
	glassert();
	glGetShaderInfoLog (VertShader_, Size, &Size, Log);
	glassert();
	if (!CompileSuccess)
	{
		error ("Vertex shader with address \"%s\" has compilation error(s). Errors:\n%s", VertAddr, Log);
	}
	else if (LogSize > 1)
	{
		LOG ("Vertex shader with address \"%s\" compilation info:\n%s", VertAddr, Log);
	}

	FragShader_ = glCreateShader (GL_FRAGMENT_SHADER);
	assert (FragShader_ != 0);
	glassert();
	char FragAddr[MAX_PATH] = "";
	sprintf (FragAddr, "Shaders/%s.frag.glsl", ShaderFileName);
	String = (const char*) ShaderToString_ (FragAddr);
	glShaderSource (FragShader_, 1, &String, NULL);
	glassert();
	glCompileShader (FragShader_);
	glassert();
	delete[] String;
	CompileSuccess = 0;
	glGetShaderiv (FragShader_, GL_COMPILE_STATUS, &CompileSuccess);
	glGetShaderiv (FragShader_, GL_INFO_LOG_LENGTH, &LogSize);
	glassert();
	Size = 2047;
	glGetShaderInfoLog (FragShader_, Size, &Size, Log);
	glassert();
	if (!CompileSuccess)
	{
		error ("Fragment shader with address \"%s\" has compilation error(s):\n%s", FragAddr, Log);
	}
	else if (LogSize > 1)
	{
		LOG ("Fragment shader with address \"%s\" compilation info:\n%s", FragAddr, Log);
	}

	Program_ = glCreateProgram();
	glassert();
	glAttachShader (Program_, VertShader_);
	glassert();
	glAttachShader (Program_, FragShader_);
	glassert();
	glLinkProgram (Program_);
	glassert();
	int LinkSuccess = 0;
	glGetProgramiv (Program_, GL_LINK_STATUS, &LinkSuccess);
	glGetShaderiv (FragShader_, GL_INFO_LOG_LENGTH, &LogSize);
	glassert();
	if (!LinkSuccess)
	{
		Size = 2047;
		glGetProgramInfoLog (Program_, Size, &Size, Log);
		glassert();

		error ("Shader program \"%s\" has linkage error(s):\n%s", ShaderFileName, Log);
	}
	
	glUseProgram (Program_);
	MatrixLoc_ = GetUniformLocation ("Matrix");
	glUseProgram (0);
	glassert();

	LOG ("Shader program \"%s\" compiled", ShaderFileName);

	Initialized_ = true;
}

char* CShaderProgram::ShaderToString_ (const char* FileName)
{
	CFile File (FileName, CFILE_READ | CFILE_BINARY);
	  
	File.Seek (0, SEEK_END);
	long Len = File.Tell();
	File.Seek (0, SEEK_SET);
	assert (Len > 0);
	
	int IgnoreStrings = 2;
	char Temp[3] = "";
	char* Str = new char[Len + 1 + 64];

#if DEVICE != DEVICE_MOBILE && OS != OS_LINUX
	const char* Version = "#version 120\n";
	strncpy (Str, Version, strlen (Version) + 1);
	IgnoreStrings--;
#else
	const char* Centroid = "#define centroid\n";
	strncpy (Str, Centroid, strlen (Centroid) + 1);
	IgnoreStrings--;
#endif

	if (IgnoreStrings != 0)
		File.Read (Temp, IgnoreStrings);
	int Size = strlen (Str);
	File.Read (&Str[Size], Len - IgnoreStrings);
	Str[Len - IgnoreStrings + Size] = 0;

	File.Destroy();
	return Str;
}

void CShaderProgram::Destroy()
{
	assert (Initialized_);
	
	glDetachShader (Program_, VertShader_);
	glassert();
	glDetachShader (Program_, FragShader_);
	glassert();
	glDeleteShader (VertShader_);
	glassert();
	glDeleteShader (FragShader_);
	glassert();
	glDeleteProgram (Program_);
	glassert();
	
	Initialized_ = false;
}

void CShaderProgram::Use (CShaderProgram* ShaderProgram)
{
	assert (ShaderProgram != 0);
	if (ShaderProgram == Current_)
		return;

	Current_ = ShaderProgram;
	glUseProgram (ShaderProgram->ProgramID());
	glassert();
}

int CShaderProgram::GetAttributeLocation (const char* Name)
{
	int Location = glGetAttribLocation (ProgramID(), Name);
	assert (Location != (unsigned int) -1);
	return Location;
}

int CShaderProgram::GetUniformLocation (const char* Name)
{
	int Location = glGetUniformLocation (Program_, Name);
	assert (Location != -1);
	return Location;
}

unsigned CShaderProgram::ProgramID()
{
	assert(Initialized_);

	return Program_;
}

int CShaderProgram::MatrixLoc()
{
	assert (MatrixLoc_ != -1);
	return MatrixLoc_;
}

void CShaderProgram::UniformInt (int Location, int I)
{
	assert (Location != -1);
	glUniform1i (Location, I);
	glassert();
}

void CShaderProgram::UniformFloat (int Location, float F)
{
	assert (Location != -1);
	glUniform1f (Location, F);
	glassert();
}

void CShaderProgram::UniformMatrix4x4 (int Location, CMatrix<4,4>& M)
{
	assert (Location != -1);
	glUniformMatrix4fv (Location, 1, GL_FALSE, M.Data());
	glassert();
}

void CShaderProgram::UniformVector2D (int Location, SFVector2D V)
{
	UniformVector2D (Location, V.x, V.y);
}

void CShaderProgram::UniformVector3D (int Location, SFVector V)
{
	UniformVector3D (Location, V.x, V.y, V.z);
}

void CShaderProgram::UniformVector2D (int Location, float x, float y)
{
	assert (Location != -1);
	glUniform2f (Location, x, y);
	glassert();
}

void CShaderProgram::UniformVector3D (int Location, float x, float y, float z)
{
	assert (Location != -1);
	glUniform3f (Location, x, y, z);
	glassert();
}

void CShaderProgram::UniformVector4D (int Location, float x, float y, float z, float w)
{
	assert (Location != -1);
	glUniform4f (Location, x, y, z, w);
	glassert();
}
