﻿#ifndef CMESH
#define CMESH

#include "../Containers/CArray.h"
#include "../Utilities/SColor.h"
#include "../Utilities/SVector.h"
#include "../Utilities/Misc.h"

#define ATTRIBUTE_OFFSET(Type, Name) ((void*) &(((Type*) NULL)->Name))

struct TAttribute
{
	unsigned		Index;
	int				Components;
	int				Type;
	void*			Offset;
};

class CMesh
{
private:
	unsigned				VBO_;
	bool					Initialized_;
	int						Size_;
	CArray<TAttribute>		Attributes_;
	int						ElementSize_;

public:
	CMesh();
	~CMesh();

	void Init (const void* Data, int Size, CArray<TAttribute>& Attributes, int ElementSize);
	void SetSubData(int Offset, const void* Data, int Size);
	void Render (CArray<TAttribute>* Attributes = NULL, bool EnableDisableVertexAttributeArray = true);
	static void EnableVertexAttributeArray (CArray<TAttribute>* Attributes, int ElementSize);
	static void DisableVertexAttributeArray (CArray<TAttribute>* Attributes);
	void Destroy();
	int  Size();

	operator bool ();
	bool operator ! ();

};

#endif