#ifndef CMODEL
#define CMODEL

#include "../Containers/CArray.h"
#include "../Utilities/CMatrix.h"
#include "../Utilities/CFile.h"
#include "../Utilities/CString.h"
#include "CMesh.h"
#include "CShaderProgram.h"
#include "CTexture.h"

/*vertex data outline:
 * vertex
 * (color)
 * (texture coordinates)
 * (normals)
 * <specific data>
 * weights
 */

struct SAttribute
{
	CString						Name;
	int							Components;
	int							Type;
	int							Offset;
};

struct SInfluence
{
	int             			Joint;
	CMatrix<4,4>    			InvBind;
};

struct SMesh
{
	bool						Texture;
	SFVector					Color;
	CArray<SAttribute>			Attributes;
	CArray<SInfluence>			Joints;
	CArray<byte>				Data;
	int							NVertices;
};

struct SJoint
{
	int							Parent;
	CMatrix<4, 4>				Transformation;
};

typedef CArray<CMatrix<4, 4> >	SAnimationFrame;

struct SAnimation
{
	float						FPS;
	CArray<int>					ActiveJoints;
	CArray<SAnimationFrame>		Frames;
	bool						Looped;
};

struct SModel
{
	CString						Texture;
	CArray<SMesh>				Meshes;
	CArray<SJoint>				Joints;
	CArray<SAnimation>			Animations;
};

struct SModelContext
{
	CArray<bool>				JointsInvolved;
	CArray<float>				Animations;
	CArray<CMatrix<4, 4> >		Joints;
};

class CModel
{
private:
	struct tInfluence
	{
		int					Uniform;
		int             	Joint;
		CMatrix<4,4>    	InvBind;
	};
	struct tMesh
	{
		CMesh				Mesh;
		CShaderProgram		Shader;
		CArray<tInfluence>	Joints;
	};

private:
	CTexture				Texture_;
	CArray<tMesh>			Meshes_;
	CArray<SJoint>			Joints_;
	CArray<SAnimation>		Animations_;

public:
	void Init(const char* FileName);
	void Init(CStream& Stream);
	void Destroy();

	void InitContext(SModelContext& Context);
	void UpdateAnimation(SModelContext& Context, int Time);
	void Draw(SModelContext& Context);
	void PlayAnimation(SModelContext& Context, int Animation);
	void StopAnimation(SModelContext& Context, int Animation);

	CMatrix<4, 4>& JointMatrix(int Joint);
};

CStream& operator << (CStream& Stream, SModel& Model);
CStream& operator >> (CStream& Stream, SModel& Model);
CStream& operator << (CStream& Stream, SAnimation& Animation);
CStream& operator >> (CStream& Stream, SAnimation& Animation);
CStream& operator << (CStream& Stream, SJoint& Joint);
CStream& operator >> (CStream& Stream, SJoint& Joint);
CStream& operator << (CStream& Stream, SMesh& Mesh);
CStream& operator >> (CStream& Stream, SMesh& Mesh);
CStream& operator << (CStream& Stream, SAttribute& Attribute);
CStream& operator >> (CStream& Stream, SAttribute& Attribute);
CStream& operator << (CStream& Stream, SInfluence& Influence);
CStream& operator >> (CStream& Stream, SInfluence& Influence);


#endif
