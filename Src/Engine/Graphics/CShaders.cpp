﻿#include "CShaders.h"

#define START(a) CShaderProgram CShaders::a##Shader; CShaders::C##a CShaders::a;
#define UNIFORM(a)
#define END(a)

#include "../../ShadersInfo.h"

#undef START
#undef UNIFORM
#undef END

void CShaders::Init()
{
	CShaderProgram* Current = NULL;
	void* CurrentClass = NULL;

#define START(a) a##Shader.Init (#a "Shader"); Current = &a##Shader; { CurrentClass = &CShaders::a; typedef CShaders::C##a __CShader;
#define UNIFORM(a) ((__CShader*) CurrentClass)->a##_ = Current->GetUniformLocation (#a);
#define END(a) }

#include "../../ShadersInfo.h"

#undef START
#undef UNIFORM
#undef END
}

void CShaders::Destroy()
{
#define START(a) a##Shader.Destroy();
#define UNIFORM(a)
#define END(a)

#include "../../ShadersInfo.h"

#undef START
#undef UNIFORM
#undef END
}
