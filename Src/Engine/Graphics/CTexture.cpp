﻿#include "CTexture.h"
#include <Engine.h>

CTexture::CTexture() : 
	ID_ (0)
{
}

CTexture::~CTexture()
{
	assert (!ID_);
}

void CTexture::Generate()
{
	assert (!ID_);
	glGenTextures (1, &ID_);
	assert (ID_);
	glassert();
}

void CTexture::Bind()
{
	assert (ID_);
	glBindTexture (GL_TEXTURE_2D, ID_);
}

void CTexture::Destroy()
{
	assert (ID_);
	glDeleteTextures (1, &ID_);
	glassert();
	ID_ = 0;
}

bool CTexture::Initialized()
{
	return ID_ != 0;
}
