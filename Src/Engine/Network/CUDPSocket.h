﻿#ifndef CUDPSOCKET
#define CUDPSOCKET

#include <PlatformMisc.h>
#include "../Utilities/CBuffer.h"

typedef sockaddr_in TSocketAddress;

TSocketAddress CreateTSocketAddress (const char* IP, unsigned short UDPPort);
bool operator == (TSocketAddress& a, TSocketAddress& b);

class CUDPSocket
{
public:
	
	CUDPSocket();
	void InitServer (unsigned short Port);
	void InitClient();
	void Destroy();
	
	void Send (TSocketAddress* Address, byte* Buffer, int Size);
	int Receive (TSocketAddress* Address, byte* Buffer, int MaxSize);
	void Send (TSocketAddress* Address, CBuffer& Buffer);
	bool Receive (TSocketAddress* Address, CBuffer& Buffer);

private:

	bool Initialized_;
	Socket Socket_;

};

#endif
