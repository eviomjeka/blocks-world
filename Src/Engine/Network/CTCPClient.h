﻿#ifndef CTCPCLIENT
#define CTCPCLIENT

#include <PlatformMisc.h>
#include "../Utilities/CBuffer.h"

class CTCPServer;

enum CTCPClientConnectCodes
{
	CTCPCLIENTCONNECT_SUCCESS, 
	CTCPCLIENTCONNECT_ADDRESS_NOT_VALID, 
	CTCPCLIENTCONNECT_TIMEOUT, 
	CTCPCLIENTCONNECT_CONNECTION_REFUSED
};

class CTCPClient : public CStream
{
public:
	
	CTCPClient();
	virtual ~CTCPClient();
	int Init (const char* IP, unsigned short Port);
	void Destroy();
		
	void Send (byte* Buffer, int Size);
	int Receive (byte* Buffer, int Size, bool Blocking = false);
	void Send(CBuffer& Buffer);
	bool Receive(CBuffer& Buffer, int Size, bool Blocking = false);
	
	bool Disconnected();
	
	virtual void Read(void* Data, int Size);
	virtual void Write(void* Data, int Size);

private:

	bool Initialized_;
	Socket Socket_;

	friend class CTCPServer;

};

#endif