﻿#include "CTCPServer.h"
#include <Engine.h>

CTCPServer::CTCPServer() :
	Initialized_ (false),
	Socket_(0)
{}

void CTCPServer::Init(unsigned short Port, int MaxConnections)
{
	assert(!Initialized_);

	Socket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	assert (Socket_ != -1);

	sockaddr_in Local = {};
	Local.sin_family		= AF_INET;
	Local.sin_addr.s_addr	= htonl (INADDR_ANY);
	Local.sin_port			= htons (Port);

	assert(bind (Socket_, (sockaddr*) &Local, sizeof (Local)) == 0);

#if OS == OS_WINDOWS
	ULONG Param = 1;
	assert (ioctlsocket (Socket_, FIONBIO, &Param) != SOCKET_ERROR);
#else
	int Flags = fcntl (Socket_, F_GETFL, 0);
	assert (!fcntl (Socket_, F_SETFL, Flags | O_NONBLOCK));
#endif

	if (listen (Socket_, MaxConnections) != 0)
	{
		closesocket (Socket_);
		error ("TCP server listen error");
	}

	Initialized_ = true;
}

void CTCPServer::Destroy()
{
	assert (Initialized_);
	assert (closesocket(Socket_) != -1);
	Initialized_ = false;
}

CTCPClient CTCPServer::AcceptClient()
{
	assert (Initialized_);

	sockaddr_in Address = {};

	socklen_t AddressSize = sizeof Address;
	
	CTCPClient Client;
	Client.Socket_ = accept(Socket_, (sockaddr*) &Address, &AddressSize);
	
	int Code = SocketError();
	if (Client.Socket_ == -1)
	{
		if (Code == EWOULDBLOCK)
		{
			Client.Initialized_ = false;
			return Client;
		}
		else
			error("Accept client error");
	}
	

#if OS == OS_LINUX || OS == OS_ANDROID
	int Flags = fcntl(Client.Socket_, F_GETFL, 0);
	fcntl(Client.Socket_, F_SETFL, Flags | O_NONBLOCK);
#endif

	Client.Initialized_ = true;
	return Client;
}
