﻿#include "CTCPClient.h"
#include <Engine.h>

CTCPClient::CTCPClient() :
Initialized_ (false),
Socket_(0) 
{}

CTCPClient::~CTCPClient()
{
}

int CTCPClient::Init (const char* IP, unsigned short Port)
{
	assert (!Initialized_);

	Socket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	assert(Socket_ != -1);
	
	sockaddr_in Dest = {};
	Dest.sin_family			= AF_INET;
	Dest.sin_port			= htons(Port);
	Dest.sin_addr.s_addr	= inet_addr(IP);
	assert(Dest.sin_addr.s_addr != INADDR_NONE);

	if (connect (Socket_, (sockaddr*) &Dest, sizeof (Dest)) != 0)
	{
		int Code = SocketError();
		if (Code == EADDRNOTAVAIL)
			return CTCPCLIENTCONNECT_ADDRESS_NOT_VALID;
		else if (Code == ETIMEDOUT)
			return CTCPCLIENTCONNECT_TIMEOUT;
		else if (Code == ECONNREFUSED)
			return CTCPCLIENTCONNECT_CONNECTION_REFUSED;
		else
			error ("\"connect\" error");
	}
	
#if OS == OS_WINDOWS
	ULONG param = 1;
	assert (ioctlsocket (Socket_, FIONBIO, &param) != SOCKET_ERROR);
#elif OS == OS_ANDROID // TODO: use linux code?
	int Param = 1; 
	assert (ioctl (Socket_, FIONBIO, &Param) != -1);
#else
	int Flags = fcntl (Socket_, F_GETFL, 0);
	assert (!fcntl (Socket_, F_SETFL, Flags | O_NONBLOCK));
#endif

	Initialized_ = true;
	return CTCPCLIENTCONNECT_SUCCESS;
}

void CTCPClient::Destroy()
{
	if (!Initialized_)
		return;

	assert (closesocket (Socket_) != -1);
	Initialized_ = false;
}

void CTCPClient::Send (byte* Buffer, int Size)
{
	assert (Size);

	if (!Initialized_)
		return;

	int Sent = 0;

	while (Sent < Size)
	{
		int CurrentSent = send(Socket_, (char*) &Buffer[Sent], Size - Sent, 0);
		if (CurrentSent == -1)
		{
			int Code = SocketError();
			if (Code == EWOULDBLOCK)
				continue;
			else
			{
				error ("Socket error %d\n", Code);
				return;
			}
		}

		Sent += CurrentSent;
	}

	assert(Sent == Size);
}

int CTCPClient::Receive (byte* Buffer, int Size, bool Blocking)
{
	if (!Initialized_)
		return 0;

	int BytesReceived = 0;
	
	while (BytesReceived < Size)
	{
		int CurrentBytesReceived = recv(Socket_, (char*) &Buffer[BytesReceived], Size - BytesReceived, 0);

		if (CurrentBytesReceived == -1)
		{
			int Code = SocketError();
			if (Code == EWOULDBLOCK)
			{
				if (BytesReceived == 0 && !Blocking)
					return 0;
				else
					continue;
			}
			else
			{
				error ("Socket error %d\n", Code);
			}
		}
		BytesReceived += CurrentBytesReceived;
	}

	assert(BytesReceived == Size);

	return BytesReceived;
}

void CTCPClient::Read(void* Data, int Size)
{
	int Received = Receive ((byte*) Data, Size, true);
	
	assert(Received == Size);
}

void CTCPClient::Write(void* Data, int Size)
{
	Send ((byte*) Data, Size);
}

bool CTCPClient::Disconnected()
{
	return !Initialized_;
}

void CTCPClient::Send(CBuffer& Buffer)
{
	Send(Buffer.Data(), Buffer.WritePosition());
	Buffer.SetReadPosition(Buffer.WritePosition());
}

bool CTCPClient::Receive(CBuffer& Buffer, int Size, bool Blocking)
{
	assert(Size <= Buffer.MaxSize());
	int Received = 0;

	Received = Receive(Buffer.Data(), Size, Blocking);

	Buffer.SetReadPosition(0);
	Buffer.SetWritePosition(Size);

	return Received != 0;
}
