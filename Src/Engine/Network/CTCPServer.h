﻿#ifndef CTCPSERVER
#define CTCPSERVER

#include "CTCPClient.h"

class CTCPServer
{
public:
	
	CTCPServer();
	void Init (unsigned short Port, int MaxConnections);
	void Destroy();
	
	CTCPClient AcceptClient();
	
private:

	bool Initialized_;
	Socket Socket_;

};

#endif