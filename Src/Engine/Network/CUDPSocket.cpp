﻿#include "CUDPSocket.h"
#include <Engine.h>

TSocketAddress CreateTSocketAddress(const char* IP, unsigned short UDPPort)
{
	TSocketAddress SocketAddress = {};
	SocketAddress.sin_family = AF_INET;
	SocketAddress.sin_addr.s_addr = inet_addr(IP);
	assert (SocketAddress.sin_addr.s_addr != INADDR_NONE);
	SocketAddress.sin_port = htons (UDPPort);

	return SocketAddress;
}

bool operator ==(TSocketAddress& a, TSocketAddress& b)
{
	return a.sin_addr.s_addr == b.sin_addr.s_addr && a.sin_port == b.sin_port;
}

CUDPSocket::CUDPSocket() :
	Initialized_ (false),
	Socket_(0)
{}

void CUDPSocket::InitServer(unsigned short Port)
{
	assert(!Initialized_);

	Socket_ = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert(Socket_ != -1);

	sockaddr_in Local = {};
	Local.sin_family		= AF_INET;
	Local.sin_addr.s_addr	= htonl (INADDR_ANY);
	Local.sin_port			= htons (Port);
	assert (bind(Socket_, (sockaddr*) &Local, sizeof (Local)) == 0);

#if OS == OS_WINDOWS
	ULONG Param = 1;
	assert (ioctlsocket (Socket_, FIONBIO, &Param) != SOCKET_ERROR);
#elif OS == OS_ANDROID
	int Param = 1; 
	assert (ioctl(Socket_, FIONBIO, &Param) != -1);
#else
	int Flags = fcntl(Socket_, F_GETFL, 0);
	fcntl(Socket_, F_SETFL, Flags | O_NONBLOCK);
#endif

	Initialized_ = true;
}

void CUDPSocket::InitClient()
{
	assert(!Initialized_);

	Socket_ = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert (Socket_ != -1);
	
#if OS == OS_WINDOWS
	ULONG param = 1;
	assert (ioctlsocket (Socket_, FIONBIO, &param) != SOCKET_ERROR);
#else
	int Flags = fcntl(Socket_, F_GETFL, 0);
	fcntl(Socket_, F_SETFL, Flags | O_NONBLOCK);
#endif

	sockaddr_in Local = {};
	Local.sin_family		= AF_INET;
	Local.sin_addr.s_addr	= INADDR_ANY;
	Local.sin_port			= 0;
	assert (bind(Socket_, (sockaddr*) &Local, sizeof (Local)) == 0);

	Initialized_ = true;
}

void CUDPSocket::Destroy()
{
	assert(Initialized_);
	assert(closesocket(Socket_) != -1);

	Initialized_ = false;
}

void CUDPSocket::Send(TSocketAddress* Address, byte* Buffer, int Size)
{
	assert(Initialized_);

	assert (Size != 0);

	int BytesSent = sendto (Socket_, (char*) Buffer, Size, 0, (sockaddr*) Address, sizeof *Address);
	if (BytesSent == -1)
	{
		int Code = SocketError();
		if (Code == ETIMEDOUT || Code == ECONNRESET)
			Destroy();
		else
			error ("Send packet error (UDP protocol)");
	}
	assert (BytesSent == Size);
}

int CUDPSocket::Receive(TSocketAddress* Address, byte* Buffer, int MaxSize)
{
	assert(Initialized_);

	socklen_t AddressSize = sizeof *Address;

	int BytesReceived = recvfrom(Socket_, (char*) Buffer, MaxSize, 0, (sockaddr*) Address, &AddressSize);
	if (BytesReceived == -1)
	{
		int Code = SocketError();
		if (Code == EWOULDBLOCK)
			return 0;
		else if (Code == ETIMEDOUT || Code == ECONNRESET)
			Destroy();
		else
			error ("Receive packet error (UDP protocol)!");

		return 0;
	}
	assert (AddressSize == sizeof *Address);
	assert (BytesReceived <= MaxSize);

	return BytesReceived;
}

void CUDPSocket::Send(TSocketAddress* Address, CBuffer& Buffer)
{
	Send(Address, Buffer.Data(), Buffer.WritePosition());
	Buffer.SetReadPosition(Buffer.WritePosition());
}

bool CUDPSocket::Receive(TSocketAddress* Address, CBuffer& Buffer)
{
	int Received = Receive(Address, Buffer.Data(), Buffer.MaxSize());
	Buffer.SetReadPosition(0);
	Buffer.SetWritePosition(Received);

	return Received != 0;
}
