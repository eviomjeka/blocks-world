﻿#ifndef APPLICATIONINFOINCLUDE
#define APPLICATIONINFOINCLUDE

#define DEVICE_COMPUTER	1
#define DEVICE_MOBILE	2
	
#define OS_WINDOWS		1
#define OS_LINUX		2
#define OS_X			3
#define OS_ANDROID		4

#define TRUE			1
#define FALSE			0

#define ASSERTS_ALL		1
#define ASSERTS_NORMAL	2
#define ASSERTS_DISABLE	3

#endif
