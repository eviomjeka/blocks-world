﻿#ifndef INCLUDE
#define INCLUDE

#include "../ApplicationInfo.h"

#if !defined APPLICATION_NAME || \
	!defined ASSERTS || (ASSERTS != ASSERTS_DISABLE && ASSERTS != ASSERTS_NORMAL && ASSERTS != ASSERTS_ALL) || \
	!defined DEVICE || (DEVICE != DEVICE_COMPUTER && DEVICE != DEVICE_MOBILE) || \
	!defined OS || (OS != OS_WINDOWS && OS != OS_LINUX && OS != OS_X && OS != OS_ANDROID)
	#if defined _MSC_VER
		__pragma (message ("Error: ApplicationInfo.h is not correct"))
	#else
		#error ApplicationInfo.h is not correct
	#endif
#endif

#include <../DeviceInfo.h>
#include <PlatformInfo.h>
#include <PlatformInclude.h>
#include <Error.h>
#include <CMutex.h>
#include <CWindow.h>
#include <CFileSystem.h>
#include <PlatformMisc.h>

#include "Containers/CArray.h"
#include "Containers/CBinaryHeap.h"
#include "Containers/CCircularArray2D.h"
#include "Containers/CCircularArray3D.h"
#include "Containers/CHashTable.h"
#include "Containers/CList.h"
#include "Containers/CQueue.h"
#include "Containers/CSArray.h"
#include "Containers/CSVectorArray2D.h"
#include "Containers/CSVectorArray3D.h"
#include "Containers/CCache.h"

#include "Graphics/CClippingFrustum.h"
#include "Graphics/CFont.h"
#include "Graphics/CMatrixEngine.h"
#include "Graphics/CMesh.h"
#include "Graphics/CModel.h"
#include "Graphics/CPngImage.h"
#include "Graphics/CShaderProgram.h"
#include "Graphics/CShaders.h"
#include "Graphics/CSprite.h"
#include "Graphics/CTexture.h"
#include "Graphics/CTextureAtlas.h"

#include "Network/CTCPClient.h"
#include "Network/CTCPServer.h"
#include "Network/CUDPSocket.h"

#include "MeshConstructors/CBasicMeshConstructor.h"
#include "MeshConstructors/CBasicMeshQueue.h"
#include "MeshConstructors/CColorMeshConstructor.h"
#include "MeshConstructors/CTextureMeshConstructor.h"

#include "Utilities/CBuffer.h"
#include "Utilities/CCompressor.h"
#include "Utilities/CFile.h"
#include "Utilities/CLog.h"
#include "Utilities/CMatrix.h"
#include "Utilities/CMemoryManager.h"
#include "Utilities/CPlane.h"
#include "Utilities/CRandom.h"
#include "Utilities/CSafePointer.h"
#include "Utilities/CStream.h"
#include "Utilities/CString.h"
#include "Utilities/Misc.h"
#include "Utilities/SColor.h"
#include "Utilities/SVector.h"
#include "Utilities/SVector2D.h"

#include "Sound/CSound.h"
#include "Sound/CSoundSource.h"
#include "Sound/COpenAL.h"

#endif
