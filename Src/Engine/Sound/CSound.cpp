#include "CSound.h"
#include <Engine.h>

void CSound::Init(const char* FileName)
{
	__assert(!Initialized_);

    alGenBuffers(1, &ID_);
    alassert();

	Load_(FileName);

	Initialized_ = true;
}

void CSound::Destroy()
{
	if (Initialized_)
	{
		alDeleteBuffers(1, &ID_);

		Initialized_ = false;
	}
}

void CSound::Load_(const char* FileName)
{
	CFile OggFile;
	OggFile.Init(FileName, CFILE_READ | CFILE_BINARY);

	OggVorbis_File VorbisFile;

	if (ov_open_callbacks(OggFile.GetFILE(), &VorbisFile, NULL, 0, OV_CALLBACKS_NOCLOSE) < 0)
		error("%s is not an ogg file", FileName);

	vorbis_info* Info      = ov_info(&VorbisFile, -1);
	assert(Info);
	assert(Info->channels == 1);

	int CurrentSection = 0;
	CArray<char> Buffer((int) ov_pcm_total(&VorbisFile, -1) * 2);
	Buffer.SetStatic();
	int BytesRead = 0;

	for (;;)
	{
		int Ret = ov_read(&VorbisFile, Buffer.Data() + BytesRead, Buffer.Size() - BytesRead,
				0, 2, 1, &CurrentSection);

		assert(Ret >= 0);

		if (!Ret)
			break;

		BytesRead += Ret;
	}

    alBufferData(ID_, AL_FORMAT_MONO16, (void*) Buffer.Data(),  BytesRead, Info->rate);
    alassert();

    ov_clear(&VorbisFile);
    OggFile.Destroy();

	LOG ("Sound \"%s\" loaded", FileName);
}

ALuint CSound::ID()
{
	__assert(Initialized_);

	return ID_;
}

CSound::CSound() :
	ID_(0),
	Initialized_(false)
{
}

CSound::~CSound()
{
	__assert(!Initialized_);
}

