#ifndef CSOUND
#define CSOUND

#include <AL/al.h>
#include <AL/alc.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

class CSound
{
private:
	ALuint      ID_;
	bool        Initialized_;

public:
	CSound();
	~CSound();

	void Init(const char* FileName);
	void Destroy();
	void Load_(const char* FileName);

	ALuint ID();
};

#endif
