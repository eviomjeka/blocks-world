#ifndef COPENAL
#define COPENAL

#include <AL/al.h>
#include <AL/alc.h>

#include "../Utilities/SVector.h"

class COpenAL
{

private:
	static ALCdevice*  Device_;
	static ALCcontext* Context_;

public:
	static void Init();
	static void Destroy();
	static void MoveListener(SFVector Position, SFVector Velocity, SFVector Orientation = SFVector(0));
	static ALCdevice* Device();

};

#endif
