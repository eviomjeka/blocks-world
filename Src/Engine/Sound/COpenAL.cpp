#include "COpenAL.h"
#include <Engine.h>

ALCdevice*  COpenAL::Device_  = NULL;
ALCcontext* COpenAL::Context_ = NULL;

void COpenAL::Init()
{
	Device_ = alcOpenDevice(NULL);

    if (!Device_)
    	error("Default sound device not present.");

    Context_ = alcCreateContext(Device_, NULL);

    if (!Context_)
    	error("Could not get OpenAL context.");

    alcMakeContextCurrent(Context_);
    alassert();
}

void COpenAL::Destroy()
{
	__assert(Device_);

	alcMakeContextCurrent(NULL);
	alcDestroyContext(Context_);
	Context_ = NULL;
	alcCloseDevice(Device_);
	Device_ = NULL;
}

void COpenAL::MoveListener(SFVector Position, SFVector Velocity, SFVector /*Orientation*/)
{
	__assert(Device_);

    alassert();
    alListener3f(AL_POSITION,    Position.x,    Position.y,    Position.z);
    alassert();
    alListener3f(AL_VELOCITY,    Velocity.x,    Velocity.y,    Velocity.z);
    alassert();
	//TODO: fix orientation
    //alListener3f(AL_ORIENTATION, Orientation.x, Orientation.y, Orientation.z);
    //alassert();
}

ALCdevice* COpenAL::Device()
{
	__assert(Device_);

	return Device_;
}
