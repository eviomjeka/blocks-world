#ifndef CMSOUND
#define CMSOUND

#include "CSound.h"
#include "../Containers/CArray.h"

class CMSound
{

private:
	CArray<CSound>		Sounds_;
	bool				Initialized_;

public:
	CMSound();
	~CMSound();

	void Init(const char* Path);
	void Destroy();

	ALuint GetRandom();

};

#endif
