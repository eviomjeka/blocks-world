#include "CSoundSource.h"

void CSoundSource::Init(CSound& Sound)
{
	if (!Initialized_)
		Init();
	else
		Stop();

	if (Queued_)
	{
		//...
	}

	Queued_ = NULL;
	alSourcei(ID_, AL_BUFFER, Sound.ID());
	alassert();
}

void CSoundSource::PlayQueue(CMSound& Sounds, int QueueSize)
{
	if (!Initialized_)
		Init();
	else if (!Queued_)
	{
		//clear buffer
	}
	else if (Queued_ != &Sounds)
	{
		//clear queue
	}

	ALint Queued;
	ALint Processed;
	alassert();
	alGetSourcei(ID_, AL_BUFFERS_QUEUED,    &Queued);
	alassert();
	alGetSourcei(ID_, AL_BUFFERS_PROCESSED, &Processed);
	alassert();

	for (int i = 0; i < Processed; i++)
	{
		ALuint BufferID;
		alSourceUnqueueBuffers(ID_, 1, &BufferID);
		alassert();
	}

	for (int i = 0; i < QueueSize - Queued; i++)
	{
		ALuint BufferID = Sounds.GetRandom();
		alSourceQueueBuffers(ID_, 1, &BufferID);
		alassert();
	}

	Queued_ = &Sounds;

	if (!Playing())
		Play();
}

void CSoundSource::Init()
{
	__assert(!Initialized_);

	alGenSources(1, &ID_);
	alassert();

	alSourcef(ID_, AL_PITCH,  1.0f);
	alassert();
	alSourcef(ID_, AL_GAIN,   1.0f);
	alassert();

	Queued_ = NULL;

	Initialized_ = true;
}

void CSoundSource::Play(bool Loop)
{
	__assert(Initialized_);

	alSourcei(ID_, AL_LOOPING,  Loop);
	alassert();
	alSourcePlay(ID_);
	alassert();
}

void CSoundSource::SetOffset(float Offset)
{
	__assert(Initialized_);

	alSourcef(ID_, AL_SEC_OFFSET,  Offset);
}

void CSoundSource::Move(SFVector Position, SFVector Velocity)
{
	__assert(Initialized_);

	alSource3f(ID_, AL_POSITION, Position.x, Position.y, Position.z);
	alassert();
	alSource3f(ID_, AL_VELOCITY, Velocity.x, Velocity.y, Velocity.z);
	alassert();
}

void CSoundSource::Stop()
{
	__assert(Initialized_);

	alSourceStop(ID_);
	alassert();
}

void CSoundSource::Destroy()
{
	if (Initialized_)
	{
		Stop();
		alDeleteSources(1, &ID_);
		alassert();

		Initialized_ = false;
	}
}


bool CSoundSource::Playing()
{
	__assert(Initialized_);

	ALint State;
	alGetSourcei(ID_, AL_SOURCE_STATE, &State);
	alassert();

	return (State == AL_PLAYING);
}

CSoundSource::CSoundSource() :
	ID_(0),
	Initialized_(false),
	Queued_(NULL)
{
}

CSoundSource::CSoundSource(const CSoundSource&) :
	ID_(0),
	Initialized_(false),
	Queued_(NULL)
{
}

CSoundSource& CSoundSource::operator =(const CSoundSource&)
{
	Destroy();

	ID_ =			0;
	Initialized_	= false;
	Queued_			= NULL;

	return *this;
}

CSoundSource::~CSoundSource()
{
	Destroy();
	//__assert(!Initialized_);
}

