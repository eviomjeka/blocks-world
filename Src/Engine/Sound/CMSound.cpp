#include "CMSound.h"
#include <Engine.h>

void CMSound::Init(const char* Path)
{
	__assert(!Initialized_);

	CArray<CFileSystemElement> SoundFiles;
	CFileSystem::FileList(&SoundFiles, Path);

	Sounds_.Resize(SoundFiles.Size());
	Sounds_.SetStatic();

	for (int i = 0; i < SoundFiles.Size(); i++)
	{
		char SoundFile[MAX_PATH] = {};
		sprintf(SoundFile, "%s/%s", Path, SoundFiles[i].Name.Data());

		Sounds_[i].Init(SoundFile);
	}

	Initialized_= true;
}

ALuint CMSound::GetRandom()
{
	__assert(Initialized_);

	return Sounds_[rand() % Sounds_.Size()].ID();
}

void CMSound::Destroy()
{
	__assert(Initialized_);

	for (int i = 0; i < Sounds_.Size(); i++)
		Sounds_[i].Destroy();

	Initialized_ = false;
}

CMSound::CMSound() :
	Initialized_(false)
{
}

CMSound::~CMSound()
{
	__assert(!Initialized_);
}
