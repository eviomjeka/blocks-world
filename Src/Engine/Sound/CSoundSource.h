#ifndef CSOUNDSOURCE
#define CSOUNDSOURCE

#include <AL/al.h>
#include <AL/alc.h>

#include "CSound.h"
#include "CMSound.h"
#include <Engine.h>

#define CSOUNDSOURCE_DEFAULT_QUEUE_SIZE 5

class CSoundSource
{
private:
	ALuint      ID_;
	bool		Initialized_;
	CMSound*	Queued_;

public:
	CSoundSource();
	CSoundSource(const CSoundSource&);
	~CSoundSource();
	CSoundSource& operator =(const CSoundSource&);

	void Init();
	void Init(CSound& Sound);
	void PlayQueue(CMSound& Sounds, int QueueSize = CSOUNDSOURCE_DEFAULT_QUEUE_SIZE);
	void SetOffset(float Time);
	void Play(bool Loop = false);
	void Move(SFVector Position, SFVector Velocity);
	void Stop();
	void Destroy();

	bool Playing();

};

#endif
