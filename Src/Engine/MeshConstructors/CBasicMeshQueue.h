﻿#ifndef CBASICMESHQUEUE
#define CBASICMESHQUEUE

#include "../Containers/CArray.h"
#include "../Containers/CQueue.h"
#include "../Graphics/CMesh.h"
#include "../Graphics/CShaderProgram.h"

template <typename tVertex, typename tKey>
class CBasicMeshQueue
{
private:
	struct tMesh
	{
		int		Offset;
		int		Size;
		tKey	Key;
	};

	CArray<tVertex>		Verteces_;
	CArray<TAttribute>	Attributes_;
	int					WriteOffset_;
	int					ReadOffset_;
	CQueue<tMesh>		Meshes_;
	tMesh				CurrentMesh_;
	bool				_Initialized_;

public:
	CBasicMeshQueue() :
		WriteOffset_(-1),
		ReadOffset_(-1),
		_Initialized_(false)
	{}
	CBasicMeshQueue(const CBasicMeshQueue&);

protected:

	void Init_(CArray<TAttribute>& Attributes, int Size, int MaxMeshes)
	{
		assert(!_Initialized_);
		
		Verteces_.Clear();
		Verteces_.Resize(Size);
		tVertex Vertex;
		for (int i = 0; i < Verteces_.MaxSize(); i++)
			Verteces_.Insert (Vertex);

		Meshes_.Resize(MaxMeshes);

		WriteOffset_		= 0;
		ReadOffset_			= 0;
		Attributes_			= Attributes;		
		CurrentMesh_.Offset = -1;
		_Initialized_		= true;
	}

	void AddVertex_(tVertex Vertex)
	{
		if (WriteOffset_ == Verteces_.MaxSize())
			WriteOffset_ = 0;

		while (!Meshes_.Empty() && WriteOffset_ == ReadOffset_)
			Sleep(1);

		Verteces_[WriteOffset_] = Vertex;
		WriteOffset_++;
		CurrentMesh_.Size++;
	}

public:

	void Begin(tKey Key)
	{
		assert(_Initialized_);
		assert(CurrentMesh_.Offset == -1);

		while (Meshes_.Full())
			Sleep(1);

		CurrentMesh_.Offset = WriteOffset_;
		CurrentMesh_.Size	= 0;
		CurrentMesh_.Key	= Key;
	}

	void End()
	{
		assert(_Initialized_);
		assert(CurrentMesh_.Offset != -1);

		Meshes_.Push(CurrentMesh_);

		CurrentMesh_.Offset = -1;
	}

	void Load(CMesh& Mesh)
	{
		assert(_Initialized_);

		tMesh MeshData = Meshes_.Pop();
		if (MeshData.Size)
		{
			if (Mesh)
				Mesh.Destroy();
			if (MeshData.Offset + MeshData.Size > Verteces_.Size())
			{
				Mesh.Init(NULL, MeshData.Size, Attributes_, sizeof (tVertex));
				Mesh.SetSubData(0, Verteces_.Data() + MeshData.Offset, Verteces_.Size() - MeshData.Offset);
				Mesh.SetSubData(Verteces_.Size() - MeshData.Offset, Verteces_.Data(), MeshData.Size - Verteces_.Size() + MeshData.Offset);
			}
			else
				Mesh.Init((Verteces_.Data() + MeshData.Offset), MeshData.Size, Attributes_, sizeof (tVertex));

		}
		else if (Mesh)
			Mesh.Destroy();

		if (ReadOffset_ < Verteces_.Size())
			ReadOffset_ += MeshData.Size;
		else
			ReadOffset_ = MeshData.Size - Verteces_.Size();
		// For atomic writing
	}

	void Clear()
	{
		ReadOffset_ = WriteOffset_;

		while (!Meshes_.Empty())
			Meshes_.Pop();
	}

	tKey GetPending()
	{
		assert(_Initialized_);
		return Meshes_.Head().Key;
	}

	bool Empty()
	{
		return Meshes_.Empty();
	}
};

#endif
