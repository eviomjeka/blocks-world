﻿#ifndef CCOLORMESHCONSTRUCTOR
#define CCOLORMESHCONSTRUCTOR

#include "CBasicMeshConstructor.h"

struct SColorMeshVertex
{
	SFVector	Vertex;
	SColor		Color;
};
// TODO: remove static_assert? + remove static_assert from linux?
static_assert (sizeof (SColorMeshVertex) == 16, "Incorrect sizeof vertex");

class CColorMeshConstructor : public CBasicMeshConstructor<SColorMeshVertex>
{
private:

	SColor					CurrentColor_;
	bool					Initialized_;

public:

	CColorMeshConstructor();
	void Init (CShaderProgram* ShaderProgram, int Size);

	void AddVertex (SFVector Vertex);
	void AddVertex (float x, float y, float z);
	
	void AddQuad (SFVector v0, SFVector v1, SFVector v2, SFVector v3);
	void AddTriangle (SFVector v0, SFVector v1, SFVector v2);
	void AddRect (SFVector v0, SFVector v1);
	void AddCircle (SFVector P, float R, int Quality);
	void AddHorizontalLine (float x, float y, float l, float w);
	void AddVerticalLine (float x, float y, float l, float w);
	void AddCylinder (SFVector P, float H, float R, int Quality);
	
	void SetColor (SColor Color);
	SColor GetCurrentColor();
	
};

#endif