﻿#include "CTextureMeshConstructor.h"

CTextureMeshConstructor::CTextureMeshConstructor() :
	Initialized_ (false)
{}

void CTextureMeshConstructor::Init (CShaderProgram* ShaderProgram, int Size)
{
	CArray<TAttribute> Attributes (3);
	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_FLOAT;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (STextureMeshVertex, Vertex);
	Attributes.Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_TextureCoord");
	Attribute.Type		 = GL_FLOAT;
	Attribute.Components = 2;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (STextureMeshVertex, TexCoord);
	Attributes.Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Color");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 4;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (STextureMeshVertex, Color);
	Attributes.Insert (Attribute);

	Init_ (Attributes, Size);
	CurrentColor_ = SColor (255, 255, 255); 
	Initialized_ = true;
}

void CTextureMeshConstructor::AddVertex (SFVector Vertex, SFVector2D TexCoord)
{
	STextureMeshVertex VertexData = { Vertex, TexCoord, CurrentColor_ };
	Verteces_.Insert (VertexData);
}

void CTextureMeshConstructor::SetColor (SColor Color)
{
	CurrentColor_ = Color;
}