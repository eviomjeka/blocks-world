﻿#ifndef CBASICMESHCONSTRUCTOR
#define CBASICMESHCONSTRUCTOR

#include "../Containers/CArray.h"
#include "../Graphics/CMesh.h"
#include "../Graphics/CShaderProgram.h"

template <typename tVertex>
class CBasicMeshConstructor
{
private:
	bool						Initialized_;
	CMesh*						CurrentMesh_;
	CArray<TAttribute>			Attributes_;

protected:

	CArray<tVertex>				Verteces_;

	void Init_(CArray<TAttribute>& Attributes, int Size);

public:
	CBasicMeshConstructor();
	~CBasicMeshConstructor();

	void LoadMesh(CStream& Stream, CMesh* Mesh);
	void Begin(CMesh* Mesh);
	bool End(CMesh* Mesh);
	
};

template<typename tVertex>
CBasicMeshConstructor<tVertex>::CBasicMeshConstructor() :
Initialized_(false), 
CurrentMesh_ (NULL)
{
}

template<typename tVertex>
CBasicMeshConstructor<tVertex>::~CBasicMeshConstructor()
{
	assert(!CurrentMesh_);
}

template<typename tVertex>
void CBasicMeshConstructor<tVertex>::Init_(CArray<TAttribute>& Attributes, int Size)
{
	assert(!Initialized_);

	Attributes_ = Attributes;
	CurrentMesh_ = NULL;
	Verteces_.Resize(Size);

	Initialized_ = true;
}


template<typename tVertex>
void CBasicMeshConstructor<tVertex>::LoadMesh(CStream& Stream, CMesh* Mesh)
{
	assert(!CurrentMesh_);
	assert(Initialized_);

	Stream >> Verteces_;
	Mesh->Init(Verteces_.Data(), Verteces_.Size(), Attributes_, sizeof (tVertex));
	Verteces_.Clear();
}

template<typename tVertex>
void CBasicMeshConstructor<tVertex>::Begin(CMesh* Mesh)
{
	assert(!CurrentMesh_);
	assert(Initialized_);

	CurrentMesh_ = Mesh;
	if (*Mesh)
		Mesh->Destroy();
}

template<typename tVertex>
bool CBasicMeshConstructor<tVertex>::End(CMesh* Mesh)
{
	assert(CurrentMesh_ == Mesh);
	assert(Initialized_);

	int Size = Verteces_.Size();

	Mesh->Init (Verteces_.Data(), Verteces_.Size(), Attributes_, sizeof (tVertex));
	Verteces_.Clear();
	CurrentMesh_ = NULL;

	return Size != 0;
}

#endif