﻿#include "CColorMeshConstructor.h"

CColorMeshConstructor::CColorMeshConstructor() :
	Initialized_ (false)
{
}

// TODO: remove CShaderProgram parameter
void CColorMeshConstructor::Init (CShaderProgram* ShaderProgram, int Size)
{
	CArray<TAttribute> Attributes (2);
	TAttribute Attribute;

	// TODO: create mesh constructor file like ShadersInfo.h
	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_FLOAT;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SColorMeshVertex, Vertex);
	Attributes.Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Color");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 4;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SColorMeshVertex, Color);
	Attributes.Insert (Attribute);

	Init_(Attributes, Size);
	Initialized_ = true;
}

void CColorMeshConstructor::AddVertex (SFVector Vertex)
{
	SColorMeshVertex VertexData = {Vertex, CurrentColor_};
	Verteces_.Insert (VertexData);
}

void CColorMeshConstructor::AddVertex (float x, float y, float z)
{
	AddVertex (SFVector (x, y, z));
}

void CColorMeshConstructor::AddQuad (SFVector v0, SFVector v1, SFVector v2, SFVector v3)
{
	AddTriangle (v0, v1, v2);
	AddTriangle (v2, v3, v0);
}

void CColorMeshConstructor::AddTriangle (SFVector v0, SFVector v1, SFVector v2)
{
	AddVertex (v0);
	AddVertex (v1);
	AddVertex (v2);
}

void CColorMeshConstructor::AddRect (SFVector v0, SFVector v1)
{
	AddQuad (SFVector (v0.x, v0.y, v0.z),
			 SFVector (v1.x, v0.y, v0.z),
			 SFVector (v1.x, v1.y, v0.z),
			 SFVector (v0.x, v1.y, v0.z));
}

void CColorMeshConstructor::AddCircle (SFVector P, float R, int Quality)
{
	float PrevX = R;
	float PrevY = 0;
	for (int i = 1; i <= Quality; i++)
	{
		float Alpha = 2 * PI * i / Quality;
		float x = R * cos (Alpha);
		float y = R * sin (Alpha);
		AddVertex (P);
		AddVertex (P + SFVector (PrevX, PrevY, P.z));
		AddVertex (P + SFVector (x, y, P.z));
		PrevX = x;
		PrevY = y;
	}
}

void CColorMeshConstructor::AddHorizontalLine (float x, float y, float l, float w)
{
	AddRect (SFVector (x, y - w / 2, 0), SFVector (x + l, y + w / 2, 0));
}

void CColorMeshConstructor::AddVerticalLine (float x, float y, float l, float w)
{
	AddRect (SFVector (x - w / 2, y, 0), SFVector (x + w / 2, y + l, 0));
}

void CColorMeshConstructor::AddCylinder (SFVector P, float H, float R, int Quality)
{
	float PrevX = R;
	float PrevZ = 0;
	for (int i = 1; i <= Quality; i++)
	{
		float Alpha = 2 * PI * i / Quality;
		float x = R * cos(Alpha);
		float z = R * sin(Alpha);
		AddQuad (
			P + SFVector(x, H, z),
			P + SFVector(x, 0, z),
			P + SFVector(PrevX, 0, PrevZ),
			P + SFVector(PrevX, H, PrevZ));
		PrevX = x;
		PrevZ = z;
	}
}

void CColorMeshConstructor::SetColor (SColor Color)
{
	CurrentColor_ = Color;
}

SColor CColorMeshConstructor::GetCurrentColor()
{
	return CurrentColor_;
}