﻿#ifndef CTEXTUREMESHCONSTRUCTOR
#define CTEXTUREMESHCONSTRUCTOR

#include "CBasicMeshConstructor.h"

struct STextureMeshVertex
{
	SFVector	Vertex;
	SFVector2D	TexCoord;
	SColor		Color;
};

static_assert (sizeof (STextureMeshVertex) == 24, "Incorrect sizeof vertex");

class CTextureMeshConstructor : public CBasicMeshConstructor<STextureMeshVertex>
{
private:

	bool					Initialized_;
	SColor					CurrentColor_;

public:

	CTextureMeshConstructor();
	void Init (CShaderProgram* ShaderProgram, int Size);
	
	void AddVertex (SFVector Vertex, SFVector2D TexCoord);

	void SetColor (SColor Color);
	
};

#endif
