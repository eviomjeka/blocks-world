﻿#ifndef CPLANE
#define CPLANE

#include "SVector.h"

class CPlane
{
private:
	float a_;
	float b_;
	float c_;
	float d_;

public:

	CPlane();
	CPlane(float a, float b, float c, float d);

	float GetDistance(SFVector p);

	void operator ()(float a, float b, float c, float d);

};

#endif