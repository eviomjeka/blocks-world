﻿#include "CCompressor.h"
#include <Engine.h>
   
void CCompressor::Compress(CBuffer& Source, CBuffer& Dest, int Level)
{
	z_stream Stream;

	Stream.zalloc = NULL;
	Stream.zfree  = NULL;
	Stream.opaque = NULL;
   
	assert(!deflateInit(&Stream, Level));

	Stream.next_in   = Source.Data();
	Stream.avail_in  = Source.WritePosition();
	Stream.next_out  = Dest.Data();
	Stream.avail_out = Dest.MaxSize();
   
	assert(deflate(&Stream, Z_FINISH) == Z_STREAM_END);
	
	Dest.SetWritePosition (Stream.total_out);

	assert (!deflateEnd(&Stream));
}

void CCompressor::Decompress(CBuffer& Source, CBuffer& Dest)
{
	z_stream Stream;

	Stream.zalloc	= NULL;
	Stream.zfree	= NULL;
	Stream.opaque	= NULL;
	Stream.avail_in	= 0;
	Stream.next_in	 = NULL;
   
	assert(!inflateInit(&Stream));

	Stream.next_in   = Source.Data();
	Stream.avail_in  = Source.WritePosition();
	Stream.next_out  = Dest.Data();
	Stream.avail_out = Dest.MaxSize();
   
	assert(inflate(&Stream, Z_NO_FLUSH) == Z_STREAM_END);

	Dest.SetWritePosition (Stream.total_out);

	assert (!inflateEnd(&Stream));
}