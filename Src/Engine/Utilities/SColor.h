﻿#ifndef SCOLOR
#define SCOLOR

#include "SVector.h"
#include "Misc.h"

struct SColor
{
	byte r;
	byte g;
	byte b;
	byte a;
	
	SColor() : r(255), g(255), b(255), a(255) {}
	SColor(byte R, byte G, byte B, byte A = 255) : r(R), g(G), b(B), a(A) {}

	SColor operator * (const SColor& color) const
	{
		int rr = r * color.r / 255;
		int gg = g * color.g / 255;
		int bb = b * color.b / 255;
		return SColor ((byte) rr, (byte) gg, (byte) bb, 255);
	}

	operator SVector<byte>() { return SVector<byte>(r, g, b); }
};

CStream& operator <<(CStream& Stream, SColor& Color);
CStream& operator >>(CStream& Stream, SColor& Color);

#endif