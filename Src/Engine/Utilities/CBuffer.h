﻿#ifndef CBUFFER
#define CBUFFER

#include "CStream.h"

//TODO: static buffer for chunk

class CBuffer : public CStream
{
private:
	byte*	Data_;
	int		MaxSize_;
	int		ReadPosition_;
	int		WritePosition_;

public:
	CBuffer();
	CBuffer(int Size);
	virtual ~CBuffer();

	CBuffer(CBuffer&);
	void operator =(CBuffer&);

	void Clear();

	virtual void Read (void* Data, int Size);
	virtual void Write (void* Data, int Size);

	int ReadPosition();
	int WritePosition();
	void SetReadPosition(int ReadPosition);
	void SetWritePosition(int WritePosition);
	int MaxSize();
	byte* Data();
};

CStream& operator <<(CStream& Stream, CBuffer& Buffer);
#endif
