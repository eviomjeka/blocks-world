﻿#ifndef CMEMORYMANAGER
#define CMEMORYMANAGER

#include <PlatformInclude.h>
#include "../Containers/CSArray.h"

template <typename T>
struct CMemoryBlock
{
	T Data;
	CMemoryBlock<T>* Next;
};

template <typename T, int N>
struct CMemoryArray
{
	CMemoryArray<T,N>* Next;
	CSArray <CMemoryBlock<T>, N> Array;

	CMemoryArray()
	{
		Next = NULL;
	}

	~CMemoryArray()
	{
		if (Next)
			delete Next;
	}
};

//==============================================================================

template <typename T, int N>
class CMemoryManager
{
private:

	CMemoryBlock<T>* FreeBlocks_;
	CMemoryArray<T, N>* Arrays_;
	int MemoryAllocated_;

public:

	void Init();
	void Destroy();
	
	T* Alloc();
	void Free (T* Data);

};

template <typename T, int N>
void CMemoryManager<T,N>::Init()
{
	FreeBlocks_ = NULL;
	Arrays_ = NULL;
	MemoryAllocated_ = 0;
}

template <typename T, int N>
void CMemoryManager<T,N>::Destroy()
{
	assert (MemoryAllocated_ == 0);

	delete Arrays_;

	FreeBlocks_ = NULL;
	Arrays_ = NULL;
}

template <typename T, int N>
T* CMemoryManager<T,N>::Alloc()
{
	if (!FreeBlocks_)
	{
		CMemoryArray<T,N>* A = new CMemoryArray<T,N>;
		assert (A);
		A->Next = Arrays_;
		Arrays_ = A;
		for (int i = 0; i < N; i++)
		{
			Arrays_->Array[i].Next = FreeBlocks_;
			FreeBlocks_ = &(Arrays_->Array[i]);
		}
	}

	CMemoryBlock<T>* B = FreeBlocks_;
	__assert (B);
	FreeBlocks_ = FreeBlocks_->Next;
	MemoryAllocated_++;
	return &B->Data;
}

template <typename T, int N>
void CMemoryManager<T,N>::Free (T* Data)
{
	__assert (Data);

	CMemoryBlock<T>* A = (CMemoryBlock<T>*) Data;
	A->Next = FreeBlocks_;
	FreeBlocks_ = A;
	MemoryAllocated_--;
}

#endif
