﻿#ifndef CCOMPRESSOR
#define CCOMPRESSOR

#include <PlatformInclude.h>
#include "CBuffer.h"

enum CCompressorLevels
{
	CCOMPRESSOR_DEFAULT_COMPRESSION	= Z_DEFAULT_COMPRESSION, 
	CCOMPRESSOR_NO_COMPRESSION		= Z_NO_COMPRESSION, 
	CCOMPRESSOR_BEST_COMPRESSION	= Z_BEST_COMPRESSION, 
	CCOMPRESSOR_BEST_SPEED			= Z_BEST_SPEED
};

class CCompressor
{
public:
   
	static void Compress (CBuffer& Source, CBuffer& Dest, int Level = CCOMPRESSOR_BEST_COMPRESSION);
	static void Decompress (CBuffer& Source, CBuffer& Dest);
   
};

#endif