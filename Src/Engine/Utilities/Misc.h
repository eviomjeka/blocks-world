﻿#ifndef MISC
#define MISC

#include "SVector.h"
#include "SVector2D.h"
#include "CString.h"

#define MOUSE_LEFT_BUTTON	0x0fffffff
#define MOUSE_RIGHT_BUTTON	0x0ffffffe
#define MOUSE_MIDDLE_BUTTON	0x0ffffffd
#define MOUSE_WHELL_UP		0x0ffffffc
#define MOUSE_WHELL_DOWN	0x0ffffffb

#define PI 3.14159265f

SIVector FloorVector(SFVector f);

int Sign(float f);
int Sign(int i);
SIVector2D Sign(SIVector2D v);

int Mod(int a, int b);
SIVector Mod(SIVector v, int b);
SIVector2D Mod2D(SIVector2D v, int b);
SIVector ModXZ(SIVector v, int b);
SIVector ModY(SIVector v, int b);
SIVector2D ModXZ2D(SIVector v, int b);

int Div(int a, int b);
SIVector Div(SIVector v, int b);
SIVector2D Div2D(SIVector2D v, int b);
SIVector DivXZ(SIVector v, int b);
SIVector DivY(SIVector v, int b);
SIVector2D DivXZ2D(SIVector v, int b);

int HashSIVector2D(SIVector2D& Vector);
int HashSIVector(SIVector& Vector);

template <typename T>
T Square(T a)
{
	return a * a;
}

#endif