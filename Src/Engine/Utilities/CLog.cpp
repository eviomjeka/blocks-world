﻿#include "CLog.h"
#include <Engine.h>

CLog CLog::Instance;

void CLog::Init (bool IsGame)
{
	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);

	CString FileName = "Logs/";
	if (!IsGame)
		FileName.Append ("Server");

	strftime (&FileName.Data()[strlen (FileName.Data())], 255, "Log [%d-%m-%y %H.%M.%S].txt", timeinfo);
	File_.Init (FileName.Data(), CFILE_WRITE);

	SetNormalColor();
}

void CLog::Destroy()
{
	File_.Destroy();
}

void CLog::Log_ (const char* Buffer)
{
	PrintLog (Buffer);

	if (!File_)
	{
		SetWarningColor();
		PrintLog ("WARNING: WRITE INFO TO LOG: ERROR! FILE NOT OPENED!\n");
		SetNormalColor();
	}
	else
	{
		File_.Print ("%s", Buffer);
		File_.Flush();
	}
}

void CLog::Log (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	
	char Buffer[LOG_STR_SIZE] = "";
	int Size = strftime (Buffer, LOG_STR_SIZE - 1, "[%H:%M:%S]: ", timeinfo);
	Size += vsnprintf (&Buffer[Size], LOG_STR_SIZE - Size - 1, Format, vl);
	
	if (Size + 1 >= LOG_STR_SIZE)
		Size = LOG_STR_SIZE - 2;
	Buffer[Size] = '\n';
	Buffer[Size + 1] = 0;

	va_end (vl);

	Log_ (Buffer);
}

void CLog::LogWithoutTime (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	char Buffer[LOG_STR_SIZE] = "";
	vsnprintf (Buffer, LOG_STR_SIZE - 1, Format, vl);

	va_end (vl);

	Log_ (Buffer);
}

void CLog::Warning (const char* Format, ...)
{
	va_list vl;
	va_start (vl, Format);

	time_t seconds = time (NULL);
	tm* timeinfo = localtime (&seconds);
	
	char Buffer[LOG_STR_SIZE] = "";
	int Size = strftime (Buffer, LOG_STR_SIZE - 1, "[%H:%M:%S]: ", timeinfo);
	strncpy (&Buffer[Size], "WARNING: ", LOG_STR_SIZE - Size - 1);
	Size = strlen (Buffer);
	Size += vsnprintf (&Buffer[Size], LOG_STR_SIZE - Size - 1, Format, vl);
	
	if (Size + 1 >= LOG_STR_SIZE)
		Size = LOG_STR_SIZE - 2;
	Buffer[Size] = '\n';
	Buffer[Size + 1] = 0;

	va_end (vl);

	SetWarningColor();
	Log_ (Buffer);
	SetNormalColor();
}