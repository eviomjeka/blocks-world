﻿#include "CPlane.h"
#include <Engine.h>

CPlane::CPlane()
{
}

CPlane::CPlane(float a, float b, float c, float d)
{
	float Len = sqrt(a * a + b * b + c * c);

	a_ = a / Len;
	b_ = b / Len;
	c_ = c / Len;
	d_ = d / Len;
}

float CPlane::GetDistance(SFVector p)
{
	return  a_ * p.x + b_ * p.y + c_ * p.z + d_;
}

void CPlane::operator ()(float a, float b, float c, float d)
{
	*this = CPlane(a, b, c, d);
}