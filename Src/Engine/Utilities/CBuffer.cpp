﻿#include "CBuffer.h"
#include <Engine.h>

CBuffer::CBuffer(int MaxSize) :
Data_(new byte[MaxSize]),
MaxSize_(MaxSize),
ReadPosition_(0),
WritePosition_(0)
{
}

void CBuffer::Read (void* Data, int Size)
{
	assert(ReadPosition_ + Size <= WritePosition_);

	byte* DataByte = (byte*) Data;
	for (int i = 0; i < Size; i++, ReadPosition_++)
		DataByte[i] = Data_[ReadPosition_];
}

void CBuffer::Write (void* Data, int Size)
{
	assert(WritePosition_ + Size <= MaxSize_);
	
	byte* DataByte = (byte*) Data;
	for (int i = 0; i < Size; i++, WritePosition_++)
		Data_[WritePosition_] = DataByte[i];
}

int CBuffer::MaxSize()
{
	return MaxSize_;
}

CBuffer::~CBuffer()
{
	delete[] Data_;
}

void CBuffer::Clear()
{
	ReadPosition_ = 0;
	WritePosition_ = 0;
}

int CBuffer::ReadPosition()
{
	return ReadPosition_;
}

int CBuffer::WritePosition()
{
	return WritePosition_;
}

void CBuffer::SetReadPosition(int ReadPosition)
{
	assert(ReadPosition <= MaxSize_);
	ReadPosition_ = ReadPosition;
}

void CBuffer::SetWritePosition(int WritePosition)
{
	assert(WritePosition <= MaxSize_);
	WritePosition_ = WritePosition;
}

byte* CBuffer::Data()
{
	return Data_;
}

CStream& operator <<(CStream& Stream, CBuffer& Buffer)
{
	Stream.Write(Buffer.Data(), Buffer.WritePosition());

	return Stream;
}