﻿#ifndef CSTRING
#define CSTRING

#include "CStream.h"

#define MAX_STR_SIZE 512 // MAX_STR_SIZE >= MAX_PATH

class CString
{
protected:

	char Data_[MAX_STR_SIZE];
	int Length_;

public:

	CString();
	CString(const char* String);
	CString(const char* String, int Start, int End);
	CString(CString& String, int Start, int End);

	void Print  (const char* Format, ...);
	void VPrint (const char* Format, va_list Args);

	static CString EmptyString;

	void Append (CString& String);
	void Append (const char* String);
	void Shorten(int Length);
	bool operator == (CString& String);
	void operator =(const char* String);
	char& operator [](int i);
	void operator +=(char c);

	int Length();
	bool Empty();
	char* Data();
	void Clear();

	static int Hash (CString& String);

};

CStream& operator <<(CStream& Stream, CString& String);
CStream& operator >>(CStream& Stream, CString& String);

#endif