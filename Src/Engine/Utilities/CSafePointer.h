﻿#ifndef CSAFEPOINTER
#define CSAFEPOINTER

#include <Engine.h>

template<typename T>
class CSafePointer
{
private:

	T* Data_;

public:

	CSafePointer();
	~CSafePointer();

	void Release();
	void operator () (T* Data);
	T* operator -> () const;
	T* Data() const;
	bool Exist() const;

};

template<typename T>
CSafePointer<T>::CSafePointer() :
	Data_ (NULL)
{}

template<typename T>
CSafePointer<T>::~CSafePointer()
{
	assert (Data_ == NULL);
}
template<typename T>
void CSafePointer<T>::Release()
{
	delete Data_;
	Data_ = NULL;
}

template<typename T>
void CSafePointer<T>::operator () (T* Data)
{
	Data_ = Data;
}

template<typename T>
T* CSafePointer<T>::operator -> () const
{
	__assert (Data_);
	return Data_;
}

template<typename T>
T* CSafePointer<T>::Data() const
{
	return Data_;
}

template<typename T>
bool CSafePointer<T>::Exist() const
{
	return Data_ != NULL;
}

#endif