﻿#include "SColor.h"
#include <Engine.h>

CStream& operator <<(CStream& Stream, SColor& Color)
{
	return Stream << Color.r << Color.g << Color.b << Color.a;
}

CStream& operator >>(CStream& Stream, SColor& Color)
{
	return Stream >> Color.r >> Color.g >> Color.b >> Color.a;
}