﻿#include "CSkyMeshConstructor.h"

CSkyMeshConstructor::CSkyMeshConstructor() :
	Initialized_ (false),
	TriangleStrip_ (-1)
{}

void CSkyMeshConstructor::Init (CShaderProgram* ShaderProgram, int Size)
{
	CArray<TAttribute> Attributes (1);
	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_FLOAT;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SSkyMeshVertex, Vertex);
	Attributes.Insert (Attribute);
	
	Init_ (Attributes, Size);
	TriangleStrip_ = 0;
	Initialized_ = true;
}

void CSkyMeshConstructor::AddVertex (SFVector Vertex)
{
	SSkyMeshVertex VertexData = { Vertex };
	Verteces_.Insert (VertexData);
}

void CSkyMeshConstructor::BeginTriangleStrip()
{
	TriangleStrip_ = 0;
}

void CSkyMeshConstructor::AddTriangleStrip (SFVector Vertex)
{
	if (TriangleStrip_ <= 2)
	{
		AddVertex (Vertex);
		TriangleStrip_++;
		return;
	}
	
	AddVertex (Verteces_[Verteces_.Size() - 2].Vertex);
	AddVertex (Verteces_[Verteces_.Size() - 2].Vertex);
	AddVertex (Vertex);

	TriangleStrip_++;
}