﻿#ifndef CBLOCKSMESHQUEUE
#define CBLOCKSMESHQUEUE

#include <Engine.h>

struct SBlocksMeshVertex
{
	SVector<byte>	Vertex;
	SVector2D<byte>	TextureCoord;
	SVector2D<byte>	LightVector;
};

static_assert (sizeof (SBlocksMeshVertex) == 7, "Incorrect sizeof vertex");

class CBlocksMeshQueue : public CBasicMeshQueue<SBlocksMeshVertex, SIVector>
{
private:

	bool Initialized_;

public:

	CBlocksMeshQueue();
	void Init (CShaderProgram* ShaderProgram, int Size, int MaxMeshes);

	void AddVertex (SVector<byte> Vertex, SVector2D<byte> TextureCoord, SVector2D<byte> LightVector);

	static void GetAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes, int* ElementSize);
	static void GetPositionAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes);

};

#endif