﻿#ifndef CCOMPLEXBLOCKSMESHQUEUE
#define CCOMPLEXBLOCKSMESHQUEUE

#include <Engine.h>

struct SComplexBlocksMeshVertex
{
	SVector<short>				Vertex;
	SVector2D<unsigned short>	TextureCoord;
	SVector2D<byte>				LightVector;
};

static_assert (sizeof (SComplexBlocksMeshVertex) == 12, "Incorrect sizeof vertex");

class CComplexBlocksMeshQueue : public CBasicMeshQueue<SComplexBlocksMeshVertex, SIVector>
{
private:

	bool Initialized_;

public:

	CComplexBlocksMeshQueue();
	void Init (CShaderProgram* ShaderProgram, int Size, int MaxMeshes);
	
	void AddVertex (SVector<short> Vertex, SVector2D<unsigned short> TextureCoord, SVector2D<byte> LightVector);
	
	static void GetAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes, int* ElementSize);
	static void GetPositionAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes);

};

#endif