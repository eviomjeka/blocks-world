﻿#ifndef CSKYMESHCONSTRUCTOR
#define CSKYMESHCONSTRUCTOR

#include <Engine.h>

struct SSkyMeshVertex
{
	SFVector Vertex;
};

static_assert (sizeof (SSkyMeshVertex) == 12, "Incorrect sizeof vertex");

class CSkyMeshConstructor : public CBasicMeshConstructor<SSkyMeshVertex>
{
private:
	bool Initialized_;
	int  TriangleStrip_;

public:

	CSkyMeshConstructor();
	void Init (CShaderProgram* ShaderProgram, int Size);

	void AddVertex (SFVector Vertex);

	void BeginTriangleStrip();
	void AddTriangleStrip (SFVector Vertex);

};

#endif