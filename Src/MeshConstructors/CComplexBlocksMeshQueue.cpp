﻿#include "CComplexBlocksMeshQueue.h"

CComplexBlocksMeshQueue::CComplexBlocksMeshQueue() :
	Initialized_(false)
{}

void CComplexBlocksMeshQueue::Init(CShaderProgram* ShaderProgram, int Size, int MaxMeshes)
{
	CArray<TAttribute> Attributes;
	int ElementSize = 0;
	GetAttributes (ShaderProgram, &Attributes, &ElementSize);

	Init_(Attributes, Size, MaxMeshes);
	Initialized_ = true;
}

void CComplexBlocksMeshQueue::AddVertex (SVector<short> Vertex, SVector2D<unsigned short> TextureCoord, SVector2D<byte> LightVector)
{
	SComplexBlocksMeshVertex VertexData = { Vertex, TextureCoord, LightVector };
	AddVertex_ (VertexData);
}

void CComplexBlocksMeshQueue::GetAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes, int* ElementSize)
{
	Attributes->Resize (3);

	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_SHORT; // TODO: use float? more speed needed
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SComplexBlocksMeshVertex, Vertex);
	Attributes->Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_TextureCoord");
	Attribute.Type		 = GL_UNSIGNED_SHORT;
	Attribute.Components = 2;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SComplexBlocksMeshVertex, TextureCoord);
	Attributes->Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_LightVector");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 2;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SComplexBlocksMeshVertex, LightVector);
	Attributes->Insert (Attribute);

	(*ElementSize) = sizeof (SComplexBlocksMeshVertex);
}

void CComplexBlocksMeshQueue::GetPositionAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes)
{
	Attributes->Resize (1);
	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_SHORT;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SComplexBlocksMeshVertex, Vertex);
	Attributes->Insert(Attribute);
}