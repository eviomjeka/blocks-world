﻿#include "CBlocksMeshQueue.h"

CBlocksMeshQueue::CBlocksMeshQueue() :
	Initialized_ (false)
{}

void CBlocksMeshQueue::Init (CShaderProgram* ShaderProgram, int Size, int MaxMeshes)
{
	CArray<TAttribute> Attributes;
	int ElementSize = 0;
	GetAttributes (ShaderProgram, &Attributes, &ElementSize);

	Init_ (Attributes, Size, MaxMeshes);
	Initialized_ = true;
}

void CBlocksMeshQueue::AddVertex (SVector<byte> Vertex, SVector2D<byte> TextureCoord, SVector2D<byte> LightVector)
{
	SBlocksMeshVertex VertexData = { Vertex, TextureCoord, LightVector };
	AddVertex_ (VertexData);
}

void CBlocksMeshQueue::GetAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes, int* ElementSize)
{
	Attributes->Resize (3);

	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SBlocksMeshVertex, Vertex);
	Attributes->Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_TextureCoord");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 2;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SBlocksMeshVertex, TextureCoord);
	Attributes->Insert (Attribute);

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_LightVector");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 2;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SBlocksMeshVertex, LightVector);
	Attributes->Insert (Attribute);

	(*ElementSize) = sizeof (SBlocksMeshVertex);
}

void CBlocksMeshQueue::GetPositionAttributes (CShaderProgram* ShaderProgram, CArray<TAttribute>* Attributes)
{
	Attributes->Resize (1);
	TAttribute Attribute;

	Attribute.Index		 = ShaderProgram->GetAttributeLocation ("Vertex_Position");
	Attribute.Type		 = GL_UNSIGNED_BYTE;
	Attribute.Components = 3;
	Attribute.Offset	 = ATTRIBUTE_OFFSET (SBlocksMeshVertex, Vertex);
	Attributes->Insert (Attribute);
}