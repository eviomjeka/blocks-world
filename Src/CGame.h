#ifndef CGAME
#define CGAME

#include <Engine.h>
#include "World/CWorld.h"
#include "MapLoader/CMapLoader.h"

class CGame
{
protected:
	CWorld							World_;
	int								PlayerID_;

public:
	virtual CMapLoader& MapLoader()													= 0;
	virtual bool Initialized()														= 0;
	virtual void Step()																= 0;
	virtual void Destroy()															= 0;
	virtual void InteractWithBlock(SInteractWithBlockInfo InteractWithBlockInfo)	= 0;
	virtual void RemoveBlock(SIVector Position)										= 0;
	virtual void ChatMessage(CString& Message)										= 0;
	virtual void SetTime(int Time)													= 0;
	virtual void FlyPlayer(int PlayerID, bool FlyMode)								= 0;


	CWorld&	GetWorld();
	int PlayerID();



};

#endif