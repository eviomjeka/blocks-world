﻿#ifndef CAPPLICATIONMODULE
#define CAPPLICATIONMODULE

#include <Engine.h>
#include "Network/CServer.h"
#include "Network/CClient.h"
#include "CPlayerController.h"
#include "CSettings.h"
#include "GUI/GUIInclude.h"
#include "Single/CSingle.h"

#define APPMODULE CApplication::ApplicationModule

#define SERVER			APPMODULE.Server
#define GUIRENDERER		APPMODULE.GUIRenderer
#define MATRIXENGINE	APPMODULE.MatrixEngine
#define FONT			APPMODULE.Font

#define CLIENTWORLD			CLIENT.GetWorld()
#define SETTINGS			APPMODULE.Settings
#define CLIENT				(*APPMODULE.GameData->Game) // TODO: fix
#define PLAYERCONTROLLER	APPMODULE.GameData->PlayerController
#define INVENTORY			APPMODULE.GameData->Inventory
#define CHATWINDOW			APPMODULE.GameData->ChatWindow

struct CGameData
{
	CGame*				Game;
	CPlayerController	PlayerController;
	CInventory			Inventory;
	CChatWindow			ChatWindow;
};

class CApplicationModule
{
public:
	
	static const char* VERSION;
	static const unsigned short STANDART_PORT;
	
	CSafePointer<CGameData> GameData;

	CServer			Server;
	CGUIRenderer	GUIRenderer;
	CMatrixEngine	MatrixEngine;
	CFont			Font;
	CSettings		Settings;

	CApplicationModule();

	void SetIsGameMode (bool IsGameMode, int Port);
	bool IsGameMode();

	void Init();
	void PrepareDirectories();
	void InitLog();
	void InitOther();
	void Version (CString& Version);
	
	void LaunchServerIfNeeded();
	void StartServer (CString& WorldName);
	void StopServer();
	void DestroyGameAndServer();
	
	void UpdateFPS (int Time);
	int FPS();
	int Port();
	void TakeScreenshot();

private:

	void ServerThread_(const char* WorldName);
	static void _ServerThread_(void* WorldName);

	bool IsGameMode_;
	int FPS_;
	int Frames_;
	int Time_;
	int Port_;
	volatile bool ServerThreadReady_;
	volatile bool ServerThreadLaunched_;

};

#endif
