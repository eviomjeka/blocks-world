﻿#include "CSettings.h"
#include "CApplication.h"

CSettings::CSettings()
{
}

void CSettings::Load()
{
	CFile File ("Settings", CFILE_READ | CFILE_BINARY, false);

	if (!File)
	{
		SightRange = 6;
		MouseSensitivity = 50;
		MouseCoef = GetMouseCoef_();

		Shadows			= true;
		SmoothLighting	= true;
		RunFullscreen	= false;
		ShowFPS			= false;
		VSync			= true;
		ThirdPerson		= false;

		KeyForward		 = KEY_W;
		KeyBackward		 = KEY_S;
		KeyLeftward		 = KEY_A;
		KeyRightward	 = KEY_D;
		KeyJump			 = KEY_SPACE;
		KeyPrevBlock	 = MOUSE_WHELL_UP;
		KeyNextBlock	 = MOUSE_WHELL_DOWN;
		KeyAddBlock		 = MOUSE_LEFT_BUTTON;
		KeyRemoveBlock	 = MOUSE_RIGHT_BUTTON;
		KeyInventory	 = KEY_R;
		KeyChat			 = KEY_ENTER;
		KeyScreenshot	 = KEY_F2;

		Save();
	}
	else
	{
		File.Read (&SightRange, 1);
		assert (SightRange >= MIN_SIGHT_RANGE && SightRange <= MAX_SIGHT_RANGE);
		File.Read (&MouseSensitivity, 1);
		assert (MouseSensitivity >= 0 && MouseSensitivity <= 100);
		MouseCoef = GetMouseCoef_();
		byte Data = 0;
		File.Read (&Data, 1);
		Shadows			= (Data & (1 << 0)) != 0;
		SmoothLighting	= (Data & (1 << 1)) != 0;
		ShowFPS			= (Data & (1 << 2)) != 0;
		VSync			= (Data & (1 << 3)) != 0;
		RunFullscreen	= (Data & (1 << 4)) != 0;
		ThirdPerson		= (Data & (1 << 5)) != 0;

#define Read(ReadKey) File.Read (&ReadKey, sizeof (unsigned)); ReadKey = SettingsKeyToKey_ (ReadKey)

		Read (KeyForward);
		Read (KeyBackward);
		Read (KeyLeftward);
		Read (KeyRightward);
		Read (KeyJump);
		Read (KeyPrevBlock);
		Read (KeyNextBlock);
		Read (KeyAddBlock);
		Read (KeyRemoveBlock);
		Read (KeyInventory);
		Read (KeyChat);
		Read (KeyScreenshot);

#undef Read

		File.Destroy();
	}
}

void CSettings::Save()
{
	CFile File("Settings", CFILE_WRITE | CFILE_BINARY);

	File.Write (&SightRange, 1);
	File.Write (&MouseSensitivity, 1);
	MouseCoef = GetMouseCoef_();
	byte Data = (Shadows != 0)			* (1 << 0) + 
				(SmoothLighting != 0)	* (1 << 1) + 
				(ShowFPS != 0)			* (1 << 2) + 
				(VSync != 0)			* (1 << 3) + 
				(RunFullscreen != 0)	* (1 << 4) + 
				(ThirdPerson != 0)		* (1 << 5);
	File.Write (&Data, 1);

	int Key = 0;
#define Write(WriteKey) Key = KeyToSettingsKey_ (WriteKey); File.Write (&Key, sizeof (unsigned))
	
	Write (KeyForward);
	Write (KeyBackward);
	Write (KeyLeftward);
	Write (KeyRightward);
	Write (KeyJump);
	Write (KeyPrevBlock);
	Write (KeyNextBlock);
	Write (KeyAddBlock);
	Write (KeyRemoveBlock);
	Write (KeyInventory);
	Write (KeyChat);
	Write (KeyScreenshot);

#undef Write

	File.Destroy();
}

float CSettings::GetMouseCoef_()
{
	if (MouseSensitivity < 50)
		return (0.1f + 0.9f * (MouseSensitivity - 0.0f) / 50.0f) * 0.3f;

	return (1.0f + 4.0f * (MouseSensitivity - 50.0f) / 50.0f) * 0.3f;
}

#define TRANSLATE_KEYS(ErrorStr) switch (Key) {												\
KEY (KEY_UP); KEY (KEY_DOWN); KEY (KEY_LEFT); KEY (KEY_RIGHT);								\
KEY (KEY_HOME); KEY (KEY_END); KEY (KEY_PAGE_DOWN); KEY (KEY_PAGE_UP);						\
KEY (KEY_DELETE); KEY (KEY_INSERT); KEY (KEY_PAUSE_BREAK); KEY (KEY_PRINTSCREEN);			\
KEY (KEY_BACKSPACE); KEY (KEY_ENTER); KEY (KEY_ESCAPE); KEY (KEY_TILDE); KEY (KEY_TAB);		\
KEY (KEY_CAPS_LOOK); KEY (KEY_SHIFT); KEY (KEY_CONTROL); KEY (KEY_ALT); KEY (KEY_SPACE);	\
KEY (KEY_F1); KEY (KEY_F2); KEY (KEY_F3); KEY (KEY_F4); KEY (KEY_F5); KEY (KEY_F6);			\
KEY (KEY_F7); KEY (KEY_F8);  KEY (KEY_F9);  KEY (KEY_F10);  KEY (KEY_F11); KEY (KEY_F12);	\
KEY (KEY_A); KEY (KEY_B); KEY (KEY_C); KEY (KEY_D); KEY (KEY_E); KEY (KEY_F); KEY (KEY_G);	\
KEY (KEY_H); KEY (KEY_I); KEY (KEY_J); KEY (KEY_K); KEY (KEY_L); KEY (KEY_M); KEY (KEY_N);	\
KEY (KEY_O); KEY (KEY_P); KEY (KEY_Q); KEY (KEY_R); KEY (KEY_S); KEY (KEY_T); KEY (KEY_U);	\
KEY (KEY_V); KEY (KEY_W); KEY (KEY_X); KEY (KEY_Y); KEY (KEY_Z);							\
KEY (KEY_0); KEY (KEY_1); KEY (KEY_2); KEY (KEY_3); KEY (KEY_4); KEY (KEY_5); KEY (KEY_6);	\
KEY (KEY_7); KEY (KEY_8); KEY (KEY_9); KEY (KEY_MINUS); KEY (KEY_PLUS);						\
KEY (KEY_LEFT_BRACKET); KEY (KEY_RIGHT_BRACKET); KEY (KEY_COLON); KEY (KEY_QUOTE);			\
KEY (KEY_BACKSLASH); KEY (KEY_COMMA); KEY (KEY_PERIOD); KEY (KEY_SLASH);					\
KEY (KEY_NUM0); KEY (KEY_NUM1); KEY (KEY_NUM2); KEY (KEY_NUM3); KEY (KEY_NUM4);				\
KEY (KEY_NUM5); KEY (KEY_NUM6); KEY (KEY_NUM7); KEY (KEY_NUM8); KEY (KEY_NUM9);				\
KEY (KEY_NUMADD); KEY (KEY_NUMSUBTRACT); KEY (KEY_NUMMULTIPLY);								\
KEY (KEY_NUMDIVIDE); KEY (KEY_NUMDECIMAL);													\
case MOUSE_LEFT_BUTTON: case MOUSE_RIGHT_BUTTON: case MOUSE_MIDDLE_BUTTON:					\
case MOUSE_WHELL_UP: case MOUSE_WHELL_DOWN: return Key;										\
default: error (ErrorStr); return 0; }

#define KEY(a); case SETTINGS_ ##a: return a;
int CSettings::SettingsKeyToKey_ (int Key)
{
	TRANSLATE_KEYS ("Load settings: incorrect key code");
}
#undef KEY

#define KEY(a); case a: return SETTINGS_ ##a;
int CSettings::KeyToSettingsKey_ (int Key)
{
	TRANSLATE_KEYS ("Save settings: incorrect key code");
}
#undef KEY

#undef TRANSLATE_KEYS
