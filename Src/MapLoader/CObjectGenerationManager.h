#ifndef CGENERATIONMANAGER
#define CGENERATIONMANAGER

#include "../World/CSubWorld.h"
#include "../WorldGenerator/CWorldObjectGenerator.h"
#include "CGenerationMap.h"

#define MAX_GENERATION_OBJECTS 1024

class CMapLoader;

class CObjectGenerationManager
{

private:
	int									Seed_;
	CMapLoader*							MapLoader_;
	CArray<CWorldObjectGenerator*>		ObjectGenerators_;


	void GetSubWorldObjects_(SIVector2D Position, CArray<CWorldObject>& Objects);
	void RemoveConflictingObjects_(CArray<CWorldObject>& Objects);
	void GetGenerationMap_(CGenerationMap& GenerationMap, CArray<CWorldObject>& Objects, CSubWorld& SubWorld);
	void ReleaseGenerationMap_(CGenerationMap& GenerationMap);
	void GenerateObjects_(CGenerationMap& GenerationMap, CArray<CWorldObject>& Objects);
	
public:
	void Init(CMapLoader* MapLoader, int Seed);
	void Destroy();

	void AddObjectGenerator(CWorldObjectGenerator* ObjectGenerator);
	void GenetateSubWorld(CSubWorld& SubWorld);



};

#endif