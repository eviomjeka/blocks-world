#include "CGenerationMap.h"


CGenerationMap::CGenerationMap(CSubWorld& Central) :
	Position_(Central.Position())
{
	for (int i = 0; i < Size(); i++)
		(*this)[i] = NULL;

	(*this)(0, 0) = &Central;
}

CBlockData CGenerationMap::GetBlock (SIVector Position)
{
	//LOG("!!!!!!!!!!!!!!!!!!!!!GETBLOCK %d %d", Position.x, Position.z);
	SIVector2D SubWorldPosition = DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	SIVector   BlockPosition    = ModXZ  (Position, SUBWORLD_SIZE_XZ);

	CSubWorld* SubWorld = (*this)(SubWorldPosition - Position_);
	assert(SubWorld);

	assert(SubWorld->Valid(SubWorldPosition));
	return SubWorld->GetBlock(BlockPosition);
}

void CGenerationMap::SetBlock (SIVector Position, CBlockData Block)
{
	//LOG("!!!!!!!!!!!!!!!!!!!!!SETBLOCK %d %d", Position.x, Position.z);
	SIVector2D SubWorldPosition = DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	SIVector   BlockPosition    = ModXZ  (Position, SUBWORLD_SIZE_XZ);

	CSubWorld* SubWorld = (*this)(SubWorldPosition - Position_);
	assert(SubWorld);

	assert(SubWorld->Valid(SubWorldPosition));
	SubWorld->SetBlock(BlockPosition, Block);
}