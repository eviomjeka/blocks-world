#ifndef CLIGHTINGMANAGER
#define CLIGHTINGMANAGER

#include <Engine.h>
#include "../World/CClientSubWorld.h"
#include "../MeshConstructors/CBlocksMeshQueue.h"
#include "../CLightingEngine.h"

class CMapLoader;

class CLightingManager
{

private:
	struct tLightingRequest
	{
		SIVector2D	Position;
		int			Targets;
	};

private:
	volatile bool						LighterThreadReady_;
	volatile int						LightingsApplied_;
	CArray<tLightingRequest>			LightingRequests_;
	CArray<SIVector2D>					LockedSubWorlds_;
	CMutex								Mutex_;
	CMapLoader*							MapLoader_;
	CLightingEngine						LightingEngine_;

	void LighterThread_();
	void ApplyLighting_			(tLightingRequest LightingRequest);
	void GenerateSubWorldMeshesIfNeeded_(SIVector2D Position);
	void CancelLighting_(int RequestID);
	int FindNearestLightingJob_();
	int GetLightingTargets_(SIVector2D Position);

public:
	CLightingManager();

	void InvalidateLighting		(SIVector2D Position);
	void InvalidateMeshes		(SIVector2D Position);
	void RequestLighting		(SIVector2D Position);
	void CancelLightingsAround	(SIVector2D Position);

	void Init(CMapLoader* MapLoader);
	void Destroy();

private:
	static void _LighterThread_(void* This);


};

#endif
