#ifndef CCHUNKCACHE
#define CCHUNKCACHE

#include "../World/CChunk.h"
#include <Engine.h>

class CChunkCache :
	public CCache<SIVector2D, CChunk, SIVector2D, false>
{

public:
	virtual int CalculatePriority(SIVector2D& Key);

};


#endif