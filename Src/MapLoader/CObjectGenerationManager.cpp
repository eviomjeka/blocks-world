#include "CObjectGenerationManager.h"
#include "CMapLoader.h"

void CObjectGenerationManager::GenetateSubWorld(CSubWorld& SubWorld)
{
	CArray<CWorldObject> Objects(MAX_GENERATION_OBJECTS);
	GetSubWorldObjects_(SubWorld.Position(), Objects);

	CGenerationMap GenerationMap(SubWorld);
	GetGenerationMap_(GenerationMap, Objects, SubWorld);

	GenerateObjects_(GenerationMap, Objects);

	GenerationMap(0, 0) = NULL;
	ReleaseGenerationMap_(GenerationMap);
}

void CObjectGenerationManager::RemoveConflictingObjects_(CArray<CWorldObject>& Objects)
{	
	for (int i = Objects.Size() - 1; i >= 0; i--)
		for (int j = i - 1; j >= 0; j--)
			if (Objects[i].Intersects(Objects[j]) && !Objects[i].CanIntersect(Objects[j]))
				Objects[Objects[i].Replaces(Objects[j]) ? j : i] = CWorldObject();

	for (int i = Objects.Size() - 1; i >= 0; i--)
		if (!Objects[i].Valid())
			Objects.Remove(i);
}

void CObjectGenerationManager::GetSubWorldObjects_(SIVector2D Position, CArray<CWorldObject>& Objects)
{
	for (int i = 0; i < ObjectGenerators_.Size(); i++)
	{
		CWorldObjectGenerator& ObjectGenerator = *ObjectGenerators_[i];
		SIVector2D SectorPosition = Div2D(Position, ObjectGenerator.MaxSize());

		for (int x = -1; x <= +1; x++)
			for (int y = -1; y <= +1; y++)
				ObjectGenerator.GetObjects(SectorPosition + SIVector2D(x, y), Objects);
	}

	for (int i = Objects.Size() - 1; i >= 0; i--)
		if (!Position.InsideOrOnAABB(Objects[i].FirstSubWorld(), Objects[i].LastSubWorld()))
			Objects.Remove(i);

	RemoveConflictingObjects_(Objects);
}

void CObjectGenerationManager::GetGenerationMap_(CGenerationMap& GenerationMap, CArray<CWorldObject>& Objects, CSubWorld& SubWorld)
{
	for (int i = 0; i < Objects.Size(); i++)
	{
		CWorldObject& Object = Objects[i];

		for (int x = Object.FirstSubWorld().x; x <= Object.LastSubWorld().x; x++)
			for (int y = Object.FirstSubWorld().y; y <= Object.LastSubWorld().y; y++)
			{
				SIVector2D CurrentPosition(x, y);

				if (!GenerationMap(CurrentPosition - SubWorld.Position()))
				{
					CSubWorld& CurrentSubWorld = MapLoader_->GetSubWorldForGeneration(CurrentPosition);
					GenerationMap(CurrentPosition - SubWorld.Position()) = &CurrentSubWorld;
				}
			}
	}
}

void CObjectGenerationManager::GenerateObjects_(CGenerationMap& GenerationMap, CArray<CWorldObject>& Objects)
{
	CSubWorld& SubWorld = *GenerationMap(0, 0);

	for (int i = 0; i < Objects.Size(); i++)
	{
		CWorldObject& Object = Objects[i];

		if (!SubWorld.IsGenerated(Object))
		{
			LOG("Generating object (%d %d %d %d) for (%d %d)", Object.Position().x, Object.Position().y, Object.Size().x, Object.Size().y, SubWorld.Position().x, SubWorld.Position().y);
			ObjectGenerators_[Object.ID()]->Generate(&GenerationMap, Object);


			for (int x = Object.FirstSubWorld().x; x <= Object.LastSubWorld().x; x++)
				for (int y = Object.FirstSubWorld().y; y <= Object.LastSubWorld().y; y++)
				{
					CSubWorld& CurrentSubWorld = *GenerationMap(SIVector2D(x, y) - SubWorld.Position());
					assert(&CurrentSubWorld);
					assert(CurrentSubWorld.Valid(SIVector2D(x, y)));
					CurrentSubWorld.SetGenerated(Object);
				}
		}
	}
}

void CObjectGenerationManager::ReleaseGenerationMap_(CGenerationMap& GenerationMap)
{
	for (int i = 0; i < GenerationMap.Size(); i++)
	{
		CSubWorld* SubWorld = GenerationMap[i];

		if (SubWorld)
			MapLoader_->ReleaseSubWorldForGeneration(*SubWorld);
	}
}

void CObjectGenerationManager::AddObjectGenerator(CWorldObjectGenerator* ObjectGenerator)
{
	ObjectGenerator->Init(ObjectGenerators_.Size(), Seed_);
	ObjectGenerators_.Insert(ObjectGenerator, true);
}

void CObjectGenerationManager::Init(CMapLoader* MapLoader, int Seed)
{
	MapLoader_	= MapLoader;
	Seed_		= Seed;
}

void CObjectGenerationManager::Destroy()
{
	for (int i = 0; i < ObjectGenerators_.Size(); i++)
		delete ObjectGenerators_[i];

	ObjectGenerators_.Clear();
}
