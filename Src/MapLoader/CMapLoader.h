#ifndef CMAPLOADER
#define CMAPLOADER

#include <Engine.h>
#include "CChunkCache.h"
#include "CSubWorldCache.h"
#include "CLightingManager.h"
#include "CObjectGenerationManager.h"
#include "../WorldGenerator/CTrees.h"
#include "../WorldGenerator/CStones.h"

class CWorld;
class CMapLoader :
	public CBlockManager
{

private:
	CBuffer								CompressionBuffer1_;
	CBuffer								CompressionBuffer2_;

	bool								Initialized_;
	SIVector2D							OldPosition_;
	CLightingManager 					LightingManager_;
	char								MapDirectory_[MAX_PATH];
	CChunkCache							ChunkCache_;
	CObjectGenerationManager			ObjectGenerationManager_;

protected:		
	CSubWorldCache						SubWorldCache_;
	CWorld*								World_;
	volatile int						LoadingStepsPassed_;


protected:
	void PrepareSubWorld_	(CClientSubWorld& SubWorld);
	void AddNeighbours_		(CClientSubWorld& SubWorld);
	void RemoveNeighbours_	(CClientSubWorld& SubWorld);
	void AddNeighbour_		(CClientSubWorld& SubWorld, CClientSubWorld& Neighbour);
	void RemoveNeighbour_	(CClientSubWorld& SubWorld, CClientSubWorld& Neighbour);

	void LoadSubWorld_		(CClientSubWorld& SubWorld, SIVector2D Position, bool Dependency = false);
	void UnloadSubWorld_	(CClientSubWorld& SubWorld, bool Destruction = false);
	void SaveSubWorld_		(CClientSubWorld& SubWorld);
	void SaveSubWorldData_	(SIVector2D Position, CBuffer& Data, int Version);
	void GenerateObjects_	(CClientSubWorld& SubWorld);

	void ApplyLightings_	();
	void UpdateOnChange_	(CClientSubWorld& SubWorld);
	void CheckLightings_	(CClientSubWorld& SubWorld, int R);
	int GetSubWorldVersion_	(SIVector2D Position);


	CChunk& GetChunk_(SIVector2D Position, int ReferenceDelta = 0);
	//CClientSubWorld GetSubWorld_(SIVector2D Position);
	friend class CLightingManager;





public:
	CMapLoader();

	void Init(const char* MapDirectory, int CacheSize, CWorld* World);
	void Destroy();

	virtual CBlockData GetBlock(SIVector Position);
	virtual void SetBlock(SIVector Position, CBlockData Block);
	SVector2D<byte> GetLight(SIVector Position);

	void GetNeighbours_(SIVector2D Position, CClientSubWorlds3x3& Neighbours);
	CSubWorldCache& GetSubWorldCache();

	CSubWorld& GetSubWorldForGeneration(SIVector2D Position);
	void ReleaseSubWorldForGeneration(CSubWorld& SubWorld);



	virtual int LoadingSteps_()			= 0;
	virtual float PercentsLoaded()		= 0;
	virtual bool GameStarted()			= 0;
	virtual int SightRange()			= 0;
	virtual bool Valid()				= 0;
};

#endif
