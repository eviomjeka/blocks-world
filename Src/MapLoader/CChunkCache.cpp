#include "CChunkCache.h"


int CChunkCache::CalculatePriority(SIVector2D& Key)
{
	//LOG("Calculating priority for chunk at %d %d", ChunkEntry.Key.x, ChunkEntry.Key.y);
	//return (ChunkEntry.Key - PlayerPosition).SqLen();
	SIVector2D P = Key - PriorityDecider_;
	return max(abs(P.x), abs(P.y));
}

/*


CChunk& CMapLoader::GetChunk_(SIVector2D Position)
{
	CChunk& Chunk = Chunks_(Position);

	if (!Chunk.Valid(Position))
	{
		Chunk.Destroy(MapDirectory_);
		Chunk.Init(MapDirectory_, Position);
	}

	return Chunk;
}


	for (int i = 0; i < Chunks_.Size(); i++)
		Chunks_[i].Destroy(MapDirectory_);

*/