#include "CSubWorldCache.h"

int CSubWorldCache::CalculatePriority(SIVector2D& Key)
{
	SIVector2D P = Key - PriorityDecider_;
	return max(abs(P.x), abs(P.y));
}
