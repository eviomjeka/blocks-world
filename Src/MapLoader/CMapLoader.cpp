#include "CMapLoader.h"
#include "../World/CWorld.h"

CMapLoader::CMapLoader() :
	CompressionBuffer1_(2 << 20),
	CompressionBuffer2_(2 << 20)
{
}

void CMapLoader::AddNeighbours_(CClientSubWorld& SubWorld)
{
	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			SIVector2D NeighbourPosition = SubWorld.Position() + SIVector2D(x, y);
			CClientSubWorld* Neighbour	 = SubWorldCache_(NeighbourPosition);

			if (Neighbour && Neighbour->Prepared_)
				AddNeighbour_(SubWorld, *Neighbour);
		}
}

void CMapLoader::RemoveNeighbours_(CClientSubWorld& SubWorld)
{
	if (!SubWorld.Prepared_)
		return;

	__assert(SubWorld.Neighbours_ & SX_SY_NEIGHBOUR_MASK);

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			SIVector2D NeighbourPosition = SubWorld.Position() + SIVector2D(x, y);
			CClientSubWorld* Neighbour   = SubWorldCache_(NeighbourPosition);

			if (Neighbour && Neighbour->Prepared_ && Neighbour != &SubWorld) /*Unloading from cache*/
				RemoveNeighbour_(SubWorld, *Neighbour);
		}

	__assert(SubWorld.Neighbours_ == SX_SY_NEIGHBOUR_MASK);
}

void CMapLoader::AddNeighbour_(CClientSubWorld& SubWorld, CClientSubWorld& Neighbour)
{
	/*
	 * no lightings FROM 9 subworlds are pending as 8 of them do not have enough neighbours
	 * and the central one is being loaded.
	 * */

	LOG("Neighbored (%d %d) and (%d %d)", SubWorld.Position().x, SubWorld.Position().y, Neighbour.Position().x, Neighbour.Position().y);
	SIVector2D NeighbourShift = Neighbour.Position() - SubWorld.Position();
	int Mask1 = CClientSubWorld::GetNeighbourMask(NeighbourShift);
	int Mask2 = CClientSubWorld::GetNeighbourMask(-NeighbourShift);

	assert(!(SubWorld.Neighbours_  & Mask1));
	assert(!(Neighbour.Neighbours_ & Mask2));

	SubWorld.Neighbours_  |= Mask1;
	Neighbour.Neighbours_ |= Mask2;

	int LightingVersion = Neighbour.LightingVerisons_(-NeighbourShift);

	if (LightingVersion != -1 && LightingVersion != SubWorld.Version())
	{
		LightingManager_.CancelLightingsAround(Neighbour.Position());
		LightingManager_.InvalidateLighting(Neighbour.Position());
	}
}

void CMapLoader::RemoveNeighbour_(CClientSubWorld& SubWorld, CClientSubWorld& Neighbour)
{
	SIVector2D NeighbourShift = Neighbour.Position() - SubWorld.Position();
	int Mask1 = CClientSubWorld::GetNeighbourMask(NeighbourShift);
	int Mask2 = CClientSubWorld::GetNeighbourMask(-NeighbourShift);

	assert(SubWorld.Neighbours_  & Mask1);
	assert(Neighbour.Neighbours_ & Mask2);

	SubWorld.Neighbours_  &= ~Mask1;
	Neighbour.Neighbours_ &= ~Mask2;

	Neighbour.LightingRequested_ = false;
}

void CMapLoader::PrepareSubWorld_(CClientSubWorld& SubWorld)
{
	__assert(!SubWorld.Prepared_);
	__assert(SubWorld.FullyGenerated_);
	SubWorld.Prepared_ = true;
	AddNeighbours_(SubWorld);
	CheckLightings_(SubWorld, 1);
}

void CMapLoader::UnloadSubWorld_(CClientSubWorld& SubWorld, bool Destruction)
{
	/*
	 * Since we got to this point, this subworld is not being used (cache references).
	 */

	if (!SubWorld.Initialized())
		return;

	LOG("Unloading subworld (%d %d)", SubWorld.Position().x, SubWorld.Position().y);

	if (!Destruction)
		LightingManager_.CancelLightingsAround(SubWorld.Position());

	SaveSubWorld_(SubWorld);
	RemoveNeighbours_(SubWorld);
	SubWorld.Destroy();
}

void CMapLoader::CheckLightings_(CClientSubWorld& SubWorld, int R)
{
	for (int x = -R; x <= R; x++)
		for (int y = -R; y <= R; y++)
		{
				SIVector2D CurrentShift(x, y);

				SIVector2D CurrentPosition = SubWorld.Position() + CurrentShift;
				CClientSubWorld* CurrentSubWorld = SubWorldCache_(CurrentPosition);

				/* check if already applied*/
				if (CurrentSubWorld)
					LOG("Requesting lighting at (%d %d) (if mask == %d)", CurrentPosition.x, CurrentPosition.y, CurrentSubWorld->Neighbours_);
				if (CurrentSubWorld && CurrentSubWorld->Neighbours_ == NEIGHBOURS_ALL_MASK)
					LightingManager_.RequestLighting(CurrentSubWorld->Position());
		}
}

void CMapLoader::GetNeighbours_(SIVector2D Position, CClientSubWorlds3x3& Neighbours)
{
	/*
	* Note: Only to be called from maploader thread.
	*/
	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
			Neighbours(x, y) = SubWorldCache_(Position + SIVector2D(x, y));
}

CBlockData CMapLoader::GetBlock(SIVector Position)
{
	/*
	 * Note: If called from main thread, the results are unreliable.
	 * However, for physics and choosing block position it is OK.
	 */

	assert (Position.y >= 0 && Position.y <= SUBWORLD_SIZE_Y);

	SIVector2D SubWorldPosition = DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	CClientSubWorld* SubWorld = SubWorldCache_(SubWorldPosition, false);
	CBlockData Data;

	if (SubWorld)
		Data = SubWorld->GetBlock(ModXZ(Position, SUBWORLD_SIZE_XZ));

	return Data;
}

void CMapLoader::SetBlock(SIVector Position, CBlockData Block)
{
	/*
	 * Note: Only to be called from 2nd thread.
	 */
	
	assert (Position.y >= 0 && Position.y < SUBWORLD_SIZE_Y);

	SIVector2D SubWorldPosition	= DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	SIVector BlockPosition		= ModXZ(Position, SUBWORLD_SIZE_XZ);
	CClientSubWorld& SubWorld	= *SubWorldCache_(SubWorldPosition);

	LightingManager_.CancelLightingsAround(SubWorldPosition);
	SubWorld.SetBlock (BlockPosition, Block);
	UpdateOnChange_(SubWorld);
}

SVector2D<byte> CMapLoader::GetLight(SIVector Position)
{
	/*
	 * Note: results are HIGHLY unreliable. Should only be used for players/mobs
	 * models lighting calculation
	 */

	if (Position.y < 0)
		return SVector2D<byte> (0, 0);
	if (Position.y > SUBWORLD_SIZE_Y)
		return SVector2D<byte> (0, MAX_LIGHT_INTENSITY);

	SIVector2D SubWorldPosition = DivXZ2D(Position, SUBWORLD_SIZE_XZ);
	CClientSubWorld* SubWorld = SubWorldCache_(SubWorldPosition, false);

	if (!SubWorld)
		return SVector2D<byte> (0, 0);

	return SubWorld->GetLight(ModXZ(Position, SUBWORLD_SIZE_XZ));
}

void CMapLoader::UpdateOnChange_(CClientSubWorld& SubWorld)
{
	/*
	 * Because of block deltas, soon this function will be deprecated
	 */
	
	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			SIVector2D CurrentPosition = SubWorld.Position() + SIVector2D(x, y);


			//WAIT FOR IT TO FINISH
			if (SubWorldCache_(CurrentPosition))
				LightingManager_.CancelLightingsAround(CurrentPosition);
		}

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			SIVector2D CurrentShift(x, y);
			SIVector2D CurrentPosition = SubWorld.Position() + CurrentShift;
			CClientSubWorld* CurrentSubWorld = SubWorldCache_(CurrentPosition);

			if (CurrentSubWorld && CurrentSubWorld->LightingVerisons_(-CurrentShift) != -1)
			{
				//assert(CurrentSubWorld->LightingVerisons_(-CurrentShift) == SubWorld.Version() - 1);
				LightingManager_.InvalidateLighting(CurrentPosition);
				LightingManager_.InvalidateMeshes(CurrentPosition);
			}

		}

	CheckLightings_(SubWorld, 2);	
}

void CMapLoader::LoadSubWorld_(CClientSubWorld& SubWorld, SIVector2D Position, bool Dependency)
{
	SIVector2D ChunkPosition = Div2D(Position, CHUNK_SIZE);
	LOG("Loading subworld (%d %d)", Position.x, Position.y);
	GetChunk_(ChunkPosition, +1).LoadSubWorld(World_->WorldGenerator(), SubWorld, Position, CompressionBuffer1_, CompressionBuffer2_);

	if (!Dependency)
		GenerateObjects_(SubWorld);
}

void CMapLoader::GenerateObjects_(CClientSubWorld& SubWorld)
{	
	if (SubWorld.FullyGenerated_)
		return;

	assert(!SubWorld.Version_);
	ObjectGenerationManager_.GenetateSubWorld(SubWorld);
	World_->WorldGenerator().PostGenerate(SubWorld);
	SubWorld.FullyGenerated_ = true;
	SubWorld.Version_ = 0;
}

void CMapLoader::SaveSubWorld_(CClientSubWorld& SubWorld)
{
	//TODO: save only if modified
	SIVector2D ChunkPosition = Div2D(SubWorld.Position(), CHUNK_SIZE);

	GetChunk_(ChunkPosition, -1).SaveSubWorld(SubWorld, CompressionBuffer1_, CompressionBuffer2_, false);
}

void CMapLoader::SaveSubWorldData_(SIVector2D Position, CBuffer& Buffer, int Version)
{
	SIVector2D ChunkPosition = Div2D(Position, CHUNK_SIZE);

	GetChunk_(ChunkPosition, 0).SaveSubWorldData(Buffer, Position, Version);
}

void CMapLoader::Init(const char* MapDirectory, int CacheSize, CWorld* World)
{
	World_		= World;
	strcpy(MapDirectory_, MapDirectory);

	ChunkCache_.Init(8 * CacheSize / CHUNK_SIZE);
	SubWorldCache_.Init(2 * CacheSize);
	LightingManager_.Init(this);
	ObjectGenerationManager_.Init(this, 666);
	ObjectGenerationManager_.AddObjectGenerator(new CTrees);
	ObjectGenerationManager_.AddObjectGenerator(new CStones);
}

CChunk& CMapLoader::GetChunk_(SIVector2D Position, int ReferenceDelta)
{
	assert(Square(ReferenceDelta) <= 1);

	CChunk* Chunk = ChunkCache_(Position);
	

	if (!Chunk)
	{
		Chunk = &ChunkCache_.Allocate(Position);

		if (Chunk->Initialized())
			Chunk->Destroy(MapDirectory_);

		Chunk->Init(MapDirectory_, Position);
	}

	if (ReferenceDelta == -1)
		ChunkCache_.RemoveReference(Position);
	else if (ReferenceDelta == 1)		
		assert(ChunkCache_.AddReference(Position));

	return *Chunk;
}

CSubWorldCache& CMapLoader::GetSubWorldCache()
{
	return SubWorldCache_;
}

void CMapLoader::Destroy()
{
	ObjectGenerationManager_.Destroy();
	LightingManager_.Destroy();

	CArray<CSubWorldCache::tEntry>& SubWorlds = SubWorldCache_.Data();
	for (int i = 0; i < SubWorlds.Size(); i++)
		if (SubWorlds[i].References != -1)
		{
			SubWorldCache_.Unload(SubWorlds[i].Data);
			UnloadSubWorld_(SubWorlds[i].Data, true);
		}

	SubWorldCache_.Destroy();

	CArray<CChunkCache::tEntry>& Chunks = ChunkCache_.Data();
	for (int i = 0; i < Chunks.Size(); i++)
		if (Chunks[i].Data.Initialized())
			Chunks[i].Data.Destroy(MapDirectory_);

	ChunkCache_.Destroy();
}

CSubWorld& CMapLoader::GetSubWorldForGeneration(SIVector2D Position)
{
	LOG("Getting subworld (%d %d)", Position.x, Position.y);
	CClientSubWorld* SubWorld = SubWorldCache_(Position);

	if (SubWorld)
	{
		LOG("Already");
		assert(!SubWorld->Version_);

		return *SubWorld;
	}

	SubWorld = &SubWorldCache_.Allocate(Position);
	UnloadSubWorld_(*SubWorld);
	LoadSubWorld_(*SubWorld, Position, true);

	return *SubWorld;
}

void CMapLoader::ReleaseSubWorldForGeneration(CSubWorld& _SubWorld)
{
	LOG("Freeing subworld (%d %d)", _SubWorld.Position().x, _SubWorld.Position().y);
	_SubWorld.Version_ = 0;

	CClientSubWorld& SubWorld = (CClientSubWorld&) _SubWorld;

	if (!SubWorldCache_(SubWorld.Position()))
	{
		assert(!SubWorld.Prepared_);
		SubWorldCache_.ReportReady(&SubWorld);
	}
}

