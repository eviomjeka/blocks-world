#ifndef CGENERATIONMAP
#define CGENERATIONMAP

#include "../BlockItemBase/BlockItemInclude.h"
#include "../World/CSubWorld.h"


#define GENERATION_VECTOR_ARRAY_RANGE 10

typedef CSVectorArray2D<CSubWorld*, 
	2 * GENERATION_VECTOR_ARRAY_RANGE + 1, 2 * GENERATION_VECTOR_ARRAY_RANGE,
	-GENERATION_VECTOR_ARRAY_RANGE	 ,    -GENERATION_VECTOR_ARRAY_RANGE>
	_CGenerationMap;

class CGenerationMap :
	public CBlockManager,
	public _CGenerationMap
{
private:
	SIVector2D Position_;

public:
	CGenerationMap(CSubWorld& Central);

	virtual CBlockData GetBlock (SIVector Position);
	virtual void SetBlock (SIVector Position, CBlockData Block);
};

#endif