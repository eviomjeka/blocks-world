#ifndef CSUBWORLDCACHE
#define CSUBWORLDCACHE

#include <Engine.h>
#include "../World/CClientSubWorld.h"


class CSubWorldCache :
	public CCache<SIVector2D, CClientSubWorld, SIVector2D, true>
{

public:
	virtual int CalculatePriority(SIVector2D& Key);

public:
	template <typename T, int sx, int sy, int px, int py>
	bool GetLockedSubWorlds(SIVector2D Position, CSVectorArray2D<T, sx, sy, px, py>& SubWorlds, bool Lock)
	{	
		if (Lock)
			Mutex_.Lock();

		for (int i = 0; i < SubWorlds.Size(); i++)
			SubWorlds[i] = 0;

		for (int x = px; x < px + sx; x++)
			for (int y = py; y < py + sy; y++)
			{
				SIVector2D CurrentPosition = Position + SIVector2D(x, y);
				SubWorlds(x, y) = AddReference(CurrentPosition, false);

				if (!SubWorlds(x, y))
				{
					for (int i = 0; i < SubWorlds.Size(); i++)
						if (SubWorlds[i])
						{
							RemoveReference(SubWorlds[i]->Position(), false);
							SubWorlds[i] = NULL;
						}

					if (Lock)
						Mutex_.Unlock();
					return false;
				}

			assert(SubWorlds(x, y)->Valid(CurrentPosition));
		}

		if (Lock)
			Mutex_.Unlock();

		return true;
	}

	template <typename T, int sx, int sy, int px, int py>
	void ReleaseSubWorlds(CSVectorArray2D<T, sx, sy, px, py>& SubWorlds, bool Lock)
	{
		if (Lock)
			Mutex_.Lock();

		for (int x = px; x < px + sx; x++)
			for (int y = py; y < py + sy; y++)
				RemoveReference(SubWorlds(x, y)->Position(), false);

		if (Lock)
			Mutex_.Unlock();
	}

};

#endif

