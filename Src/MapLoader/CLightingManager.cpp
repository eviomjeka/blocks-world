#include "CLightingManager.h"
#include "../CApplicationModule.h"
#include "../CApplication.h"

CLightingManager::CLightingManager() :
	LightingRequests_(1024)
{
}


void CLightingManager::RequestLighting(SIVector2D Position)
{
	/*
	 * This is called from MapLoader thread.
	 * So we can be sure that while we stay here
	 * our 9 subworlds remain untouched.
	 */

	CClientSubWorld* SubWorld = MapLoader_->GetSubWorldCache()(Position);
	assert(SubWorld);

	if (SubWorld->LightingRequested_)
		return;

	SubWorld->LightingRequested_ = true;

	int Targets = GetLightingTargets_(Position);
	if (!Targets)
		return;

	LOG("Requesting lighting at (%d %d)", Position.x, Position.y);

	tLightingRequest LightingRequest = {Position, Targets};

	Mutex_.Lock();

	for (int i = 0; i < LightingRequests_.Size(); i++)
		assert(LightingRequests_[i].Position != Position);

	LightingRequests_.Insert(LightingRequest, true);

	Mutex_.Unlock();
}

void CLightingManager::ApplyLighting_(tLightingRequest LightingRequest)
{
	LOG("Applying lighting at (%d %d)", LightingRequest.Position.x, LightingRequest.Position.y);

	CClientSubWorlds3x3 SubWorlds;
	assert(MapLoader_->GetSubWorldCache().GetLockedSubWorlds(LightingRequest.Position, SubWorlds, true));

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			CClientSubWorld* CurrentSubWorld = SubWorlds(x, y);

			if (!CurrentSubWorld->ValidLighting_)
				CurrentSubWorld->ClearLighting();

			if (CurrentSubWorld->LightingVerisons_(0, 0) == -1)
				LightingEngine_.PrepareSubWorldLighting(CurrentSubWorld);
		}

	/*was locked in LighterThread_*/
	Mutex_.Unlock();

	CClientSubWorld& SubWorld = *SubWorlds(SIVector2D(0, 0));
	assert(SubWorld.Valid(LightingRequest.Position));

#if 1
	LightingEngine_.CalculateSubWorldLighting(SubWorlds, LightingRequest.Targets);
#else
	LightingEngine_.CalculateSubWorldLighting(SubWorlds, NEIGHBOURS_ALL_MASK);
#endif

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			SIVector2D CurrentShift(x, y);
			SIVector2D CurrentPosition = SubWorld.Position() + CurrentShift;
			//int Mask1 = CClientSubWorld::GetNeighbourMask(CurrentShift);

			CClientSubWorld& CurrentSubWorld = *SubWorlds(CurrentShift);

			//!!!!!!!!!!!!!!!! TODO: fix
			//if (!(LightingsToApply & Mask1))
			//	continue;

			assert(CurrentSubWorld.Valid(CurrentPosition));

			CurrentSubWorld.LightingVerisons_(-CurrentShift) = SubWorld.Version();
		}

		for (int x = -1; x <= 1; x++)
			for (int y = -1; y <= 1; y++)
				if (!SubWorlds(x, y)->ValidMeshes_)
					GenerateSubWorldMeshesIfNeeded_(SubWorld.Position() + SIVector2D(x, y));


	MapLoader_->GetSubWorldCache().ReleaseSubWorlds(SubWorlds, true);
	LightingsApplied_++;
}
void CLightingManager::CancelLightingsAround(SIVector2D Position)
{
	/*
	 * cancel all lightings around (as not all subworlds will be lighted)
	 * wait for current lightings to finish to guarantee this thread has
	 * exclusive rights on the 9 subworlds.
	 */
	//assert(false);

	Mutex_.Lock();

	for (int i = 0; i < LightingRequests_.Size(); i++)
		if ((LightingRequests_[i].Position - Position).SqLen() <= 2)
			CancelLighting_(i--);

	Mutex_.Unlock();
}

void CLightingManager::CancelLighting_(int i)
{
	/*
	all subworlds around must be loaded at this point
	mutex is locked and all lightnings around have been performed
	*/

	SIVector2D Position = LightingRequests_[i].Position;
	LightingRequests_.Remove(i);

	CClientSubWorld* SubWorld = MapLoader_->SubWorldCache_(Position);
	assert(SubWorld);
	SubWorld->LightingRequested_ = false;
}

void CLightingManager::InvalidateMeshes(SIVector2D Position)
{
	MapLoader_->SubWorldCache_(Position)->ValidMeshes_ = false;
}

void CLightingManager::InvalidateLighting(SIVector2D Position)
{
	/*
	the caller is expected to cancel the lightings around before calling this function.
	However, it is all about performance.
	after that, the caller must check if lighting should be applied himself.
	*/
	CClientSubWorlds3x3 SubWorlds;
	MapLoader_->GetNeighbours_(Position, SubWorlds);

	Mutex_.Lock();

	SubWorlds(0, 0)->InvalidateLighting();

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
			if (SubWorlds(x, y))
				SubWorlds(x, y)->LightingRequested_ = false;

	Mutex_.Unlock();
}

void CLightingManager::LighterThread_()
{
	LighterThreadReady_ = true;

	while (LighterThreadReady_)
	{
		Mutex_.Lock();

		if (LightingRequests_.Size())
		{
			int Nearest = FindNearestLightingJob_();
			tLightingRequest LightingRequest = LightingRequests_[Nearest];
			LightingRequests_.Remove(Nearest);

			LOG("Pulled lighing request");
			/*It will also close the mutex*/
			ApplyLighting_(LightingRequest);

		}
		else
		{
			//LOG("No lighing requests");
			Mutex_.Unlock();
			Sleep(1);
		}

	}

	LighterThreadReady_ = true;
}

int CLightingManager::FindNearestLightingJob_()
{
	int Best = 0;
	int BestSqLen = 1 << 20;
	SIVector2D PlayerPosition = MapLoader_->OldPosition_;

	for (int i = 0; i < LightingRequests_.Size(); i++)
	{
		int SqLen = (PlayerPosition - LightingRequests_[i].Position).SqLen();

		if (SqLen < BestSqLen)
		{
			Best = i;
			BestSqLen = SqLen;
		}
	}

	return Best;
}

void CLightingManager::Init(CMapLoader* MapLoader)
{
	MapLoader_ = MapLoader;
	LighterThreadReady_	= false;

	LightingEngine_.Init();

	LaunchThread(_LighterThread_, (void*) this);

	while (!LighterThreadReady_)
		Sleep (1);
}

void CLightingManager::Destroy()
{
	LighterThreadReady_ = false;

	while (!LighterThreadReady_)
	{
		PLAYERCONTROLLER.WorldScene()->ClearQueues();
		Sleep (1);
	}
}

void CLightingManager::_LighterThread_(void* This)
{
	((CLightingManager*) This)->LighterThread_();
}

void CLightingManager::GenerateSubWorldMeshesIfNeeded_(SIVector2D Position)
{
	CClientSubWorlds3x3 SubWorlds;
	if (MapLoader_->GetSubWorldCache().GetLockedSubWorlds(Position, SubWorlds, true))
	{
		bool LightingsReady = true;

		for (int x1 = -1; x1 <= 1; x1++)
			for (int y1 = -1; y1 <= 1; y1++)
				for (int x2 = -1; x2 <= 1; x2++)
					for (int y2 = -1; y2 <= 1; y2++)
						if (Square(x1 - x2) + Square(y1 - y2) <= 2)
						{
							SIVector2D Vector12(x2 - x1, y2 - y1);

							if (SubWorlds(x1, y1)->LightingVerisons_(Vector12)  == -1)
							{
								LightingsReady = false;
								break;
							}
						}

		if (LightingsReady)
		{
			PLAYERCONTROLLER.WorldScene()->GenerateMeshes(SubWorlds);
			SubWorlds(0, 0)->ValidMeshes_ = true;
		}

		MapLoader_->GetSubWorldCache().ReleaseSubWorlds(SubWorlds, true);
	}
}

int CLightingManager::GetLightingTargets_(SIVector2D Position)
{
	int Targets = 0;

	CClientSubWorld* SubWorld = MapLoader_->SubWorldCache_(Position);
	assert(SubWorld);

	for (int x = -1; x <= 1; x++)
		for (int y = -1; y <= 1; y++)
		{
			CClientSubWorld* CurrentSubWorld = MapLoader_->SubWorldCache_(Position + SIVector2D(x, y));
			assert(CurrentSubWorld);

			int LightingVersion = CurrentSubWorld->LightingVerisons_(-x, -y);

			if (LightingVersion == -1)
				Targets |= CClientSubWorld::GetNeighbourMask(SIVector2D(x, y));
			//else
			//	assert(LightingVersion == SubWorld->Version());
		}

	return Targets;
}
