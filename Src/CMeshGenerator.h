﻿#ifndef CMESHGENERATOR
#define CMESHGENERATOR

#include <Engine.h>
#include "MeshConstructors/CBlocksMeshQueue.h"
#include "MeshConstructors/CComplexBlocksMeshQueue.h"
#include "World/CClientSubWorld.h"


struct SSubWorldMeshQueues
{
	CBlocksMeshQueue		BlocksMQ;
	CComplexBlocksMeshQueue	ComplexBlocksMQ;
	CComplexBlocksMeshQueue	TranslucentComplexBlocksMQ;
};

class CMeshGenerator : private CBlockManagerReadOnly, CVertexListener
{
public:

	void Init();
	void UpdateSmoothLighting();

	void GenerateMeshes (SSubWorldMeshQueues& MeshQueues, int Sector, CClientSubWorlds3x3& SubWorlds);

private:
	
	virtual CBlockData GetBlock (SIVector Position); // for complex blocks
	virtual void AddVertex (SFVector VertexPosition, SVector2D<byte> TextureShift, SVector2D<unsigned short> TextureCoord);

	CBlockData GetBlock_ (SIVector Position);
	SVector2D<byte> GetLight_ (SIVector Position);

	void RenderBlock_ (CBlockData Block, SSubWorldMeshQueues& MeshQueues);
	byte TrilinearInterpolationLight_ (SFVector v, byte f[2][2][2]);
	
	void AddFace_ (CBlockData Block, byte Face, CBlockData NBlock, 
		SIVector Position, int Sector, SSubWorldMeshQueues& MeshQueues);
	void GetFaceLightVector_ (SVector2D<byte> LightVector[4], SIVector Position, byte Face, float Coef);

	bool SmoothLighting_;
	CMatrixEngine MatrixEngine_;
	bool Rotate_;
	byte BlockRotate_;
	CClientSubWorlds3x3 SubWorlds_;
	CClientSubWorld* SubWorld_;
	CComplexBlocksMeshQueue* CurrentComplexBlocksMQ_;
	int CurrentSector_;
	SIVector BlockPosition_;
	byte BlockLight_[2][2][2][2];
	bool BlockLightCalculated_;

};

#endif