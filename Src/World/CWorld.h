﻿#ifndef CWORLD
#define CWORLD

#include <Engine.h>
#include "../BlockItemBase/BlockItemInclude.h"
#include "../BlockItemBase/CBlockManager.h"
#include "../WorldGenerator/CWorldGenerator.h"
#include "../Network/CPlayer.h"
#include "../Network/CClientMapLoader.h"
#include "CClientSubWorld.h"
#include "CChunk.h"

#define WORLD_NAME_SIZE		20
#define WORLD_GRAVITY		75.0f
#define PHYSICS_TIME_STEP	3
#define MAX_FREEZE_TIME		500
#define DAY_DURATION		(24*60*1000)
#define MAX_PATCH_LEN		64

#define MIN_SIGHT_RANGE		1
#define MAX_SIGHT_RANGE		15

class CWorld
{
private:

	bool				Initialized_;
	int					Time_;

public: CArray<CPlayer>	Players_; // TODO: FIX

	CWorldGenerator		WorldGenerator_;
	
private:

	void Physics_ (int Time);
	void PhysicsStep_ (int Time);
	
	static void _LoaderThread_(void* data);

	friend class CClient;

public:

	CWorld();

	static void InitShaderPrograms();
	void Init (SWorldParams& WorldParams);
	void Destroy();

	void Proc (int Time);
	int Time();
	void SetTime (int time);
	
	void InteractWithBlock (SInteractWithBlockInfo& InteractWithBlockInfo);
	void RemoveBlock (SIVector Position);
	
	SVector2D<byte> GetLight (SIVector Position);
	
	CWorldGenerator& WorldGenerator();

};

#endif