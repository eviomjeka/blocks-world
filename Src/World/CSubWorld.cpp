﻿#include "CSubWorld.h"
#include "../CApplication.h"

CMemoryManager<CBlocksSector, 64> CSubWorld::SubWorldMemoryManager;

CSubWorld::CSubWorld() :
	Initialized_ (false),
	Version_(-1)
{}

void CSubWorld::Init(CWorldGenerator& WorldGenerator, SIVector2D Position)
{
	assert(!Valid(Position));

	if (!Initialized_)
	{
		for (int i = 0; i < SUBWORLD_SECTORS; i++)
		{
			Blocks_[i].NumBlocks = 0;
			Blocks_[i].Blocks = NULL;
		}
	}
	else
		Destroy();

	GeneratedObjects_.Clear();
	Position_		= Position;
	FullyGenerated_ = false;
	Initialized_	= true;

	int Time = TimeMS();
	WorldGenerator.Generate (*this);
	LOG ("Subworld (%d,%d) generated: %dms", Position_.x, Position_.y, TimeMS() - Time);

	Version_		= 0; // generator increments version on block add
}

void CSubWorld::Init(SIVector2D Position, CStream& Data, int Version)
{
	assert(Position_ != Position || !Initialized_);

	if (!Initialized_)
	{
		for (int i = 0; i < SUBWORLD_SECTORS; i++)
		{
			Blocks_[i].NumBlocks = 0;
			Blocks_[i].Blocks = NULL;
		}
	}
	else
		Destroy();

	GeneratedObjects_.Clear();
	Position_		= Position;
	FullyGenerated_ = false;
	Version_		= Version;
	Initialized_	= true;
	Read(Data);
}

void CSubWorld::Destroy()
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		if (Blocks_[i].Blocks)
		{
			Blocks_[i].NumBlocks = 0;
			SubWorldMemoryManager.Free (Blocks_[i].Blocks);
			Blocks_[i].Blocks = NULL;
		}
	
	Initialized_ = false;
}

void CSubWorld::Write(CStream& Stream)
{
	int ContainsLightings = 0;

	Stream << Blocks_;
	Stream << GeneratedObjects_;
	Stream << FullyGenerated_;
	Stream << ContainsLightings;
}

void CSubWorld::Read(CStream& Stream)
{
	int ContainsLightings = 0;

	Stream >> Blocks_;
	Stream >> GeneratedObjects_;
	Stream >> FullyGenerated_;
	Stream >> ContainsLightings;
}

SIVector2D CSubWorld::Position()
{
	assert (Initialized_);
	return Position_;
}

bool CSubWorld::Initialized()
{
	return Initialized_;
}

int CSubWorld::DataSize()
{
	int NumSectors = 0;
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		if (Blocks_[i].Blocks)
			NumSectors++;

	return SUBWORLD_SECTORS * sizeof (bool) + 
		NumSectors * (sizeof (int) + (sizeof (CBlockData) * 
		SECTOR_SIZE * SECTOR_SIZE * SECTOR_SIZE));
}

bool CSubWorld::IsGenerated(_SWorldObject& Object)
{
	for (int i = 0; i < GeneratedObjects_.Size(); i++)
		if (Object == GeneratedObjects_[i])
			return true;

	return false;
}

void CSubWorld::SetGenerated(_SWorldObject& Object)
{
	GeneratedObjects_.Insert(Object, true);
}

int CSubWorld::ObjectsGenerated()
{
	return GeneratedObjects_.Size();
}

//==============================================================================

CBlockData CSubWorld::GetBlock(SIVector Position)
{
	__assert (Initialized_);
	__assert (Position.y >= 0 && Position.y <= SUBWORLD_SIZE_Y);

	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);

	if (!Blocks_[Sector].Blocks)
		return CBlockData();
		
	return (*Blocks_[Sector].Blocks) (SectorPosition);
}

void CSubWorld::SetBlock (SIVector Position, CBlockData Block)
{
	__assert (Initialized_);
	__assert (Position.y >= 0 && Position.y < SUBWORLD_SIZE_Y);

	CBlockData PrevBlock = GetBlock (Position);
	if (Block == PrevBlock)
		return;

	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);

	bool VoidBlock = (Block.ID() == 0);
	bool PrevVoidBlock = (PrevBlock.ID() == 0);
	
	if (!Blocks_[Sector].Blocks && !VoidBlock)
	{
		Blocks_[Sector].Blocks = SubWorldMemoryManager.Alloc();
		memset (Blocks_[Sector].Blocks, 0, sizeof (CBlocksSector));
	}
	if (VoidBlock && !PrevVoidBlock)
	{
		Blocks_[Sector].NumBlocks--;
		__assert (Blocks_[Sector].NumBlocks >= 0);
		if (Blocks_[Sector].NumBlocks == 0)
		{
			SubWorldMemoryManager.Free (Blocks_[Sector].Blocks);
			Blocks_[Sector].Blocks = NULL;

			Version_++;
			return;
		}
	}
	else if (!VoidBlock && PrevVoidBlock)
	{
		Blocks_[Sector].NumBlocks++;	
		__assert (Blocks_[Sector].NumBlocks <= SECTOR_SIZE * SECTOR_SIZE * SECTOR_SIZE);
	}
	
	(*Blocks_[Sector].Blocks) (SectorPosition) = Block;
	Version_++;
}

CBlocksSector* CSubWorld::GetSector (int Sector)
{
	assert (Sector >= 0 && Sector < SUBWORLD_SECTORS);
	return Blocks_[Sector].Blocks;
}

bool CSubWorld::Valid(SIVector2D _Position)
{
	return Initialized() && Position() == _Position;
}

//==============================================================================

CStream& operator <<(CStream& Stream, CSubWorld& SubWorld)
{
	SubWorld.Write(Stream);

	return Stream;
}

CStream& operator <<(CStream& Stream, CBlocksSectorData& Sector)
{
	Stream << Sector.NumBlocks;
	Stream << (*Sector.Blocks);
	
	return Stream;
}

CStream& operator >>(CStream& Stream, CBlocksSectorData& Sector)
{
	Stream >> Sector.NumBlocks;
	Stream >> (*Sector.Blocks);
	
	return Stream;
}

CStream& operator <<(CStream& Stream, CBlocks& Blocks)
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
	{
		bool SectorExist = (Blocks[i].Blocks != NULL);
		Stream << SectorExist;
	}
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
	{
		if (!Blocks[i].Blocks)
			continue;

		Stream << Blocks[i];
	}
	
	return Stream;
}

CStream& operator >>(CStream& Stream, CBlocks& Blocks)
{
	bool SectorExist[SUBWORLD_SECTORS] = {};
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		Stream >> SectorExist[i];

	for (int i = 0; i < SUBWORLD_SECTORS; i++)
	{
		if (!SectorExist[i])
		{
			if (Blocks[i].Blocks)
			{
				CSubWorld::SubWorldMemoryManager.Free (Blocks[i].Blocks);
				Blocks[i].Blocks = NULL;
				Blocks[i].NumBlocks = 0;
			}
			continue;
		}

		if (!Blocks[i].Blocks)
			Blocks[i].Blocks = CSubWorld::SubWorldMemoryManager.Alloc();

		Stream >> Blocks[i];
	}
	
	return Stream;
}

int CSubWorld::Version()
{
	return Version_;
}

CSubWorld::~CSubWorld()
{
//assert(!Initialized_);
}

CStream& operator <<(CStream& Stream, SInteractWithBlockInfo& InteractWithBlockInfo)
{
	Stream << InteractWithBlockInfo.BlockPosition;
	Stream << InteractWithBlockInfo.SelectionPosition;
	Stream << InteractWithBlockInfo.Block;
	Stream << InteractWithBlockInfo.Item;
	Stream << InteractWithBlockInfo.Face;

	return Stream;
}

CStream& operator >>(CStream& Stream, SInteractWithBlockInfo& InteractWithBlockInfo)
{
	Stream >> InteractWithBlockInfo.BlockPosition;
	Stream >> InteractWithBlockInfo.SelectionPosition;
	Stream >> InteractWithBlockInfo.Block;
	Stream >> InteractWithBlockInfo.Item;
	Stream >> InteractWithBlockInfo.Face;

	return Stream;
}
