﻿#ifndef CLIGHTSECTOR
#define CLIGHTSECTOR

#include <Engine.h>
#include "CSubWorld.h"

typedef CSVectorArray3D<byte, SECTOR_SIZE, SECTOR_SIZE, SECTOR_SIZE> CLightsArray;

class CLightSector;
typedef CSArray<CLightSector, SUBWORLD_SECTORS> CLightSectors;

class CLightSector
{
public:

	void Init();
	void Destroy();

	byte GetLight1 (SIVector Position);
	byte GetLight2 (SIVector Position);
	SVector2D<byte> GetLight (SIVector Position);
	void SetLight1 (SIVector Position, byte Light);
	void SetLight2 (SIVector Position, byte Light);
	void SetFullLight2();

	bool IsLight1Empty();
	bool IsLight2Empty();
	bool IsLight2Full();

	friend CStream& operator << (CStream& Stream, CLightSector& Array);
	friend CStream& operator >> (CStream& Stream, CLightSector& Array);

private:

	int				Light1_Num_;
	int				Light2_Num_;
	int				Light2_MaxLightingNum_;
	CLightsArray*	Lights1_;
	CLightsArray*	Lights2_;

};

#endif