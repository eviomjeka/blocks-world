﻿#include "CClientSubWorld.h"
#include "../CApplication.h"

CMemoryManager<CLightsArray, 64> CClientSubWorld::ClientSubWorldMemoryManager;

CClientSubWorld::CClientSubWorld()/* :
	LightingsToReady_(-1),
	LightingsAppliedTo_(-1),
	LightingsAppliedFrom_(-1)
	*/
{}

void CClientSubWorld::Init (CWorldGenerator& Generator, SIVector2D Position)
{
	ClientSpecificInit_();
	InitLightings_();
	CSubWorld::Init (Generator, Position);
	AddLightSources_();
}

void CClientSubWorld::Init (SIVector2D Position, CStream& Data, int Version)
{
	ClientSpecificInit_();
	InitLightings_(); //!!!!!!!!!!!!!!
	CSubWorld::Init (Position, Data, Version);
	AddLightSources_();
}

void CClientSubWorld::InitLightings_()
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		LightSectors_[i].Init();

	for (int i = 0; i < LightingVerisons_.Size(); i++)
		LightingVerisons_[i] = -1;

	ValidLighting_ = true;
}

void CClientSubWorld::ClientSpecificInit_()
{
	Neighbours_				= 0;
	LightingRequested_		= false;
	ValidMeshes_			= false;
	Prepared_				= false;
	/*
	LightingsToReady_		= 0;
	LightingsAppliedTo_		= 0;
	LightingsAppliedFrom_	= 0;
	LightingsRequested_		= 0;
	*/
}

void CClientSubWorld::AddLightSources_()
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
	{
		if (!Blocks_[i].Blocks)
			continue;

		CBlocksSector* Blocks = Blocks_[i].Blocks;
		for (int x = 0; x < SECTOR_SIZE; x++)
			for (int y = 0; y < SECTOR_SIZE; y++)
				for (int z = 0; z < SECTOR_SIZE; z++)
				{
					CBlockData Block = (*Blocks) (x, y, z);
					if (Block.LightIntensity() != 0)
					{
						AddLightSource_ (SIVector (x, y + i * SECTOR_SIZE, z), Block.LightIntensity());
					}
				}
	}
}

void CClientSubWorld::Destroy()
{
	CSubWorld::Destroy();
	ClearLighting();
}

void CClientSubWorld::Write (CStream& Stream)
{
	LOG("Writing subworld (%d %d)", Position_.x, Position_.y);
	int ContainsLightings = 0; // TODO: bool?
	for (int i = 0; i < LightingVerisons_.Size(); i++)
		if (LightingVerisons_[i] != -1)
		{
			ContainsLightings = 1;
			break;
		}

	Stream << Blocks_;
	Stream << GeneratedObjects_;
	Stream << FullyGenerated_;
	Stream << ContainsLightings;

	if (ContainsLightings)
	{	
		Stream << LightingVerisons_;
		Stream << LightSectors_;
	}
}

void CClientSubWorld::Read (CStream& Stream)
{
	int ContainsLightings = 0; // TODO: bool?

	LOG("Reading subworld (%d %d)", Position_.x, Position_.y);

	Stream >> Blocks_;
	Stream >> GeneratedObjects_;
	Stream >> FullyGenerated_;
	Stream >> ContainsLightings;

	if (ContainsLightings)
	{	
		Stream >> LightingVerisons_;
		Stream >> LightSectors_;
	}
	else
		InitLightings_();
}

void CClientSubWorld::InvalidateLighting()
{
	ValidLighting_ = false;

	for (int i = 0; i < LightingVerisons_.Size(); i++)
		LightingVerisons_[i] = -1;
}

void CClientSubWorld::ClearLighting()
{
	for (int i = 0; i < SUBWORLD_SECTORS; i++)
		LightSectors_[i].Destroy();

	ValidLighting_ = true;
}

void CClientSubWorld::AddLightSource_ (SIVector Position, byte LightIntensity)
{
	assert (LightIntensity > 0 && LightIntensity <= MAX_LIGHT_INTENSITY);

	SIVector LightSorucePosition = ModXZ (Position, SUBWORLD_SIZE_XZ);
	SLightSource LightSource = SLightSource (LightSorucePosition, LightIntensity);
	LightSources_.Insert (LightSource, true);
}

void CClientSubWorld::RemoveLightSource_ (SIVector Position)
{
	for (int i = 0; i < LightSources_.Size(); i++)
	{
		if (LightSources_[i].Position == Position)
		{
			LightSources_.Remove (i);
			break;
		}
	}
}

void CClientSubWorld::SetBlock (SIVector Position, CBlockData& Block)
{
	CSubWorld::SetBlock (Position, Block);
	if (Block.LightIntensity() != 0)
		AddLightSource_ (Position, Block.LightIntensity());
	else if (Block.ID() == 0)
		RemoveLightSource_ (Position);
}

void CClientSubWorld::SetLight1 (SIVector Position, byte Light)
{
	__assert (Initialized_);
	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);
	LightSectors_[Sector].SetLight1 (SectorPosition, Light);
}

void CClientSubWorld::SetLight2 (SIVector Position, byte Light)
{
	__assert (Initialized_);
	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);
	LightSectors_[Sector].SetLight2 (SectorPosition, Light);
}

void CClientSubWorld::SetFullLight2 (int Sector)
{
	__assert (Initialized_);
	__assert (Sector >= 0 && Sector < SUBWORLD_SECTORS);

	LightSectors_[Sector].SetFullLight2();
}

byte CClientSubWorld::GetLight1 (SIVector Position)
{
	__assert (Initialized_);
	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);
	return LightSectors_[Sector].GetLight1 (SectorPosition);
}

byte CClientSubWorld::GetLight2 (SIVector Position)
{
	__assert (Initialized_);
	int Sector = Div (Position.y, SECTOR_SIZE);
	SIVector SectorPosition = ModY (Position, SECTOR_SIZE);
	return LightSectors_[Sector].GetLight2 (SectorPosition);
}

SVector2D<byte> CClientSubWorld::GetLight (SIVector Position)
{
	__assert (Initialized_);
	int Sector = Div (Position.y, SUBWORLD_SIZE_XZ);
	SIVector SectorPosition = ModY (Position, SUBWORLD_SIZE_XZ);
	return LightSectors_[Sector].GetLight (SectorPosition);
}

int CClientSubWorld::GetNeighbourMask (SIVector2D Shift)
{
	if		(Shift == SIVector2D(-1, -1))
		return LX_LY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(-1, 0))
		return LX_SY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(-1, 1))
		return LX_HY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(0, -1))
		return SX_LY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(0, 0))
		return SX_SY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(0, 1))
		return SX_HY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(1, -1))
		return HX_LY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(1, 0))
		return HX_SY_NEIGHBOUR_MASK;
	else if (Shift == SIVector2D(1, 1))
		return HX_HY_NEIGHBOUR_MASK;
	else
	{
		error ("GetNeighbourMask: Shift parameter is not correct");
		return 0;
	}
}
