﻿#ifndef CCHUNK
#define CCHUNK

#include <Engine.h>
#include "CSubWorld.h"

#define CHUNK_SIZE	32
#define MAX_CHUNK_SIZE (1 << 24)

struct tSubWorldInfo
{
	int  Offset;
	int  Size;
	int  Version;
	int  ObjectsGenerated;
	bool Loaded;
};

class CChunk
{
private:
	bool		Initialized_;
	CFile		File_;
	SIVector2D	Position_;
	CSVectorArray2D<tSubWorldInfo, CHUNK_SIZE, CHUNK_SIZE> SubWorlds_;
	
	static const char* CHUNK_SIGNATURE;
	static const int CHUNK_SIGNATURE_SIZE;
	static const int CHUNK_VERSION;

public:
	CChunk();
	~CChunk();

	void Init(const char* MapDirectory, SIVector2D Position);
	void LoadSubWorld(CSubWorld& SubWorld, SIVector2D Position, CBuffer& Buffer1, CBuffer& Buffer2);
	void LoadSubWorld(CWorldGenerator& WorldGenerator, CSubWorld& SubWorld, SIVector2D Position, CBuffer& Buffer1, CBuffer& Buffer2);
	void LoadSubWorldData(CBuffer& Data, SIVector2D Position);
	void SaveSubWorld(CSubWorld& SubWorld, CBuffer& Buffer1, CBuffer& Buffer2, bool CheckLoaded = true);
	void SaveSubWorldData(CBuffer& Data, SIVector2D Position, int Version);

	bool SubWorldSaved(SIVector2D Position);
	bool SubWorldLoaded(SIVector2D Position);
	int SubWorldVersion(SIVector2D Position);
	int SubWorldDataSize(SIVector2D Position);

	void Save(const char* MapDirectory);
	
	bool Valid(SIVector2D Position);
	void Destroy(const char* MapDirectory);
	SIVector2D Position();
	bool Initialized();


};

CStream& operator >>(CStream& Stream, tSubWorldInfo& SubWorldInfo);
CStream& operator <<(CStream& Stream, tSubWorldInfo& SubWorldInfo);

#endif