#include "CPlayerModel.h"
#include "../CApplication.h"


void CPlayerModel::Init(const char* FileName)
{
	CModel::Init(FileName);

	PlayerSpecificInit_();
}

void CPlayerModel::Init(CStream& Stream)
{
	CModel::Init(Stream);

	PlayerSpecificInit_();
}

void CPlayerModel::PlayerSpecificInit_()
{
}

void CPlayerModel::Draw(CPlayer& Player, int Time)
{
	//TODO: refactor

	glDisable(GL_CULL_FACE);
	CModel::UpdateAnimation(PLAYERCONTROLLER.Animations_[Player.ID_].ModelContext_, Time);

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.Translate(Player.Position_);
	MATRIXENGINE.Translate(0, 1.1f, 0);
	MATRIXENGINE.RotateY(-Player.Orientation_.y);
	MATRIXENGINE.RotateY(180);
	MATRIXENGINE.RotateX(-90);

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ResetModelViewMatrix();
	MATRIXENGINE.RotateX(Player.Orientation_.x);

	SModelContext& ModelContext = PLAYERCONTROLLER.Animations_[Player.ID_].ModelContext_;

	assert(!ModelContext.JointsInvolved[eTorso]);
	CMatrix<4, 4>& TorsoMatrix = ModelContext.Joints[eTorso];
	TorsoMatrix = MATRIXENGINE.ModelViewMatrix() * TorsoMatrix;
	MATRIXENGINE.PopModelViewMatrix();

	CModel::Draw(ModelContext);
	DrawItem_(ModelContext.Joints[6], Player.ItemData_);

	MATRIXENGINE.PopModelViewMatrix();
	glEnable(GL_CULL_FACE);
}

void CPlayerModel::DrawItem_(CMatrix<4,4>& ItemMatrix, CItemData ItemData)
{
	CMesh ItemMesh;
	CComplexBlocksMeshQueue MeshQueue;
	MeshQueue.Init(&CShaders::ComplexBlocksShader, 1 << 15, 1);
	
	PLAYERCONTROLLER.WorldTexture().Bind();
	//CShaders::ComplexBlocksShader.UniformInt(CShaders::ComplexBlocks.Texture(), PLAYERCONTROLLER.WorldTexture());

	float MaxSize;
	MeshQueue.Begin(SIVector(0));
	CItemRenderer::Instance.Render(SIVector(0, 0/**/, 0), ItemData, &MeshQueue, MaxSize, false);

	MeshQueue.End();
	MeshQueue.Load(ItemMesh);

	if (!ItemMesh)
		return;

	MATRIXENGINE.PushModelViewMatrix();
	MATRIXENGINE.ModelViewMatrix_ = MATRIXENGINE.ModelViewMatrix_ * ItemMatrix;
	MATRIXENGINE.Translate(0.0f, -0.175f, 0.0f);
	MATRIXENGINE.Scale(0.35f, 0.35f, 0.35f);

	MATRIXENGINE.RotateY(-55);


	UseShaderProgram(ComplexBlocksShader);
	MATRIXENGINE.Apply(&CShaders::ComplexBlocksShader);

	ItemMesh.Render();
	ItemMesh.Destroy();
	MATRIXENGINE.PopModelViewMatrix();
}

void CPlayerModel::Destroy()
{
	CModel::Destroy();
}
