﻿#include "CChunk.h"
#include "../CApplication.h"

const char* CChunk::CHUNK_SIGNATURE = "BWC";
const int CChunk::CHUNK_SIGNATURE_SIZE = 3;
const int CChunk::CHUNK_VERSION = 0;

CChunk::CChunk() :
Initialized_(false)
{
}

CChunk::~CChunk()
{
	//assert(!Initialized_);
}

void CChunk::Init(const char* MapDirectory, SIVector2D Position)
{
	//TODO: read Subworlds to buffer first
	if (Initialized_ && Position == Position_)
		return;

	assert(!Initialized_);

	char FileName[MAX_PATH] = "";
	sprintf(FileName, "%s/Chunk_%d_%d.bwc", MapDirectory, Position.x, Position.y);

	File_.Init (FileName, CFILE_READ | CFILE_WRITE | CFILE_BINARY, false);
	if (!File_)
	{
		for (int i = 0; i < SubWorlds_.Size(); i++)
		{
			SubWorlds_[i].Offset			= 0;
			SubWorlds_[i].Size				= 0;
			SubWorlds_[i].Version			= 0;
			SubWorlds_[i].ObjectsGenerated	= 0;			
			SubWorlds_[i].Loaded			= false;
		}
		File_.Init(FileName, CFILE_READ | CFILE_WRITE | CFILE_CREATE | CFILE_BINARY);

		//иди нахуй. пока...
		//int ChunkVersion = CHUNK_VERSION;
		//File_.Write ((void*) CHUNK_SIGNATURE, CHUNK_SIGNATURE_SIZE);
		//File_ >> ChunkVersion;

		File_.Seek(0, SEEK_SET);
		File_ << SubWorlds_;
		File_.Flush();
	}
	else
	{
		//assert(false);
		/*
		char Signature[CHUNK_SIGNATURE_SIZE + 1] = "";
		int ChunkVersion = 0;
		File_.Read (Signature, CHUNK_SIGNATURE_SIZE);
		if (strcmp (CHUNK_SIGNATURE, Signature) != 0)
			error ("Chunk signature is not correct (file: %s)", FileName);
		File_ >> ChunkVersion;
		if (ChunkVersion != CHUNK_VERSION)
			error ("Chunk version is not correct (file: %s)", FileName);
		*/
		File_.Seek(0, SEEK_SET);
		File_ >> SubWorlds_;
	}
	
	Position_ = Position;
	Initialized_ = true;
}

void CChunk::LoadSubWorld(CSubWorld& SubWorld, SIVector2D Position, CBuffer& Buffer1, CBuffer& Buffer2)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);

	assert(SubWorldInfo.Offset);
	assert(!SubWorldInfo.Loaded);

	Buffer1.Clear();
	Buffer2.Clear();
	LoadSubWorldData(Buffer1, Position);
	CCompressor::Decompress(Buffer1, Buffer2);
	SubWorld.Init(Position, Buffer2, SubWorldInfo.Version);

	SubWorldInfo.Loaded = true;
}

void CChunk::LoadSubWorld(CWorldGenerator& WorldGenerator, CSubWorld& SubWorld, SIVector2D Position, CBuffer& Buffer1, CBuffer& Buffer2)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);

	assert(!SubWorldInfo.Loaded);

	if (SubWorldInfo.Offset)
		LoadSubWorld(SubWorld, Position, Buffer1, Buffer2);
	else
		SubWorld.Init(WorldGenerator, Position);

	SubWorldInfo.Loaded = true;
}

void CChunk::LoadSubWorldData(CBuffer& Data, SIVector2D Position)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);
	assert(SubWorldInfo.Offset);
	File_.Seek (SubWorldInfo.Offset, SEEK_SET);
	assert(Data.WritePosition() + SubWorldInfo.Size <= Data.MaxSize());
	File_.Read (Data.Data() + Data.WritePosition(), SubWorldInfo.Size);
	Data.SetWritePosition(Data.WritePosition() + SubWorldInfo.Size);
}

void CChunk::SaveSubWorld(CSubWorld& SubWorld, CBuffer& Buffer1, CBuffer& Buffer2, bool CheckLoaded)
{
	SIVector2D RelativePosition = Mod2D(SubWorld.Position(), CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);

	assert(!CheckLoaded || SubWorldInfo.Loaded);
	SubWorldInfo.Loaded = false;

	if (SubWorld.Version() && SubWorld.Version() == SubWorldInfo.Version)
		return;

	if (!SubWorld.Version() && SubWorld.ObjectsGenerated() == SubWorldInfo.ObjectsGenerated)
		return;

	 SubWorldInfo.ObjectsGenerated = SubWorld.ObjectsGenerated();
	
	Buffer1.Clear();
	Buffer2.Clear();
	Buffer1 << SubWorld;
	CCompressor::Compress(Buffer1, Buffer2);
	SaveSubWorldData(Buffer2, SubWorld.Position(), SubWorld.Version());
}

int CChunk::SubWorldDataSize(SIVector2D Position)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);
	assert(SubWorldInfo.Offset);
	return SubWorldInfo.Size;
}

void CChunk::Destroy(const char* MapDirectory)
{
	if (Initialized_)
	{
		Save(MapDirectory);
		File_.Destroy();
		Initialized_ = false;
	}
}

void CChunk::SaveSubWorldData(CBuffer& Data, SIVector2D Position, int Version)
{
	//assert(!Version);
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);	

	assert(!SubWorldInfo.Loaded);

	if (Version && Version == SubWorldInfo.Version)
		return;

	SubWorldInfo.Version = Version;
	
	File_.Seek(0, SEEK_END);
	SubWorldInfo.Offset = File_.Tell();

	File_ << Data;
	File_.Flush();
	
	SubWorldInfo.Size = File_.Tell() - SubWorldInfo.Offset;
	assert(SubWorldInfo.Size);
}

void CChunk::Save(const char*) // TODO: fix
{
	assert(Initialized_);

	File_.Seek(0, SEEK_SET);
	File_ << SubWorlds_;
}


bool CChunk::SubWorldSaved(SIVector2D Position)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);

	//TODO: !Loaded?
	return SubWorldInfo.Offset != 0;
}

bool CChunk::SubWorldLoaded(SIVector2D Position)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);

	return SubWorldInfo.Loaded;
}

int CChunk::SubWorldVersion(SIVector2D Position)
{
	SIVector2D RelativePosition = Mod2D(Position, CHUNK_SIZE);
	tSubWorldInfo& SubWorldInfo = SubWorlds_(RelativePosition);
	//assert(SubWorldInfo.Offset || SubWorldInfo.Loaded);

	//assert(!SubWorldInfo.Version);
	return SubWorldInfo.Version;
}

SIVector2D CChunk::Position()
{
	return Position_;
}

bool CChunk::Valid(SIVector2D Position)
{
	return Initialized_ && Position_ == Position;
}

bool CChunk::Initialized()
{
	return Initialized_;
}

CStream& operator <<(CStream& Stream, tSubWorldInfo& SubWorldInfo)
{
	Stream << SubWorldInfo.Offset << SubWorldInfo.Size << SubWorldInfo.Version << SubWorldInfo.ObjectsGenerated << SubWorldInfo.Loaded;
	return Stream;
}

CStream& operator >>(CStream& Stream, tSubWorldInfo& SubWorldInfo)
{
	Stream >> SubWorldInfo.Offset >> SubWorldInfo.Size >> SubWorldInfo.Version >> SubWorldInfo.ObjectsGenerated >> SubWorldInfo.Loaded;
	return Stream;
}
