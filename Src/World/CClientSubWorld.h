﻿#ifndef CCLIENTSUBWORLD
#define CCLIENTSUBWORLD

#include <Engine.h>
#include "CSubWorld.h"
#include "CLightSector.h"

class CClientSubWorld;
typedef CSVectorArray2D<CClientSubWorld*, 3, 3, -1, -1> CClientSubWorlds3x3;

enum ENeighbourMasks
{
	LX_LY_NEIGHBOUR_MASK = 1, 
	LX_SY_NEIGHBOUR_MASK = 2, 
	LX_HY_NEIGHBOUR_MASK = 4, 
	SX_LY_NEIGHBOUR_MASK = 8, 
	SX_SY_NEIGHBOUR_MASK = 16, 
	SX_HY_NEIGHBOUR_MASK = 32, 
	HX_LY_NEIGHBOUR_MASK = 64, 
	HX_SY_NEIGHBOUR_MASK = 128, 
	HX_HY_NEIGHBOUR_MASK = 256, 
	NEIGHBOURS_ALL_MASK  = 511
};

struct SLightSource
{
	SLightSource()
	{}
	SLightSource (SVector<byte> _Position, byte _Intensity) :
		Position (_Position), 
		Intensity (_Intensity)
	{}

	SVector<byte> Position;
	byte Intensity;
};

class CClientSubWorld : public CSubWorld
{
private:
	
	friend class CLightingEngine;
	friend class CMeshGenerator;
	friend class CMapLoader;
	friend class CLightingManager;

	CArray<SLightSource> LightSources_; // TODO: fix. Resize every time?
	CLightSectors LightSectors_;

	int Neighbours_;
	bool LightingRequested_;
	bool ValidMeshes_;
	bool ValidLighting_;

	SIVector2D LoadingPosition_;
	CSVectorArray2D<int, 3, 3, -1, -1>	LightingVerisons_;

	void InitLightings_(); // TODO: rename this function
	void ClientSpecificInit_();
	void AddLightSources_();
	
	void AddLightSource_		(SIVector Position, byte LightIntensity);
	void RemoveLightSource_		(SIVector Position);
	
public:
	bool Prepared_;
	
	static CMemoryManager<CLightsArray, 64> ClientSubWorldMemoryManager;

	CClientSubWorld();
	virtual ~CClientSubWorld() {}

	virtual void Init			(SIVector2D Position, CStream& Data, int Version);
	virtual void Init			(CWorldGenerator& Generator, SIVector2D Position);
	virtual void Destroy		();
	virtual void Write			(CStream& Stream);
	virtual void Read			(CStream& Stream);

	void SetBlock				(SIVector Position, CBlockData& Block);

	void InvalidateLighting		();
	void ClearLighting			();
	void SetLight1				(SIVector Position, byte Light);
	void SetLight2				(SIVector Position, byte Light);
	void SetFullLight2			(int Sector);
	byte GetLight1				(SIVector Position);
	byte GetLight2				(SIVector Position);
	SVector2D<byte> GetLight	(SIVector Position);
	
	static int GetNeighbourMask (SIVector2D Shift);

};

#endif
