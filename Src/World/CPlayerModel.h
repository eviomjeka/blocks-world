#ifndef CPLAYERMODEL
#define CPLAYERMODEL

#include <Engine.h>
#include "../Network/CPlayer.h"

class CPlayerModel :
	public CModel
{
	enum tBone
	{
		ePelvis = 0,
		eTorso  = 3
	};

	void PlayerSpecificInit_();
	void DrawItem_(CMatrix<4,4>& ItemMatrix, CItemData ItemData);

public:
	void Init(const char* FileName);
	void Init(CStream& Stream);

	void Draw(CPlayer& Player, int Time);
	void Destroy();

};

#endif
