﻿#ifndef CSUBWORLD
#define CSUBWORLD

#include <Engine.h>
#include "../BlockItemBase/BlockItemInclude.h"
#include "../WorldGenerator/CWorldObject.h"

#define SECTOR_SIZE			32
#define SECTOR_VOLUME		(SECTOR_SIZE * SECTOR_SIZE * SECTOR_SIZE)
#define SUBWORLD_SECTORS	8
#define SUBWORLD_SIZE_XZ	SECTOR_SIZE
#define SUBWORLD_SIZE_Y		(SECTOR_SIZE*SUBWORLD_SECTORS-1)

typedef CSVectorArray3D<CBlockData, SECTOR_SIZE, SECTOR_SIZE, SECTOR_SIZE> CBlocksSector;

struct CBlocksSectorData
{
	int NumBlocks;
	CBlocksSector* Blocks;
};

typedef CSArray<CBlocksSectorData, SUBWORLD_SECTORS> CBlocks;

class CWorldGenerator;

class CSubWorld
{
protected:
	SIVector2D						Position_;
	int								Version_;
	CBlocks							Blocks_;
	bool							Initialized_;
	CArray<_SWorldObject>			GeneratedObjects_;
public: // TODO: fix
	bool							FullyGenerated_;
	
public:
	
	static CMemoryManager<CBlocksSector, 64> SubWorldMemoryManager;

	CSubWorld();
	virtual ~CSubWorld();

	virtual void Init			(CWorldGenerator& Generator, SIVector2D Position);
	virtual void Init			(SIVector2D Position, CStream& Data, int Version);
	virtual void Destroy		();
	virtual void Read			(CStream& Stream);
	virtual void Write			(CStream& Stream);

	void SetBlock				(SIVector Position, CBlockData Block);
	CBlockData GetBlock			(SIVector Position);
	CBlocksSector* GetSector	(int Sector); // TODO: remove
	int DataSize				();
	SIVector2D Position			();
	bool Initialized			();
	bool Valid					(SIVector2D _Position);
	int Version					();

	bool IsGenerated			(_SWorldObject& Object);
	void SetGenerated			(_SWorldObject& Object);
	int ObjectsGenerated();

	friend class CMapLoader;

};

struct SInteractWithBlockInfo
{
	SIVector		BlockPosition;
	SIVector		SelectionPosition;
	CBlockData		Block;
	byte			Face;
	SFVector2D		Angle;
	CItemData		Item;
};

CStream& operator <<(CStream& Stream, CBlocks& Blocks);
CStream& operator >>(CStream& Stream, CBlocks& Blocks);
CStream& operator <<(CStream& Stream, CBlocksSectorData& Sector);
CStream& operator >>(CStream& Stream, CBlocksSectorData& Sector);
CStream& operator <<(CStream& Stream, SInteractWithBlockInfo& InteractWithBlockInfo);
CStream& operator >>(CStream& Stream, SInteractWithBlockInfo& InteractWithBlockInfo);
CStream& operator <<(CStream& Stream, CSubWorld& SubWorld);

#endif
