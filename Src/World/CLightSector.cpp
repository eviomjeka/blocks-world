﻿#include "CLightSector.h"
#include "../CLightingEngine.h"

void CLightSector::Init()
{
	Lights1_ = NULL;
	Lights2_ = NULL;

	Light1_Num_ = 0;
	Light2_Num_ = 0;
	Light2_MaxLightingNum_ = 0;
}

void CLightSector::Destroy()
{
	if (Lights1_)
	{
		CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights1_);
		Lights1_ = NULL;
	}
	if (Lights2_)
	{
		CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights2_);
		Lights2_ = NULL;
	}

	Light1_Num_ = 0;
	Light2_Num_ = 0;
	Light2_MaxLightingNum_ = 0;
}

SVector2D<byte> CLightSector::GetLight (SIVector Position)
{
	SVector2D<byte> Light (GetLight1 (Position), GetLight2 (Position));
	return Light;
}

byte CLightSector::GetLight1 (SIVector Position)
{
	if (!Lights1_)
		return 0;	
	return (*Lights1_) (Position);
}

byte CLightSector::GetLight2 (SIVector Position)
{
	if (!Lights2_)
	{
		if (Light2_MaxLightingNum_ == SECTOR_VOLUME)
			return MAX_LIGHT_INTENSITY;
		else
			return 0;
	}
	return (*Lights2_) (Position);
}

void CLightSector::SetLight1 (SIVector Position, byte Light)
{
	__assert (Light <= MAX_LIGHT_INTENSITY);

	if (!Lights1_ && Light != 0)
	{
		__assert (Light1_Num_ == 0);
		assert(!Lights1_);
		Lights1_ = CClientSubWorld::ClientSubWorldMemoryManager.Alloc();
		memset (Lights1_, 0, sizeof (CLightsArray));
	}
	
	byte PrevLight = GetLight1 (Position);
	if (Light != 0 && PrevLight == 0)
	{
		Light1_Num_++;
		__assert (Light1_Num_ <= SECTOR_VOLUME);
	}
	else if (Light == 0 && PrevLight != 0)
	{
		Light1_Num_--;
		__assert (Light1_Num_ >= 0);
		if (Light1_Num_ == 0)
		{
			CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights1_);
			Lights1_ = NULL;
		}
	}
	
	if (Lights1_)
		(*Lights1_) (Position) = Light;
}

void CLightSector::SetLight2 (SIVector Position, byte Light)
{
	__assert (Light <= MAX_LIGHT_INTENSITY);

	if (!Lights2_ && Light != 0 && Light2_Num_ == 0)
	{
		assert(!Lights2_);
		Lights2_ = CClientSubWorld::ClientSubWorldMemoryManager.Alloc();
		memset (Lights2_, 0, sizeof (CLightsArray));
	}
	else if (!Lights2_ && Light != MAX_LIGHT_INTENSITY && Light2_MaxLightingNum_ == SECTOR_VOLUME)
	{
		assert(!Lights2_);
		Lights2_ = CClientSubWorld::ClientSubWorldMemoryManager.Alloc();
		for (int i = 0; i < Lights2_->Size(); i++) // TODO: memset
			(*Lights2_)[i] = MAX_LIGHT_INTENSITY;
	}

	byte PrevLight = GetLight2 (Position);
	if (Light != 0 && PrevLight == 0)
	{
		Light2_Num_++;
		__assert (Light2_Num_ <= SECTOR_VOLUME);
	}
	else if (Light == 0 && PrevLight != 0)
	{
		Light2_Num_--;
		__assert (Light2_Num_ >= 0);
		if (Light2_Num_ == 0)
		{
			CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights2_);
			Lights2_ = NULL;
		}
	}
	
	if (Light == MAX_LIGHT_INTENSITY && PrevLight != MAX_LIGHT_INTENSITY)
	{
		Light2_MaxLightingNum_++;
		__assert (Light2_MaxLightingNum_ <= SECTOR_VOLUME);
		if (Light2_MaxLightingNum_ == SECTOR_VOLUME)
		{
			CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights2_);
			Lights2_ = NULL;
		}
	}
	else if (Light != MAX_LIGHT_INTENSITY && PrevLight == MAX_LIGHT_INTENSITY)
	{
		Light2_MaxLightingNum_--;
		__assert (Light2_MaxLightingNum_ >= 0);
	}
	
	if (Lights2_)
		(*Lights2_) (Position) = Light;
}

void CLightSector::SetFullLight2()
{
	if (Lights2_)
	{
		CClientSubWorld::ClientSubWorldMemoryManager.Free (Lights2_);
		Lights2_ = NULL;
	}

	Light2_Num_ = SECTOR_VOLUME;
	Light2_MaxLightingNum_ = SECTOR_VOLUME;
}

bool CLightSector::IsLight1Empty()
{
	return !Lights1_;
}

bool CLightSector::IsLight2Empty()
{
	return !Lights2_ && Light2_MaxLightingNum_ != SECTOR_VOLUME;
}

bool CLightSector::IsLight2Full()
{
	return !Lights2_ && Light2_MaxLightingNum_ == SECTOR_VOLUME;
}

CStream& operator << (CStream& Stream, CLightSector& LightSector)
{
	Stream << LightSector.Light1_Num_;
	Stream << LightSector.Light2_Num_;
	Stream << LightSector.Light2_MaxLightingNum_;

	if (LightSector.Lights1_)
		Stream << (*LightSector.Lights1_);
	if (LightSector.Lights2_)
		Stream << (*LightSector.Lights2_);

	return Stream;
}

CStream& operator >> (CStream& Stream, CLightSector& LightSector)
{
	assert (!LightSector.Lights1_);
	assert (!LightSector.Lights2_);

	Stream >> LightSector.Light1_Num_;
	Stream >> LightSector.Light2_Num_;
	Stream >> LightSector.Light2_MaxLightingNum_;

	if (LightSector.Light1_Num_ != 0)
	{
		assert(!LightSector.Lights1_);
		LightSector.Lights1_ = CClientSubWorld::ClientSubWorldMemoryManager.Alloc();
		Stream >> (*LightSector.Lights1_);
	}
	if (LightSector.Light2_Num_ != 0 && LightSector.Light2_MaxLightingNum_ != SECTOR_VOLUME)
	{
		assert(!LightSector.Lights2_);
		LightSector.Lights2_ = CClientSubWorld::ClientSubWorldMemoryManager.Alloc();
		Stream >> (*LightSector.Lights2_);
	}

	return Stream;
}
