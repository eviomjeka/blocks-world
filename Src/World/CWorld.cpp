﻿#include "CWorld.h"
#include "../CApplication.h"

CWorld::CWorld() :
Initialized_(false),
Time_(0),
Players_(SERVER_MAX_PLAYERS)
{
}

void CWorld::Init(SWorldParams& WorldParams)
{
	WorldGenerator_.Init (WorldParams);
	Initialized_ = true;
}
void CWorld::Destroy()
{
	if (!Initialized_)
		return;

	Initialized_ = false;
}

void CWorld::Proc (int Time)
{
	if (Time <= MAX_FREEZE_TIME) // if time more than MAX_FREEZE_TIME, we must skip physics, otherwise we fucked
	{
		/*int t = TimeMS();
		for (int i = 0; i < 100; i++)*/
		Physics_ (Time);
		//LOG ("TIME = %d", TimeMS() - t);
	}

	Time_ += Time;
	while (Time_ > DAY_DURATION)
		Time_ -= DAY_DURATION;
}

void CWorld::InteractWithBlock (SInteractWithBlockInfo& InteractWithBlockInfo)
{
	if (InteractWithBlockInfo.Block != CLIENT.MapLoader().GetBlock (InteractWithBlockInfo.BlockPosition))
		return;

	bool Interaction = BlocksList (InteractWithBlockInfo.Block.ID())->OnInteraction 
		(InteractWithBlockInfo.Block.Attributes(), &CLIENT.MapLoader(), InteractWithBlockInfo.BlockPosition, 
		 InteractWithBlockInfo.Face, InteractWithBlockInfo.Angle, InteractWithBlockInfo.Item);

	if (!Interaction && !CLIENT.MapLoader().GetBlock (InteractWithBlockInfo.SelectionPosition).ID())
		ItemsList (InteractWithBlockInfo.Item.ID())->OnAdd (InteractWithBlockInfo.Item.Attributes(), &CLIENT.MapLoader(), 
			InteractWithBlockInfo.SelectionPosition, InteractWithBlockInfo.Face, InteractWithBlockInfo.Angle);
}

void CWorld::RemoveBlock (SIVector Position)
{
	CBlockData Block = CLIENT.MapLoader().GetBlock (Position);
	BlocksList (Block.ID())->OnRemove (Block.Attributes(), &CLIENT.MapLoader(), Position);
}

void CWorld::Physics_ (int Time)
{
	int TimeStep = PHYSICS_TIME_STEP;
	int i = 1;

	while (i * TimeStep < Time)
	{
		PhysicsStep_ (TimeStep);
		i++;
	}
	TimeStep = Time - (i - 1) * TimeStep;
	if (TimeStep != 0)
		PhysicsStep_ (TimeStep);
}

void CWorld::PhysicsStep_ (int Time)
{
	for (int i = 0; i < Players_.Size(); i++)
		Players_[i].PhysicsStep (Time);
}

int CWorld::Time()
{
	return Time_;
}

void CWorld::SetTime (int time)
{
	Time_ = time;
}

CWorldGenerator& CWorld::WorldGenerator()
{
	return WorldGenerator_;
}