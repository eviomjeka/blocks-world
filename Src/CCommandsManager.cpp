﻿#include "CCommandsManager.h"
#include "CApplication.h"

CCommandsManager::SCommand::SCommand 
	(int CID, CString CName, CString CDescription, int CNumArguments, int CNumRequiredArguments, ...) : 
	ID (CID), Name (CName), Description (CDescription), 
	NumArguments (CNumArguments), NumRequiredArguments (CNumRequiredArguments)
{
	assert (CNumArguments >= CNumRequiredArguments);

	va_list ap;
	va_start (ap, CNumRequiredArguments);
	for (int i = 0; i < CNumArguments; i++)
		ArgumentTypes[i] = (byte) va_arg (ap, int);
	va_end (ap);
}

CCommandsManager::SArgument::SArgument() :
	Type(0),
	AInt(0),
	AFloat(0),
	AWord(),
	ABool(false)
{
	/* TODO: remove
		* to avoid gnu bug #57427
		* http://gcc.gnu.org/bugzilla/show_bug.cgi?id=57427
		*/
}

void CCommandsManager::AddCommands_()
{
	SCommand TempCommand;
#define ADD_COMMAND(cmd)			\
TempCommand = cmd;					\
AddCommand_(TempCommand);

	ADD_COMMAND (SCommand (COMMAND_HELP, CString ("help"), 
				 CString ("Show help info (help [command] - help info about command)"),
				 1, 0, ARGUMENT_TYPE_WORD));
	ADD_COMMAND (SCommand (COMMAND_SET_TIME, CString ("time"), 
				 CString ("Set current time (H M)"),
				 2, 2, ARGUMENT_TYPE_INT, ARGUMENT_TYPE_INT));
	ADD_COMMAND (SCommand (COMMAND_FLY_PLAYER, CString ("fly"), 
				 CString ("Enable/disable fly mode (fly [player] - for this player)"),
				 1, 0, ARGUMENT_TYPE_INT));
	ADD_COMMAND (SCommand (COMMAND_PLAYER_POSITION, CString ("position"), 
				 CString ("Show player position (position [player] - for this player)"),
				 1, 0, ARGUMENT_TYPE_INT));

#undef ADD_COMMAND
}

void CCommandsManager::CommandListener_ (int ID, SArgument Arguments[MAX_ARGUMENTS], int NumArguments)
{
	switch (ID)
	{
		case COMMAND_HELP:
			Help_ (Arguments[0].AWord, NumArguments != 0);
		break;

		case COMMAND_SET_TIME:
			SendTime_ (Arguments[0].AInt, Arguments[1].AInt);
		break;

		case COMMAND_FLY_PLAYER:
			FlyPlayer_ (Arguments[0].AInt, NumArguments != 0);
		break;

		case COMMAND_PLAYER_POSITION:
			PlayerPosition_ (Arguments[0].AInt, NumArguments != 0);
		break;

		default:
			error ("Unknown command");
		break;
	}
}

void CCommandsManager::SendChatMessage_ (CString& Message)
{
	CLIENT.ChatMessage (Message);
}

void CCommandsManager::Help_ (CString& Command, bool IsSpecifiedCommand)
{
	if (!IsSpecifiedCommand)
	{
		CString String ("Console commands: ");
		AddMessageToChat_ (String, SColor (0, 255, 0));
		for (int i = 0; i < Commands_.Size(); i++)
			HelpCommand_ (i);
	}
	else
	{
		int CommandNumber = -1;
		for (int i = 0; i < Commands_.Size(); i++)
			if (strcmp (Command.Data(), Commands_[i].Name.Data()) == 0)
			{
				CommandNumber = i;
				break;
			}
		if (CommandNumber == -1)
		{
			CString ErrorString;
			ErrorString.Print ("Error: Unknown command: \"%s\"", Command.Data());
			AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
			return;
		}
		HelpCommand_ (CommandNumber);
	}
}

void CCommandsManager::HelpCommand_ (int i)
{
	SCommand& Command = Commands_[i];
	CString String;
	String.Print ("%s - %s", Command.Name.Data(), Command.Description.Data());

	AddMessageToChat_ (String, SColor (0, 255, 0));
}

void CCommandsManager::SendTime_ (int Hours, int Minutes)
{
	if ((Hours < 0 || Hours >= 24) || (Minutes < 0 || Minutes >= 60))
	{
		CString String ("Error: Incorrect input values");
		AddMessageToChat_ (String, SColor (255, 0, 0));
	}

	int Time = PLAYERCONTROLLER.ToGameTime (Hours, Minutes);
	CLIENT.SetTime (Time);
}

void CCommandsManager::FlyPlayer_ (int PlayerID, bool IsNotMe)
{
	CPlayer* Player = IsNotMe ? PLAYERCONTROLLER.FindPlayer (PlayerID - 1) : PLAYERCONTROLLER.Controlled();
	if (!Player)
	{
		CString String;
		String.Print ("Error: Player #%d doesn't exist", PlayerID);
		AddMessageToChat_ (String, SColor (255, 0, 0));
		return;
	}
	CLIENT.FlyPlayer (Player->ID_, !Player->FlyMode_);
}

void CCommandsManager::PlayerPosition_ (int PlayerID, bool IsNotMe)
{
	CPlayer* Player = IsNotMe ? PLAYERCONTROLLER.FindPlayer (PlayerID - 1) : PLAYERCONTROLLER.Controlled();
	if (!Player)
	{
		CString String;
		String.Print ("Error: Player #%d doesn't exist", PlayerID);
		AddMessageToChat_ (String, SColor (255, 0, 0));
		return;
	}
	CString String;
	if (IsNotMe)
		String.Print ("Player #%d position: %f %f %f", PlayerID, Player->Position_.x, Player->Position_.y, Player->Position_.z);
	else
		String.Print ("Your position: %f %f %f", Player->Position_.x, Player->Position_.y, Player->Position_.z);
	AddMessageToChat_ (String, CPlayerController::INFO_MESSAGE_COLOR);
}

//==============================================================================

void CCommandsManager::Init()
{
	AddCommands_();
}

void CCommandsManager::AddCommand_ (SCommand& Command)
{
	Commands_.Insert (Command, true);
}

void CCommandsManager::AddMessageToChat_ (CString& Message, SColor Color)
{
	PLAYERCONTROLLER.AddMessageToChat (Message, Color);
}

void CCommandsManager::SendMessage (CString& Message)
{
	assert (Message.Length() != 0);
	if (Message[0] != '/')
	{
		SendChatMessage_ (Message);
		return;
	}
	char* CommandName = strtok (&Message.Data()[1], " \t");
	if (Message.Length() == 1 || !CommandName)
	{
		CString ErrorString ("Error: Empty command");
		AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
		return;
	}

	int CommandNumber = -1;
	for (int i = 0; i < Commands_.Size(); i++)
		if (strcmp (CommandName, Commands_[i].Name.Data()) == 0)
		{
			CommandNumber = i;
			break;
		}

	if (CommandNumber == -1)
	{
		CString ErrorString; ErrorString.Print ("Error: Unknown command: \"%s\"", CommandName);
		AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
		return;
	}

	SArgument Arguments[MAX_ARGUMENTS];
	for (int i = 0;; i++)
	{
		char* Argument = strtok (NULL, " \t");
		if (Argument == NULL)
		{
			if (i < Commands_[CommandNumber].NumRequiredArguments)
			{
				CString ErrorString; ErrorString.Print ("Error: command \"%s\" has more arguments", CommandName);
				AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
				return;
			}
			else
			{
				CommandListener_ (Commands_[CommandNumber].ID, Arguments, i);
				return;
			}
		}

		if (i >= Commands_[CommandNumber].NumArguments)
		{
			CString ErrorString; ErrorString.Print ("Error: command \"%s\" has less arguments", CommandName);
			AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
			return;
		}

		byte Type = Commands_[CommandNumber].ArgumentTypes[i];
		switch (Type)
		{
		case ARGUMENT_TYPE_INT:
		{
			int Number = 0;
			bool Minus = false;
			for (int j = 0; Argument[j]; j++)
			{
				if ((Argument[j] < '0' || Argument[j] > '9') &&
					!(j == 0 && Argument[j] == '-'))
				{
					CString ErrorString; ErrorString.Print ("Error: argument #%d \"%s\" in command \"%s\" is not int", 
						i + 1, Argument, CommandName);
					AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
					return;
				}
				if (j == 0 && Argument[j] == '-')
				{
					Minus = true;
					continue;
				}

				Number *= 10;
				Number += Argument[j] - '0';
			}
			Arguments[i].AInt = Number;
			if (Minus)
				Arguments[i].AInt *= -1;
		}
		break;

		case ARGUMENT_TYPE_FLOAT:
		{
			int A = 0;
			int B = 0;
			int BNum = 0;
			bool Minus = false;
			bool Point = false;
			for (int j = 0; Argument[j]; j++)
			{
				if (((Argument[j] < '0' || Argument[j] > '9') && Argument[j] != '.') &&
					!(j == 0 && Argument[j] == '-'))
				{
					CString ErrorString; ErrorString.Print ("Error: argument #%d \"%s\" in command \"%s\" is not float", 
						i + 1, Argument, CommandName);
					AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
					return;
				}
				if (j == 0 && Argument[j] == '-')
				{
					Minus = true;
					continue;
				}
				if (Argument[j] == '.')
				{
					if (Point)
					{
						CString ErrorString; ErrorString.Print ("Error: argument #%d \"%s\" in command \"%s\" is not float", 
							i + 1, Argument, CommandName);
						AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
						return;
					}
					Point = true;
					continue;
				}

				if (!Point)
				{
					A *= 10;
					A += Argument[j] - '0';
				}
				else
				{
					B *= 10;
					B += Argument[j] - '0';
					BNum++;
				}
			}
			int K = 1;
			for (int j = 0; j < BNum; j++)
				K *= 10;
			Arguments[i].AFloat = A + ((K == 0) ? 0.0f : (B / (float) K));

			if (Minus)
				Arguments[i].AFloat *= -1;
		}
		break;

		case ARGUMENT_TYPE_WORD:
		{
			strcpy (Arguments[i].AWord.Data(), Argument);
		}
		break;

		case ARGUMENT_TYPE_BOOL:
		{
			if (strcmp (Argument, "true") == 0)
				Arguments[i].ABool = true;
			else if (strcmp (Argument, "false") == 0)
				Arguments[i].ABool = false;
			else
			{
				CString ErrorString; ErrorString.Print ("Error: argument #%d \"%s\" in command \"%s\" is not bool", 
					i + 1, Argument, CommandName);
				AddMessageToChat_ (ErrorString, SColor (255, 0, 0));
				return;
			}
		}
		break;

		default:
			error ("Unknown argument type");
		break;
		}
	}
}
