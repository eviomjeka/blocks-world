﻿#include "CLightingEngine.h"
#include "CApplication.h"

void CLightingEngine::Init()
{
	for (int i = 0; i < Pools_.Size() - 1; i++)
		Pools_[i].Next = &Pools_[i + 1];
	Pools_[Pools_.Size() - 1].Next = NULL;

	EmptyPoolsStart_ = &Pools_[0];
	EmptyPoolsEnd_ = &Pools_[Pools_.Size() - 1];

	for (int i = 0; i < MAX_LIGHT_INTENSITY - 1; i++)
	{
		LightIntensityPoolsStart_[i] = NULL;
		LightIntensityPoolsEnd_[i] = NULL;
	}
}

void CLightingEngine::DumpCheck_ (bool CheckEmpty)
{
	__LOG ("==================================================\n");
	__LOG ("LIGHT ENGINE DUMP & CHECK (Check empty: %s)\n", CheckEmpty ? "true" : "false");

	if (!EmptyPoolsEnd_)
		__LOG ("ERROR: EmptyPoolEnd_ is NULL\n");

	int NumPools = 0;
	int NumEmptyPools = 0;
	SLightSourcePool* p = EmptyPoolsStart_;
	while (p)
	{
		NumEmptyPools++;
		if (!p->Next)
			break;
		int Shift = (byte*) p->Next - (byte*) &Pools_[0];
		if (Shift % sizeof (SLightSourcePool) != 0)
			__LOG ("ERROR: (Empty pools) p->Next references to not pools array\n");
		Shift /= sizeof (SLightSourcePool);
		if (Shift < 0 || Shift >= Pools_.Size())
			__LOG ("ERROR: (Empty pools) p->Next references to incorrect element (%d)\n", Shift);

		p = p->Next;
	}
	__LOG ("Num empty pools: %d", NumEmptyPools);
	if (NumEmptyPools > Pools_.Size())
		__LOG ("\nERROR: Num empty elements > pool size\n");
	else if (NumEmptyPools != Pools_.Size() && CheckEmpty)
		__LOG ("\nERROR: Num empty elements != pool size (but CheckEmpty == true)\n");
	else
		__LOG (" (OK)\n");

	if (CheckEmpty)
	{
		for (int i = 0; i < MAX_LIGHT_INTENSITY - 1; i++)
		{
			if (LightIntensityPoolsEnd_[i])
				__LOG ("ERROR: (Light intensity %d pool) not empty pool (but CheckEmpty == true)\n", i + 2);
		}
	}
	int NumLIPools[MAX_LIGHT_INTENSITY - 1] = {};
	int NumLILightSources[MAX_LIGHT_INTENSITY - 1] = {};
	for (int i = 0; i < MAX_LIGHT_INTENSITY - 1; i++)
	{
		p = LightIntensityPoolsStart_[i];
		while (p)
		{
			if (p->NumLightSoruces < 0)
				__LOG ("ERROR: (Light intensity %d pool) negative number of light sources (%d)\n", i + 2, p->NumLightSoruces);
			if (p->NumLightSoruces == 0)
				__LOG ("ERROR: (Light intensity %d pool) no light sources in pool\n", i + 2);
			if (p->NumLightSoruces > LIGHT_SOURCE_POOL_SIZE)
				__LOG ("ERROR: (Light intensity %d pool) light sources in pool (%d) more than LIGHT_SOURCE_POOL_SIZE\n", i + 2, p->NumLightSoruces);
			if (p->NumLightSoruces != LIGHT_SOURCE_POOL_SIZE && p->Next)
				__LOG ("ERROR: (Light intensity %d pool) light sources in pool (%d) not LIGHT_SOURCE_POOL_SIZE but p->Next is not NULL\n", i + 2, p->NumLightSoruces);

			NumLILightSources[i] += p->NumLightSoruces;
			NumLIPools[i]++;
			if (!p->Next)
				break;
			int Shift = (byte*) p->Next - (byte*) &Pools_[0];
			if (Shift % sizeof (SLightSourcePool) != 0)
				__LOG ("ERROR: (Light intensity %d pool) p->Next references to not pools array\n", i + 2);
			Shift /= sizeof (SLightSourcePool);
			if (Shift < 0 || Shift >= Pools_.Size())
				__LOG ("ERROR: (Light intensity %d pool) p->Next references to incorrect element (%d)\n", i + 2, Shift);

			p = p->Next;
		}
	}
	__LOG ("Num light intensity pools");
	for (int i = 0; i < LightIntensityPoolsEnd_.Size(); i++)
	{
		if (NumLIPools[i] != 0)
			__LOG (" | I%d-%d(%d)", i + 2, NumLIPools[i],NumLILightSources[i]);
		NumPools += NumLIPools[i];
	}
	if (NumPools == 0)
		__LOG (": empty");
	NumPools += NumEmptyPools;
	__LOG ("\nNum pools in lighting engine: %d", NumPools);
	if (NumPools < Pools_.Size())
		__LOG ("\nERROR: Not enough (%d but needed %d)\n", NumPools, Pools_.Size());
	else if (NumPools > Pools_.Size())
		__LOG ("\nERROR: More than needed (%d but needed %d)\n", NumPools, Pools_.Size());
	else
		__LOG (" (OK)\n");

	__LOG ("==================================================\n");
}

void CLightingEngine::AddLightSource_ (SVector<byte> Position, byte Intensity)
{
	if (!LightIntensityPoolsEnd_[Intensity - 2])
	{
		__assert (EmptyPoolsStart_);
		LightIntensityPoolsStart_[Intensity - 2] = EmptyPoolsStart_;
		LightIntensityPoolsEnd_[Intensity - 2] = EmptyPoolsStart_;
		LightIntensityPoolsEnd_[Intensity - 2]->NumLightSoruces = 0;

		if (EmptyPoolsStart_->Next)
			EmptyPoolsStart_ = EmptyPoolsStart_->Next;

		LightIntensityPoolsEnd_[Intensity - 2]->Next = NULL;
	}
	else if (LightIntensityPoolsEnd_[Intensity - 2]->NumLightSoruces == LIGHT_SOURCE_POOL_SIZE)
	{
		__assert (EmptyPoolsStart_);
		LightIntensityPoolsEnd_[Intensity - 2]->Next = EmptyPoolsStart_;
		LightIntensityPoolsEnd_[Intensity - 2] = EmptyPoolsStart_;
		LightIntensityPoolsEnd_[Intensity - 2]->NumLightSoruces = 0;
		
		if (EmptyPoolsStart_->Next)
			EmptyPoolsStart_ = EmptyPoolsStart_->Next;

		LightIntensityPoolsEnd_[Intensity - 2]->Next = NULL;
	}

	SLightSourcePool* Pool = LightIntensityPoolsEnd_[Intensity - 2];
	Pool->LightSources[Pool->NumLightSoruces] = Position;
	Pool->NumLightSoruces++;
}

SLightSourcePool* CLightingEngine::RemovePool_ (SLightSourcePool* Pool)
{
	__assert (EmptyPoolsEnd_);
	__assert (!EmptyPoolsEnd_->Next);
	SLightSourcePool* Next = Pool->Next;

	EmptyPoolsEnd_->Next = Pool;
	Pool->Next = NULL;
	EmptyPoolsEnd_ = Pool;

	return Next;
}

void CLightingEngine::RemoveLightIntensityPool_ (byte Intensity)
{
	LightIntensityPoolsStart_[Intensity - 2] = NULL;
	LightIntensityPoolsEnd_[Intensity - 2] = NULL;
}

byte CLightingEngine::GetLight_ (SIVector Position, CClientSubWorld* SubWorld, bool SunMoon)
{
	return SunMoon ? SubWorld->GetLight2 (Position) : 
					 SubWorld->GetLight1 (Position);
}

void CLightingEngine::SetLight_	(SIVector Position, byte Light, CClientSubWorld* SubWorld, bool SunMoon)
{
	if (SunMoon)
		SubWorld->SetLight2 (Position, Light);
	else
		SubWorld->SetLight1 (Position, Light);
}

//==============================================================================

void CLightingEngine::PrepareSubWorldLighting (CClientSubWorld* SubWorld)
{
	int Time1 = TimeMS();
	AddSunMoonLightSources_ (SubWorld);

	int Time2 = TimeMS();
	CaclulateLightSources_ (SubWorld, true);

	int Time3 = TimeMS();
	AddUsualLightSources_ (SubWorld);
	
	int Time4 = TimeMS();
	CaclulateLightSources_ (SubWorld, false);
	
	int Time5 = TimeMS();
	LOG ("Subworld (%d %d) lighting prepared: %dms (1:%d; 2:%d; 3:%d; 4: %d)", SubWorld->Position().x, 
		 SubWorld->Position().y, Time5 - Time1, Time2 - Time1, Time3 - Time2, Time4 - Time3, Time5 - Time4);
}

void CLightingEngine::AddSunMoonLightSources_ (CClientSubWorld* SubWorld)
{
	CSVectorArray2D<int, SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ> HighY;
	CSVectorArray2D<bool, SUBWORLD_SIZE_XZ, SUBWORLD_SIZE_XZ> NeedLightSource;
	int SectorY = SUBWORLD_SECTORS - 1;
	for (; SectorY >= 0; SectorY--)
	{
		if (!SubWorld->GetSector (SectorY))
			SubWorld->SetFullLight2 (SectorY);
		else
			break;
	}
	
	for (int x = 0; x < SUBWORLD_SIZE_XZ; x++)
		for (int z = 0; z < SUBWORLD_SIZE_XZ; z++)
		{
			HighY (x, z) = 0;
			for (int y = (SectorY + 1) * SECTOR_SIZE - 1; y >= 0; y--)
			{
				byte LightAttenuation = SubWorld->
					GetBlock (SIVector (x, y, z)).LightAttenuation (HYFACE_MASK);
				if (LightAttenuation != 0)
				{
					HighY (x, z) = y + 1;
					NeedLightSource (x, z) = (LightAttenuation != LIGHT_NOT_PASS);
					break;
				}

				SubWorld->SetLight2 (SIVector (x, y, z), MAX_LIGHT_INTENSITY);
			}
		}
	
#define LIGHT_TO_NEIGHBOUR_BLOCK(dx, dz, Mask1, Mask2)								\
	(Div (x + dx, SUBWORLD_SIZE_XZ) == 0 && Div (z + dz, SUBWORLD_SIZE_XZ) == 0 &&	\
	 y <= HighY (x + dx, z + dz) - 1 &&												\
	 SubWorld->GetBlock (ModXZ (SIVector (x + dx, y, z + dz), SUBWORLD_SIZE_XZ)).	\
	 LightAttenuation (Mask2) != LIGHT_NOT_PASS &&									\
	 SubWorld->GetBlock (SIVector (x, y, z)).LightAttenuation (Mask1) != LIGHT_NOT_PASS)
	
	for (int x = 0; x < SUBWORLD_SIZE_XZ; x++)
		for (int z = 0; z < SUBWORLD_SIZE_XZ; z++)
		{
			int StartY = HighY (x,z);
			if (x > 0 && HighY (x-1,z) > StartY)
				StartY = HighY (x-1,z);
			if (x < SUBWORLD_SIZE_XZ - 1 && HighY (x+1,z) > StartY)
				StartY = HighY (x+1,z);
			if (z > 0 && HighY (x,z-1) > StartY)
				StartY = HighY (x,z-1);
			if (z < SUBWORLD_SIZE_XZ - 1 && HighY (x,z+1) > StartY)
				StartY = HighY (x,z+1);

			for (int y = StartY; y >= HighY (x,z); y--)
			{
				if ((y == HighY (x,z) && NeedLightSource (x,z)) || 
					LIGHT_TO_NEIGHBOUR_BLOCK (-1, 0, LXFACE_MASK, HXFACE_MASK) || 
					LIGHT_TO_NEIGHBOUR_BLOCK (+1, 0, HXFACE_MASK, LXFACE_MASK) || 
					LIGHT_TO_NEIGHBOUR_BLOCK (0, -1, LZFACE_MASK, HZFACE_MASK) || 
					LIGHT_TO_NEIGHBOUR_BLOCK (0, +1, HZFACE_MASK, LZFACE_MASK))
				{
					AddLightSource_ (SIVector (x, y, z), MAX_LIGHT_INTENSITY);
				}
			}
		}

#undef LIGHT_TO_NEIGHBOUR_BLOCK
}

void CLightingEngine::AddUsualLightSources_ (CClientSubWorld* SubWorld)
{
	for (int i = 0; i < SubWorld->LightSources_.Size(); i++)
	{
		SLightSource LightSource = (SubWorld->LightSources_)[i];
		if (SubWorld->GetLight1 (LightSource.Position) < LightSource.Intensity)
		{
			SubWorld->SetLight1 (LightSource.Position, LightSource.Intensity);
			AddLightSource_ (LightSource.Position, LightSource.Intensity);
		}
	}
}

void CLightingEngine::CaclulateLightSources_ (CClientSubWorld* SubWorld, bool SunMoon)
{
	for (int i = MAX_LIGHT_INTENSITY; i >= 2; i--)
	{
		SLightSourcePool* Pool = LightIntensityPoolsStart_[i - 2];
		while (Pool)
		{
			for (int j = 0; j < Pool->NumLightSoruces; j++)
				CalculateLightSourceStep_ (SubWorld, SunMoon, Pool->LightSources[j], (byte) i);
			
			Pool = RemovePool_ (Pool); 
		}

		RemoveLightIntensityPool_ ((byte) i);
	}
}

void CLightingEngine::CalculateLightSourceStep_ (CClientSubWorld* SubWorld, bool SunMoon, 
												 SVector<byte> Position, byte Intensity)
{
	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);
		SIVector NeighbourPosition = FaceToVector (Position, Face);
		
		SIVector2D NeighbourSubWorldPosition = DivXZ2D (NeighbourPosition, SUBWORLD_SIZE_XZ);
		if ((abs (NeighbourSubWorldPosition.x) >= 1 || abs (NeighbourSubWorldPosition.y) >= 1) || 
			(NeighbourPosition.y < 0 || NeighbourPosition.y > SUBWORLD_SIZE_Y))
			continue;

		SIVector2D SubWorldPosition = DivXZ2D (Position, SUBWORLD_SIZE_XZ);
		SIVector BlockPosition = ModXZ (Position, SUBWORLD_SIZE_XZ);
		CBlockData Block = SubWorld->GetBlock (BlockPosition);
		if (Block.LightAttenuation (Face) == LIGHT_NOT_PASS && Block.LightIntensity() == 0)
			continue;

		SIVector NeighbourBlockPosition = ModXZ (NeighbourPosition, SUBWORLD_SIZE_XZ);

		CBlockData NeighbourBlock = SubWorld->GetBlock (NeighbourBlockPosition);
		byte LightAttenuation = NeighbourBlock.LightAttenuation (InvertFace (Face));
		if (LightAttenuation == 1)
			LightAttenuation = 0;
		if (LightAttenuation == LIGHT_NOT_PASS)
			continue;

		byte BlockLight = GetLight_ (NeighbourBlockPosition, SubWorld, SunMoon);

		if (Intensity - 1 < LightAttenuation)
			continue;
		byte NewIntensity = Intensity - 1 - LightAttenuation;
		if (NewIntensity != 0 && BlockLight < NewIntensity)
		{
			SetLight_ (NeighbourBlockPosition, NewIntensity, SubWorld, SunMoon);

			if (NewIntensity > 1)
				AddLightSource_ ((SVector<byte>) NeighbourPosition, NewIntensity);
		}
	}
}

//==============================================================================

void CLightingEngine::CalculateSubWorldLighting (CClientSubWorlds3x3& SubWorlds, int NeighboursMask)
{
	int Time1 = TimeMS();
	AddLightSources_ (SubWorlds, NeighboursMask, true);

	int Time2 = TimeMS();
	CaclulateLightSources_ (SubWorlds, NeighboursMask, true);

	int Time3 = TimeMS();
	AddLightSources_ (SubWorlds, NeighboursMask, false);
	
	int Time4 = TimeMS();
	CaclulateLightSources_ (SubWorlds, NeighboursMask, false);
	
	int Time5 = TimeMS();
	LOG ("Subworld (%d %d) lighting calculated: %dms (1:%d; 2:%d; 3:%d; 4: %d)", SubWorlds (0, 0)->Position().x, 
		 SubWorlds (0, 0)->Position().y, Time5 - Time1, Time2 - Time1, Time3 - Time2, Time4 - Time3, Time5 - Time4);
}

void CLightingEngine::AddLightSources_ (CClientSubWorlds3x3& SubWorlds, int NeighboursMask, bool SunMoon)
{ // TODO: function
#define SUBWORLD_EDGE(Vector1, Vector2, SubWorldPosition, Mask1, Mask2)									\
	for (int i = 0; i < SUBWORLD_SIZE_XZ; i++)															\
	{																									\
		if (SubWorlds (0, 0)->GetBlock (Vector1).LightAttenuation (Mask1) != LIGHT_NOT_PASS)			\
		{																								\
			byte LightAttenuation = SubWorlds (SubWorldPosition)->										\
				GetBlock (Vector2).LightAttenuation (Mask2);											\
			if (LightAttenuation != LIGHT_NOT_PASS)														\
			{																							\
				if (LightAttenuation == 0)																\
					LightAttenuation = 1;																\
				byte Intensity = GetLight_ (Vector1, SubWorlds (0, 0), SunMoon);						\
				if ((int) Intensity - (int) LightAttenuation >= 1)										\
				{																						\
					byte NIntensity = GetLight_ (Vector2, SubWorlds (SubWorldPosition), SunMoon);		\
																										\
					if ((int) Intensity - (int) LightAttenuation > NIntensity)							\
					{																					\
						byte NewIntensity = Intensity - LightAttenuation;								\
						SetLight_ (Vector2, NewIntensity, SubWorlds (SubWorldPosition), SunMoon);		\
																										\
						if (NewIntensity != 1)															\
							AddLightSource_ (SVector<byte> (1 + (byte) SubWorldPosition.x, 0,			\
												1 + (byte) SubWorldPosition.y) * SUBWORLD_SIZE_XZ +		\
												(SVector<byte>) Vector2, NewIntensity);					\
					}																					\
				}																						\
			}																							\
		}																								\
	}
	
	for (int SectorY = 0; SectorY < SUBWORLD_SECTORS; SectorY++)
	{
		if ((!SunMoon && SubWorlds (0, 0)->LightSectors_[SectorY].IsLight1Empty()) || 
			( SunMoon && SubWorlds (0, 0)->LightSectors_[SectorY].IsLight2Empty()))
			continue;

		bool LX = (NeighboursMask & LX_SY_NEIGHBOUR_MASK) != 0 && 
			!(SunMoon && SubWorlds (-1, 0)->LightSectors_[SectorY].IsLight2Full());

		bool HX = (NeighboursMask & HX_SY_NEIGHBOUR_MASK) != 0 && 
			!(SunMoon && SubWorlds (1, 0)->LightSectors_[SectorY].IsLight2Full());

		bool LZ = (NeighboursMask & SX_LY_NEIGHBOUR_MASK) != 0 && 
			!(SunMoon && SubWorlds (0, -1)->LightSectors_[SectorY].IsLight2Full());

		bool HZ = (NeighboursMask & SX_HY_NEIGHBOUR_MASK) != 0 && 
			!(SunMoon && SubWorlds (0, 1)->LightSectors_[SectorY].IsLight2Full());

		for (int y = SectorY * SECTOR_SIZE; y < (SectorY + 1) * SECTOR_SIZE; y++)
		{
			if (LX)
				SUBWORLD_EDGE (SIVector (0, y, i), SIVector (SUBWORLD_SIZE_XZ - 1, y, i), 
							   SIVector2D (-1, 0), LXFACE_MASK, HXFACE_MASK);
		
			if (HX)
				SUBWORLD_EDGE (SIVector (SUBWORLD_SIZE_XZ - 1, y, i), SIVector (0, y, i), 
							   SIVector2D (1, 0), HXFACE_MASK, LXFACE_MASK);
		
			if (LZ)
				SUBWORLD_EDGE (SIVector (i, y, 0), SIVector (i, y, SUBWORLD_SIZE_XZ - 1), 
							   SIVector2D (0, -1), LZFACE_MASK, HZFACE_MASK);
		
			if (HZ)
				SUBWORLD_EDGE (SIVector (i, y, SUBWORLD_SIZE_XZ - 1), SIVector (i, y, 0), 
							   SIVector2D (0, 1), HZFACE_MASK, LZFACE_MASK);
		}
	}

#undef SUBWORLD_EDGE
}

//==============================================================================

void CLightingEngine::RecalculateSubWorldLighting (CClientSubWorlds3x3& SubWorlds, CArray<SChangedBlock> ChangedBlocks)
{
	int Time1 = TimeMS();
	AddLightSourcesAndClear_ (SubWorlds, ChangedBlocks, true);

	int Time2 = TimeMS();
	CaclulateLightSources_ (SubWorlds, NEIGHBOURS_ALL_MASK, true);

	int Time3 = TimeMS();
	AddLightSourcesAndClear_ (SubWorlds, ChangedBlocks, false);
	
	int Time4 = TimeMS();
	CaclulateLightSources_ (SubWorlds, NEIGHBOURS_ALL_MASK, false);
	
	int Time5 = TimeMS();
	LOG ("Subworld (%d %d) recalculated: %dms (1:%d; 2:%d; 3:%d; 4: %d)", SubWorlds (0, 0)->Position().x, 
		 SubWorlds (0, 0)->Position().y, Time5 - Time1, Time2 - Time1, Time3 - Time2, Time4 - Time3, Time5 - Time4);
}

void CLightingEngine::AddLightSourcesAndClear_ (CClientSubWorlds3x3&/* SubWorlds*/, CArray<SChangedBlock>/* ChangedBlocks*/, bool/* SunMoon*/)
{
	/*for (int i = 0; i < ChangedBlocks.Size(); i++)
	{
		CBlockData Block = SubWorlds (0, 0)->GetBlock (ChangedBlocks[i].Position);

		byte L1 = GetLight_ (ChangedBlocks[i].Position, SubWorlds (0, 0), SunMoon);
		byte L2 = 0;
		if (!SunMoon)
			L2 = Block.LightIntensity();
		else
		{
			int HighY = 0;

			int SectorY = SUBWORLD_SECTORS - 1;
			for (; SectorY >= 0; SectorY--)
			{
				if (SubWorlds (0, 0)->GetSector (SectorY))
					break;
			}
			for (int y = (SectorY + 1) * SECTOR_SIZE - 1; y >= SectorY * SECTOR_SIZE; y--)
			{
				byte LightAttenuation = SubWorlds (0, 0)->
					GetBlock (SIVector (ChangedBlocks[i].Position.x, y, 
						ChangedBlocks[i].Position.z)).LightAttenuation (HYFACE_MASK);
				if (LightAttenuation != 0)
				{
					HighY = y + 1;
					break;
				}
			}
		}
		// TODO: remove ls from y to HighY if needed
		SetLight_ (ChangedBlocks[i].Position, 0, SubWorlds (0, 0), SunMoon);



		// TODO: add here add light sources after add/remove block
	}*/
}

//==============================================================================

void CLightingEngine::CaclulateLightSources_ (CClientSubWorlds3x3& SubWorlds, int NeighboursMask, bool SunMoon)
{
	for (int i = MAX_LIGHT_INTENSITY; i >= 2; i--)
	{
		SLightSourcePool* Pool = LightIntensityPoolsStart_[i - 2];
		while (Pool)
		{
			for (int j = 0; j < Pool->NumLightSoruces; j++)
				CalculateLightSourceStep_ (SubWorlds, SunMoon, Pool->LightSources[j], (byte) i, NeighboursMask);
			
			Pool = RemovePool_ (Pool); 
		}

		RemoveLightIntensityPool_ ((byte) i);
	}
}

void CLightingEngine::CalculateLightSourceStep_ (CClientSubWorlds3x3& SubWorlds, bool SunMoon, 
												 SVector<byte> Position, byte Intensity, int NeighboursMask)
{
	for (int i = 0; i < NUM_FACES; i++)
	{
		byte Face = IndexToFace ((byte) i);
		SIVector NeighbourPosition = FaceToVector (Position, Face);
		
		SIVector2D NeighbourSubWorldPosition = DivXZ2D (NeighbourPosition, SUBWORLD_SIZE_XZ) - SIVector2D (1, 1);
		if ((abs (NeighbourSubWorldPosition.x) >= 2 || abs (NeighbourSubWorldPosition.y) >= 2) || 
			(NeighbourPosition.y < 0 || NeighbourPosition.y > SUBWORLD_SIZE_Y) || 
			(NeighboursMask & CClientSubWorld::GetNeighbourMask (NeighbourSubWorldPosition)) == 0)
			continue;

		SIVector2D SubWorldPosition = DivXZ2D (Position, SUBWORLD_SIZE_XZ) - SIVector2D (1, 1);
		SIVector BlockPosition = ModXZ (Position, SUBWORLD_SIZE_XZ);
		CBlockData Block = SubWorlds (SubWorldPosition)->GetBlock (BlockPosition);
		if (Block.LightAttenuation (Face) == LIGHT_NOT_PASS && Block.LightIntensity() == 0)
			continue;

		SIVector NeighbourBlockPosition = ModXZ (NeighbourPosition, SUBWORLD_SIZE_XZ);

		CBlockData NeighbourBlock = SubWorlds (NeighbourSubWorldPosition)->GetBlock (NeighbourBlockPosition);
		byte LightAttenuation = NeighbourBlock.LightAttenuation (InvertFace (Face));
		if (LightAttenuation == 0)
			LightAttenuation = 1;
		if (LightAttenuation == LIGHT_NOT_PASS)
			continue;

		byte BlockLight = GetLight_ (NeighbourBlockPosition, SubWorlds (NeighbourSubWorldPosition), SunMoon);
		if (Intensity - 1 < LightAttenuation)
			continue;
		byte NewIntensity = Intensity - LightAttenuation;
		if (NewIntensity != 0 && BlockLight < NewIntensity)
		{
			SetLight_ (NeighbourBlockPosition, NewIntensity, SubWorlds (NeighbourSubWorldPosition), SunMoon);

			if (NewIntensity > 1)
				AddLightSource_ ((SVector<byte>) NeighbourPosition, NewIntensity);
		}
	}
}