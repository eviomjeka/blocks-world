﻿#include "CKeysUtilities.h"

void CKeyToCharacter::Init (CCharacterListener* Listener, bool AdditionalSymbols)
{
	Listener_ = Listener;
	RepeatKey_ = false;
	Shift_ = false;
	AdditionalSymbols_ = AdditionalSymbols;
	KeyCode_ = 0;
	KeyTime_ = 0;
}

void CKeyToCharacter::KeyDown (unsigned Key)
{
	if (Key == KEY_SHIFT)
	{
		Shift_ = true;
		return;
	}
	if (!IsKeyNeeded_ (Key))
		return;

	KeyCode_ = Key;
	KeyTime_ = -500;
	RepeatKey_ = true;

	KeyEvent_ (Key);
}

void CKeyToCharacter::KeyUp (unsigned Key)
{
	if (Key == KEY_SHIFT)
	{
		Shift_ = false;
		return;
	}

	if (KeyCode_ == Key)
		RepeatKey_ = false;
}

void CKeyToCharacter::Update (int Time)
{
	if (!RepeatKey_)
		return;

	KeyTime_ += Time;
	while (KeyTime_ >= 35)
	{
		KeyEvent_ (KeyCode_);
		KeyTime_ -= 35;
	}
}

void CKeyToCharacter::StopKeyRepetition()
{
	RepeatKey_ = false;
}

#define OR || Key == 

bool CKeyToCharacter::IsKeyNeeded_ (unsigned Key)
{
	return Key == KEY_UP OR KEY_DOWN OR KEY_LEFT OR KEY_RIGHT OR KEY_PAGE_DOWN OR
		KEY_PAGE_UP OR KEY_DELETE OR KEY_BACKSPACE OR KEY_ENTER OR KEY_TILDE OR 
		KEY_SPACE OR KEY_MINUS OR KEY_PLUS OR KEY_LEFT_BRACKET OR KEY_BACKSLASH OR
		KEY_RIGHT_BRACKET OR KEY_COLON OR KEY_QUOTE OR KEY_COMMA OR KEY_PERIOD OR KEY_SLASH OR 
		KEY_1 OR KEY_2 OR KEY_3 OR KEY_4 OR KEY_5 OR KEY_6 OR KEY_7 OR KEY_8 OR KEY_9 OR
		KEY_0 OR KEY_A OR KEY_B OR KEY_C OR KEY_D OR KEY_E OR KEY_F OR KEY_G OR KEY_H OR 
		KEY_I OR KEY_J OR KEY_K OR KEY_L OR KEY_M OR KEY_N OR KEY_O OR KEY_P OR KEY_Q OR 
		KEY_R OR KEY_S OR KEY_T OR KEY_U OR KEY_V OR KEY_W OR KEY_X OR KEY_Y OR KEY_Z;
}

#define KEY(CaseKey,CharNotShift,CharShift)														\
	case CaseKey: Listener_->Character (Shift_ ? CharShift : CharNotShift); return;
#define KEY_ADD(CaseKey,NotShiftAddictional,CharNotShift,ShiftAddictional,CharShift)			\
	case CaseKey:																				\
		if (!Shift_ && (AdditionalSymbols_ || (!AdditionalSymbols_ && NotShiftAddictional)))	\
			Listener_->Character (CharNotShift);												\
		else if (Shift_ && (AdditionalSymbols_ || (!AdditionalSymbols_ && ShiftAddictional)))	\
			Listener_->Character (CharShift);													\
	return;
#define KEY_NOT_SHIFT(CaseKey,Char)																\
	case CaseKey: Listener_->Character (Char); return;

void CKeyToCharacter::KeyEvent_ (unsigned Key)
{
	if (Key == KEY_UP OR KEY_DOWN OR KEY_LEFT OR KEY_RIGHT OR KEY_PAGE_DOWN OR
		KEY_PAGE_UP OR KEY_DELETE OR KEY_BACKSPACE OR KEY_ENTER)
	{
		Listener_->SpecialKey (Key);
		return;
	}
	
	switch (Key)
	{
		KEY_NOT_SHIFT (KEY_SPACE, ' ');
		
		KEY (KEY_TILDE			, '`', '~');
		KEY (KEY_MINUS			, '-', '_');
		KEY (KEY_PLUS			, '=', '+');
		KEY (KEY_LEFT_BRACKET	, '[', '{');
		KEY (KEY_RIGHT_BRACKET	, ']', '}');
		KEY_ADD (KEY_BACKSLASH	, false, '\\', false, '|');
		KEY_ADD (KEY_COLON		, true , ';' , false, ':');
		KEY_ADD (KEY_QUOTE		, true , '\'', false, '"');
		KEY_ADD (KEY_COMMA		, true , ',' , false, '<');
		KEY_ADD (KEY_PERIOD		, true , '.' , false, '>');
		KEY_ADD (KEY_SLASH		, false, '/' , false, '?');

		KEY (KEY_1, '1', '!'); KEY (KEY_2, '2', '@'); KEY (KEY_3, '3', '#');
		KEY (KEY_4, '4', '$'); KEY (KEY_5, '5', '%'); KEY (KEY_6, '6', '^');
		KEY (KEY_7, '7', '&'); KEY_ADD (KEY_8, true, '8', false, '*'); KEY (KEY_9, '9', '('); 
		KEY (KEY_0, '0', ')'); KEY (KEY_A, 'a', 'A'); KEY (KEY_B, 'b', 'B'); 
		KEY (KEY_C, 'c', 'C'); KEY (KEY_D, 'd', 'D'); KEY (KEY_E, 'e', 'E');
		KEY (KEY_F, 'f', 'F'); KEY (KEY_G, 'g', 'G'); KEY (KEY_H, 'h', 'H');
		KEY (KEY_I, 'i', 'I'); KEY (KEY_J, 'j', 'J'); KEY (KEY_K, 'k', 'K');
		KEY (KEY_L, 'l', 'L'); KEY (KEY_M, 'm', 'M'); KEY (KEY_N, 'n', 'N');
		KEY (KEY_O, 'o', 'O'); KEY (KEY_P, 'p', 'P'); KEY (KEY_Q, 'q', 'Q');
		KEY (KEY_R, 'r', 'R'); KEY (KEY_S, 's', 'S'); KEY (KEY_T, 't', 'T');
		KEY (KEY_U, 'u', 'U'); KEY (KEY_V, 'v', 'V'); KEY (KEY_W, 'w', 'W');
		KEY (KEY_X, 'x', 'X'); KEY (KEY_Y, 'y', 'Y'); KEY (KEY_Z, 'z', 'Z');
	};
}

#undef KEY
#undef KEY_ADD
#undef KEY_NOT_SHIFT
#undef OR

#define KEY_TO_STR(CaseKey,CaseStr) case CaseKey: Str = CaseStr; return true; break;

bool KeyToStr (unsigned Key, CString& Str)
{
	switch (Key)
	{
		KEY_TO_STR (MOUSE_LEFT_BUTTON	, str_mouse_left_button);
		KEY_TO_STR (MOUSE_RIGHT_BUTTON	, str_mouse_right_button);
		KEY_TO_STR (MOUSE_MIDDLE_BUTTON	, str_mouse_middle_button);
		KEY_TO_STR (MOUSE_WHELL_UP		, str_mouse_wheel_up);
		KEY_TO_STR (MOUSE_WHELL_DOWN	, str_mouse_wheel_down);

		KEY_TO_STR (KEY_UP				, str_key_up);
		KEY_TO_STR (KEY_DOWN			, str_key_down);
		KEY_TO_STR (KEY_LEFT			, str_key_left);
		KEY_TO_STR (KEY_RIGHT			, str_key_right);

		KEY_TO_STR (KEY_HOME			, str_key_home);
		KEY_TO_STR (KEY_END				, str_key_end);
		KEY_TO_STR (KEY_PAGE_DOWN		, str_key_pagedown);
		KEY_TO_STR (KEY_PAGE_UP			, str_key_pageup);
		KEY_TO_STR (KEY_DELETE			, str_key_delete);
		KEY_TO_STR (KEY_INSERT			, str_key_insert);
		KEY_TO_STR (KEY_PAUSE_BREAK		, str_key_pause_break);
		KEY_TO_STR (KEY_PRINTSCREEN		, str_key_print_screen);

		KEY_TO_STR (KEY_BACKSPACE		, str_key_backspace);
		KEY_TO_STR (KEY_ENTER			, str_key_enter);
		KEY_TO_STR (KEY_ESCAPE			, str_key_escape);
		KEY_TO_STR (KEY_TILDE			, str_key_tilde);
		KEY_TO_STR (KEY_TAB				, str_key_tab);
		KEY_TO_STR (KEY_CAPS_LOOK		, str_key_caps);
		KEY_TO_STR (KEY_SHIFT			, str_key_shift);
		KEY_TO_STR (KEY_CONTROL			, str_key_ctrl);
		KEY_TO_STR (KEY_ALT				, str_key_alt);
		KEY_TO_STR (KEY_SPACE			, str_key_space);

		KEY_TO_STR (KEY_F1				, str_key_f1);
		KEY_TO_STR (KEY_F2				, str_key_f2);
		KEY_TO_STR (KEY_F3				, str_key_f3);
		KEY_TO_STR (KEY_F4				, str_key_f4);
		KEY_TO_STR (KEY_F5				, str_key_f5);
		KEY_TO_STR (KEY_F6				, str_key_f6);
		KEY_TO_STR (KEY_F7				, str_key_f7);
		KEY_TO_STR (KEY_F8				, str_key_f8);
		KEY_TO_STR (KEY_F9				, str_key_f9);
		KEY_TO_STR (KEY_F10				, str_key_f10);
		KEY_TO_STR (KEY_F11				, str_key_f11);
		KEY_TO_STR (KEY_F12				, str_key_f12);

		KEY_TO_STR (KEY_A				, str_key_a);
		KEY_TO_STR (KEY_B				, str_key_b);
		KEY_TO_STR (KEY_C				, str_key_c);
		KEY_TO_STR (KEY_D				, str_key_d);
		KEY_TO_STR (KEY_E				, str_key_e);
		KEY_TO_STR (KEY_F				, str_key_f);
		KEY_TO_STR (KEY_G				, str_key_g);
		KEY_TO_STR (KEY_H				, str_key_h);
		KEY_TO_STR (KEY_I				, str_key_i);
		KEY_TO_STR (KEY_J				, str_key_j);
		KEY_TO_STR (KEY_K				, str_key_k);
		KEY_TO_STR (KEY_L				, str_key_l);
		KEY_TO_STR (KEY_M				, str_key_m);
		KEY_TO_STR (KEY_N				, str_key_n);
		KEY_TO_STR (KEY_O				, str_key_o);
		KEY_TO_STR (KEY_P				, str_key_p);
		KEY_TO_STR (KEY_Q				, str_key_q);
		KEY_TO_STR (KEY_R				, str_key_r);
		KEY_TO_STR (KEY_S				, str_key_s);
		KEY_TO_STR (KEY_T				, str_key_t);
		KEY_TO_STR (KEY_U				, str_key_u);
		KEY_TO_STR (KEY_V				, str_key_v);
		KEY_TO_STR (KEY_W				, str_key_w);
		KEY_TO_STR (KEY_X				, str_key_x);
		KEY_TO_STR (KEY_Y				, str_key_y);
		KEY_TO_STR (KEY_Z				, str_key_z);

		KEY_TO_STR (KEY_0				, str_key_0);
		KEY_TO_STR (KEY_1				, str_key_1);
		KEY_TO_STR (KEY_2				, str_key_2);
		KEY_TO_STR (KEY_3				, str_key_3);
		KEY_TO_STR (KEY_4				, str_key_4);
		KEY_TO_STR (KEY_5				, str_key_5);
		KEY_TO_STR (KEY_6				, str_key_6);
		KEY_TO_STR (KEY_7				, str_key_7);
		KEY_TO_STR (KEY_8				, str_key_8);
		KEY_TO_STR (KEY_9				, str_key_9);
		KEY_TO_STR (KEY_MINUS			, str_key_minus);
		KEY_TO_STR (KEY_PLUS			, str_key_plus);

		KEY_TO_STR (KEY_LEFT_BRACKET	, str_key_left_bracket);
		KEY_TO_STR (KEY_RIGHT_BRACKET	, str_key_right_bracket);
		KEY_TO_STR (KEY_COLON			, str_key_colon);
		KEY_TO_STR (KEY_QUOTE			, str_key_quote);
		KEY_TO_STR (KEY_BACKSLASH		, str_key_backslash);
		KEY_TO_STR (KEY_COMMA			, str_key_comma);
		KEY_TO_STR (KEY_PERIOD			, str_key_period);
		KEY_TO_STR (KEY_SLASH			, str_key_slash);

		KEY_TO_STR (KEY_NUM0			, str_key_num0);
		KEY_TO_STR (KEY_NUM1			, str_key_num1);
		KEY_TO_STR (KEY_NUM2			, str_key_num2);
		KEY_TO_STR (KEY_NUM3			, str_key_num3);
		KEY_TO_STR (KEY_NUM4			, str_key_num4);
		KEY_TO_STR (KEY_NUM5			, str_key_num5);
		KEY_TO_STR (KEY_NUM6			, str_key_num6);
		KEY_TO_STR (KEY_NUM7			, str_key_num7);
		KEY_TO_STR (KEY_NUM8			, str_key_num8);
		KEY_TO_STR (KEY_NUM9			, str_key_num9);
		KEY_TO_STR (KEY_NUMADD			, str_key_num_add);
		KEY_TO_STR (KEY_NUMSUBTRACT		, str_key_num_subtract);
		KEY_TO_STR (KEY_NUMMULTIPLY		, str_key_num_multiply);
		KEY_TO_STR (KEY_NUMDIVIDE		, str_key_num_divide);
		KEY_TO_STR (KEY_NUMDECIMAL		, str_key_num_decimal);
	}

	return false;
}

#undef KEY_TO_STR
