﻿#ifndef CCOMMANDSMANAGER
#define CCOMMANDSMANAGER

#include <Engine.h>

#define MAX_ARGUMENTS 10

enum ArgumentType
{
	ARGUMENT_TYPE_INT, 
	ARGUMENT_TYPE_FLOAT, 
	ARGUMENT_TYPE_WORD, 
	ARGUMENT_TYPE_BOOL
};

enum CommandsID
{
	COMMAND_HELP, 
	COMMAND_SET_TIME, 
	COMMAND_FLY_PLAYER, 
	COMMAND_PLAYER_POSITION
};

class CCommandsManager
{
public:

	struct SCommand
	{
		SCommand() {}
		SCommand (int CID, CString CName, CString CDescription, int CNumArguments, int CNumRequiredArguments, ...);

		int ID;
		CString Name;
		CString Description;
		int NumArguments;
		int NumRequiredArguments;
		byte ArgumentTypes[MAX_ARGUMENTS];
	};
	struct SArgument
	{
		SArgument();

		byte Type;
		int AInt;
		float AFloat;
		CString AWord;
		bool ABool;
	};

private:
	
	CArray<SCommand> Commands_;

	void AddMessageToChat_ (CString& Message, SColor Color);
	void AddCommand_ (SCommand& Command);
	
	void AddCommands_();
	void CommandListener_ (int ID, SArgument Arguments[MAX_ARGUMENTS], int NumArguments);

	void SendChatMessage_ (CString& Message);
	void Help_ (CString& Command, bool IsSpecifiedCommand);
	void HelpCommand_ (int i);
	void SendTime_ (int Hours, int Minutes);
	void FlyPlayer_ (int PlayerID, bool IsNotMe);
	void PlayerPosition_ (int PlayerID, bool IsNotMe);

public:
	
	void Init();
	void SendMessage (CString& Message);

};

#endif
